package com.niafikra.dimension.core.api.controller.mail;


import com.niafikra.dimension.mail.MailService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Map;

@RestController
@RequestMapping(AMailController.PATH)
public class AMailController {
    public static final String PATH = "/api/v1/mail";

    @Inject
    private MailService mailService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Map<String,String> send(@RequestBody Map<String,String> data) {
        mailService.sendEmailAsync(data.get("subject"),data.get("body"),false,false,data.get("to"));
        return data;
    }
}
