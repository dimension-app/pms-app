package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.ui.*;
import org.vaadin.viritin.layouts.MPanel;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 2:51 PM
 */
public abstract class SplitCreateViewPanel<T> extends GridCreateViewPanel<T> {

    private HorizontalSplitPanel baseSplitPanel;

    public SplitCreateViewPanel(Grid<T> entriesGrid) {
        super(entriesGrid);
        this.baseSplitPanel = new HorizontalSplitPanel();

        baseSplitPanel.setSizeFull();
        baseSplitPanel.setSplitPosition(60);
        addComponent(baseSplitPanel);
        setExpandRatio(baseSplitPanel, 1);
    }

    @Override
    public void attach() {
        super.attach();
        //initialise by showing the new form
        createButton.click();
    }

    protected void attachEntriesGrid(Grid<T> entriesGrid) {
        baseSplitPanel.setFirstComponent(entriesGrid);
    }

    protected void attachCreateButton(Button createButton) {
        addComponentAsFirst(createButton);
        setComponentAlignment(createButton, Alignment.BOTTOM_LEFT);
    }

    protected void showCreateContent() {
        Component createView = getCreateComponent();
        baseSplitPanel.setSecondComponent(new MPanel(createView).withStyleName(Theme.PANEL_BORDERLESS).withFullSize());
    }

    protected void showViewContent(T entry) {
        Component entryView = getViewComponent(entry);
        baseSplitPanel.setSecondComponent(new MPanel(entryView).withCaption(entry.toString()).withStyleName(Theme.PANEL_BORDERLESS).withFullSize());
    }

    public HorizontalSplitPanel getBaseSplitPanel() {
        return baseSplitPanel;
    }

    protected abstract Component getCreateComponent();

    protected abstract Component getViewComponent(T entry);


}
