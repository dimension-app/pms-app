package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.ui.vaadin.AbstractUI;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.Resource;
import org.vaadin.viritin.button.MButton;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/26/16.
 */
public class OpenerButton extends MButton {

    private BrowserWindowOpener opener;

    public OpenerButton(String caption, Resource icon) {
        opener = new BrowserWindowOpener(AbstractUI.class);
        opener.extend(this);
        setIcon(icon);
        setCaption(caption);
    }

    public OpenerButton(String caption, Resource icon, String url) {
        this(caption, icon);
        getOpener().setUrl(url);
    }

    public BrowserWindowOpener getOpener() {
        return opener;
    }
}
