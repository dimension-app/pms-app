package com.niafikra.dimension.core.ui.vaadin;

import com.niafikra.dimension.core.security.SecurityUtils;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.SideBarItemDescriptor;
import org.vaadin.spring.sidebar.components.AbstractSideBar;

public class SideBarMenuItemFilter implements AbstractSideBar.ItemFilter {

    @Override
    public boolean passesFilter(SideBarItemDescriptor descriptor) {
        Secured secured = (Secured)descriptor.findAnnotationOnBean(Secured.class);
        return secured != null ? SecurityUtils.hasAnyAuthority(secured.value()) : true;
    }
}
