package com.niafikra.dimension.core.ui.vaadin.util.token;

import com.fo0.advancedtokenfield.main.AdvancedTokenField;
import com.fo0.advancedtokenfield.main.Token;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MTokenField<T> extends AdvancedTokenField {

    public MTokenField<T> addMTokensToInputField(List<MToken<T>> tokens) {
        tokens.stream().forEach(token -> addTokenToInputField(token));
        return this;
    }

    public MTokenField<T> addMTokensToInputField(List<T> references, Function<T, String> valueProvider) {
        return addMTokensToInputField(references.stream().map(reference -> new MToken<T>(valueProvider.apply(reference), reference)).collect(Collectors.toList()));
    }

    public List<MToken<T>> getMTokens() {
        List<Token> tokens = super.getTokens();
        return tokens.stream().map(obj -> (MToken<T>) obj).collect(Collectors.toList());
    }

    public List<T> getReferences() {
        return getMTokens().stream().map(token -> token.getReference()).collect(Collectors.toList());
    }

    public MTokenField<T> addMTokens(List<MToken<T>> tokens) {
        addTokens(tokens.stream().map(obj -> (Token) obj).collect(Collectors.toList()));
        return this;
    }


    public MTokenField<T> addMTokens(List<T> references, Function<T, String> valueProvider) {
        return addMTokens(references.stream().map(reference -> new MToken<T>(valueProvider.apply(reference), reference)).collect(Collectors.toList()));
    }
}
