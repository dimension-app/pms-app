package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.data.ValueProvider;
import com.vaadin.ui.Button;

import java.util.function.Consumer;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 4:02 PM
 */
public class ButtonGenerator<T> implements ValueProvider<T, Button> {

    private Consumer<T> onClick;
    private Consumer<Button> decorator;
    private String confirmationMessage;

    public ButtonGenerator(Consumer<T> onClick) {
        this("Are you sure?", onClick, button -> {
        });
    }


    public ButtonGenerator(String confirmMessage, Consumer<T> onClick, Consumer<Button> decorator) {
        this.onClick = onClick;
        this.decorator = decorator;
        this.confirmationMessage = confirmMessage;
    }

    @Override
    public ItemButton apply(T t) {
        ItemButton itemButton = new ItemButton(t) {
            @Override
            public void processItem(Object itemID) {
                onProcessItem(t);
            }
        };

        decorator.accept(itemButton);
        itemButton.setConfirmationText(confirmationMessage);
        return itemButton;
    }

    private void onProcessItem(T t) {
        onClick.accept(t);
    }

    public String getConfirmationMessage() {
        return confirmationMessage;
    }

    public void setConfirmationMessage(String confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }
}
