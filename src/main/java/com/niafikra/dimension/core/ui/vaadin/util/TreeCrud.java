package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.data.TreeData;
import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Tree;
import org.vaadin.crudui.crud.AbstractCrud;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.CrudOperationException;
import org.vaadin.crudui.form.CrudFormFactory;
import org.vaadin.crudui.form.impl.form.factory.VerticalCrudFormFactory;
import org.vaadin.crudui.layout.CrudLayout;
import org.vaadin.crudui.layout.impl.WindowBasedCrudLayout;

import java.util.Collection;

public class TreeCrud<T> extends AbstractCrud<T> {

    protected String rowCountCaption = "%d items(s) found";
    protected String savedMessage = "Item saved";
    protected String deletedMessage = "Item deleted";

    protected Button findAllButton;
    protected Button addButton;
    protected Button updateButton;
    protected Button deleteButton;
    protected Tree<T> tree;

    protected Collection<T> items;
    private boolean clickRowToUpdate;

    public TreeCrud(Class<T> domainType) {
        this(domainType, new WindowBasedCrudLayout(), new VerticalCrudFormFactory<>(domainType), null);
    }

    public TreeCrud(Class<T> domainType, CrudLayout crudLayout) {
        this(domainType, crudLayout, new VerticalCrudFormFactory<>(domainType), null);
    }

    public TreeCrud(Class<T> domainType, CrudFormFactory<T> crudFormFactory) {
        this(domainType, new WindowBasedCrudLayout(), crudFormFactory, null);
    }

    public TreeCrud(Class<T> domainType, CrudListener<T> crudListener) {
        this(domainType, new WindowBasedCrudLayout(), new VerticalCrudFormFactory<>(domainType), crudListener);
    }

    public TreeCrud(Class<T> domainType, CrudLayout crudLayout, CrudFormFactory<T> crudFormFactory) {
        this(domainType, crudLayout, crudFormFactory, null);
    }

    public TreeCrud(Class<T> domainType, CrudLayout crudLayout, CrudFormFactory<T> crudFormFactory, CrudListener<T> crudListener) {
        super(domainType, crudLayout, crudFormFactory, crudListener);
        initLayout();
    }

    protected void initLayout() {
        findAllButton = new Button("", e -> findAllButtonClicked());
        findAllButton.setDescription("Refresh list");
        findAllButton.setIcon(VaadinIcons.REFRESH);
        crudLayout.addToolbarComponent(findAllButton);

        addButton = new Button("", e -> addButtonClicked());
        addButton.setDescription("Add");
        addButton.setIcon(VaadinIcons.PLUS);
        crudLayout.addToolbarComponent(addButton);

        updateButton = new Button("", e -> updateButtonClicked());
        updateButton.setDescription("Update");
        updateButton.setIcon(VaadinIcons.PENCIL);
        crudLayout.addToolbarComponent(updateButton);

        deleteButton = new Button("", e -> deleteButtonClicked());
        deleteButton.setDescription("Delete");
        deleteButton.setIcon(VaadinIcons.TRASH);
        crudLayout.addToolbarComponent(deleteButton);

        tree = new Tree<>("", new TreeData<>());
        tree.setSizeFull();
        tree.addSelectionListener(e -> treeSelectionChanged());
        crudLayout.setMainComponent(tree);

        updateButtons();
    }

    @Override
    public void attach() {
        super.attach();
        refreshTree();
    }

    @Override
    public void setAddOperationVisible(boolean visible) {
        addButton.setVisible(visible);
    }

    @Override
    public void setUpdateOperationVisible(boolean visible) {
        updateButton.setVisible(visible);
    }

    @Override
    public void setDeleteOperationVisible(boolean visible) {
        deleteButton.setVisible(visible);
    }

    @Override
    public void setFindAllOperationVisible(boolean visible) {
        findAllButton.setVisible(false);
    }

    public void refreshTree() {
        items = findAllOperation.findAll();
        tree.setItems(items);
    }

    public void setClickRowToUpdate(boolean clickRowToUpdate) {
        this.clickRowToUpdate = clickRowToUpdate;
    }

    protected void updateButtons() {
        boolean rowSelected = !tree.asSingleSelect().isEmpty();
        updateButton.setEnabled(rowSelected);
        deleteButton.setEnabled(rowSelected);
    }

    protected void treeSelectionChanged() {
        updateButtons();
        T domainObject = tree.asSingleSelect().getValue();

        if (domainObject != null) {
            Component form = crudFormFactory.buildNewForm(CrudOperation.READ, domainObject, true, null, event -> {
                tree.asSingleSelect().clear();
            });

            crudLayout.showForm(CrudOperation.READ, form);

            if (clickRowToUpdate) {
                updateButtonClicked();
            }

        } else {
            crudLayout.hideForm();
        }
    }

    protected void findAllButtonClicked() {
        tree.asSingleSelect().clear();
        refreshTree();
        Notification.show(String.format(rowCountCaption, tree.getDataProvider().size(new Query())));
    }

    protected void addButtonClicked() {
        try {
            T domainObject = domainType.newInstance();
            showForm(CrudOperation.ADD, domainObject, false, savedMessage, event -> {
                try {
                    T addedObject = addOperation.perform(domainObject);
                    refreshTree();
                    if (items.contains(addedObject)) {
                        tree.asSingleSelect().setValue(addedObject);
                        // TODO: tree.scrollTo(addedObject);
                    }
                } catch (CrudOperationException e1) {
                    refreshTree();
                } catch (Exception e2) {
                    refreshTree();
                    throw e2;
                }
            });
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected void updateButtonClicked() {
        T domainObject = tree.asSingleSelect().getValue();
        showForm(CrudOperation.UPDATE, domainObject, false, savedMessage, event -> {
            try {
                T updatedObject = updateOperation.perform(domainObject);
                tree.asSingleSelect().clear();
                refreshTree();
                if (items.contains(updatedObject)) {
                    tree.asSingleSelect().setValue(updatedObject);
                    // TODO: tree.scrollTo(updatedObject);
                }
            } catch (CrudOperationException e1) {
                refreshTree();
            } catch (Exception e2) {
                refreshTree();
                throw e2;
            }
        });
    }

    protected void deleteButtonClicked() {
        T domainObject = tree.asSingleSelect().getValue();
        showForm(CrudOperation.DELETE, domainObject, true, deletedMessage, event -> {
            try {
                deleteOperation.perform(domainObject);
                refreshTree();
                tree.asSingleSelect().clear();
            } catch (CrudOperationException e1) {
                refreshTree();
            } catch (Exception e2) {
                refreshTree();
                throw e2;
            }
        });
    }

    protected void showForm(CrudOperation operation, T domainObject, boolean readOnly, String successMessage, Button.ClickListener buttonClickListener) {
        Component form = crudFormFactory.buildNewForm(operation, domainObject, readOnly,
                cancelClickEvent -> {
                    if (clickRowToUpdate) {
                        tree.asSingleSelect().clear();
                    } else {
                        T selected = tree.asSingleSelect().getValue();
                        crudLayout.hideForm();
                        tree.asSingleSelect().clear();
                        tree.asSingleSelect().setValue(selected);
                    }
                },
                operationPerformedClickEvent -> {
                    crudLayout.hideForm();
                    buttonClickListener.buttonClick(operationPerformedClickEvent);
                    Notification.show(successMessage);
                });

        crudLayout.showForm(operation, form);
    }

    public Tree<T> getTree() {
        return tree;
    }

    public Button getFindAllButton() {
        return findAllButton;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getUpdateButton() {
        return updateButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setRowCountCaption(String rowCountCaption) {
        this.rowCountCaption = rowCountCaption;
    }

    public void setSavedMessage(String savedMessage) {
        this.savedMessage = savedMessage;
    }

    public void setDeletedMessage(String deletedMessage) {
        this.deletedMessage = deletedMessage;
    }

}