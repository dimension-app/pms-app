package com.niafikra.dimension.core.ui.vaadin;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 10:20 AM
 */
public class Templates {
    public static final String EXPENSE = "plan/_expense";
    public static final String REQUISITION = "plan/_requisition";
    public static final String ALLOCATION = "plan/_allocation";
    public static final String BALANCE = "plan/_balance";

    public static final String ISSUE = "issue/_issue";
    public static final String ISSUE_ROW = "issue/_issue_row";

    public static final String APPROVAL_TRACKER = "approval/_approval_tracker";
    public static final String MAIL_PENDING_APPROVAL = "approval/mail_pending_approval";
    public static final String MAIL_SUBMIT_APPROVAL = "approval/mail_submit_approval";
    public static final String MAIL_SUCCESS_APPROVAL = "approval/mail_success_approval";
    public static final String MAIL_DECLINED_APPROVAL = "approval/mail_declined_approval";
    public static final String MAIL_CANCELLED_CREATOR_APPROVAL = "approval/mail_cancelled_creator_approval";
    public static final String MAIL_CANCELLED_APPROVED_APPROVAL = "approval/mail_cancelled_approved_approval";
    public static final String MAIL_APPROVAL_ACTION = "approval/mail_approval_action";
    public static final String MAIL_APPROVAL_OVERRODE = "approval/mail_approval_overrode";
    public static final String MAIL_OVERRIDE_APPROVAL = "approval/mail_override_approval";

    public static final String MAIL_DELEGATION_ACTIVATED = "core/mail_delegation_activated";
    public static final String MAIL_DELEGATION_CANCELLED = "core/mail_delegation_cancelled";

    public static final String WELCOME = "core/_welcome";
    public static final String DELEGATION = "core/_delegation";
    public static final String BUDGET_SUMMARY = "plan/_budget_summary";
    public static final String BUDGET_HEADER = "plan/_budget_header";
    public static final String PERIOD_ADJUSTMENT = "plan/_period_adjustment";

    public static final String ITEM = "inventory/_item";
    public static final String ITEM_REQUISITION = "inventory/_item_requisition";
    public static final String ITEM_REQUISITION_APPROVAL = "inventory/_item_requisition_approval";
    public static final String BATCH = "inventory/_batch";
    public static final String STORE_ITEM_BALANCE = "inventory/_store_item_balance";
    public static final String RETIREMENT = "plan/_retirement";
    public static final String RETIREMENT_ENTRY_ROW = "plan/_retirement_entry";
    public static final String PAY_PERIOD_TYPE = "payroll/_pay_period_type";
    public static final String EMPLOYEE = "hr/_employee";
    public static final String CONTRACT = "payroll/_contract";
    ;
    public static final String SUBMITTED_INCOME = "payroll/_submitted_income";
    public static final String SUBMITTED_DEDUCTION = "payroll/_submitted_deduction";
    public static final String PAY = "payroll/_pay";
    public static final String PAYROLL = "payroll/_payroll";
    public static final String ALLOCATION_ADJUSTMENT ="plan/_allocation_adjustment";
    public static final String DISPENSE_DATA_ENTRIES = "inventory/_dispense_data_entries";
    public static final String DEPOSIT_DATA_ENTRIES = "inventory/_deposit_data_entries";
    public static final String INVENTORY_REQUISITION_PREVIEW = "inventory/_inventory_requisition_preview";
    public static final String INVENTORY_REQUISITION = "inventory/_inventory_requisition";
    public static final String INVENTORY_REQUISITION_SUMMARY = "inventory/_inventory_requisition_summary";
}
