package com.niafikra.dimension.core.ui.vaadin.util;

import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.CrudFormFactory;
import org.vaadin.crudui.layout.CrudLayout;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

public class MGridCrud<T> extends GridCrud<T> {
    public MGridCrud(Class<T> domainType) {
        this(domainType, new HorizontalSplitCrudLayout());
    }

    public MGridCrud(Class<T> domainType, CrudLayout crudLayout) {
        this(domainType, crudLayout, new MVerticalCrudFormFactory<>(domainType));
    }

    public MGridCrud(Class<T> domainType, CrudLayout crudLayout, CrudFormFactory<T> crudFormFactory) {
        super(domainType, crudLayout, crudFormFactory);

        //prevent error notifications from crud
        getCrudFormFactory().setErrorListener(e -> {
        });
    }
}
