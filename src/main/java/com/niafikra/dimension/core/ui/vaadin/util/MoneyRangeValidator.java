package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.money.Money;
import com.vaadin.data.validator.RangeValidator;

import java.util.Comparator;

public class MoneyRangeValidator extends RangeValidator<Money> {

    public MoneyRangeValidator(String errorMessage, Money minValue, Money maxValue) {
        super(errorMessage, Comparator.naturalOrder(), minValue, maxValue);
    }
}
