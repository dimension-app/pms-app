package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MPanel;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.ui.vaadin.user.UserManagementView.VIEW_NAME;

@Secured({PMSPermission.CONFIGURE_USERS})
@ViewComponent(value = SettingDisplay.class, caption = "Users")
@ViewInfo(value = "Users", section = "Security", icon = VaadinIcons.USERS)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class UserManagementView extends VerticalLayout implements SettingView, TableExportable, Notifier {

    public static final String VIEW_NAME = "users";

    private UsersGrid usersGrid;
    private UserForm userForm;


    public UserManagementView(UsersGrid usersGrid, UserForm userForm) {
        this.usersGrid = usersGrid;
        this.userForm = userForm;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        HorizontalSplitPanel base = new HorizontalSplitPanel();
        base.setSizeFull();
        base.setSplitPosition(60);
        addComponent(base);

        usersGrid.setSizeFull();
        usersGrid.setSelectionMode(Grid.SelectionMode.NONE);
        usersGrid.addItemClickListener(event -> showForm(event.getItem()));
        base.setFirstComponent(usersGrid);

        base.setSecondComponent(new MPanel(userForm).withFullSize().withStyleName(Theme.PANEL_BORDERLESS));
        userForm.setVisible(false);

        userForm.addSavedHandler(user -> usersGrid.getDataProvider().refreshAll());
        userForm.addDeleteHandler(user -> usersGrid.getDataProvider().refreshAll());
    }

    private void showForm(User user) {
        userForm.setVisible(true);
        userForm.setEntity(user);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(usersGrid);
    }
}
