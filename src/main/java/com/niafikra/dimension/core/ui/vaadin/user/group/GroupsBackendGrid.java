package com.niafikra.dimension.core.ui.vaadin.user.group;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.niafikra.dimension.group.domain.Group;
import com.niafikra.dimension.group.service.GroupService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 8:17 PM
 */
@SpringComponent
@PrototypeScope
public class GroupsBackendGrid extends NameFilterableBackendGrid<Group> {

    @Inject
    private GroupService groupService;

    public GroupsBackendGrid(GroupProvider dataProvider) {
        super(dataProvider);

        addColumn(t -> groupService.getUsers(t).size())
                .setId("memberCount")
                .setCaption("Members");
    }
}
