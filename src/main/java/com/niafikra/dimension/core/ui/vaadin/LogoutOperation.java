package com.niafikra.dimension.core.ui.vaadin;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.UI;

import java.io.Serializable;

@SpringComponent
@VaadinSessionScope
public class LogoutOperation implements Serializable {

    public void logout() {
        UI.getCurrent().getSession().getSession().invalidate();
        UI.getCurrent().getPage().open("/logout", "");
    }

}
