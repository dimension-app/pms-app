package com.niafikra.dimension.core.ui.vaadin;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import org.vaadin.spring.sidebar.SideBarItemDescriptor;
import org.vaadin.spring.sidebar.SideBarUtils;
import org.vaadin.spring.sidebar.components.ValoSideBar;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/13/17 12:30 PM
 */
@SpringComponent
@UIScope
public class SideBar extends ValoSideBar {

    public SideBar(SideBarUtils sideBarUtils) {
        super(sideBarUtils);
        setItemFilter(new SideBarMenuItemFilter());
    }

    @Override
    protected ItemComponentFactory createDefaultItemComponentFactory() {
        return new ItemFactory();
    }

    public class ItemFactory extends DefaultItemComponentFactory {

        @Override
        public Component createItemComponent(SideBarItemDescriptor descriptor) {
            return super.createItemComponent(descriptor);
        }
    }
}
