package com.niafikra.dimension.core.ui.vaadin.util.date;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractDateRangeSelector;
import com.vaadin.ui.DateField;

import java.time.LocalDate;

public class LocalDateRangeSelector extends AbstractDateRangeSelector<DateField, LocalDate> {

    public LocalDateRangeSelector() {
        super(new DateField(),new DateField());
    }
}
