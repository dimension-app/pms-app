package com.niafikra.dimension.core.ui.vaadin.view;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.util.*;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAnyAuthority;


public abstract class ViewSectionsPanel extends Panel implements View {

    private Navigator navigator;
    private ResponsiveLayout base;
    private ResponsiveRow sectionsRow;
    private Map<String, List<ViewLinkInfo>> sections = new LinkedHashMap<>();

    public ViewSectionsPanel(Navigator navigator) {
        this.navigator = navigator;
    }

    @PostConstruct
    private void build() throws ClassNotFoundException {
        base = new ResponsiveLayout();
        setContent(base);
        setSizeFull();
        addStyleName(Theme.PANEL_BORDERLESS);

        loadSections();

        sectionsRow = base.addRow();
        sections.keySet().stream().sorted().forEach(section -> {
            Component sectionColumn = createSection(section);
            sectionsRow.addColumn().withDisplayRules(12, 12, 4, 4).withComponent(sectionColumn);
        });

    }

    private Component createSection(String section) {
//        sectionRow.setMargin(true);
        VerticalLayout sectionRow = new MVerticalLayout()
                .withCaption(section)
                .withSpacing(false)
                .withMargin(false);
        sectionRow.setCaption(section);

        sections.get(section)
                .stream()
                .sorted(Comparator.comparing(linkInfo -> linkInfo.caption))
                .forEach(linkInfo -> {
                    sectionRow.addComponent(createLink(linkInfo));
                });
        return sectionRow;
    }

    private Component createLink(ViewLinkInfo viewInfo) {
        return new MButton(viewInfo.icon, viewInfo.caption, e -> showView(viewInfo)).withStyleName(Theme.BUTTON_LINK);
    }

    private void showView(ViewLinkInfo viewInfo) {
        navigator.navigateTo(viewInfo.view);
    }

    private void loadSections() throws ClassNotFoundException {
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        configureViewProvider(provider);
        Set<BeanDefinition> definitions = provider.findCandidateComponents("com.niafikra.dimension");

        for (BeanDefinition definition : definitions) {
            Class<View> viewClass = (Class<View>) Class.forName(definition.getBeanClassName());
            Secured securedAnnotation = viewClass.getAnnotation(Secured.class);
            if (securedAnnotation == null)
                throw new IllegalArgumentException("View " + viewClass + " has no " + Secured.class + " annotation");

            //if the viewInfo authority is not granted to user then skip
            if (!hasAnyAuthority(securedAnnotation.value())) continue;

            ViewInfo viewInfoAnnotation = viewClass.getAnnotation(ViewInfo.class);
            if (viewInfoAnnotation == null)
                throw new IllegalArgumentException("View " + viewClass + " has no " + ViewInfo.class + " annotation");

            ViewLinkInfo viewInfo = new ViewLinkInfo(viewClass, viewInfoAnnotation);

            List<ViewLinkInfo> viewInfos = sections.get(viewInfo.section);
            if (viewInfos == null) {
                viewInfos = new LinkedList<>();
                sections.put(viewInfo.section, viewInfos);
            }

            viewInfos.add(viewInfo);
        }
    }

    protected abstract void configureViewProvider(ClassPathScanningCandidateComponentProvider provider);

    private class ViewLinkInfo {
        private Class<? extends View> view;
        private String caption;
        private String section;
        private VaadinIcons icon;

        public ViewLinkInfo(Class<? extends View> viewClass, ViewInfo annotation) {
            view = viewClass;
            caption = annotation.value();
            section = annotation.section();
            icon = annotation.icon();
        }
    }
}
