package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.ui.RadioButtonGroup;

import java.util.Arrays;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 11/19/16.
 */
public class BooleanOptionGroup extends RadioButtonGroup<Integer> {

    public BooleanOptionGroup(String caption, String trueCaption, String falseCaption, String nullCaption) {
        super(caption, Arrays.asList(2, 1, 0));
        setItemCaptionGenerator(item -> {
            if (item == 1) return trueCaption;
            if (item == 0) return falseCaption;
            else return nullCaption;

        });
        addStyleName(Theme.OPTIONGROUP_HORIZONTAL);
    }

    public Boolean getBooleanValue() {
        Integer val = (Integer) super.getValue();
        if (val == null) return null;
        return val == 1 ? Boolean.TRUE : val == 0 ? Boolean.FALSE : null;
    }
}