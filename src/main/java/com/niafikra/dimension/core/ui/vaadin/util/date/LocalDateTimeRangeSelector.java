package com.niafikra.dimension.core.ui.vaadin.util.date;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractDateRangeSelector;
import com.vaadin.ui.DateTimeField;

import java.time.LocalDateTime;

public class LocalDateTimeRangeSelector extends AbstractDateRangeSelector<DateTimeField, LocalDateTime> {

    public LocalDateTimeRangeSelector() {
        super(new DateTimeField(), new DateTimeField());
    }

}