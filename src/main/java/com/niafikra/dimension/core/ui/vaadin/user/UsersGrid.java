package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.core.security.domain.User;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

@SpringComponent
@PrototypeScope
public class UsersGrid extends Grid<User> {

    private UsersProvider usersProvider;

    public UsersGrid(UsersProvider usersProvider) {
        super(User.class);
        setDataProvider(usersProvider);
        this.usersProvider = usersProvider;
    }

    @PostConstruct
    private void build() {
        setColumns("loginId", "name", "email", "phoneNumber");
    }

}
