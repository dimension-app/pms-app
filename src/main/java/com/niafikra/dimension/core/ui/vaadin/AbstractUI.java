package com.niafikra.dimension.core.ui.vaadin;

import com.niafikra.dimension.PMSApp;
import com.niafikra.dimension.mail.MailService;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.Locale;

import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;
import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/20/16.
 */
public abstract class AbstractUI extends UI {

    protected MailService mailService;
    @Value("${dimension.mail.errors}")
    protected String errorsEmail;

    public AbstractUI(MailService mailService) {
        this.mailService = mailService;

        setLocale(Locale.UK);
        VaadinSession.getCurrent().setLocale(Locale.UK);
    }

    @PostConstruct
    private void setup() {
        VaadinSession.getCurrent().setErrorHandler((ErrorHandler) event -> {
            sendErrorsEmail(event.getThrowable());
            event.getThrowable().printStackTrace();
        });
    }

    private void sendErrorsEmail(Throwable throwable) {
        String heading = String.format("%s:%s", PMSApp.baseUrl, getRootCauseMessage(throwable));
        mailService.sendEmailAsync(heading, getStackTrace(throwable), false, false, errorsEmail);
    }
}
