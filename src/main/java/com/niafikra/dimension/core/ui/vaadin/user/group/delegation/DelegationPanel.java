package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.niafikra.dimension.delegation.service.DelegationService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.PMSPermission.CANCEL_DELEGATION;
import static com.niafikra.dimension.PMSPermission.DELETE_DELEGATION;
import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

@PrototypeScope
@SpringComponent
@RequiredArgsConstructor
public class DelegationPanel extends VerticalLayout implements Notifier {

    private final DelegationService delegationService;
    private final User currentUser;
    private final DelegationTemplatePanel templatePanel;

    private ConfirmButton deleteButton, cancelButton;
    private Delegation delegation;

    private SubWindow subWindow;


    @PostConstruct
    private void build() {

        deleteButton = new ConfirmButton(VaadinIcons.CLOSE, "Do you want to delete delegation?", () -> doDelete())
                .withCaption("Delete")
                .withWidth("150px")
                .withStyleName(Theme.BUTTON_DANGER)
                .withVisible(hasAuthority(DELETE_DELEGATION));

        cancelButton = new ConfirmButton(VaadinIcons.CLOSE, "Do you want to cancel delegation?", () -> doCancel())
                .withCaption("Cancel")
                .withWidth("150px")
                .withStyleName(Theme.BUTTON_PRIMARY)
                .withVisible(hasAuthority(CANCEL_DELEGATION));


        setSizeFull();
        addComponent(new MHorizontalLayout(cancelButton, deleteButton));
        templatePanel.setSizeFull();
        addComponent(templatePanel);
        setExpandRatio(templatePanel, 1);
    }

    private void doCancel() {
        try {
            delegation = delegationService.cancel(delegation,currentUser);
            setDelegation(delegation);
            showSuccess("Successful cancelled delegation", delegation.toString());
        } catch (Exception e) {
            showError("Failed to cancel delegation", e);
        }
    }

    private void doDelete() {
        try {
            delegationService.delete(delegation);
            showSuccess("Successful deleted delegation", delegation.toString());
            subWindow.close();
        } catch (Exception e) {
            showError("Failed to delete delegation", e);
        }
    }

    public void setDelegation(Delegation delegation) {
        this.delegation = delegation;

        templatePanel.setDelegation(delegation);
        deleteButton.setVisible(delegation.isWaiting());
        cancelButton.setVisible(delegationService.isActive(delegation));
    }

    public SubWindow popUp(Delegation delegation) {
        setDelegation(delegation);
        subWindow = new SubWindow(this).show();
        subWindow.withCaption(delegation.toString())
                .withModal(true)
                .withCenter()
                .withSize(MSize.size("80%", "90%"));
        return subWindow;
    }
}
