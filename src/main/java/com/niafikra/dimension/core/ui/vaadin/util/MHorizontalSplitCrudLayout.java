package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.ui.AbstractSplitPanel;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

public abstract class MHorizontalSplitCrudLayout  extends HorizontalSplitCrudLayout {

    public static MHorizontalSplitCrudLayout create(float splitPosition){
        return new MHorizontalSplitCrudLayout() {
            @Override
            protected float getSplitPosition() {
                return splitPosition;
            }
        };
    }

    @Override
    protected AbstractSplitPanel getMainLayout() {
        AbstractSplitPanel splitPanel = super.getMainLayout();
        splitPanel.setSplitPosition(getSplitPosition());
        return splitPanel;
    }

    protected abstract float getSplitPosition();
}
