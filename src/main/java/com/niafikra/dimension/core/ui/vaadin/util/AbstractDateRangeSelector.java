package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.data.HasValue;
import com.vaadin.shared.Registration;
import com.vaadin.ui.AbstractDateField;
import com.vaadin.ui.Composite;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import java.time.LocalDateTime;
import java.time.temporal.Temporal;

public class AbstractDateRangeSelector<T extends AbstractDateField,R extends Temporal> extends Composite {
    private T startDateField, endDateField;

    public AbstractDateRangeSelector( T startField, T endField) {
        startDateField =startField;
        startDateField.setWidth("100%");
        startDateField.addStyleNames(Theme.DATEFIELD_TINY, Theme.DATEFIELD_BORDERLESS);

        endDateField = endField;
        endDateField.setWidth("100%");
        endDateField.addStyleNames(Theme.DATEFIELD_TINY, Theme.DATEFIELD_BORDERLESS);

        setCompositionRoot(
                new MHorizontalLayout(startDateField, endDateField)
                        .withSpacing(false)
                        .withMargin(false)
                        .withFullWidth()
        );
    }

    public R getStart() {
        return (R) getStartField().getValue();
    }

    public R getEnd() {
        return (R) getEndField().getValue();
    }

    public T getStartField() {
        return startDateField;
    }

    public T getEndField() {
        return endDateField;
    }

    public Pair<Registration, Registration> addValueChangeListener(HasValue.ValueChangeListener<LocalDateTime> listener) {
        Registration startRegistration = startDateField.addValueChangeListener(listener);
        Registration endRegistration = endDateField.addValueChangeListener(listener);

        return ImmutablePair.of(startRegistration, endRegistration);
    }

    public void setPeriod(LocalDateTime startTime, LocalDateTime endTime) {
        getStartField().setValue(startTime);
        getEndField().setValue(endTime);
    }
}
