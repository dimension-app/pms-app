package com.niafikra.dimension.core.ui.vaadin.user.group;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableListGrid;
import com.niafikra.dimension.group.domain.Group;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 5:11 PM
 */

public class GroupListGrid extends NameFilterableListGrid<Group> {

//    public GroupListGrid() {
//        addColumn(Group::getName)
//                .setId("group")
//                .setCaption("Group")
//                .setSortable(false);
//    }

}
