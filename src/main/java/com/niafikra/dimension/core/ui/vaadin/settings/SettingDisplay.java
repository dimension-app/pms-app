package com.niafikra.dimension.core.ui.vaadin.settings;

import com.niafikra.dimension.core.ui.vaadin.view.AbstractMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.vaadin.navigator.View;

import javax.annotation.PostConstruct;

@DisplayComponent
public class SettingDisplay extends AbstractMainContentDisplay {

    @PostConstruct
    private void build() {
        setMargin(false);
        setSizeFull();
    }

    @Override
    public void show(View view) {
        removeAllComponents();
        addComponent(view.getViewComponent());
        setExpandRatio(view.getViewComponent(), 1);
    }
}
