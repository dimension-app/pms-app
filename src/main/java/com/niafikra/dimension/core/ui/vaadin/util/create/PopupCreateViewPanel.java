package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 3:56 PM
 */
public abstract class PopupCreateViewPanel<T> extends GridCreateViewPanel<T> {

    protected SubWindow subWindow = new SubWindow();

    public PopupCreateViewPanel(Grid<T> entriesGrid) {
        super(entriesGrid);

        //refresh the grid after closing the window
        subWindow.addCloseListener(e -> entriesGrid.getDataProvider().refreshAll());
    }

    @Override
    protected void attachEntriesGrid(Grid<T> entriesGrid) {
        entriesGrid.setSizeFull();
        addComponent(entriesGrid);
        setExpandRatio(entriesGrid, 1);
    }

    @Override
    protected void showCreateContent() {
        subWindow.setContent(getCreateComponent());
        subWindow.show();
    }

    @Override
    protected void showViewContent(T entry) {
        subWindow.setContent(getViewComponent(entry));
        subWindow.show();
    }

    public SubWindow getSubWindow() {
        return subWindow;
    }

    protected abstract Component getCreateComponent();

    protected abstract Component getViewComponent(T entry);
}
