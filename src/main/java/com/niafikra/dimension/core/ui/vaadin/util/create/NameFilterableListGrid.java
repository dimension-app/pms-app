package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.util.HasName;
import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;

import java.util.Collection;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 7:31 PM
 */
public class NameFilterableListGrid<T extends HasName> extends PropertyFilterableGrid<T> {

    public NameFilterableListGrid() {
        super("name", T::getName);
    }

    @Override
    protected void filter(HasValue.ValueChangeEvent<String> event) {
        ListDataProvider<T> dataProvider = (ListDataProvider<T>) super.getDataProvider();
        dataProvider.setFilter(t -> t.getName().toLowerCase().contains(event.getValue().toLowerCase()));
    }

    public Collection<T> getItems() {
        ListDataProvider<T> dataProvider = (ListDataProvider<T>) super.getDataProvider();
        return dataProvider.getItems();
    }
}
