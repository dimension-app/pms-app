package com.niafikra.dimension.core.ui.vaadin.report;

import com.niafikra.dimension.core.ui.vaadin.view.AbstractMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.vaadin.navigator.View;

import javax.annotation.PostConstruct;

@DisplayComponent
public class ReportDisplay extends AbstractMainContentDisplay {

    @PostConstruct
    private void build() {
        setMargin(false);
        setSizeFull();
    }

    @Override
    public void show(View view) {
        removeAllComponents();
        addComponent(view.getViewComponent());
        setExpandRatio(view.getViewComponent(), 1);
    }
}
