package com.niafikra.dimension.core.ui.vaadin.actions;

import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import org.vaadin.viritin.button.MButton;

public class TableExportButton extends MButton {

    private TableExport exporter;

    public TableExportButton() {
        super(VaadinIcons.FILE_TABLE);
        addClickListener(() -> exporter.export());
    }

    public TableExport getExporter() {
        return exporter;
    }

    public void setExporter(TableExport exporter) {
        this.exporter = exporter;
    }
}
