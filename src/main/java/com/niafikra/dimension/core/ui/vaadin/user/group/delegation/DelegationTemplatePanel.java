package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.vaadin.ui.VerticalLayout;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Component
@PrototypeScope
public class DelegationTemplatePanel extends VerticalLayout {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        addComponentsAndExpand(templatePanel);
        templatePanel
                .getTemplateView()
                .setTemplatePath(Templates.DELEGATION);
    }

    public void setDelegation(Delegation delegation) {
        templatePanel
                .getTemplateView()
                .putBinding("delegation", delegation)
                .render();
    }
}
