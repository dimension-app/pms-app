package com.niafikra.dimension.core.ui.vaadin.util.provider;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 11:14 AM
 */
public interface NameCategoryFilterableDataProvider<T extends HasCategory> extends StringFilterableDataProvider<T> {
    Category getCategoryFilter();

    void setCategoryFilter(Category categoryFilter);
}
