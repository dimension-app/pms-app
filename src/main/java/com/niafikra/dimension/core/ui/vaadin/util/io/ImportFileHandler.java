package com.niafikra.dimension.core.ui.vaadin.util.io;

import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.upload.UploadWindow;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/20/16
 */
public class ImportFileHandler implements Notifier {

    private ImportProcessing importProcessor;
    private List<ImportListener> importListeners;

    public ImportFileHandler(UploadWindow uploadWindow) {
        importListeners = new LinkedList<>();
        uploadWindow.addUploadListener(file -> handleFile(file));
    }

    private void handleFile(File file) {
        try (InputStream fileInputStream = new FileInputStream(file)) {
            if (importProcessor.importFile(fileInputStream)) {
                notifyImportFinished(true);
            } else {
                notifyImportFinished(false);
            }
        } catch (IOException ex) {
            showError("Failed to import file", ex);
        }
    }

    public ImportProcessing getImportProcessor() {
        return importProcessor;
    }

    public void setImportProcessor(ImportProcessing importFileProcessor) {
        this.importProcessor = importFileProcessor;
    }

    public void addListener(ImportListener listener) {
        this.importListeners.add(listener);
    }

    public void removeListener(ImportListener listener) {
        this.importListeners.remove(listener);
    }

    private void notifyImportFinished(boolean importResult) {
        for (ImportListener listener : importListeners)
            listener.onImportFinished(importResult);
    }

    public interface ImportListener {
        void onImportFinished(boolean successful);
    }
}
