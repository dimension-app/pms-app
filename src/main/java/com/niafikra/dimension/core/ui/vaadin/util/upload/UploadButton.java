package com.niafikra.dimension.core.ui.vaadin.util.upload;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Upload;
import org.vaadin.viritin.button.MButton;

/**
 * @author Boniface Chacha <boniface.chacha@niafikra.com,bonifacechacha@gmail.com>
 * Date: 1/13/14
 * Time: 2:19 AM
 */
public class UploadButton extends MButton {

    private UploadWindow uploadWindow;

    public UploadButton() {
        this(null);
    }

    /**
     * Give the destination dir for the upload
     *
     * @param destinationDir
     */
    public UploadButton(String destinationDir) {
        setIcon(VaadinIcons.UPLOAD);
        uploadWindow = new UploadWindow(destinationDir);
        addClickListener(event -> uploadWindow.show());
    }


    public Upload getUpload() {
        return uploadWindow.getUpload();
    }

    public Upload.SucceededEvent getRecentSucceededEvent() {
        return uploadWindow.getRecentSucceededEvent();
    }

    public UploadWindow getUploadWindow() {
        return uploadWindow;
    }
}
