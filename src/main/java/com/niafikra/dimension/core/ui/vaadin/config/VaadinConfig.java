package com.niafikra.dimension.core.ui.vaadin.config;

import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.SystemMessagesProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/19/17 10:07 PM
 */
@Configuration
public class VaadinConfig {
    @Bean
    SystemMessagesProvider systemMessagesProvider() {
        return (SystemMessagesProvider) systemMessagesInfo -> {
            CustomizedSystemMessages systemMessages = new CustomizedSystemMessages();
//            systemMessages.setSessionExpiredNotificationEnabled(false);
            return systemMessages;
        };
    }

}
