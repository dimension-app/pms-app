package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.data.HasValue;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderCell;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.viritin.fields.IntegerField;

import java.util.Collection;
import java.util.function.Consumer;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/7/17 10:38 AM
 */
public class VaadinUtils {


    public static <T> ComboBox<T> createFilteringComboBox(HeaderRow headerRow, String propertyId, String placeHolder, Collection<T> items, HasValue.ValueChangeListener<T> listener) {
        return createFilteringComboBox(headerRow.getCell(propertyId), placeHolder, items, listener);
    }

    public static CheckBox createFilteringCheckBox(HeaderRow headerRow, String propertyId, String caption, HasValue.ValueChangeListener<Boolean> listener) {
        return createFilteringCheckBox(headerRow.getCell(propertyId), caption, listener);
    }

    public static TextField createFilteringTextField(HeaderRow headerRow, String propertyId, String placeHolder, HasValue.ValueChangeListener<String> listener) {
        return createFilteringTextField(headerRow.getCell(propertyId), placeHolder, listener);
    }

    public static TextField createFilteringTextField(HeaderCell headerCell, String placeHolder, HasValue.ValueChangeListener<String> listener) {
        TextField filterField = new TextField();
        filterField.setPlaceholder(placeHolder);
        filterField.setWidth("100%");
        filterField.addStyleNames(Theme.TEXTFIELD_TINY, Theme.TEXT_SMALL);
        filterField.addValueChangeListener(listener);
        headerCell.setComponent(filterField);
        return filterField;
    }

    public static IntegerField createFilteringIntegerField(HeaderRow headerRow, String propertyId, String description, HasValue.ValueChangeListener<Integer> listener) {
        return createFilteringIntegerField(headerRow.getCell(propertyId), description, listener);
    }

    public static IntegerField createFilteringIntegerField(HeaderCell headerCell, String description, HasValue.ValueChangeListener<Integer> listener) {
        IntegerField filterField = new IntegerField();
        filterField.setDescription(description);
        filterField.setWidth("100%");
        filterField.addStyleNames(Theme.TEXTFIELD_TINY, Theme.TEXT_SMALL);
        filterField.addValueChangeListener(listener);
        headerCell.setComponent(filterField);
        return filterField;
    }


    @Deprecated
    /**
     * @deprecated in favour of creating a specific combobox and prevent the tradition of creating data providers which uses string as a filter
     */
    public static <T> ComboBox<T> createFilteringComboBox(HeaderCell headerCell, String placeHolder, Collection<T> items, HasValue.ValueChangeListener<T> listener) {
        ComboBox<T> filterField = new ComboBox<>();
        filterField.setItems(items);
        filterField.setPlaceholder(placeHolder);
        filterField.setWidth("100%");
        filterField.addStyleNames(Theme.COMBOBOX_TINY, Theme.TEXT_SMALL);
        filterField.addValueChangeListener(listener);
        headerCell.setComponent(filterField);
        return filterField;
    }

    @Deprecated
    /**
     * @deprecated in favour of creating a specific combobox and prevent the tradition of creating data providers which uses string as a filter
     */
    public static <T> ComboBox<T> createFilteringComboBox(HeaderCell headerCell, String placeHolder, DataProvider<T, String> provider, HasValue.ValueChangeListener<T> listener) {
        ComboBox<T> filterField = new ComboBox<>();
        filterField.setDataProvider(provider);
        filterField.setPlaceholder(placeHolder);
        filterField.setWidth("100%");
        filterField.addStyleNames(Theme.COMBOBOX_TINY, Theme.TEXT_SMALL);
        filterField.addValueChangeListener(listener);
        headerCell.setComponent(filterField);
        return filterField;
    }

    @Deprecated
    /**
     * @deprecated in favour of creating a specific combobox and prevent the tradition of creating data providers which uses string as a filter
     */
    public static <T> ComboBox<T> createFilteringComboBox(HeaderRow headerRow, String propertyId, String placeHolder, DataProvider<T, String> provider, HasValue.ValueChangeListener<T> listener) {
        return createFilteringComboBox(headerRow.getCell(propertyId), placeHolder, provider, listener);
    }


    public static CheckBox createFilteringCheckBox(HeaderCell headerCell, String caption, HasValue.ValueChangeListener<Boolean> listener) {
        CheckBox filterField = new CheckBox(caption);
        filterField.addStyleNames(Theme.CHECKBOX_SMALL, Theme.TEXT_SMALL);
        filterField.addValueChangeListener(listener);
        headerCell.setComponent(filterField);
        return filterField;
    }

    @Deprecated
    /**
     * @deprecated use addActionButton instead in order to put a more descriptive confirmation message
     */
    public static <T> ButtonGenerator<T> addRemoveButton(Grid<T> grid, Consumer<T> onClick) {
        return addActionButton(grid, "Are you sure?", onClick, button -> {
            button.setIcon(VaadinIcons.CLOSE);
            button.setCaption("Remove");
        });
    }

    public static <T> ButtonGenerator<T> addActionButton(Grid<T> grid, String confirmationMessage, Consumer<T> onClick, Consumer<Button> decorator) {
        ButtonGenerator<T> buttonGenerator = new ButtonGenerator<>(confirmationMessage, onClick, decorator);
        grid.addComponentColumn(buttonGenerator).setCaption("").setWidth(70).setSortable(false);
        return buttonGenerator;
    }
}
