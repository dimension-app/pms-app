package com.niafikra.dimension.core.ui.vaadin.util.provider;

import com.niafikra.dimension.core.util.HasName;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 8:43 PM
 */
public abstract class AbstractNameFilterablePageableDataProvider<T extends HasName>
        extends PageableDataProvider<T, String>
        implements StringFilterableDataProvider<T> {

    private String nameFilter = "";

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("name", SortDirection.ASCENDING));
        return sortOrders;
    }

    public String getFilter() {
        return nameFilter;
    }

    public void setFilter(String nameFilter) {
        this.nameFilter = nameFilter;
        refreshAll();
    }

}
