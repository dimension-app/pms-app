package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.vaadin.viritin.layouts.MWindow;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 6:01 PM
 */
public class SubWindow extends MWindow {

    public SubWindow(Component component) {
        this();
        withContent(component);
    }

    public SubWindow() {
        setModal(true);
        setWidth("-1");
        center();
    }

    public SubWindow show() {
        UI.getCurrent().addWindow(this);
        return this;
    }

    public SubWindow show(Component component) {
        withContent(component);
        return show();
    }

    public SubWindow withCloseListener(CloseListener listener) {
        addCloseListener(listener);
        return this;
    }

    public SubWindow withCaption(String caption) {
        setCaption(caption);
        return this;
    }
}
