package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.util.HasName;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 9:11 PM
 */
public class NameForm<T extends HasName> extends AbstractForm<T> {
    protected TextField name = new TextField("Name");

    public NameForm(Class<T> type) {
        super(type);
        setWidth("100%");
        name.setWidth("100%");
    }

    @Override
    protected Component createContent() {
        ComponentContainer fieldsContainer = createFormFields();
        HorizontalLayout toolbar = getToolbar();
        fieldsContainer.addComponent(toolbar);
        return new MVerticalLayout(fieldsContainer);
    }

    protected ComponentContainer createFormFields() {
        return new MVerticalLayout(name).withMargin(false);
    }

    public TextField getNameField() {
        return name;
    }
}