package com.niafikra.dimension.core.ui.vaadin.util.grid;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Getter;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.List;
import java.util.function.Supplier;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/13/16 12:37 AM
 */

public class ItemsEditor<T> extends MVerticalLayout {

    @Getter
    private final ItemEditGrid<T> entriesGrid;
    @Getter
    private final Supplier<T> newItemSupplier;

    @Getter
    private Button addItemButton = new Button("Add", VaadinIcons.PLUS_CIRCLE);
    @Getter
    private HorizontalLayout footer = new MHorizontalLayout();

    public ItemsEditor(ItemEditGrid<T> entriesGrid, Supplier<T> newItemSupplier) {
        this.entriesGrid = entriesGrid;
        this.newItemSupplier = newItemSupplier;

        build();
    }

    private void build() {
        setSizeFull();
        add(entriesGrid);
        setExpandRatio(entriesGrid,1);

        addItemButton.addStyleNames(ValoTheme.BUTTON_SMALL,ValoTheme.BUTTON_QUIET);

        footer.addComponent(addItemButton);
        add(footer);
        setComponentAlignment(footer, Alignment.MIDDLE_RIGHT);

        addItemButton.addClickListener(event -> {
            entriesGrid.addEntry(newItemSupplier.get());
        });

    }

    public List<T> getValidEntries() {
        return entriesGrid.getValidItems();
    }
}
