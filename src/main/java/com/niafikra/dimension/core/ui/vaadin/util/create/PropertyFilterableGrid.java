package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:37 PM
 */
public abstract class PropertyFilterableGrid<T> extends Grid<T> {

    protected HeaderRow headerRow;

    public PropertyFilterableGrid(String nameProperty, ValueProvider<T, Object> valueProvider) {
        build(nameProperty, valueProvider);
    }

    public PropertyFilterableGrid(DataProvider<T, Object> dataProvider,
                                  String nameProperty,
                                  ValueProvider<T, Object> valueProvider) {
        super(dataProvider);
        build(nameProperty, valueProvider);
    }

    private void build(String nameProperty, ValueProvider<T, Object> valueProvider) {
        addColumn(valueProvider)
                .setId(nameProperty)
                .setCaption(nameProperty)
                .setMinimumWidth(200)
                .setExpandRatio(1)
                .setSortable(false);

        headerRow = appendHeaderRow();
        createFilteringTextField(
                headerRow,
                nameProperty,
                "Type here to filter",
                event -> filter(event)
        );
    }

    protected abstract void filter(HasValue.ValueChangeEvent<String> event);

}
