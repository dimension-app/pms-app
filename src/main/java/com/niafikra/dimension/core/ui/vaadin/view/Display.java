package com.niafikra.dimension.core.ui.vaadin.view;

import com.vaadin.navigator.View;
import com.vaadin.ui.Component;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/12/17 11:54 AM
 */
public interface Display extends Component {
    void show(View view);

    void start();
}
