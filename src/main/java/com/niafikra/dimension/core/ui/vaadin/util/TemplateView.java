package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.shared.ui.ContentMode;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.expression.ThymeleafEvaluationContext;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.label.MLabel;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/14/17 8:44 PM
 */
@Component
@PrototypeScope
public final class TemplateView extends MLabel {

    private SpringTemplateEngine templateEngine;
    private String templatePath;
    private Context context;

    public TemplateView(SpringTemplateEngine templateEngine, ApplicationContext applicationContext) {
        this.templateEngine = templateEngine;

        context = new Context();
        context.setVariable("applicationContext", applicationContext);

        context.setVariable(
                ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME,
                new ThymeleafEvaluationContext(applicationContext, null)
        );

        withContentMode(ContentMode.HTML);
    }

    public TemplateView setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
        return this;
    }

    public TemplateView putBinding(String key, Object value) {
        context.setVariable(key, value);
        return this;
    }

    public TemplateView render() {
        setValue(templateEngine.process(templatePath, context));
        return this;
    }

    public void clear() {
        setValue(null);
    }
}
