package com.niafikra.dimension.core.ui.vaadin.util.io;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.OLE2NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;

/**
 * Used to import data from an excel file
 *
 * @author mbwana jaffari mbura
 * Date: 5/8/14
 * Time: 6:11 PM
 */
public interface ExcelImporter extends ImportProcessing {

    /**
     * Import data from the given workbook.
     *
     * @param workbook
     * @return true if successful false otherwise
     */
    boolean importExcel(Workbook workbook);

    default boolean importFile(InputStream importStream) throws IOException {
        Workbook workbook;

        try {
            workbook = new XSSFWorkbook(importStream);
        } catch (OLE2NotOfficeXmlFileException e) {
            workbook = new HSSFWorkbook(importStream);
        }

        return importExcel(workbook);
    }
}
