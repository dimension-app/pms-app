package com.niafikra.dimension.core.ui.vaadin.util.provider;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.Query;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 4:35 PM
 */

//TODO THIS CLASS ADDS UNNECESSARY COMPLEXITY WHICH CAN BE EASILY BE RESOLVED BY A UTILITY CLASS
@Deprecated
public interface StringFilterableDataProvider<T> extends DataProvider<T, String> {
    String getFilter();

    void setFilter(String filter);

    default String getFilter(Query<T, String> query) {
        if (query.getFilter().isPresent())
            return query.getFilter().get();
        else return getFilter();
    }
}