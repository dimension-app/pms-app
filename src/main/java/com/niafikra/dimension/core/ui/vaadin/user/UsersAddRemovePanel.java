package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.util.grid.GridAddRemovePanel;
import com.niafikra.dimension.core.ui.vaadin.util.grid.GridAddRemovePanel.AddRemoveListener;
import com.niafikra.dimension.core.util.HasLogger;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 3:09 PM
 */
@SpringComponent
@PrototypeScope
public abstract class UsersAddRemovePanel extends VerticalLayout implements HasLogger {

    private Button saveButton = new Button("Save users", VaadinIcons.CHECK_CIRCLE);
    private GridAddRemovePanel<User> addRemovePanel;
    private UsersListGrid usersGrid;
    private UsersProvider userProvider;

    private Set<User> selectedUsers  = new HashSet<>();
    private UserService.UserFilter userFilter = UserService.UserFilter.builder().build();

    public UsersAddRemovePanel(UsersProvider userProvider) {
        this.userProvider = userProvider;
        usersGrid = new UsersListGrid();
        addRemovePanel = new GridAddRemovePanel<>(usersGrid);

        build();
    }

    private void build() {
        setSizeFull();

        saveButton.setWidth("100%");
        saveButton.addClickListener((Button.ClickListener) event -> {
            Collection<User> users = usersGrid.getUsers();
            onSave(users);
        });

        addRemovePanel.setSizeFull();
        addRemovePanel.setMargin(false);
        addComponents(addRemovePanel, saveButton);
        setExpandRatio(addRemovePanel, 1);

        ConfigurableFilterDataProvider<User,String, UserService.UserFilter> configurableFilterDataProvider =
                userProvider.withConfigurableFilter((s, filter) -> {
                    filter.setName(s);
                    return filter;
                });
        configurableFilterDataProvider.setFilter(userFilter);
        addRemovePanel.getSelector().setDataProvider(configurableFilterDataProvider);
        addRemovePanel.addAddRemoveListener(new AddRemoveListener<User>() {
            @Override
            public void onAdd(User entry) {
                selectedUsers.add(entry);
                userFilter.setExcludedUsers(selectedUsers);
                configurableFilterDataProvider.refreshAll();
            }

            @Override
            public void onRemove(User entry) {
                selectedUsers.remove(entry);
                userFilter.setExcludedUsers(selectedUsers);
                configurableFilterDataProvider.refreshAll();
            }
        });
    }

    public UsersProvider getUserProvider() {
        return userProvider;
    }

    public void setSelectedUsers(Set<User> users) {
        this.selectedUsers = users;
        usersGrid.setItems(users);
    }

    public GridAddRemovePanel<User> getUsersAddRemovePanel() {
        return addRemovePanel;
    }

    public UsersListGrid getUsersGrid() {
        return usersGrid;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public GridAddRemovePanel<User> getAddRemovePanel() {
        return addRemovePanel;
    }

    protected abstract void onSave(Collection<User> users);
}
