package com.niafikra.dimension.core.ui.vaadin.view;

import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.ClientConnector;
import com.vaadin.server.Resource;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.MenuBar;
import org.springframework.aop.support.AopUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.ReflectionUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/12/17 12:00 PM
 */
@UIScope
@SpringViewDisplay
public class ViewDisplayManager implements ViewDisplay {
    private ApplicationContext applicationContext;
    private MainHeader mainHeader;

    public ViewDisplayManager(ApplicationContext applicationContext, MainHeader mainHeader) {
        this.applicationContext = applicationContext;
        this.mainHeader = mainHeader;
    }

    @Override
    public void showView(View view) {
        handleDisplay(view);
        handleViewMenu(view);
    }

    private void handleDisplay(View view) {
        ViewComponent annotation = AnnotationUtils.findAnnotation(view.getClass(), ViewComponent.class);
        if (annotation == null)
            throw new IllegalArgumentException("View must be annotated with " + ViewComponent.class);

        Display display = getDisplay(annotation.value());
        if (display == null)
            throw new IllegalArgumentException("Display with name " + annotation.value() + " must be registered");

        if (!display.isAttached()) {
            display.start();
            handleMenu(display, display);
        }

        display.show(view);
        mainHeader.getHeaderLabel().setValue(annotation.caption());

    }

    private void handleViewMenu(View view) {
        handleMenu(view, view.getViewComponent());
    }

    private void handleMenu(Object target, ClientConnector component) {
        Map<String, MenuBar.MenuItem> menus = new HashMap<>();

        Class targetClass = target.getClass();
        if (AopUtils.isAopProxy(target)) {
            targetClass = AopUtils.getTargetClass(target);
        }

        Arrays.stream(targetClass.getMethods()).filter(method -> {
            ViewMenuOption option = AnnotationUtils.findAnnotation(method, ViewMenuOption.class);
            Secured secured = AnnotationUtils.findAnnotation(method, Secured.class);
            return option != null && hasAuthority(secured);
        }).sorted((m1, m2) -> {
            ViewMenuOption option1 = AnnotationUtils.findAnnotation(m1, ViewMenuOption.class);
            ViewMenuOption option2 = AnnotationUtils.findAnnotation(m2, ViewMenuOption.class);
            return Integer.compare(option1.order(), option2.order());
        }).forEach(method -> {
            ViewMenuOption option = AnnotationUtils.findAnnotation(method, ViewMenuOption.class);
            MenuBar.MenuItem item;
            String menuCaption = getMenu(option);

            if (!menus.keySet().stream().anyMatch(s -> s.equals(menuCaption))) {
                Resource icon = option.menuIcon().equals(VaadinIcons.ESC) ? null : option.menuIcon();
                item = mainHeader.getMenuBar().addItemAsFirst(menuCaption, icon, null);
                menus.put(menuCaption, item);

                component.addDetachListener(event -> {
                    mainHeader.getMenuBar().removeItem(item);
                });

            } else item = menus.get(menuCaption);

            Resource icon = option.icon().equals(VaadinIcons.ESC) ? null : option.icon();
            if (menuCaption.equals(option.value())) {
                item.setCommand(selectedItem -> ReflectionUtils.invokeMethod(method, target));
                if (item.getIcon() == null) item.setIcon(icon);
            } else {
                MenuBar.MenuItem menuItem = item.addItem(option.value(), icon, selectedItem -> ReflectionUtils.invokeMethod(method, target));
            }
        });
    }

    private String getMenu(ViewMenuOption option) {
        if (option.menu().isEmpty()) return option.value();
        else return option.menu();
    }

    public Display getDisplay(Class<? extends Display> type) {
        return applicationContext.getBean(type);
    }
}