package com.niafikra.dimension.core.ui.vaadin.actions;

import com.niafikra.dimension.core.domain.Printable;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.MViewChangeListener;
import com.niafikra.dimension.core.ui.vaadin.util.PrintButton;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Composite;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
public class ViewActions extends Composite {

    private TableExportButton tableExportAction;
    private PrintButton printButton;

    public ViewActions(Navigator navigator) {
        navigator.addViewChangeListener(new MViewChangeListener() {
            @Override
            public void afterViewChange(ViewChangeEvent event) {
                updateActions(event.getNewView());
            }
        });
    }

    private void updateActions(View view) {
        if (isTableExportable(view)) {
            TableExportable exportable = (TableExportable) view;
            tableExportAction.setExporter(exportable.getTableExport());
            tableExportAction.setVisible(true);
        } else tableExportAction.setVisible(false);

        if (isPrintable(view)) {
            Printable printable = (Printable) view;
            printButton.setPrintable(printable);
            printButton.setVisible(true);
        } else printButton.setVisible(false);

    }

    private boolean isPrintable(View view) {
        return Printable.class.isAssignableFrom(view.getClass());
    }

    private boolean isTableExportable(View view) {
        return TableExportable.class.isAssignableFrom(view.getClass());
    }

    @PostConstruct
    private void build() {

        tableExportAction = new TableExportButton();
        tableExportAction.setWidth("100%");
        tableExportAction.addStyleNames(Theme.BUTTON_ICON_ONLY);

        printButton = new PrintButton();
        printButton.setWidth("100%");
        printButton.addStyleNames(Theme.BUTTON_ICON_ONLY);

        setCompositionRoot(new MHorizontalLayout(tableExportAction, printButton));
        setPrimaryStyleName(Theme.MENU_ROOT);
    }

}
