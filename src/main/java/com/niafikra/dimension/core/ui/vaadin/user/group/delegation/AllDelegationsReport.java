package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.core.ui.vaadin.user.group.delegation.AllDelegationsReport.VIEW_NAME;

@Secured(PMSPermission.VIEW_ALL_DELEGATIONS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "All delegations")
@ViewInfo(icon = VaadinIcons.USER_CLOCK, section = "Authentication", value = "All Delegations")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllDelegationsReport extends VerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "all-delegations";

    private DelegationsGrid delegationsGrid;

    public AllDelegationsReport(DelegationsGrid delegationsGrid) {
        this.delegationsGrid = delegationsGrid;
        setSizeFull();
        setMargin(false);
        delegationsGrid.setSizeFull();
        addComponent(delegationsGrid);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(delegationsGrid);
    }
}
