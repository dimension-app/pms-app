package com.niafikra.dimension.core.ui.vaadin.util.token;

import com.fo0.advancedtokenfield.main.Token;

public class MToken<T> extends Token {

    private T reference;

    public MToken(String value, T reference) {
        super(value);
        this.reference = reference;
    }

    public MToken(String value, T reference, String style) {
        super(value, style);
        this.reference = reference;
    }

    public T getReference() {
        return reference;
    }
}
