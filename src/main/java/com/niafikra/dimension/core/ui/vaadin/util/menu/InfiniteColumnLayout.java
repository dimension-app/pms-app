package com.niafikra.dimension.core.ui.vaadin.util.menu;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.Arrays;

/**
 * Acts like a grid but with an infinite amount of columns!
 *
 * @author mbwana on 2/2/15.
 */
public class InfiniteColumnLayout extends MHorizontalLayout {
    private int componentCount;
    private int numberOfRows;
    private VerticalLayout mostRecentColumn;
    private boolean leftWise = false;

    public InfiniteColumnLayout(int numberOfRows, boolean leftWise) {
        this.leftWise = leftWise;
        this.numberOfRows = numberOfRows != 0 ? numberOfRows : 1;

        buildUI();
    }

    public InfiniteColumnLayout(int numberOfRows) {
        this(numberOfRows, false);
    }

    private void buildUI() {
        setHeight("-1");
    }

    public void addComponent(Component component) {
        if (componentCount % numberOfRows == 0) {
            mostRecentColumn = new MVerticalLayout().withMargin(false);

            if (leftWise) super.addComponent(mostRecentColumn, 0);
            else super.addComponent(mostRecentColumn);

            setComponentAlignment(mostRecentColumn, Alignment.TOP_LEFT);
        }

        mostRecentColumn.addComponent(component);
        componentCount++;

    }

    public void addComponents(Component... components) {
        Arrays.stream(components).forEachOrdered(this::addComponent);
    }

    public boolean isLeftWise() {
        return leftWise;
    }

    public void setLeftWise(boolean leftWise) {
        this.leftWise = leftWise;
    }

    public void clear() {
        componentCount = 0;
        removeAllComponents();
    }
}
