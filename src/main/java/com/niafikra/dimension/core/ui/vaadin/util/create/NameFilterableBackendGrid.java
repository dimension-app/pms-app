package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.util.provider.StringFilterableDataProvider;
import com.niafikra.dimension.core.util.HasName;
import com.vaadin.data.HasValue;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 10:44 PM
 */
public class NameFilterableBackendGrid<T extends HasName> extends PropertyFilterableGrid<T> {
    private StringFilterableDataProvider dataProvider;

    public NameFilterableBackendGrid(StringFilterableDataProvider dataProvider) {
        super(dataProvider, "name", T::getName);
        this.dataProvider = dataProvider;
    }

    @Override
    protected void filter(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(event.getValue());
    }
}
