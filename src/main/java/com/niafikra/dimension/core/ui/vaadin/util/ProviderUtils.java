package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.data.provider.Query;

public class ProviderUtils {

    public static <T, K> K getFilter(Query<T, K> query, K filter) {
        if (query.getFilter().isPresent())
            return query.getFilter().get();
        else return filter;
    }
}
