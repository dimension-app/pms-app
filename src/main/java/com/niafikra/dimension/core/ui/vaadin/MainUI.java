package com.niafikra.dimension.core.ui.vaadin;

import com.niafikra.dimension.core.ui.vaadin.view.AccessDeniedView;
import com.niafikra.dimension.mail.MailService;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/18/17 9:30 PM
 */
@PushStateNavigation
@SpringUI(path = MainUI.PATH)
@com.vaadin.annotations.Theme(Theme.THEME_NAME)
public class MainUI extends AbstractUI {
    public static final String PATH = "/app";
    private MainScreen mainScreen;
    private SpringViewProvider viewProvider;

    public MainUI(MainScreen mainScreen, SpringViewProvider viewProvider, MailService mailService) {
        super(mailService);
        this.mainScreen = mainScreen;
        this.viewProvider = viewProvider;
        this.viewProvider.setAccessDeniedViewClass(AccessDeniedView.class);
    }

    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle("Dimension");
        setContent(mainScreen);
    }
}
