package com.niafikra.dimension.core.ui.vaadin.util.create;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.util.HasLogger;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.viritin.grid.MGrid;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 3:44 PM
 */
public abstract class GridCreateViewPanel<T> extends VerticalLayout implements HasLogger, TableExportable {

    protected Grid<T> entriesGrid;
    protected Button createButton;

    public GridCreateViewPanel(Class<T> type) {
        this(new MGrid<>(type));
    }


    public GridCreateViewPanel() {
        this(new Grid<>());
    }

    public GridCreateViewPanel(Grid<T> entriesGrid) {
        this.entriesGrid = entriesGrid;
        createButton = new Button("New");

        setSizeFull();

        createButton.setIcon(VaadinIcons.PLUS_CIRCLE);
        createButton.addStyleNames(Theme.BUTTON_PRIMARY, Theme.BUTTON_BORDERLESS, Theme.BUTTON_TINY);
        createButton.addClickListener(event -> showCreateContent());
        attachCreateButton(createButton);

        entriesGrid.setSizeFull();
        entriesGrid.setSelectionMode(Grid.SelectionMode.NONE);
        entriesGrid.addItemClickListener(event -> showViewContent(event.getItem()));
    }

    @Override
    public void attach() {
        super.attach();
        attachEntriesGrid(entriesGrid);
    }

    protected abstract void attachEntriesGrid(Grid<T> entriesGrid);

    protected void attachCreateButton(Button createButton) {
        addComponentAsFirst(createButton);
        setComponentAlignment(createButton, Alignment.BOTTOM_LEFT);
    }

    protected abstract void showCreateContent();

    protected abstract void showViewContent(T event);


    public Grid<T> getEntriesGrid() {
        return entriesGrid;
    }

    public Button getCreateButton() {
        return createButton;
    }

    public void resetView() {
        entriesGrid.getDataProvider().refreshAll();
        showCreateContent();
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(entriesGrid);
    }
}
