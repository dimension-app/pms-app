package com.niafikra.dimension.core.ui.vaadin;

import com.niafikra.dimension.PMSApp;
import com.niafikra.dimension.core.ui.vaadin.report.ReportsPanel;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingsPanel;
import com.niafikra.dimension.core.ui.vaadin.util.menu.MMenuBar;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/19/17 8:31 PM
 */
@SpringComponent
@UIScope
public class MainHeader extends MHorizontalLayout {
    private Label headerLabel;
    private MMenuBar menuBar;
    private MenuBar.MenuItem settingMenu, reportsMenu, exitMenu;

    private Navigator navigator;

    public MainHeader(Navigator navigator) {
        this.navigator = navigator;

        headerLabel = new Label();
        menuBar = new MMenuBar();
    }

    @PostConstruct
    private void build() {
        setMargin(false);
        withFullWidth();
        addComponent(headerLabel);
        setComponentAlignment(headerLabel, Alignment.BOTTOM_LEFT);
        headerLabel.addStyleNames(Theme.LABEL_COLORED, Theme.LABEL_BOLD);

        menuBar.addStyleNames(Theme.MENUBAR_BORDERLESS, Theme.MENUBAR_SMALL);
        addComponent(menuBar);
        setComponentAlignment(menuBar, Alignment.BOTTOM_RIGHT);


        reportsMenu = menuBar.addItem(
                "Reports",
                VaadinIcons.LINE_BAR_CHART,
                cmd -> navigator.navigateTo(ReportsPanel.class)
        );
        settingMenu = menuBar.addItem(
                "Settings",
                VaadinIcons.COG, cmd -> navigator.navigateTo(SettingsPanel.class));

        exitMenu = menuBar.addItem("Exit",
                VaadinIcons.POWER_OFF,
                sel -> UI.getCurrent().getPage().open(String.format("%s/logout", PMSApp.baseUrl), "")
        );
    }

    public MMenuBar getMenuBar() {
        return menuBar;
    }

    public MenuBar.MenuItem getSettingMenu() {
        return settingMenu;
    }

    public Label getHeaderLabel() {
        return headerLabel;
    }

    public void setHeading(String caption) {
        getHeaderLabel().setValue(caption);
    }
}