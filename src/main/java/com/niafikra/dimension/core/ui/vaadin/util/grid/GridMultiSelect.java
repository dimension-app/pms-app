package com.niafikra.dimension.core.ui.vaadin.util.grid;

import com.vaadin.shared.Registration;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Grid;

import java.util.Set;

public class GridMultiSelect<T> extends CustomField<Set<T>> {
    private Grid<T> grid;

    public GridMultiSelect() {
        grid = new Grid<>();
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
    }

    public Grid<T> getGrid() {
        return grid;
    }

    @Override
    protected Component initContent() {
        return grid;
    }

    @Override
    protected void doSetValue(Set<T> value) {
        if (value == null)
            grid.asMultiSelect().clear();
        else grid.asMultiSelect().setValue(value);
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<Set<T>> listener) {
        return grid.asMultiSelect().addValueChangeListener(listener);
    }

    @Override
    public Set<T> getValue() {
        return grid.asMultiSelect().getValue();
    }
}
