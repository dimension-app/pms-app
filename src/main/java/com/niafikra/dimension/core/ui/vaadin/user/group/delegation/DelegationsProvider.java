package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.niafikra.dimension.delegation.service.DelegationService;
import com.niafikra.dimension.group.domain.Group;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@PrototypeScope
@SpringComponent
public class DelegationsProvider extends PageableDataProvider<Delegation, String> {

    private DelegationService delegationService;
    private LocalDateTime startDate, endDate;
    private Group group;
    private User delegator, delegatee;
    private User creator;

    public DelegationsProvider(DelegationService delegationService) {
        this.delegationService = delegationService;
    }

    @Override
    protected Page<Delegation> fetchFromBackEnd(Query<Delegation, String> query, Pageable pageable) {
        return delegationService.findAll(group, delegator, delegatee, startDate, endDate, creator, pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Delegation, String> query) {
        return delegationService.countAll(group, delegator, delegatee, startDate, endDate, creator).intValue();
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        refreshAll();
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        refreshAll();
    }

    public void setGroup(Group group) {
        this.group = group;
        refreshAll();
    }

    public void setDelegator(User delegator) {
        this.delegator = delegator;
        refreshAll();
    }

    public void setDelegatee(User delegatee) {
        this.delegatee = delegatee;
        refreshAll();
    }

    public void setDateRange(LocalDateTime startDate, LocalDateTime endDate) {
        setStartDate(startDate);
        setEndDate(endDate);
    }

    public void setCreator(User creator) {
        this.creator = creator;
        refreshAll();
    }
}
