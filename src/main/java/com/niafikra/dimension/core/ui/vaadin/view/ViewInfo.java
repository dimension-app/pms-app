package com.niafikra.dimension.core.ui.vaadin.view;

import com.vaadin.icons.VaadinIcons;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({java.lang.annotation.ElementType.TYPE})
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Documented
public @interface ViewInfo {

    VaadinIcons icon();

    String section();

    String value();
}
