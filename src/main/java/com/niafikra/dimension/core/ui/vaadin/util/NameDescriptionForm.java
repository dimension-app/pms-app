package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.util.HasName;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.TextArea;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 11:07 AM
 */
public class NameDescriptionForm<T extends HasName> extends NameForm<T> {

    protected TextArea description = new TextArea("Description");

    public NameDescriptionForm(Class<T> type) {
        super(type);
        description.setWidth("100%");
    }

    @Override
    protected ComponentContainer createFormFields() {
        return new MVerticalLayout(name, description).withMargin(false);
    }

    public TextArea getDescriptionField() {
        return description;
    }
}
