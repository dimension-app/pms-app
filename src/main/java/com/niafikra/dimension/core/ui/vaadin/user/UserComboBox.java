package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

@Component
@PrototypeScope
public class UserComboBox extends MComboBox<User> {

    public UserComboBox(UsersProvider provider) {
        setDataProvider(provider,name -> UserService.UserFilter.builder().name(name).build());
    }

}
