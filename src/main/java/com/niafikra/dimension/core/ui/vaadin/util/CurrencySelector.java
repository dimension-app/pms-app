package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.core.context.ApplicationContextHolder;
import com.niafikra.dimension.money.GeneralExchangeService;
import com.vaadin.ui.ComboBox;
import org.vaadin.viritin.fluency.ui.FluentAbstractField;

public class CurrencySelector extends ComboBox<String> implements FluentAbstractField<CurrencySelector, String> {
    public CurrencySelector() {
        setItems(ApplicationContextHolder
                .get()
                .getBean(GeneralExchangeService.class)
                .getSupportedCurrencies().keySet()
        );
    }
}
