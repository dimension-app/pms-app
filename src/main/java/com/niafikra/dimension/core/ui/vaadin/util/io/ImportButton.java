package com.niafikra.dimension.core.ui.vaadin.util.io;


import com.niafikra.dimension.core.ui.vaadin.util.upload.UploadButton;
import lombok.Getter;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 2/25/16
 */
public class ImportButton extends UploadButton {

    @Getter
    private ImportFileHandler importFileHandler;

    public ImportButton() {
        super();
        importFileHandler = new ImportFileHandler(getUploadWindow());
    }

    public ImportProcessing getImportProcessor() {
        return importFileHandler.getImportProcessor();
    }

    public void setImportProcessor(ImportProcessing importFileProcessor) {
        this.importFileHandler.setImportProcessor(importFileProcessor);
    }

}
