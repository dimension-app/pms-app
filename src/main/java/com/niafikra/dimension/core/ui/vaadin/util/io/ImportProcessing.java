package com.niafikra.dimension.core.ui.vaadin.util.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com,bonifacechacha@gmail.com
 * @date 12/18/15
 */
public interface ImportProcessing {

    boolean importFile(InputStream importStream) throws IOException;

}
