package com.niafikra.dimension.core.ui.vaadin.view;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/12/17 11:45 AM
 */
@Retention(RUNTIME)
@Target({TYPE})
@Documented
@SpringComponent
@ViewScope
@SpringView
public @interface ViewComponent {
    Class<? extends Display> value();

    String caption();
}