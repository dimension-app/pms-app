package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import java.math.BigInteger;

public class IntegerToBigIntegerConverter implements Converter<Integer, BigInteger> {

    @Override
    public Result<BigInteger> convertToModel(Integer value, ValueContext context) {
        if (value == null) return Result.ok(null);
        return Result.ok(BigInteger.valueOf(value));
    }

    @Override
    public Integer convertToPresentation(BigInteger value, ValueContext context) {
        if (value == null) return null;
        return value.intValue();
    }
}
