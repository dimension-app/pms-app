package com.niafikra.dimension.core.ui.vaadin.util;

import com.niafikra.dimension.money.Money;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.converter.StringToBigDecimalConverter;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.fluency.ui.FluentAbstractComponent;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import java.math.BigDecimal;

public class MoneyField extends CustomField<Money> implements FluentAbstractComponent<MoneyField> {
    private BeanValidationBinder<Money> binder = new BeanValidationBinder<>(Money.class);
    private Money oldValue;
    private MTextField amount = new MTextField();
    private ComboBox<String> currency = new ComboBox<>();

    public MoneyField() {
        binder.forField(amount)
                .withValidator(new BeanValidator(Money.class, "amount"))
                .withConverter(new StringToBigDecimalConverter(BigDecimal.ZERO, ""))
                .bind(Money::getAmount, Money::setAmount);

        currency.setItems(Money.getSupportedCurrencies().keySet());
        currency.setEmptySelectionAllowed(false);
        binder.forField(currency)
                .withValidator(new BeanValidator(Money.class, "currency"))
                .bind(Money::getCurrency, Money::setCurrency);

        currency.setWidth("80px");
        amount.setWidth("100%");

        amount.addTextChangeListener(event -> fireValueChangeEvent(event.isUserOriginated()));
        currency.addValueChangeListener(event -> fireValueChangeEvent(event.isUserOriginated()));
    }

    public MoneyField(String caption) {
        this();
        setCaption(caption);
    }

    @Override
    protected Component initContent() {
        return new MHorizontalLayout(amount, currency).withExpand(amount, 1).withFullWidth();
    }

    @Override
    protected void doSetValue(Money value) {
        if (value != null) binder.readBean(value);
        else {
            amount.clear();
            currency.clear();
        }
        oldValue = value;
    }

    @Override
    public Money getValue() {
//        return binder.getBean();
        if (binder.isValid()) {
            Money value = new Money();
            binder.writeBeanIfValid(value);
            return value;
        } else return null;
    }

    private void fireValueChangeEvent(boolean userOriginated) {
        if (!binder.isValid()) return;

        fireEvent(new ValueChangeEvent<>(this, oldValue, userOriginated));
        oldValue = getValue();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        amount.setReadOnly(readOnly);
        currency.setReadOnly(readOnly);
    }

    public MTextField getAmount() {
        return amount;
    }

    public ComboBox<String> getCurrency() {
        return currency;
    }
}