package com.niafikra.dimension.core.ui.vaadin.user.group.delegation;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.delegation.domain.Delegation;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import static com.niafikra.dimension.PMSPermission.REQUEST_DELEGATION;
import static com.niafikra.dimension.PMSPermission.VIEW_USER_DELEGATIONS;
import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.core.ui.vaadin.user.group.delegation.UserDelegationsView.VIEW_NAME;

@Secured(VIEW_USER_DELEGATIONS)
@ViewComponent(value = MainDisplay.class, caption = "My Delegations")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INITIAL, caption = "My Delegations")
@VaadinFontIcon(VaadinIcons.USER_CLOCK)
public class UserDelegationsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "user-delegations";
    private MenuBar.MenuItem newDelegationMenuItem;
    private MainHeader mainHeader;
    private DelegationsGrid delegationsGrid;
    private DelegationRequestForm requestForm;
    private User currentUser;

    public UserDelegationsView(MainHeader mainHeader,
                               DelegationsGrid delegationsGrid,
                               DelegationRequestForm requestForm,
                               User currentUser) {
        this.mainHeader = mainHeader;
        this.delegationsGrid = delegationsGrid;
        this.requestForm = requestForm;
        this.currentUser = currentUser;

        setSizeFull();
        delegationsGrid.setSizeFull();
        delegationsGrid.filterByCreator(currentUser);
        addComponent(delegationsGrid);
        setMargin(false);
    }

    @Override
    public void attach() {
        super.attach();

        if (hasAuthority(REQUEST_DELEGATION)) {
            newDelegationMenuItem = mainHeader.getMenuBar().addItemAsFirst(
                    "Request Delegation",
                    VaadinIcons.PLUS,
                    selectedItem -> showRequestForm()
            );
        }
    }

    private void showRequestForm() {
        requestForm.setEntity(new Delegation(currentUser));
        requestForm.openInModalPopup()
                .addCloseListener(closeEvent -> delegationsGrid.getDataProvider().refreshAll());
    }


    @Override
    public void detach() {
        super.detach();
        if (hasAuthority(REQUEST_DELEGATION)) {
            mainHeader.getMenuBar().removeItem(newDelegationMenuItem);
        }
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(delegationsGrid);
    }
}