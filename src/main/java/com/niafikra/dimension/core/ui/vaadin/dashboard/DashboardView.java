/*
 * Copyright 2015 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.niafikra.dimension.core.ui.vaadin.dashboard;

import com.byteowls.vaadin.chartjs.ChartJs;
import com.byteowls.vaadin.chartjs.config.LineChartConfig;
import com.byteowls.vaadin.chartjs.config.PieChartConfig;
import com.byteowls.vaadin.chartjs.data.LineDataset;
import com.byteowls.vaadin.chartjs.data.PieDataset;
import com.byteowls.vaadin.chartjs.options.InteractionMode;
import com.byteowls.vaadin.chartjs.options.Position;
import com.byteowls.vaadin.chartjs.options.scale.Axis;
import com.byteowls.vaadin.chartjs.options.scale.CategoryScale;
import com.byteowls.vaadin.chartjs.options.scale.LinearScale;
import com.byteowls.vaadin.chartjs.utils.ColorUtils;
import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Charts;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAnyAuthority;
import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.core.ui.vaadin.dashboard.DashboardView.VIEW_NAME;


@ViewComponent(value = MainDisplay.class, caption = "Dashboard")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INITIAL, caption = "Home")
@VaadinFontIcon(VaadinIcons.LINE_BAR_CHART)
public class DashboardView extends Panel implements View {

    public static final String VIEW_NAME = "";

    private User currentUser;
    private AllocationService allocationService;
    private ExpenseService expenseService;
    private RequisitionService requisitionService;

    private UserBudgetSummary userBudgetSummary;

    public DashboardView(User currentUser,
                         AllocationService allocationService,
                         ExpenseService expenseService,
                         RequisitionService requisitionService,
                         UserBudgetSummary userBudgetSummary) {
        this.currentUser = currentUser;
        this.allocationService = allocationService;
        this.expenseService = expenseService;
        this.requisitionService = requisitionService;
        this.userBudgetSummary = userBudgetSummary;
    }

    @PostConstruct
    public void build() {
        //if user is has not login yet then ignore
        if (currentUser == null) return;

        this.userBudgetSummary.setUser(currentUser);

        setSizeFull();
        addStyleNames(Theme.PANEL_BORDERLESS, Theme.PANEL_SCROLL_INDICATOR);
        MVerticalLayout base = new MVerticalLayout();
        setContent(base);

        MHorizontalLayout summaryBar = new MHorizontalLayout().withFullWidth().withMargin(false);

        if (hasAuthority(PMSPermission.VIEW_MY_BUDGET_SUMMARY)) {
            Money myBudget = allocationService.calculateTotalMonthlyAllocated(currentUser);
            summaryBar.add(createSummaryPanel(myBudget.shorten(), "My Budget"));
        }

        if (hasAuthority(PMSPermission.VIEW_PREV_BUDGET_SUMMARY)) {
            Money prevBudget = allocationService.calculateTotalPrevMonthAllocated(currentUser);
            summaryBar.add(createSummaryPanel(prevBudget.shorten(), "Prev Budget"));
        }

        if (hasAuthority(PMSPermission.VIEW_MY_EXPENSES_SUMMARY)) {
            Money myExpense = expenseService.calculateTotalMonthlyExpenses(currentUser);
            summaryBar.add(createSummaryPanel(myExpense.shorten(), "My Expenses"));
        }

        if (hasAuthority(PMSPermission.VIEW_PREV_EXPENSES_SUMMARY)) {
            Money prevExpense = expenseService.calculateTotalPrevMonthExpenses(currentUser);
            summaryBar.add(createSummaryPanel(prevExpense.shorten(), "Prev Expenses"));
        }

        if (hasAuthority(PMSPermission.VIEW_PENDING_PAYMENT_SUMMARY)) {
            Money pendingPayment = requisitionService.calculateTotalPendingPayments(currentUser);
            summaryBar.add(createSummaryPanel(pendingPayment.shorten(), "Pending Payment"));
        }

        if (hasAuthority(PMSPermission.VIEW_PENDING_REQUESTS_SUMMARY)) {
            Money pendingRequisitions = requisitionService.calculateTotalPendingApprovalRequisitions(currentUser);
            summaryBar.add(createSummaryPanel(pendingRequisitions.shorten(), "Pending Requests"));
        }

        if (hasAuthority(PMSPermission.VIEW_MONTHLY_BUDGET_SUMMARY)) {
            Money monthlyBudget = allocationService.calculateTotalMonthlyAllocated();
            summaryBar.add(createSummaryPanel(monthlyBudget.shorten(), "Monthly Budget"));
        }

        if (hasAuthority(PMSPermission.VIEW_MONTHLY_EXPENDITURE_SUMMARY)) {
            Money monthlyExpenses = expenseService.calculateTotalMonthlyExpenses();
            summaryBar.add(createSummaryPanel(monthlyExpenses.shorten(), "Monthly Expenses"));
        }

        if (hasAuthority(PMSPermission.VIEW_MONTHLY_REQUISITIONS_SUMMARY)) {
            Money monthlyRequisitions = requisitionService.calculateTotalMonthlyRequisitions();
            summaryBar.add(createSummaryPanel(monthlyRequisitions.shorten(), "Monthly Requisitions"));
        }

        base.addComponent(summaryBar);


        if (hasAuthority(PMSPermission.VIEW_DAILY_EXPENDITURE_TREND_CHART)) {
            ChartJs trendChart = createUserExpenditureTrendLineChart();
            MHorizontalLayout trendChartLayout = new MHorizontalLayout(trendChart)
                    .withStyleName(Theme.LAYOUT_CARD)
                    .withHeight(350, Unit.PIXELS)
                    .withMargin(true)
                    .withFullWidth();
            base.addComponent(trendChartLayout);
        }

        if (hasAnyAuthority(PMSPermission.VIEW_MONTHLY_BALANCE_EXPENDITURE_CHART, PMSPermission.VIEW_USER_BUDGETS_SUMMARY)) {
            MHorizontalLayout budgetsLayout = new MHorizontalLayout()
                    .withHeight(300, Unit.PIXELS)
                    .withMargin(true)
                    .withFullWidth();

            if (hasAuthority(PMSPermission.VIEW_MONTHLY_BALANCE_EXPENDITURE_CHART)) {
                ChartJs balanceExpenditurePieChart = createBalanceExpenditurePieChart();
                budgetsLayout
                        .withComponent(balanceExpenditurePieChart)
                        .withExpand(balanceExpenditurePieChart, 2);
            }

            if (hasAuthority(PMSPermission.VIEW_USER_BUDGETS_SUMMARY)) {
                userBudgetSummary.setSizeFull();
                budgetsLayout
                        .withComponent(userBudgetSummary)
                        .withExpand(userBudgetSummary, 3);
            }

            base.addComponent(budgetsLayout);
        }

    }

    private ChartJs createUserExpenditureTrendLineChart() {


        LocalDateTime start = DateUtils.getStartOfThisMonth();
        LocalDateTime end = DateUtils.getEndOfThisMonth();

        List<LocalDateTime> dates = Stream.iterate(start, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(start, end))
                .collect(Collectors.toList());

        List<String> days = dates.stream()
                .map(date -> String.valueOf(date.getDayOfMonth()))
                .collect(Collectors.toList());


        LineChartConfig config = new LineChartConfig();
        config.data()
                .labelsAsList(days)
                .addDataset(
                        new LineDataset()
                                .label("Expenses")
                                .borderColor(Charts.COLOR_1)
                                .backgroundColor(Charts.COLOR_1)
                                .fill(false))
                .addDataset(
                        new LineDataset()
                                .label("Requisitions")
                                .borderColor(Charts.COLOR_2)
                                .backgroundColor(Charts.COLOR_2)
                                .fill(false))
                .and()
                .options()
                .responsive(true)
                .maintainAspectRatio(false)
                .title()
                .display(true)
                .text("Daily expenditure trend ")
                .and()
                .tooltips()
                .mode(InteractionMode.INDEX)
                .intersect(false)
                .and()
                .hover()
                .mode(InteractionMode.NEAREST)
                .intersect(true)
                .and()
                .scales()
                .add(Axis.X, new CategoryScale()
                        .display(true)
                        .scaleLabel()
                        .display(true)
                        .labelString("Days of Month")
                        .and()
                        .position(Position.BOTTOM))
                .add(Axis.Y, new LinearScale()
                        .display(true)
                        .scaleLabel()
                        .display(true)
                        .labelString("Amount")
                        .and()
                        .ticks()
                        .and()
                        .position(Position.LEFT))
                .and()
                .done();

        LineDataset expDataSet = (LineDataset) config.data().getDatasetAtIndex(0);
        List<Double> expData = dates.stream()
                .map(date -> expenseService.calculateTotalExpenditure(currentUser, date, date.plusDays(1)).toBaseCurrency().getAmount().doubleValue())
                .collect(Collectors.toList());
        expDataSet.dataAsList(expData);

        LineDataset reqDataSet = (LineDataset) config.data().getDatasetAtIndex(1);
        List<Double> reqData = dates.stream()
                .map(date -> requisitionService.calculateTotalRequisitions(currentUser, date, date.plusDays(1)).toBaseCurrency().getAmount().doubleValue())
                .collect(Collectors.toList());
        reqDataSet.dataAsList(reqData);

        ChartJs chart = new ChartJs(config);
        chart.setSizeFull();
        chart.setJsLoggingEnabled(true);


        return chart;
    }

    private ChartJs createBalanceExpenditurePieChart() {

        Money userBalance = expenseService.calculateTotalMonthlyBalance(currentUser);
        Money userExpenditure = expenseService.calculateTotalMonthlyExpenses(currentUser);

        PieChartConfig config = new PieChartConfig();
        config
                .data()
                .labels("Balance", "Expenditure")
                .addDataset(new PieDataset().label("Summary"))
                .and();


        config.
                options()
                .legend()
                .position(Position.RIGHT)
                .and()
                .responsive(true)
                .maintainAspectRatio(false)
                .title()
                .display(true)
                .text("General Balance Vs Expenditure")
                .and()
                .animation()
                .animateRotate(true)
                .and()
                .done();


        PieDataset budgetDataSet = (PieDataset) config.data().getFirstDataset();
        budgetDataSet.backgroundColor(ColorUtils.randomColor(0.7), ColorUtils.randomColor(0.7));
        budgetDataSet.data(userBalance.toBaseCurrency().getAmount().doubleValue(), userExpenditure.toBaseCurrency().getAmount().doubleValue());

        ChartJs chart = new ChartJs(config);
//        chart.setWidth(100, Unit.PERCENTAGE);
//        chart.setHeight(400,Unit.PIXELS);
        chart.setSizeFull();
        chart.setJsLoggingEnabled(true);


        return chart;
    }


    private Component createSummaryPanel(String value, String caption) {
        MLabel valueLabel = new MLabel(value)
                .withStyleName(Theme.LABEL_LIGHT, Theme.LABEL_HUGE);
        MLabel captionLabel = new MLabel(caption)
                .withStyleName(Theme.LABEL_SMALL);

        return new MVerticalLayout(valueLabel, captionLabel)
                .withFullWidth()
                .withStyleName(Theme.LAYOUT_CARD, Theme.SUMMARY_CARD)
                .withAlign(valueLabel, Alignment.BOTTOM_CENTER)
                .withAlign(captionLabel, Alignment.TOP_CENTER);
    }


}
