package com.niafikra.dimension.core.ui.vaadin.user;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 3:26 PM
 */
@PrototypeScope
@Component
public class UsersProvider extends PageableDataProvider<User, UserService.UserFilter> {

    private UserService userService;

    public UsersProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected Page<User> fetchFromBackEnd(Query<User, UserService.UserFilter> query, Pageable pageable) {
        return userService.findAll(query.getFilter().orElse(UserService.UserFilter.builder().build()), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("name", SortDirection.ASCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<User, UserService.UserFilter> query) {
        return userService.countAll(query.getFilter().orElse(UserService.UserFilter.builder().build())).intValue();
    }

}