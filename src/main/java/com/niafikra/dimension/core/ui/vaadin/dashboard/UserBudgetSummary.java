package com.niafikra.dimension.core.ui.vaadin.dashboard;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Set;

@PrototypeScope
@SpringComponent
public class UserBudgetSummary extends Grid<Budget> {
    private BudgetService budgetService;
    private ExpenseService expenseService;
    private AllocationService allocationService;
    private User user;

    public UserBudgetSummary(BudgetService budgetService, ExpenseService expenseService, AllocationService allocationService) {
        this.budgetService = budgetService;
        this.expenseService = expenseService;
        this.allocationService = allocationService;

        addColumn(budget -> budget.toString())
                .setId("budget")
                .setCaption("Budget")
                .setSortable(true)
                .setExpandRatio(1);

        addColumn(budget -> allocationService.calculateTotalRolesAllocated(budget, user, null).shorten())
                .setId("allocation")
                .setCaption("Allocated")
                .setWidth(100)
                .setSortable(true);

        addColumn(budget -> expenseService.calculateTotalRolesExpenditure(budget, user, null).shorten())
                .setId("expenditure")
                .setCaption("Expenditure")
                .setWidth(100)
                .setSortable(true);

        addColumn(budget -> expenseService.calculateTotalRolesBalance(budget, user, null).shorten())
                .setId("balance")
                .setCaption("Balance")
                .setWidth(100)
                .setSortable(true);

        setSelectionMode(SelectionMode.NONE);
    }

    public void setUser(User user) {
        this.user = user;
        Set<Budget> budgets = budgetService.findActiveAllocatedBudgets(user);
        setItems(budgets);
    }
}
