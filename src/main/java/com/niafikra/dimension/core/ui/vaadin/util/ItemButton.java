package com.niafikra.dimension.core.ui.vaadin.util;

import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.viritin.button.ConfirmButton;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 4/10/16
 */
public abstract class ItemButton extends ConfirmButton implements Notifier {

    private Object itemID;

    public ItemButton(Object itemID) {
        this(itemID, true);
    }

    public ItemButton(Object itemID, boolean confirm) {
        super();
        this.itemID = itemID;

        addStyleName(ValoTheme.BUTTON_ICON_ONLY);
        addStyleName(ValoTheme.BUTTON_BORDERLESS);
        addStyleName(ValoTheme.BUTTON_TINY);
        addClickListener(() -> {
            requestProcessItem(itemID);
        });
    }

    private void requestProcessItem(Object itemID) {
        try {
            processItem(itemID);
        } catch (Exception e) {
            showError("Failed to remove", e.getMessage());
        }
    }

    public Object getItemID() {
        return itemID;
    }

    public void setItemID(Object itemID) {
        this.itemID = itemID;
    }

    public abstract void processItem(Object itemID);
}
