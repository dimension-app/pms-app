package com.niafikra.dimension.core.ui.vaadin.util;

import org.vaadin.viritin.layouts.MHorizontalLayout;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 2:05 PM
 */
public abstract class AbstractForm<T> extends org.vaadin.viritin.form.AbstractForm<T> implements Notifier {

    private List<Consumer<T>> savedHandlers = new LinkedList<>();
    private List<Consumer<T>> deleteHandlers = new LinkedList<>();

    public AbstractForm(Class<T> entityType) {
        super(entityType);

        super.setSavedHandler(entity -> {
            savedHandlers.forEach(handler -> {
                handler.accept(entity);
            });
        });

        super.setDeleteHandler(entity -> {
            for (Consumer<T> handler : deleteHandlers) {
                handler.accept(entity);
            }
        });

        getSaveButton().setVisible(false);
        getDeleteButton().setVisible(false);
    }

    public void addSavedHandler(Consumer<T> savedHandler) {
        if (savedHandler == null) return;
        savedHandlers.add(savedHandler);
        if (!getSaveButton().isVisible())
            getSaveButton().setVisible(true);
    }

    public void addDeleteHandler(Consumer<T> deleteHandler) {
        if (deleteHandler == null) return;
        deleteHandlers.add(deleteHandler);
        if (!getDeleteButton().isVisible())
            getDeleteButton().setVisible(true);
    }

    public void removeSavedHandler(Consumer<T> savedHandler) {
        savedHandlers.remove(savedHandler);
        getSaveButton().setVisible(!savedHandlers.isEmpty());
    }

    public void removeDeleteHandler(Consumer<T> deleteHandler) {
        deleteHandlers.remove(deleteHandler);
        getDeleteButton().setVisible(!deleteHandlers.isEmpty());
    }

    @Override
    public void setSavedHandler(SavedHandler<T> savedHandler) {
        throw new UnsupportedOperationException("User add save handler");
    }

    @Override
    public void setDeleteHandler(DeleteHandler<T> deleteHandler) {
        throw new UnsupportedOperationException("User add delete handler");
    }

    @Override
    public MHorizontalLayout getToolbar() {
        return (MHorizontalLayout) super.getToolbar();
    }
}
