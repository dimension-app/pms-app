package com.niafikra.dimension.core.domain;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/3/17 5:41 PM
 */
public interface Printable {

    String getPrintURL();
}
