package com.niafikra.dimension.script.ui.vaadin;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.niafikra.dimension.script.domain.ScriptInfo;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 10:47 PM
 */
@SpringComponent
@PrototypeScope
public class ScriptBackendGrid extends NameFilterableBackendGrid<ScriptInfo> {

    public ScriptBackendGrid(ScriptProvider dataProvider) {
        super(dataProvider);
    }
}
