package com.niafikra.dimension.script.ui.vaadin;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.script.domain.ScriptInfo;
import com.niafikra.dimension.script.service.ScriptService;
import com.vaadin.annotations.PropertyId;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.vaadin.aceeditor.AceEditor;
import org.vaadin.aceeditor.AceMode;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:25 PM
 */
@SpringComponent
@PrototypeScope
public class ScriptForm extends AbstractForm<ScriptInfo> {

    //the script form require to set the size for the full full size with no scrolling
    //hence no need to extend the name form since it dont need to make the form full size
    @PropertyId("name")
    protected TextField nameField = new TextField("Name");
    private ScriptService scriptService;
    @PropertyId("code")
    private AceEditor codeEditor = new AceEditor();

    public ScriptForm(ScriptService scriptService) {
        super(ScriptInfo.class);
        this.scriptService = scriptService;

        addSavedHandler(script -> doSave(script));
        setSizeFull();
    }

    @Override
    protected Component createContent() {
        nameField.setWidth("100%");
        codeEditor.setSizeFull();
        codeEditor.setMode(AceMode.groovy);
        getSaveButton().setWidth("200px");
        getSaveButton().setIcon(FontAwesome.SAVE);
        HorizontalLayout toolbar = getToolbar();
        return new MVerticalLayout(nameField, codeEditor, toolbar)
                .withFullSize()
                .withMargin(new MarginInfo(false, true, false, true))
                .withExpandRatio(codeEditor, 1)
                .withComponentAlignment(toolbar, Alignment.MIDDLE_CENTER);
    }

    protected void doSave(ScriptInfo script) {
        try {
            script = scriptService.save(script);
            setEntity(script);

            String msg = "Saved script successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save script";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

}
