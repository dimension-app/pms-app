package com.niafikra.dimension.script.ui.vaadin;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.create.FormSplitCreateViewPanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.script.domain.ScriptInfo;
import com.niafikra.dimension.script.service.ScriptService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:28 PM
 */
@Secured({PMSPermission.CONFIGURE_SCRIPTS})
@ViewComponent(value = SettingDisplay.class, caption = "Scripts")
@ViewInfo(value = "Scripts", section = "Platform", icon = VaadinIcons.CODE)
@SpringView(name = ScriptCreateViewPanel.VIEW_NAME, ui = MainUI.class)
public class ScriptCreateViewPanel extends FormSplitCreateViewPanel<ScriptInfo> implements SettingView {
    public static final String VIEW_NAME = "scripts";

    private ScriptService scriptService;

    public ScriptCreateViewPanel(
            ScriptBackendGrid entriesGrid,
            ScriptForm scriptForm,
            ScriptService scriptService) {
        super(entriesGrid, scriptForm);
        this.scriptService = scriptService;
    }

    @PostConstruct
    protected void build() {
        setSizeFull();
        setMargin(false);
        getBaseSplitPanel().setSplitPosition(30);
    }

    @Override
    protected ScriptInfo createNewEntity() {
        return new ScriptInfo();
    }

    protected void onDelete(ScriptInfo script) {
        try {
            scriptService.delete(script);
            getEntriesGrid().getDataProvider().refreshAll();
            String msg = "Deleted script successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to script center";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    @Override
    public ScriptBackendGrid getEntriesGrid() {
        return (ScriptBackendGrid) super.getEntriesGrid();
    }

}
