package com.niafikra.dimension.script.ui.vaadin;

import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameFilterablePageableDataProvider;
import com.niafikra.dimension.script.domain.ScriptInfo;
import com.niafikra.dimension.script.service.ScriptService;
import com.niafikra.dimension.script.service.ScriptService.ScriptFilter;
import com.vaadin.data.provider.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 9:08 PM
 */
@Component
@PrototypeScope
public class ScriptProvider extends AbstractNameFilterablePageableDataProvider<ScriptInfo> {
    private ScriptService scriptService;

    public ScriptProvider(ScriptService scriptService) {
        this.scriptService = scriptService;
    }

    @Override
    protected Page<ScriptInfo> fetchFromBackEnd(Query<ScriptInfo, String> query, Pageable pageable) {
        Page<ScriptInfo> scripts = scriptService.findScripts(ScriptFilter.builder().name(getFilter(query)).build(), pageable);
        return scripts;
    }

    @Override
    protected int sizeInBackEnd(Query<ScriptInfo, String> query) {
        Long count = scriptService.countScripts(ScriptFilter.builder().name(getFilter(query)).build());
        return count.intValue();
    }

}
