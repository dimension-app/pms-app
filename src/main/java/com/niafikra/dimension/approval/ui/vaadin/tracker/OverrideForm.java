package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.domain.User;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import lombok.Data;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.Set;

@SpringComponent
@PrototypeScope
public class OverrideForm extends AbstractForm<OverrideForm.OverrideInfo> {

    public static final String FIELD_WIDTH = "100%";

    @Inject
    private ApprovalTrackerService trackerService;

    private ComboBox<User> user = new ComboBox<>("Select person");
    private TextArea comment = new TextArea("Comment");

    public OverrideForm() {
        super(OverrideInfo.class);
        user.setWidth(FIELD_WIDTH);
        comment.setWidth(FIELD_WIDTH);
    }

    public void init(Tracker tracker) {
        Set<User> approvers = trackerService.getCurrentApprovers(tracker);

        user.setItems(approvers);
        comment.clear();
        setEntity(new OverrideInfo());


        //if approver is one person then autoselect and disable
        if (approvers.size() == 1) {
            user.setValue(trackerService.getCurrentApprovers(tracker).stream().findFirst().get());
            user.setEnabled(false);
        } else user.setEnabled(true);
    }

    @Override
    protected Component createContent() {
        Component toolbar = getToolbar();
        return new MVerticalLayout(user, comment, toolbar)
                .withComponentAlignment(toolbar, Alignment.MIDDLE_CENTER)
                .withMargin(true);
    }

    public Window openInModalPopup(String caption) {
        Window window = super.openInModalPopup();
        window.setCaption(caption);
        window.setWidth("600px");
        return window;
    }

    @Data
    public class OverrideInfo {
        @NotNull
        private User user;
        private String comment;
    }
}
