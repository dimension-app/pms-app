package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 7:22 PM
 */
@SpringComponent
@PrototypeScope
public class StrategyBackendGrid extends NameFilterableBackendGrid<Strategy> {

    public StrategyBackendGrid(StrategyProvider dataProvider) {
        super(dataProvider);
        addColumn(t -> t.getLevels().size())
                .setId("levelsCount")
                .setCaption("Levels");
    }
}
