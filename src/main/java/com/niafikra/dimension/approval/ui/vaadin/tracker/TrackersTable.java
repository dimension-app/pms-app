package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.service.ApprovalTrackerService.TrackerFilter;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.user.UserComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.BooleanOptionGroup;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.renderers.HtmlRenderer;
import de.steinwedel.messagebox.MessageBox;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.vaadin.ui.themes.ValoTheme.MENUBAR_BORDERLESS;
import static com.vaadin.ui.themes.ValoTheme.MENUBAR_SMALL;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/21/17 2:16 PM
 */

@SpringComponent
@ViewScope
public class TrackersTable extends Grid<Tracker> implements Notifier {

    private TextField typeFilterField, descriptionFilterField;
    private BooleanOptionGroup approvalStatusFilter;
    private LocalDateTimeRangeSelector timeCreatedRangePanel;
    private TextArea approvalNotesArea = new TextArea("Notes");

    @Inject
    private Navigator navigator;
    @Inject
    private TrackerInfoPanel trackerInfoPanel;
    @Inject
    private User currentUser;
    @Inject
    private ApprovalTrackerService trackerService;

    @Inject
    private OverrideForm overrideForm;
    @Inject
    private UserComboBox creatorFilter;

    private TrackerFilter filter;

    public TrackersTable(TrackerProvider trackerProvider) {
        super(trackerProvider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {
        addColumn(tracker -> DateUtils.formatDateTime(tracker.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Created")
                .setWidth(170);

        addColumn(Tracker::getType)
                .setId("type")
                .setCaption("Type")
                .setWidth(120);

        addColumn(Tracker::getCreator)
                .setId("creator")
                .setCaption("By")
                .setWidth(180);

        addColumn(tracker -> tracker.getDescription(), new HtmlRenderer())
                .setId("description")
                .setCaption("Description").setMinimumWidth(350).setExpandRatio(1);

        addColumn(Tracker::getStatus)
                .setId("status")
                .setCaption("Status")
                .setWidth(120);


        addComponentColumn(tracker -> {

            MenuBar menu = new MenuBar();
            menu.addStyleNames(MENUBAR_BORDERLESS, MENUBAR_SMALL);
            MenuBar.MenuItem optionsMenu = menu.addItem("", null);

            if (hasAuthority(PMSPermission.VIEW_APPROVAL_REQUEST_INFO))
                optionsMenu.addItem("Info", VaadinIcons.INFO_CIRCLE_O, selectedItem -> showInfo(tracker));

            if (trackerService.hasApprovalAuthority(tracker, currentUser))
                optionsMenu.addItem("Approve", VaadinIcons.CHECK_CIRCLE_O, selectedItem -> doApprove(tracker));

            if (trackerService.hasDeclineAuthority(tracker, currentUser))
                optionsMenu.addItem("Decline", VaadinIcons.CLOSE_CIRCLE_O, selectedItem -> doDecline(tracker));


            if (trackerService.hasApprovalOverrideAuthority(tracker, currentUser))
                optionsMenu.addItem("Override Approve", VaadinIcons.CHECK_CIRCLE_O, selectedItem -> doOverrideApprove(tracker));

            if (trackerService.hasDeclineOverrideAuthority(tracker, currentUser))
                optionsMenu.addItem("Override Decline", VaadinIcons.CLOSE_CIRCLE_O, selectedItem -> doOverrideDecline(tracker));


            return menu;
        }).setId("actions").setWidth(70);


        filter = TrackerFilter.builder().build();
        getDataProvider().setFilter(filter);

        HeaderRow headerRow = appendHeaderRow();

        timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");
        headerRow.getCell("timeCreated").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            filter.setTimeCreatedRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
            getDataProvider().refreshAll();
        });
        typeFilterField = VaadinUtils.createFilteringTextField(
                headerRow,
                "type",
                "Filter type",
                event -> {
                    filter.setTypeFilter(event.getValue());
                    getDataProvider().refreshAll();
                });

        descriptionFilterField = VaadinUtils.createFilteringTextField(
                headerRow,
                "description",
                "Filter description",
                event -> {
                    filter.setDescriptionFilter(event.getValue());
                    getDataProvider().refreshAll();
                });


        headerRow.getCell("creator")
                .setComponent(creatorFilter);
        creatorFilter.addStyleNames(Theme.COMBOBOX_TINY);
        creatorFilter.addValueChangeListener(event -> {
            filter.setCreator(event.getValue());
            getDataProvider().refreshAll();
        });


        approvalStatusFilter = new BooleanOptionGroup(null, "AP", "DC", "AL");
        approvalStatusFilter.addStyleNames(Theme.OPTIONGROUP_SMALL, Theme.TEXT_SMALL);
        approvalStatusFilter.addValueChangeListener(event -> {
            filter.setApprovedFilter(approvalStatusFilter.getBooleanValue());
            getDataProvider().refreshAll();
        });
        headerRow.getCell("status").setComponent(approvalStatusFilter);

        //TODO RETURN BELWO STATEMENT WHEN SWITCH TO VAADIN 8.2+
        //setBodyRowHeight(45);

        setSelectionMode(SelectionMode.SINGLE);
        addSelectionListener(event -> {
            Optional<Tracker> tracker = event.getFirstSelectedItem();
            if (tracker.isPresent()) {
                navigator.navigateTo(Tracker.createNavigationPath(tracker.get()));
            }
        });

        approvalNotesArea.setWidth("400px");
        approvalNotesArea.setRows(4);
    }

    private void doOverrideApprove(Tracker currentTracker) {

        overrideForm.init(currentTracker);
        overrideForm.openInModalPopup("Are you sure you want to override and approve this request?");
        overrideForm.setSavedHandler(override -> {
            try {
                trackerService.overrideApproval(currentTracker, currentUser, override.getUser(), true, override.getComment());
                getDataProvider().refreshAll();
                showSuccess("Successfully overrode approval of " + currentTracker.getType(), currentTracker.toString());
                overrideForm.closePopup();
            } catch (Exception e) {
                showError("Failed to overrode approval of " + currentTracker.getType(), e);
            }
        });
    }


    private void doOverrideDecline(Tracker currentTracker) {
        overrideForm.init(currentTracker);
        overrideForm.openInModalPopup("Are you sure you want to override and decline this request?");
        overrideForm.setSavedHandler(override -> {
            try {
                trackerService.overrideApproval(currentTracker, currentUser, override.getUser(), false, override.getComment());
                getDataProvider().refreshAll();
                showSuccess("Successfully overrode decline approval of " + currentTracker.getType(), currentTracker.toString());
                overrideForm.closePopup();
            } catch (Exception e) {
                showError("Failed to overrode decline approval for " + currentTracker.getType(), e);
            }
        });
    }

    private void doDecline(Tracker tracker) {
        approvalNotesArea.clear();
        MessageBox.createQuestion()
                .withCaption("Are you sure that you decline this?")
                .withMessage(approvalNotesArea)
                .withOkButton(() -> {
                    try {
                        trackerService.processApproval(tracker, currentUser, false, approvalNotesArea.getValue());
                        getDataProvider().refreshAll();
                        showSuccess("Successfully declined approval of " + tracker.getType(), tracker.toString());
                    } catch (Exception e) {
                        showError("Failed to decline approval for " + tracker.getType(), e);
                    }
                })
                .withCancelButton()
                .open();
    }

    private void doApprove(Tracker tracker) {
        approvalNotesArea.clear();
        MessageBox.createQuestion()
                .withCaption("Are you sure that you approve this?")
                .withMessage(approvalNotesArea)
                .withOkButton(() -> {
                    try {
                        trackerService.processApproval(tracker, currentUser, true, approvalNotesArea.getValue());
                        getDataProvider().refreshAll();
                        showSuccess("Successfully approved " + tracker.getType(), tracker.toString());
                    } catch (Exception e) {
                        showError("Failed to approved " + tracker.getType(), e);
                    }
                })
                .withCancelButton()
                .open();
    }

    public ConfigurableFilterDataProvider<Tracker, Void, TrackerFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Tracker, Void, TrackerFilter>) super.getDataProvider();
    }

    private void showInfo(Tracker tracker) {
        trackerInfoPanel.show(tracker);
    }

    public TextField getTypeFilterField() {
        return typeFilterField;
    }

    public TextField getDescriptionFilterField() {
        return descriptionFilterField;
    }

    public ComboBox<User> getCreatorFilterSelector() {
        return creatorFilter;
    }

    public BooleanOptionGroup getApprovalStatusFilter() {
        return approvalStatusFilter;
    }

    public TrackerFilter getFilter() {
        return filter;
    }
}
