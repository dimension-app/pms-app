package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.approval.service.StrategyService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 12:39 AM
 */
@SpringComponent
@PrototypeScope
public class StrategyViewPanel extends VerticalLayout {

    private StrategyForm strategyForm;
    private StrategyLevelsManagementPanel levelsManagementPanel;
    private StrategyService strategyService;
    private TabSheet tabs;

    private StrategyCreateViewPanel createViewPanel;

    public StrategyViewPanel(StrategyForm strategyForm,
                             StrategyLevelsManagementPanel levelsManagementPanel,
                             StrategyService strategyService) {
        this.strategyForm = strategyForm;
        this.levelsManagementPanel = levelsManagementPanel;
        this.strategyService = strategyService;

        tabs = new TabSheet();

        build();
    }

    private void build() {
        setSizeFull();
        addComponent(tabs);

        tabs.setSizeFull();
        tabs.addTab(strategyForm, "Basics");
        strategyForm.addSavedHandler(strategy -> {
            strategy = strategyService.getStrategy(strategy.getId());
            setStrategy(strategy);
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();
        });

        levelsManagementPanel.setSizeFull();
        tabs.addTab(levelsManagementPanel, "Levels");
        ;
    }

    public void setStrategy(Strategy strategy) {
        strategyForm.setEntity(strategy);
        levelsManagementPanel.setStrategy(strategy);
    }

    public void setCreateViewPanel(StrategyCreateViewPanel createViewPanel) {
        this.createViewPanel = createViewPanel;
        levelsManagementPanel.setCreateViewPanel(createViewPanel);
    }
}
