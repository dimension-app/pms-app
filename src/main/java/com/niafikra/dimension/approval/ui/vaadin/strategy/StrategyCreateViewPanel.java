package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.approval.service.StrategyService;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.create.SplitCreateViewPanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 8:38 PM
 */

@Secured({PMSPermission.CONFIGURE_APPROVAL_STRATEGIES})
@ViewComponent(value = SettingDisplay.class, caption = "Strategies")
@ViewInfo(value = "Strategies", section = "Approval", icon = VaadinIcons.KEY_O)
@SpringView(name = StrategyCreateViewPanel.VIEW_NAME, ui = MainUI.class)
public class StrategyCreateViewPanel extends SplitCreateViewPanel<Strategy> implements SettingView {

    public static final String VIEW_NAME = "strategies";

    private StrategyService strategyService;
    private StrategyForm createForm;
    private StrategyViewPanel strategyViewPanel;

    public StrategyCreateViewPanel(StrategyBackendGrid strategysGrid,
                                   StrategyForm strategyForm,
                                   StrategyViewPanel strategyViewPanel,
                                   StrategyService strategyService) {
        super(strategysGrid);
        this.createForm = strategyForm;
        this.strategyViewPanel = strategyViewPanel;
        this.strategyService = strategyService;

        VaadinUtils.addRemoveButton(getEntriesGrid(), this::onDelete);
    }

    @PostConstruct
    protected void build() {
        setMargin(false);
        createForm.addSavedHandler(strategy -> resetView());
    }

    @Override
    protected Component getCreateComponent() {
        createForm.setEntity(new Strategy());
        return createForm;
    }


    @Override
    protected Component getViewComponent(Strategy strategy) {
        strategyViewPanel.setStrategy(strategy);
        strategyViewPanel.setCreateViewPanel(this);
        return strategyViewPanel;
    }


    protected void onDelete(Strategy strategy) {
        try {
            strategyService.delete(strategy);
            getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Deleted strategy successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to delete strategy";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

}