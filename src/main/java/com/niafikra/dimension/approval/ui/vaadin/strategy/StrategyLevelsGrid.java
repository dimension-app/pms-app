package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.core.ui.vaadin.user.group.GroupListGrid;
import com.niafikra.dimension.group.domain.Group;

import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 5:26 PM
 */
public class StrategyLevelsGrid extends GroupListGrid {
    private Strategy strategy;

    public StrategyLevelsGrid() {
        super();
        addColumn(level -> this.strategy.getLevels().indexOf(level) + 1)
                .setId("stage")
                .setCaption("Stage")
                .setSortable(false)
                .setWidth(100);

        getColumn("name").setCaption("Level");
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
        setItems(strategy.getLevels());
    }

    public List<Group> getLevels() {
        return (List<Group>) getItems();
    }
}
