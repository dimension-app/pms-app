package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.service.ApprovalTrackerService.TrackerFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/21/17 2:18 PM
 */

@Component
@PrototypeScope
public class TrackerProvider extends PageableDataProvider<Tracker, TrackerFilter> {
    private ApprovalTrackerService trackerService;

    public TrackerProvider(ApprovalTrackerService trackerService) {
        this.trackerService = trackerService;
    }

    @Override
    protected Page<Tracker> fetchFromBackEnd(Query<Tracker,TrackerFilter> query, Pageable pageable) {
        return trackerService.getTrackers(query.getFilter().orElse(TrackerFilter.builder().build()),pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("timeCreated", SortDirection.DESCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Tracker, TrackerFilter> query) {
        Long count = trackerService.countTrackers(query.getFilter().orElse(TrackerFilter.builder().build()));
        return count.intValue();
    }

}
