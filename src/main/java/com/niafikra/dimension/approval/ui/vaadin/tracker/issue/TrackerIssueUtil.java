package com.niafikra.dimension.approval.ui.vaadin.tracker.issue;

import com.niafikra.dimension.approval.domain.Tracker;

public class TrackerIssueUtil {
    public static String getReference(Tracker tracker) {
        return String.format("%s:%d", Tracker.class.getCanonicalName(), tracker.getId());
    }
}
