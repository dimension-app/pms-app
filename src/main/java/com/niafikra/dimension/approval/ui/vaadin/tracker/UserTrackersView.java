package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.layouts.MVerticalLayout;

import static com.niafikra.dimension.PMSPermission.VIEW_USER_APPROVAL_REQUESTS;
import static com.niafikra.dimension.approval.ui.vaadin.tracker.UserTrackersView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/21/17 3:34 PM
 */
@Secured(VIEW_USER_APPROVAL_REQUESTS)
@ViewComponent(value = MainDisplay.class, caption = "My requests")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.APPROVAL, caption = "My Requests")
@VaadinFontIcon(VaadinIcons.ACCESSIBILITY)
public class UserTrackersView extends MVerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "myApprovals";

    private TrackersTable trackersTable;

    public UserTrackersView(TrackersTable trackersTable, UserService userService) {
        this.trackersTable = trackersTable;
        trackersTable.getFilter().setCreator(SecurityUtils.getCurrentUser(userService));
        trackersTable.getCreatorFilterSelector().setVisible(false);
        setSizeFull();
        setMargin(false);
        trackersTable.setSizeFull();
        withComponent(trackersTable).withExpandRatio(trackersTable, 1);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(trackersTable);
    }
}
