package com.niafikra.dimension.approval.ui.vaadin.tracker.issue;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.service.IssueService;
import com.niafikra.dimension.issue.ui.vaadin.IssueView;
import com.niafikra.dimension.issue.ui.vaadin.IssuesGrid;
import com.niafikra.dimension.issue.ui.vaadin.IssuesView;
import com.niafikra.dimension.issue.ui.vaadin.ResolutionForm;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

import java.util.List;

@UIScope
@SpringComponent
public class TrackerIssuesView extends IssuesView {

    private Tracker tracker;
    private ApprovalTrackerService trackerService;
    private IssueService issueService;

    public TrackerIssuesView(IssuesGrid issuesGrid,
                             IssueView issueView,
                             TrackerIssueForm issueForm,
                             ResolutionForm resolutionForm,
                             IssueService issueService,
                             ApprovalTrackerService trackerService,
                             User user) {
        super(issuesGrid, issueView, issueForm, resolutionForm, issueService, user);
        this.trackerService = trackerService;
        this.issueService = issueService;
    }

    @Override
    protected void load() {
        tracker = trackerService.getTracker(tracker.getId());
        super.load();
    }

    @Override
    protected List<Issue> getIssues() {
        return issueService.getIssues(TrackerIssueUtil.getReference(tracker));
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
        getIssueForm().setTracker(tracker);
        load();
    }


    @Override
    public TrackerIssueForm getIssueForm() {
        return (TrackerIssueForm) super.getIssueForm();
    }
}
