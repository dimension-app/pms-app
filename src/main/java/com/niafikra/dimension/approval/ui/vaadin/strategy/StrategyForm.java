package com.niafikra.dimension.approval.ui.vaadin.strategy;

import com.niafikra.dimension.approval.domain.Strategy;
import com.niafikra.dimension.approval.service.StrategyService;
import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 8:48 PM
 */
@Component
@PrototypeScope
public class StrategyForm extends NameDescriptionForm<Strategy> {

    private StrategyService strategyService;

    public StrategyForm(StrategyService strategyService) {
        super(Strategy.class);
        this.strategyService = strategyService;

        addSavedHandler(strategy -> doSave(strategy));
    }

    private void doSave(Strategy strategy) {

        try {
            strategy = strategyService.save(strategy);
            setEntity(strategy);
            String msg = "Saved strategy successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save strategy";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }
}
