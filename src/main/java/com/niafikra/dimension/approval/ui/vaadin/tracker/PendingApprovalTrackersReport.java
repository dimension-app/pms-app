package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.components.grid.HeaderRow;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.approval.ui.vaadin.tracker.PendingApprovalTrackersReport.VIEW_NAME;


@Secured(PMSPermission.VIEW_PENDING_REQUESTS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "Pending Requests")
@ViewInfo(icon = VaadinIcons.HANDS_UP, section = "Approval", value = "Pending Requests")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PendingApprovalTrackersReport extends MVerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "pending-approvals-report";

    @Inject
    private TrackersTable trackersTable;
    @Inject
    private UsersProvider approversProvider;
    @Inject
    private ApprovalTrackerService trackerService;

    @PostConstruct
    private void build() {
        trackersTable.addColumn(tracker -> trackerService.getCurrentApprovers(tracker))
                .setCaption("Approvers")
                .setId("approvers")
                .setSortable(false);
        trackersTable.setColumnOrder("timeCreated", "type", "creator", "description", "approvers", "actions");
        trackersTable.getColumn("status").setHidden(true);

        trackersTable.getFilter().setPendingFilter(true);
        trackersTable.getApprovalStatusFilter().setVisible(false);
        setSizeFull();
        setMargin(false);
        trackersTable.setSizeFull();
        withComponent(trackersTable).withExpandRatio(trackersTable, 1);

        HeaderRow headerRow = trackersTable.getHeaderRow(1);
        VaadinUtils.createFilteringComboBox(
                headerRow,
                "approvers",
                "Filter approver",
                approversProvider.withConvertedFilter(name -> UserService.UserFilter.builder().name(name).build()),
                event -> trackersTable.getFilter().setApprover(event.getValue())
        );
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(trackersTable);
    }
}
