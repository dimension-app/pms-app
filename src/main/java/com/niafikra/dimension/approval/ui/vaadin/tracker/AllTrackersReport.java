package com.niafikra.dimension.approval.ui.vaadin.tracker;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

import static com.niafikra.dimension.approval.ui.vaadin.tracker.AllTrackersReport.VIEW_NAME;


@Secured(PMSPermission.VIEW_ALL_APPROVAL_REQUESTS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "All Requests")
@ViewInfo(icon = VaadinIcons.TASKS, section = "Approval", value = "All Requests")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllTrackersReport extends MVerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "all-approvals-report";

    private TrackersTable trackersTable;

    public AllTrackersReport(TrackersTable trackersTable) {
        this.trackersTable = trackersTable;
        setSizeFull();
        setMargin(false);
        trackersTable.setSizeFull();
        withComponent(trackersTable).withExpandRatio(trackersTable, 1);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(trackersTable);
    }
}
