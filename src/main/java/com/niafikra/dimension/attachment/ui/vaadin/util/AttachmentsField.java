package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fluency.ui.FluentCustomField;

import java.util.Collection;

@PrototypeScope
@SpringComponent
public class AttachmentsField<T extends Collection<Attachment>>
        extends CustomField<T>
        implements FluentCustomField<AttachmentsField<T>, T> {

    private AttachmentsPanel attachmentPanel;
    private T oldValue;

    public AttachmentsField(AttachmentsPanel attachmentPanel) {
        this.attachmentPanel = attachmentPanel;
        this.attachmentPanel.setMargin(false);

        this.attachmentPanel
                .getMultiFileUpload()
                .setAllUploadFinishedHandler(() -> fireValueChangeEvent(true));

        this.attachmentPanel
                .setOnRemoveCallback(attachment -> fireValueChangeEvent(true));
    }

    private void fireValueChangeEvent(boolean userOriginated) {
        fireEvent(new ValueChangeEvent<T>(this, oldValue, userOriginated));
        oldValue = getValue();
    }

    @Override
    protected Component initContent() {
        return attachmentPanel;
    }

    @Override
    protected void doSetValue(T attachments) {
        oldValue = getValue();

        if (attachments != null)
            attachmentPanel.getAttachmentTable().setItems(attachments);
    }

    @Override
    public T getValue() {
        return (T) attachmentPanel.getAttachmentTable().getDataProvider().getItems();
    }

    public AttachmentsPanel getAttachmentPanel() {
        return attachmentPanel;
    }
}
