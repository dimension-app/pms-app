package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.service.AttachmentService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.StreamResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Image;
import lombok.RequiredArgsConstructor;
import org.vaadin.spring.annotation.PrototypeScope;

import java.io.IOException;
import java.io.InputStream;


@PrototypeScope
@SpringComponent
@RequiredArgsConstructor
public class AttachmentPicture extends Image {

    private final AttachmentService attachmentService;
    private Attachment attachment;

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;

        if (attachment == null) setSource(VaadinIcons.USER);
        else {
            try {
                InputStream attachmentStream = attachmentService.readAttachmentStream(attachment.getId());
                setSource(new StreamResource(() -> attachmentStream, String.valueOf(attachment.getId())));
            } catch (IOException e) {
                setSource(VaadinIcons.USER);
            }
        }

        markAsDirty();
    }

}
