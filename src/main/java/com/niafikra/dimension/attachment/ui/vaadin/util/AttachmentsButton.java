package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.MButton;

import java.util.Collection;

@PrototypeScope
@SpringComponent
public class AttachmentsButton<T extends Collection<Attachment>> extends MButton {
    private AttachmentsField<T> attachmentsField;

    public AttachmentsButton(AttachmentsField<T> attachmentsField) {
        this.attachmentsField = attachmentsField;

        attachmentsField.getAttachmentPanel().setMargin(true);

        addClickListener(() -> {
            new SubWindow(attachmentsField)
                    .withCaption("Add attachments")
                    .withCloseListener(closeEvent -> {
                        int attachmentCount = attachmentsField.getValue().size();
                        setCaption(attachmentCount == 0 ? "Attach" : String.valueOf(attachmentCount));
                    })
                    .show()
                    .withCenter()
                    .withWidth("400px");
        });
    }

    public AttachmentsField<T> getAttachmentsField() {
        return attachmentsField;
    }
}
