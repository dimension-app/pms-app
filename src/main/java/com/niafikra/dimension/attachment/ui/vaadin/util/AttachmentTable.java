package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.HashSet;

@PrototypeScope
@SpringComponent
public class AttachmentTable extends Grid<Attachment> {
    public AttachmentTable() {

        setItems(new HashSet<>());
        addColumn(attachment -> attachment.getName())
                .setId("name")
                .setSortable(true)
                .setExpandRatio(1);

        addItemClickListener(event -> {
            Attachment attachment = event.getItem();
            showAttachment(attachment);
        });

        setHeaderVisible(false);
        setHeightByRows(4);
    }

    private void showAttachment(Attachment attachment) {

    }

    public void addAttachment(Attachment attachment) {
        getDataProvider().getItems().add(attachment);
        getDataProvider().refreshAll();
    }

    @Override
    public ListDataProvider<Attachment> getDataProvider() {
        return (ListDataProvider<Attachment>) super.getDataProvider();
    }

    public void removeAttachment(Attachment attachment) {
        getDataProvider().getItems().remove(attachment);
        getDataProvider().refreshAll();
    }
}
