package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class AttachmentPictureField extends CustomField<Attachment> {


    @Inject
    private AttachmentPicture picture;

    @Inject
    private AttachmentUpload upload;

    @PostConstruct
    private void build() {
        picture.setWidth("150px");
        picture.setHeight("150px");

        addStyleNames(Theme.PICTURE_FIELD);
        upload.setUploadButtonCaptions("Upload picture", null);
        upload.setOnFileUploadFinishedListener(attachment -> {
            Attachment old = picture.getAttachment();
            picture.setAttachment(attachment);
            fireEvent(new ValueChangeEvent<>(this, old, true));
        });
    }

    @Override
    protected Component initContent() {
        return new MVerticalLayout(picture, upload).alignAll(Alignment.MIDDLE_CENTER).withMargin(false);
    }

    @Override
    protected void doSetValue(Attachment value) {
        picture.setAttachment(value);
    }

    @Override
    public Attachment getValue() {
        return picture.getAttachment();
    }
}
