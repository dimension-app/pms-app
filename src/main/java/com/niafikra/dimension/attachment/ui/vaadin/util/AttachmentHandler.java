package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.service.AttachmentService;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.vaadin.spring.annotation.SpringComponent;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadFinishedHandler;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

@PrototypeScope
@SpringComponent
public class AttachmentHandler implements UploadFinishedHandler, Notifier {

    @Inject
    private AttachmentService attachmentService;

    private Consumer<Attachment> onFileUploadFinished;

    @Override
    public void handleFile(InputStream inputStream, String fileName, String mimeType, long length, int filesLeftInQueue) {
        try {
            Attachment attachment = attachmentService.storeFile(inputStream,fileName, mimeType, length);
            onFileUploadFinished.accept(attachment);
        } catch (IOException e) {
            showError("Failed to upload attachments", e);
        }
    }

    public void setOnFileUploadFinishedListener(Consumer<Attachment> onFileUploadFinished) {
        this.onFileUploadFinished = onFileUploadFinished;
    }
}
