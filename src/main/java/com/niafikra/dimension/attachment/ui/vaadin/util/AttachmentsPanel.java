package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.MultiFileUpload;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadStateWindow;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.function.Consumer;

@PrototypeScope
@SpringComponent
public class AttachmentsPanel extends MVerticalLayout {
    private AttachmentTable attachmentTable;
    private MultiFileUpload multiFileUpload;
    private UploadStateWindow uploadStateWindow;
    private Consumer<Attachment> onRemoveCallback;

    public AttachmentsPanel(AttachmentTable attachmentTable, AttachmentHandler handler) {
        this.attachmentTable = attachmentTable;

        uploadStateWindow = new UploadStateWindow();
        handler.setOnFileUploadFinishedListener(attachment -> getAttachmentTable().addAttachment(attachment));
        multiFileUpload = new MultiFileUpload(handler, uploadStateWindow);
        multiFileUpload.setUploadButtonIcon(VaadinIcons.UPLOAD);
        multiFileUpload.setUploadButtonCaptions(null, "Upload attachments");
        multiFileUpload.setWidth("200px");

        attachmentTable.setWidth("100%");
        addComponents(attachmentTable, multiFileUpload);
        setExpandRatio(attachmentTable, 1);
        setComponentAlignment(multiFileUpload, Alignment.MIDDLE_CENTER);

        VaadinUtils.addRemoveButton(attachmentTable, attachment -> removeAttachment(attachment));
    }

    private void removeAttachment(Attachment attachment) {

        //remove document from the ftp server
        attachmentTable.removeAttachment(attachment);
        if (onRemoveCallback != null) onRemoveCallback.accept(attachment);
    }

    public AttachmentTable getAttachmentTable() {
        return attachmentTable;
    }

    public void setOnRemoveCallback(Consumer<Attachment> onRemoveCallback) {
        this.onRemoveCallback = onRemoveCallback;
    }

    public MultiFileUpload getMultiFileUpload() {
        return multiFileUpload;
    }

}
