package com.niafikra.dimension.attachment.ui.vaadin.util;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.FileDetailBean;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.MultiFileUpload;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadStateWindow;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.function.Consumer;

@PrototypeScope
@SpringComponent
public class AttachmentUpload extends MultiFileUpload {

    private AttachmentHandler attachmentHandler;

    public AttachmentUpload(AttachmentHandler attachmentHandler) {
        super(attachmentHandler, new UploadStateWindow(), false);
        this.attachmentHandler = attachmentHandler;

        setUploadButtonIcon(VaadinIcons.CLOUD_UPLOAD);
        setUploadButtonCaptions("Upload attachment", "Upload attachments");


        //remove the recent file from the que afrter approve to allow re-uploading the file
        setAllUploadFinishedHandler(() -> {
            FileDetailBean recentFile = getUploadStatePanel().getCurrentUploadingLayout().getFileDetailBean();
            getUploadStatePanel().removeFromQueue(recentFile);
        });
    }

    public AttachmentHandler getAttachmentHandler() {
        return attachmentHandler;
    }

    public void setOnFileUploadFinishedListener(Consumer<Attachment> onFileUploadFinished) {
        this.attachmentHandler.setOnFileUploadFinishedListener(onFileUploadFinished);
    }

}
