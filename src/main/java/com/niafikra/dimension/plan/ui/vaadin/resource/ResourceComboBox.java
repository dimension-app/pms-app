package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.service.ResourceService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class ResourceComboBox extends MComboBox<Resource> {

    public ResourceComboBox(ResourceProvider provider) {
        setDataProvider(provider.withConvertedFilter(nameFilter -> ResourceService.ResourceFilter.builder().name(nameFilter).build()));
    }
}
