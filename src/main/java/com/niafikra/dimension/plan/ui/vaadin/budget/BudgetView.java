package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;

public interface BudgetView extends View {

    default void enter(ViewChangeListener.ViewChangeEvent event) {
        Long budgetId = Long.parseLong(event.getParameters());
        show(budgetId);
    }

    void show(Long budgetId);

}
