package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameCategoryFilterablePageableDataProvider;
import com.niafikra.dimension.plan.domain.Consumer;
import com.niafikra.dimension.plan.service.ConsumerService;
import com.vaadin.data.provider.Query;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 9:54 AM
 */
@SpringComponent
@PrototypeScope
public class ConsumerProvider extends AbstractNameCategoryFilterablePageableDataProvider<Consumer> {

    private ConsumerService consumerService;
    private String codeFilter;

    public ConsumerProvider(ConsumerService consumerService) {
        this.consumerService = consumerService;
    }

    @Override
    protected Page<Consumer> fetchFromBackEnd(Query<Consumer, String> query, Pageable pageable) {
        return consumerService.findConsumers(codeFilter, getFilter(query), categoryFilter, pageable);
    }

    @Override
    protected int sizeInBackEnd(Query<Consumer, String> query) {
        return consumerService.countConsumers(codeFilter, getFilter(query), categoryFilter).intValue();
    }

    public void setCodeFilter(String codeFilter) {
        this.codeFilter = codeFilter;
        refreshAll();
    }
}
