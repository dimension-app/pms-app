package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameFilterablePageableDataProvider;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.vaadin.data.provider.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 9:08 PM
 */
@Component
@PrototypeScope
public class CostCenterProvider extends AbstractNameFilterablePageableDataProvider<CostCenter> {
    private CostCenterService costCenterService;

    public CostCenterProvider(CostCenterService costCenterService) {
        this.costCenterService = costCenterService;
    }

    @Override
    protected Page<CostCenter> fetchFromBackEnd(Query<CostCenter, String> query, Pageable pageable) {
        Page<CostCenter> costCenters = costCenterService.findCostCenters(getFilter(query), pageable);
        return costCenters;
    }

    @Override
    protected int sizeInBackEnd(Query<CostCenter, String> query) {
        Long count = costCenterService.countCostCenters(getFilter(query));
        return count.intValue();
    }

}
