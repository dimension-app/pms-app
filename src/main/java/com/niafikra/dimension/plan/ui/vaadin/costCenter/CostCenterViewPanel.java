package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.util.HasLogger;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.niafikra.dimension.plan.ui.vaadin.resource.ResourceListGrid;
import com.niafikra.dimension.plan.ui.vaadin.role.RoleListGrid;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Set;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:47 PM
 */
@Component
@PrototypeScope
public class CostCenterViewPanel extends VerticalLayout implements HasLogger {

    private TabSheet tabs;
    private CostCenterCreateViewPanel createViewPanel;

    private CostCenterForm costCenterForm;
    private RoleListGrid rolesGrid;
    private ResourceListGrid resourcesGrid;
    private CostCenterService costCenterService;
    private CostCenterUsersManagementPanel usersPanel;

    private CostCenter costCenter;

    public CostCenterViewPanel(
            CostCenterForm costCenterForm,
            CostCenterUsersManagementPanel usersPanel,
            ResourceListGrid resourcesGrid,
            RoleListGrid rolesGrid,
            CostCenterService costCenterService) {
        this.costCenterForm = costCenterForm;
        this.usersPanel = usersPanel;
        this.resourcesGrid = resourcesGrid;
        this.rolesGrid = rolesGrid;
        this.costCenterService = costCenterService;

        build();
    }

    private void build() {
        setSizeFull();
        setMargin(new MarginInfo(false, true, true, true));

        tabs = new TabSheet();
        tabs.setSizeFull();
        addComponent(tabs);

        tabs.addTab(costCenterForm, "Basics");
        costCenterForm.addSavedHandler(costCenter -> {
            reload();
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();
        });

        resourcesGrid.setSizeFull();
        resourcesGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tabs.addTab(resourcesGrid, "Resources");
        resourcesGrid.addSelectionListener(event -> {
            if (!event.isUserOriginated()) return;
            Set<Resource> selectedResources = event.getAllSelectedItems();
            updateResources(selectedResources);
        });

        rolesGrid.setSizeFull();
        rolesGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tabs.addTab(rolesGrid, "Roles");
        rolesGrid.addSelectionListener(event -> {
            if (!event.isUserOriginated()) return;
            Set<Role> selectedRoles = event.getAllSelectedItems();
            updateRoles(selectedRoles);
        });

        usersPanel.setSizeFull();
        tabs.addTab(usersPanel, "Planners");
        usersPanel.getSaveButton().addClickListener(event -> reload());
    }

    private void reload() {
        costCenter = costCenterService.getCostCenter(costCenter.getId());
        setCostCenter(costCenter);
    }

    private void updateResources(Set<Resource> resources) {
        try {
            CostCenter center = costCenterService.updateCostCenterResources(costCenter, resources);
            setCostCenter(center);
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Updated cost center resources successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to updated cost center resources";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    private void updateRoles(Set<Role> roles) {
        try {
            CostCenter center = costCenterService.updateCostCenterRoles(costCenter, roles);
            setCostCenter(center);
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Updated cost center roles successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to updated cost center roles";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }


    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
        costCenterForm.setEntity(costCenter);

        rolesGrid.deselectAll();
        Set<Role> costCenterRoles = costCenterService.getRoles(costCenter);
        if (!CollectionUtils.isEmpty(costCenterRoles))
            rolesGrid.asMultiSelect().select(costCenterRoles.stream().toArray(Role[]::new));

        resourcesGrid.deselectAll();
        Set<Resource> costCenterResources = costCenterService.getResources(costCenter);
        if (!CollectionUtils.isEmpty(costCenterResources))
            resourcesGrid.asMultiSelect().select(costCenterResources.stream().toArray(Resource[]::new));

        usersPanel.setCostCenter(costCenter);
    }

    public void setCreateViewPanel(CostCenterCreateViewPanel createViewPanel) {
        this.createViewPanel = createViewPanel;
        usersPanel.setCostCenterCreateViewPanel(createViewPanel);
    }
}
