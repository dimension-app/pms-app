package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.create.SplitCreateViewPanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:28 PM
 */

@Secured({PMSPermission.CONFIGURE_COST_CENTERS})
@ViewComponent(value = SettingDisplay.class, caption = "Cost centers")
@ViewInfo(value = "Cost centers", section = "Planning", icon = VaadinIcons.BUILDING_O)
@SpringView(name = CostCenterCreateViewPanel.VIEW_NAME, ui = MainUI.class)
public class CostCenterCreateViewPanel extends SplitCreateViewPanel<CostCenter> implements SettingView {
    public static final String VIEW_NAME = "costCenters";

    private CostCenterForm costCenterForm;
    private CostCenterViewPanel costCenterViewPanel;
    private CostCenterService costCenterService;

    public CostCenterCreateViewPanel(
            CostCenterBackendGrid entriesGrid,
            CostCenterForm createPanel,
            CostCenterViewPanel viewPanel,
            CostCenterService costCenterService) {
        super(entriesGrid);
        this.costCenterForm = createPanel;
        this.costCenterViewPanel = viewPanel;
        this.costCenterService = costCenterService;
        costCenterViewPanel.setCreateViewPanel(this);
    }

    @PostConstruct
    protected void build() {
        setSizeFull();
        setMargin(false);
        costCenterForm.addSavedHandler(costCenter -> resetView());
        VaadinUtils.addRemoveButton(getEntriesGrid(), this::onDelete);
    }

    private void onDelete(CostCenter costCenter) {

        try {
            costCenterService.delete(costCenter);
            getEntriesGrid().getDataProvider().refreshAll();
            String msg = "Deleted cost center successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to delete cost center";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }


    @Override
    protected Component getCreateComponent() {
        costCenterForm.setEntity(new CostCenter());
        return costCenterForm;
    }

    @Override
    protected Component getViewComponent(CostCenter costCenter) {
        costCenterViewPanel.setCostCenter(costCenter);
        return costCenterViewPanel;
    }

    @Override
    public CostCenterBackendGrid getEntriesGrid() {
        return (CostCenterBackendGrid) super.getEntriesGrid();
    }

}
