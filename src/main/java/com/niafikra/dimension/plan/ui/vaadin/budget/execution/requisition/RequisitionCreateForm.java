package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.plan.domain.Requisition;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class RequisitionCreateForm extends RequisitionEditForm implements Notifier {

    private ConfirmButton createSubmitButton;

    public RequisitionCreateForm() {
    }

    @PostConstruct
    private void build() {

        addSavedHandler(requisition -> create());
        getSaveButton().setWidth("200px");
        getSaveButton().setCaption("Create requisition");

        createSubmitButton = new ConfirmButton(
                "Create and submit",
                "Are you sure?",
                event -> createSubmit()
        );
        createSubmitButton.setWidth("200px");
        createSubmitButton.addStyleNames(Theme.BUTTON_PRIMARY);
        createSubmitButton.setEnabled(false);
    }

    private void createSubmit() {
        Requisition requisition = getRequisition();
        try {
            requisitionService.createSubmit(requisition);
            showSuccess("Successfully submitted requisition", requisition.toString());
            closePopup();
        } catch (Exception e) {
            showError("Failed to submit requisition", e);
        }
    }

    private void create() {
        Requisition requisition = getRequisition();
        try {
            requisitionService.create(requisition);
            showSuccess("Successfully created requisition", requisition.toString());
            closePopup();
        } catch (Exception e) {
            showError("Failed to create requisition", e);
        }
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Create new requisition");
        return window;
    }

    @Override
    public MHorizontalLayout getToolbar() {
        return super.getToolbar().withComponentAsFirst(createSubmitButton);
    }

    public ConfirmButton getCreateSubmitButton() {
        return createSubmitButton;
    }

    @Override
    protected void adjustSaveButtonState() {
        super.adjustSaveButtonState();
        createSubmitButton.setEnabled(getSaveButton().isEnabled());
    }
}
