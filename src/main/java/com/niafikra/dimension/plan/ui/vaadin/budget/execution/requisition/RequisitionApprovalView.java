package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Window;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition.RequisitionApprovalView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/24/17 7:02 PM
 */

@Secured(PMSPermission.VIEW_REQUISITION_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@ViewComponent(value = ApprovalDisplay.class, caption = "Requisition approval")
public class RequisitionApprovalView extends MVerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-requisition-approval";

    @Inject
    private RequisitionPanel requisitionPanel;
    @Inject
    private RequisitionService requisitionService;
    @Inject
    private ApprovalTrackerService trackerService;
    @Inject
    private RequisitionUpdateForm updateForm;
    @Inject
    private MainHeader mainHeader;
    @Inject
    private Navigator navigator;
    @Inject
    private User currentUser;

    private Requisition requisition;

    @PostConstruct
    public void build(){
        setSizeFull();
        setMargin(false);
        requisitionPanel.setSizeFull();
        addComponent(requisitionPanel);
        setExpandRatio(requisitionPanel, 1);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);

        this.requisition = requisitionService.getRequisition(tracker);
        requisitionPanel.setRequisition(requisition);

        mainHeader.getMenuBar().setMenuVisible("Change", requisitionService.isEditable(requisition, currentUser));
    }


    @Secured(PMSPermission.EDIT_REQUISITION_DURING_APPROVAL)
    @ViewMenuOption(value = "Change", icon = VaadinIcons.EDIT)
    public void doChange() {
        updateForm.setRequisition(requisition);
        Window subWindow = updateForm.openInModalPopup();

        subWindow.addCloseListener(e -> {
            //when the window close refresh view
            navigator.reload();
        });
    }
}
