package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.service.AllocationAdjustmentService;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.PMSPermission.VIEW_ALLOCATION_ADJUSTMENT_APPROVAL_REQUEST;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment.AllocationAdjustmentApprovalView.VIEW_NAME;

/**
 * @Author Juma mketto
 * @Date 1/3/19.
 */
@Secured(VIEW_ALLOCATION_ADJUSTMENT_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME)
@ViewComponent(value = ApprovalDisplay.class, caption = "Allocation adjustment approval")
public class AllocationAdjustmentApprovalView extends VerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-allocationadjustment-approval";
    private AllocationAdjustmentPanel adjustmentPanel;
    private ApprovalTrackerService trackerService;
    private AllocationAdjustmentService adjustmentService;
    private Navigator navigator;
    private User currentUser;
    private AllocationAmountAdjustmentForm adjustmentForm;

    public AllocationAdjustmentApprovalView(AllocationAdjustmentPanel adjustmentPanel,
                                            ApprovalTrackerService trackerService,
                                            AllocationAdjustmentService adjustmentService,
                                            Navigator navigator,
                                            User currentUser,
                                            AllocationAmountAdjustmentForm adjustmentForm) {
        this.adjustmentPanel = adjustmentPanel;
        this.trackerService = trackerService;
        this.adjustmentService = adjustmentService;
        this.navigator = navigator;
        this.currentUser = currentUser;
        this.adjustmentForm = adjustmentForm;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        addComponent(adjustmentPanel);
        adjustmentForm.setMargin(true);
        setSpacing(true);


        addComponent(new Label());
        addComponent(new Label());
        addComponent(adjustmentForm);
        adjustmentForm.getSubmitButton().addClickListener(() -> navigator.reload());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);

        AllocationAdjustment adjustment = adjustmentService.findAdjustment(tracker);

        adjustmentPanel.setAllocationAdjustment(adjustment);
        adjustmentForm.setAllocationAdjustment(adjustment);
        adjustmentForm.setVisible(!adjustment.isApproved() && trackerService.canApproveOrOverride(adjustment, currentUser));
    }
}
