package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.PrintButton;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.Expense;
import com.vaadin.spring.annotation.SpringComponent;
import de.steinwedel.messagebox.MessageBox;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/25/17 12:02 AM
 */

@PrototypeScope
@SpringComponent
public class ExpenseInfoPanel extends MVerticalLayout {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        setSizeFull();
        addComponentsAndExpand(templatePanel);
        templatePanel
                .getTemplateView()
                .setTemplatePath(Templates.EXPENSE);
    }

    public ExpenseInfoPanel setExpense(Expense expense) {
        templatePanel
                .getTemplateView()
                .putBinding("expense", expense)
                .render();
        return this;
    }

    public void show(Expense expense) {
        setExpense(expense);
        MessageBox messageBox = MessageBox.create()
                .withCaption(expense.toString())
                .withMessage(this)
                .withButton(new PrintButton(PaymentPrintable.create(expense.getPayment()))
                        .withCaption("Print Payment Voucher"))
                .withCloseButton()
                .withWidth("70%")
                .withHeight("80%");

        messageBox.open();
    }

}
