package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition.AllRequisitionsReport.VIEW_NAME;


@Secured(PMSPermission.VIEW_ALL_REQUISITIONS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "All requisitions")
@ViewInfo(icon = VaadinIcons.WARNING, section = "Expenditure", value = "All Requisitions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllRequisitionsReport extends VerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "all-requisitions";

    private RequisitionsGrid requisitionsGrid;

    public AllRequisitionsReport(RequisitionsGrid requisitionsGrid) {
        this.requisitionsGrid = requisitionsGrid;

        setSizeFull();
        setMargin(false);
        requisitionsGrid.setSizeFull();
        addComponent(requisitionsGrid);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(requisitionsGrid);
    }
}
