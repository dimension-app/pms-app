package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.Retirement;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class RetirementPanel extends MVerticalLayout implements Notifier {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        this.templatePanel.getTemplateView().setTemplatePath(Templates.RETIREMENT);
        this.templatePanel.setSizeFull();
        withComponents(templatePanel).withExpand(templatePanel, 1);

    }

    public void setRetirement(Retirement retirement) {
        templatePanel
                .getTemplateView()
                .putBinding("retirement", retirement)
                .render();
    }
}
