package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.category.ui.vaadin.CategoryCreateEditPanel;
import com.niafikra.dimension.plan.domain.Consumer;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 5:08 PM
 */
@SpringComponent
@PrototypeScope
public class ConsumerCategoryCRUDPanel extends CategoryCreateEditPanel {

    public ConsumerCategoryCRUDPanel(ConsumerCategoryForm categoryForm, CategoryService categoryService) {
        super(Consumer.CATEGORY_TYPE, categoryForm, categoryService);
        setSizeFull();
        setMargin(false);
    }
}
