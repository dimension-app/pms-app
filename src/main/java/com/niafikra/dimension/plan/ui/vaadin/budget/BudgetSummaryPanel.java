package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.Budget;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class BudgetSummaryPanel extends VerticalLayout {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        templatePanel
                .getTemplateView()
                .setTemplatePath(Templates.BUDGET_SUMMARY);
        addComponentsAndExpand(templatePanel);
    }

    public void setBudget(Budget budget) {
        templatePanel
                .getTemplateView()
                .putBinding("budget", budget)
                .render();
    }
}
