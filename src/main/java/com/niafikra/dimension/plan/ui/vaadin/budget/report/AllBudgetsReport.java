package com.niafikra.dimension.plan.ui.vaadin.budget.report;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetsTable;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.plan.ui.vaadin.budget.report.AllBudgetsReport.VIEW_NAME;

@Secured(PMSPermission.VIEW_ALL_BUDGETS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "All budgets")
@ViewInfo(icon = VaadinIcons.BOOK_DOLLAR, section = "Expenditure", value = "All Budgets")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllBudgetsReport extends VerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "all-budgets";

    private BudgetsTable budgetsTable;

    public AllBudgetsReport(BudgetsTable budgetsTable) {
        this.budgetsTable = budgetsTable;

        setSizeFull();
        setMargin(false);
        budgetsTable.setSizeFull();
        addComponent(budgetsTable);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(budgetsTable);
    }
}
