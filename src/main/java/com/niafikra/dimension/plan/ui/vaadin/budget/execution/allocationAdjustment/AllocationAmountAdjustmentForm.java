package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.service.AllocationAdjustmentService;
import com.vaadin.annotations.PropertyId;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.MButton;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @Author Juma mketto
 * @Date 1/3/19.
 */
@PrototypeScope
@SpringComponent
public class AllocationAmountAdjustmentForm extends VerticalLayout implements Notifier {
    private MButton submitButton = new MButton("Save changes");

    @PropertyId("allocatedAmount")
    private MoneyField allocatedAmount = new MoneyField("Propose different Amount");

    private AllocationAdjustment allocationAdjustment;

    @Inject
    private AllocationAdjustmentService adjustmentService;

    @Inject
    private User user;

    @PostConstruct
    private void build() {
        addComponents(allocatedAmount, submitButton);
        submitButton.addClickListener(() -> submitChanges());
        submitButton.setIcon(VaadinIcons.CHECK_CIRCLE_O);

        setMargin(true);
        allocatedAmount.setWidth("300px");
        submitButton.setWidth("300px");
    }

    private void submitChanges() {
        Money amount = allocatedAmount.getValue();
        if (amount == null) {
            showWarning("Please specify a new Amount to allocate");
            return;
        }
        try {
            adjustmentService.changeAllocatedAmount(allocationAdjustment, amount,user);
            showSuccess("Successfully changed allocated amount for " + allocationAdjustment.getResource());
        } catch (Exception e) {
            showError("Failed to change amount allocated to the adjustment", e.toString());

        }
    }

    public MButton getSubmitButton() {
        return submitButton;
    }

    public AllocationAdjustment getAllocationAdjustment() {
        return allocationAdjustment;
    }

    public void setAllocationAdjustment(AllocationAdjustment adjustment) {
        this.allocationAdjustment = adjustment;
        allocatedAmount.setValue(allocationAdjustment.getAllocatedAmount());
    }
}
