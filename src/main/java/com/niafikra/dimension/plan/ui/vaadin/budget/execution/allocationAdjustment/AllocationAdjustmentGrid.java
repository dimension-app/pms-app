package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.service.AllocationAdjustmentService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.MSize;

import javax.annotation.PostConstruct;

/**
 * @Author Juma mketto
 * @Date 1/2/19.
 */
@PrototypeScope
@SpringComponent
public class AllocationAdjustmentGrid extends Grid<AllocationAdjustment> {
    private AllocationAdjustmentPanel adjustmentPanel;
    private AllocationAdjustmentService service;
    private AllocationAdjustmentProvider provider;

    public AllocationAdjustmentGrid(AllocationAdjustmentPanel adjustmentPanel,
                                    AllocationAdjustmentService service,
                                    AllocationAdjustmentProvider provider) {
        super(provider);
        this.adjustmentPanel = adjustmentPanel;
        this.service = service;
        this.provider = provider;
    }
    @PostConstruct
    private void build() {

        setSelectionMode(SelectionMode.NONE);

        addColumn(adjustment -> adjustment.getTimeCreated())
                .setId("timeCreated")
                .setCaption("Created On")
                .setSortable(true);

        addColumn(adjustment -> adjustment.getCreator())
                .setId("creator")
                .setCaption("Created By")
                .setSortable(true);

        addColumn(adjustment -> adjustment.getResource())
                .setId("resource")
                .setCaption("Resource")
                .setSortable(true);


        addColumn(adjustment -> adjustment.getProposedAmount())
                .setId("proposedAmount")
                .setCaption("Adjusted Amount")
                .setSortable(true);

        addColumn(adjustment -> adjustment.getReason())
                .setId("reason")
                .setCaption("Reason")
                .setSortable(true);

        addColumn(adjustment -> generateStatus(adjustment))
                .setId("status")
                .setCaption("Status")
                .setSortable(true);

        addItemClickListener(event -> showAdjustment(event.getItem()));
    }

    private void showAdjustment(AllocationAdjustment adjustment) {
    adjustmentPanel.setAllocationAdjustment(adjustment);
    new SubWindow(adjustmentPanel)
            .withCaption(adjustment.toString())
            .show()
            .withSize(MSize.size("60%", "70%"));
    }

    private String generateStatus(AllocationAdjustment adjustment) {
        return service.generateStatus(adjustment);
    }

    public AllocationAdjustmentProvider getDataProvider(){
        return (AllocationAdjustmentProvider) super.getDataProvider();
    }
}
