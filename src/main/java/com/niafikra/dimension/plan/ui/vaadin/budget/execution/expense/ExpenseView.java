package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.RetirementCreateView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.UI;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.ExpenseView.VIEW_NAME;

@Secured(PMSPermission.VIEW_EXPENSE)
@ViewComponent(value = MainDisplay.class, caption = "Expense")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ExpenseView extends MVerticalLayout implements Notifier, View {

    public static final String VIEW_NAME = "expense";

    @Inject
    private ExpensePanel expensePanel;

    @Inject
    private MainHeader mainHeader;
    @Inject
    private Navigator navigator;

    @Inject
    private ExpenseService expenseService;

    @Inject
    private SettlementPaymentForm settlementPaymentForm;

    private Expense expense;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        expensePanel.setSizeFull();
        withComponents(expensePanel)
                .withExpand(expensePanel, 1);
    }

    @Secured(PMSPermission.RETIRE_EXPENSE)
    @ViewMenuOption(value = "Retire", icon = VaadinIcons.MONEY_EXCHANGE, menu = "Expense", menuIcon = VaadinIcons.MONEY)
    public void doRetire() {
        navigator.navigateTo(RetirementCreateView.class, expense.getId());
    }

    @Secured(PMSPermission.SETTLE_RETIREMENT)
    @ViewMenuOption(value = "Settle", menu = "Expense", icon = VaadinIcons.MONEY, menuIcon = VaadinIcons.MONEY)
    public void showPaymentWindow() {
        settlementPaymentForm.setExpense(expense);
        settlementPaymentForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }

    @Secured(PMSPermission.RECONCILE_EXPENSE)
    @ViewMenuOption(value = "Reconcile", menu = "Expense", icon = VaadinIcons.CHECK, menuIcon = VaadinIcons.MONEY)
    public void confirmExpenseReconcile() {
        ConfirmDialog.show(
               getUI(),
               "Should this expense be marked reconciled?",
               dialog -> {
                   if(dialog.isConfirmed()){
                       try {
                           expenseService.markReconciled(expense);
                           showSuccess("Successful marked expense as reconciled",expense.toString());
                           UI.getCurrent().access(() -> navigator.reload());
                       }catch (Exception e){
                           showError("Failed to mark expense as reconciled",e);
                       }
                   }
               }
        );
    }

    @Secured(PMSPermission.PRINT_PAYMENT_VOUCHER)
    @ViewMenuOption(value = "Print", icon = VaadinIcons.PRINT, menu = "Expense", menuIcon = VaadinIcons.MONEY)
    public void doPrint() {
        UI.getCurrent().getPage().open(PaymentPrintable.create(expense.getPayment()).getPrintURL(), "_blank", true);
    }


    public void setExpense(Expense expense) {
        this.expense = expense;
        expensePanel.setExpense(expense);

        mainHeader.getMenuBar().setMenuVisible("Retire", expense.canRetire());
        mainHeader.getMenuBar().setMenuVisible("Settle", expense.canSettle());
        mainHeader.getMenuBar().setMenuVisible("Reconcile", expense.canReconcile());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long requestId = Long.parseLong(event.getParameters());
        setExpense(expenseService.getExpense(requestId));
    }

}
