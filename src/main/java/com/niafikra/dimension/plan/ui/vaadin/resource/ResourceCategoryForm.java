package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.category.ui.vaadin.CategoryForm;
import com.niafikra.dimension.plan.domain.Resource;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 9:45 PM
 * <p>
 * <p>
 * Munny was disturbing me while writing this class
 */
@SpringComponent
@PrototypeScope
public class ResourceCategoryForm extends CategoryForm {

    public ResourceCategoryForm(CategoryService categoryService) {
        super(Resource.CATEGORY_TYPE, categoryService);
    }
}
