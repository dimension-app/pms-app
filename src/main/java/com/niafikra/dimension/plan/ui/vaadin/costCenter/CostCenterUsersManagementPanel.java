package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.user.UsersAddRemovePanel;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 9:49 PM
 */
@SpringComponent
@PrototypeScope
public class CostCenterUsersManagementPanel extends UsersAddRemovePanel {

    private CostCenterService costCenterService;
    private CostCenterCreateViewPanel costCenterCreateViewPanel;
    private CostCenter costCenter;

    public CostCenterUsersManagementPanel(UsersProvider userProvider, CostCenterService costCenterService) {
        super(userProvider);
        this.costCenterService = costCenterService;
    }

    @Override
    protected void onSave(Collection<User> users) {
        try {
            costCenterService.updateCostCenterPlanners(costCenter, users.stream().collect(Collectors.toSet()));
            costCenterCreateViewPanel.getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Saved cost center users successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save cost center users";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    public void setCostCenterCreateViewPanel(CostCenterCreateViewPanel costCenterCreateViewPanel) {
        this.costCenterCreateViewPanel = costCenterCreateViewPanel;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
        getUsersGrid().setItems(costCenterService.getPlanners(costCenter));
    }
}
