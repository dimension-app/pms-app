package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.MinimalExpensesGrid;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.RetirementCreateView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition.RequisitionView.VIEW_NAME;

@Secured(PMSPermission.VIEW_REQUISITION)
@ViewComponent(value = MainDisplay.class, caption = "Requisition")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RequisitionView extends MVerticalLayout implements Notifier, View {

    public static final String VIEW_NAME = "requisition";

    @Inject
    private RequisitionPanel requisitionPanel;
    @Inject
    private RequisitionUpdateForm updateForm;
    @Inject
    private MinimalExpensesGrid expenseSelector;
    @Inject
    private RequisitionPaymentForm paymentForm;
    @Inject
    private MainHeader mainHeader;
    @Inject
    private Navigator navigator;

    @Inject
    private RequisitionService requisitionService;
    @Inject
    private User currentUser;

    private Requisition requisition;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        requisitionPanel.setSizeFull();
        withComponents(requisitionPanel)
                .withExpand(requisitionPanel, 1);

        expenseSelector.setSizeFull();
        expenseSelector.setSelectionMode(Grid.SelectionMode.NONE);
        expenseSelector.addItemClickListener(event -> {
            showRetirementForm(event.getItem());
        });


    }

    @Secured(PMSPermission.EDIT_REQUISITION)
    @ViewMenuOption(value = "Edit", menu = "Request", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.WARNING)
    public void showEditWindow() {
        updateForm.setRequisition(requisition);
        Window subWindow = updateForm.openInModalPopup();
        subWindow.addCloseListener(e -> {
            //when the window close then update requisition
            setRequisition(requisitionService.getRequisition(requisition.getId()));
        });
    }

    @Secured(PMSPermission.PAY_REQUISITION)
    @ViewMenuOption(value = "Pay", menu = "Request", icon = VaadinIcons.MONEY_WITHDRAW, menuIcon = VaadinIcons.WARNING)
    public void showPaymentWindow() {
        paymentForm.setRequisition(requisition);
        paymentForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }


    @Secured(PMSPermission.DELETE_REQUISITION)
    @ViewMenuOption(value = "Delete", menu = "Request", icon = VaadinIcons.TRASH, menuIcon = VaadinIcons.WARNING)
    public void confirmDelete() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you wish to delete this request?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            requisitionService.delete(requisition);
                            navigator.navigateBack();
                            showSuccess("Deleted requisition successful", requisition.toString());
                        } catch (Exception e) {
                            showError("Failed to delete requisition", e);
                        }
                    }
                }
        );
    }

    @Secured(PMSPermission.SUBMIT_REQUISITION)
    @ViewMenuOption(value = "Submit", menu = "Request", icon = VaadinIcons.KEY, menuIcon = VaadinIcons.WARNING)
    public void confirmSubmit() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you wish to submit this request?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            requisitionService.submit(requisition);
                            UI.getCurrent().access(() -> navigator.reload());
                            showSuccess("Submitted requisition for approval successful", requisition.toString());
                        } catch (Exception e) {
                            showError("Failed to submit requisition for approval", e);
                        }
                    }
                }
        );
    }

    @Secured(PMSPermission.RETIRE_EXPENSE)
    @ViewMenuOption(value = "Retire", menu = "Request", icon = VaadinIcons.MONEY, menuIcon = VaadinIcons.WARNING)
    public void doRetire() {
        List<Expense> expenses = requisition.getPendingRetirementExpenses();
        if (expenses.size() == 1) {
            showRetirementForm(expenses.stream().findFirst().get());
        } else {
            expenseSelector.setItems(expenses);
            new SubWindow(expenseSelector)
                    .show()
                    .withCaption("Select expense to retire")
                    .withModal(true)
                    .withCenter()
                    .withSize(MSize.size("70%", "40%"));

        }
    }

    private void showRetirementForm(Expense expense) {
        navigator.navigateTo(RetirementCreateView.class, expense.getId());
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
        requisitionPanel.setRequisition(requisition);

        mainHeader.getMenuBar().setMenuVisible("Edit", requisitionService.isEditable(requisition, currentUser));
        mainHeader.getMenuBar().setMenuVisible("Pay", requisitionService.isPayable(requisition));
        mainHeader.getMenuBar().setMenuVisible("Delete", !requisition.isPaymentProcessed() && !requisition.isLocked());
        mainHeader.getMenuBar().setMenuVisible("Submit", !requisitionService.isSubmitted(requisition));
        mainHeader.getMenuBar().setMenuVisible("Retire", !requisition.isReconciled());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long requestId = Long.parseLong(event.getParameters());
        setRequisition(requisitionService.getRequisition(requestId));
    }

}
