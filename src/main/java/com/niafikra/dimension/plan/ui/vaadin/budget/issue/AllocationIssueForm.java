package com.niafikra.dimension.plan.ui.vaadin.budget.issue;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsButton;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.service.IssueService;
import com.niafikra.dimension.issue.ui.vaadin.IssueForm;
import com.niafikra.dimension.plan.domain.Allocation;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Set;

@PrototypeScope
@SpringComponent
public class AllocationIssueForm extends IssueForm {

    private IssueService issueService;
    private Allocation allocation;

    public AllocationIssueForm(IssueService issueService,
                               AttachmentsButton<Set<Attachment>> attachmentsButton) {
        super(attachmentsButton);
        this.issueService = issueService;

        addSavedHandler(issue -> doSave(issue));
    }

    public Allocation getAllocation() {
        return allocation;
    }

    public void setAllocation(Allocation allocation) {
        this.allocation = allocation;
    }

    private void doSave(Issue issue) {
        issue.setReference(AllocationIssueUtil.getReference(allocation));
        try {
            issueService.createIssue(issue);
            closePopup();
            showSuccess("Created new issue successful");
        } catch (Exception e) {
            showError("Failed to create issue", e);
        }
    }

}
