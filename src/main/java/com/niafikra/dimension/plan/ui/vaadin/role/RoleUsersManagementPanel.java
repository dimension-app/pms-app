package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.user.UsersAddRemovePanel;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.group.service.GroupService;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 9:49 PM
 */
@SpringComponent
@PrototypeScope
public class RoleUsersManagementPanel extends UsersAddRemovePanel {

    @Inject
    private RoleService roleService;

    @Inject
    private GroupService groupService;

    private RoleCreateViewPanel roleCreateViewPanel;
    private Role role;

    public RoleUsersManagementPanel(UsersProvider userProvider) {
        super(userProvider);
    }

    @Override
    protected void onSave(Collection<User> users) {
        try {
            roleService.updateRoleUsers(role, users.stream().collect(Collectors.toSet()));
            roleCreateViewPanel.getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Saved role users successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save role users";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    public void setRoleCreateViewPanel(RoleCreateViewPanel roleCreateViewPanel) {
        this.roleCreateViewPanel = roleCreateViewPanel;
    }

    public void setRole(Role role) {
        this.role = role;
        setSelectedUsers(groupService.getUsers(role));

    }
}
