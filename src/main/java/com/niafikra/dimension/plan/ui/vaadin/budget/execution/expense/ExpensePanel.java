package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.Expense;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class ExpensePanel extends MVerticalLayout implements Notifier {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        this.templatePanel.getTemplateView().setTemplatePath(Templates.EXPENSE);
        templatePanel.setSizeFull();
        withComponents(templatePanel).withExpand(templatePanel, 1);

    }

    public void setExpense(Expense expense) {
        templatePanel.getTemplateView().putBinding("expense", expense).render();
    }
}
