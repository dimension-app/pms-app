package com.niafikra.dimension.plan.ui.vaadin.paymentMethod;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.create.FormSplitCreateViewPanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.PaymentMethod;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.plan.ui.vaadin.paymentMethod.PaymentMethodCreateViewPanel.VIEW_NAME;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/26/17 12:34 PM
 */
@Secured({PMSPermission.CONFIGURE_PAYMENT_METHODS})
@ViewComponent(value = SettingDisplay.class, caption = "Payment methods")
@ViewInfo(value = "Payment Methods", section = "Finance", icon = VaadinIcons.MONEY)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PaymentMethodCreateViewPanel extends FormSplitCreateViewPanel<PaymentMethod> implements SettingView {
    public static final String VIEW_NAME = "paymentMethods";

    public PaymentMethodCreateViewPanel(
            PaymentMethodBackendGrid paymentMethodGrid,
            PaymentMethodForm paymentMethodForm) {
        super(paymentMethodGrid, paymentMethodForm);
    }

    @PostConstruct
    protected void build() {
        setSizeFull();
        setMargin(false);
    }

    @Override
    protected PaymentMethod createNewEntity() {
        return new PaymentMethod();
    }

    @Override
    protected void onDelete(PaymentMethod entity) {

    }
}
