package com.niafikra.dimension.plan.ui.vaadin.budget.planning;

import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.plan.domain.AllocationProposal;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.annotations.PropertyId;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MFormLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/18/17 9:55 PM
 */
@SpringComponent
@PrototypeScope
public class CreateAllocationForm extends AbstractForm<AllocationProposal> {

    public static final String FIELD_WIDTH = "400px";

    @PropertyId("role")
    private ComboBox<Role> roleSelector = new ComboBox<>("Role");

    @PropertyId("resource")
    private ComboBox<Resource> resourceSelector = new ComboBox<>("Resource");

    @PropertyId("proposedAmount")
    private MoneyField amountField = new MoneyField("Amount");

    @PropertyId("description")
    private TextArea descriptionField = new TextArea("Description");

    @PropertyId("reason")
    private TextArea reasonField = new TextArea("Reason");

    @Inject
    private CostCenterService costCenterService;

    @Inject
    private RoleService roleService;
    @Inject
    private AllocationService allocationService;
    @Inject
    private ApprovalTrackerService trackerService;
    @Inject
    private User currentUser;

    private Budget budget;

    public CreateAllocationForm() {
        super(AllocationProposal.class);
    }

    @PostConstruct
    private void build(){
        roleSelector.setWidth(FIELD_WIDTH);
        roleSelector.addValueChangeListener(event -> {
            loadResources();
        });

        resourceSelector.setWidth(FIELD_WIDTH);
        amountField.setWidth(FIELD_WIDTH);
        descriptionField.setWidth(FIELD_WIDTH);
        descriptionField.setRows(4);
        reasonField.setWidth(FIELD_WIDTH);

        addSavedHandler(allocation -> doSubmit(allocation));
    }

    @Override
    protected Component createContent() {
        return new MFormLayout(
                roleSelector,
                resourceSelector,
                amountField,
                descriptionField,
                reasonField,
                getToolbar()
        ).withMargin(true);
    }

    private void doSubmit(AllocationProposal proposal) {
        try {
            allocationService.propose(
                    proposal.getBudget(),
                    proposal.getRole(),
                    proposal.getResource(),
                    proposal.getProposedAmount(),
                    proposal.getDescription(),
                    proposal.getReason()
            );
            init();
        } catch (Exception e) {
            showError("Failed to allocate resource", e.getMessage());
        }
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
        loadRoles();
        init();
    }

    private void loadRoles() {

        //if user is currently required to approve the associated budget then he/she can change or propose allocations
        //from any role/resource combination allowed
        if (trackerService.canApprove(budget, currentUser)) {
            roleSelector.setItems(costCenterService.getRoles(budget.getCostCenter()));
        } else {
            roleSelector.setItems(roleService.findAllowedRoles(budget.getCostCenter(), currentUser));
        }
    }

    private void init() {
        if (budget == null) throw new IllegalStateException("Allocation form must have a budget specified");

        AllocationProposal allocation = new AllocationProposal(budget);
        setEntity(allocation);
    }

    private void loadResources() {
        if (budget == null) throw new IllegalStateException("Allocation form must have a budget specified");
        Optional<Role> role = this.roleSelector.getOptionalValue();

        resourceSelector.clear();
        if (role.isPresent()) {
            resourceSelector.setItems(allocationService.getNotAllocatedResources(budget, role.get()));
        }
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("600px");
        window.setCaption("Create allocation");
        return window;
    }
}
