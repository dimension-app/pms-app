package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 10:47 PM
 */
@SpringComponent
@PrototypeScope
public class CostCenterBackendGrid extends NameFilterableBackendGrid<CostCenter> {

    @Inject
    private CostCenterService costCenterService;

    public CostCenterBackendGrid(CostCenterProvider dataProvider) {
        super(dataProvider);


        addColumn(t -> costCenterService.getRoles(t).size())
                .setId("roleCount")
                .setCaption("Roles");

        addColumn(t -> costCenterService.getResources(t).size())
                .setId("resourceCount")
                .setCaption("Resources");

        addColumn(t -> costCenterService.getPlanners(t).size())
                .setId("plannerCount")
                .setCaption("Planners");
    }
}
