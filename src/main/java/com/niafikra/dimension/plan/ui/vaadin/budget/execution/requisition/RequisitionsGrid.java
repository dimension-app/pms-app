package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.niafikra.dimension.plan.ui.vaadin.consumer.ConsumerProvider;
import com.niafikra.dimension.plan.ui.vaadin.resource.ResourceComboBox;
import com.niafikra.dimension.plan.ui.vaadin.role.RoleProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.core.ui.vaadin.Theme.TEXT_RIGHT;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;

@ViewScope
@SpringComponent
public class RequisitionsGrid extends Grid<Requisition> implements Notifier {

    @Inject
    private RequisitionService requisitionService;
    @Inject
    private Navigator navigator;
    @Inject
    private UsersProvider usersProvider;
    @Inject
    private RoleProvider roleProvider;
    @Inject
    private ResourceComboBox resourceFilter;
    @Inject
    private ConsumerProvider consumerProvider;

    public RequisitionsGrid(RequisitionsProvider provider) {
        super(provider);
    }

    @PostConstruct
    private void build() {

        setSelectionMode(SelectionMode.NONE);

        addColumn(requisition -> DateUtils.formatDateTime(requisition.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Time")
                .setHidable(true)
                .setSortable(true);

        addColumn(Requisition::getCreator)
                .setId("creator")
                .setCaption("Person")
                .setHidable(true)
                .setSortable(true);


        addColumn(Requisition::getRole)
                .setId("role")
                .setCaption("Role")
                .setHidable(true)
                .setSortable(true);

        addColumn(Requisition::getResource)
                .setId("resource")
                .setCaption("Resource")
                .setHidable(true)
                .setSortable(true);

        addColumn(requisition -> requisition.getRequestedAmount().toBaseCurrency().getFormattedAmount())
                .setId("requestedAmount")
                .setCaption("Requested")
                .setHidable(true)
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(requisition -> requisition.getApprovedAmount().toBaseCurrency().getFormattedAmount())
                .setId("approvedAmount")
                .setCaption("Approved")
                .setHidable(true)
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(requisition -> requisition.getPendingAmount().toBaseCurrency().getFormattedAmount())
                .setId("pendingAmount")
                .setCaption("Pending")
                .setHidable(true)
                .setSortable(false)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(requisition -> requisitionService.getStatus(requisition))
                .setId("status")
                .setCaption("Status")
                .setHidable(true)
                .setSortable(false);

        addColumn(Requisition::getConsumer)
                .setId("consumer")
                .setCaption("Consumer")
                .setHidable(true)
                .setSortable(true);

        addColumn(Requisition::getBudget)
                .setId("budget")
                .setCaption("Budget")
                .setHidable(true)
                .setSortable(true);

        addColumn(Requisition::getDescription)
                .setId("description")
                .setCaption("Description")
                .setHidable(true)
                .setSortable(false);

        addItemClickListener(event -> {
            showRequisition(event.getItem());
        });

        LocalDateTimeRangeSelector timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");

        HeaderRow headerRow = appendHeaderRow();
        headerRow.getCell("timeCreated").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            getDataProvider().setDateRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
        });


        createFilteringComboBox(
                headerRow,
                "creator",
                "Filter creator",
                usersProvider.withConvertedFilter(name -> UserService.UserFilter.builder().name(name).build()),
                event -> getDataProvider().setCreator(event.getValue())
        );

        createFilteringComboBox(
                headerRow,
                "role",
                "Filter role",
                roleProvider,
                event -> getDataProvider().setRole(event.getValue())
        );

        resourceFilter.setPlaceholder("Filter resource");
        headerRow.getCell("resource").setComponent(
                resourceFilter.withValueChangeListener(event -> {
                    getDataProvider().setResource(event.getValue());
                })
        );

        createFilteringComboBox(
                headerRow,
                "consumer",
                "Filter consumer",
                consumerProvider,
                event -> getDataProvider().setConsumer(event.getValue())
        );
    }

    private void showRequisition(Requisition requisition) {
        navigator.navigateTo(RequisitionView.class, requisition.getId());
    }

    public RequisitionsProvider getDataProvider() {
        return (RequisitionsProvider) super.getDataProvider();
    }

    public void filterCreator(User currentUser) {
        getDataProvider().setCreator(currentUser);
    }

    public void filterFullfilled(Boolean fullFilled) {
        getDataProvider().setFullFilled(fullFilled);
    }

    public void filterApproved(Boolean approved) {
        getDataProvider().setApproved(approved);
    }
}
