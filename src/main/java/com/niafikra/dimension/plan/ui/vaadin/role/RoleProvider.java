package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameFilterablePageableDataProvider;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.data.provider.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/28/17 11:40 AM
 */
@Component
@PrototypeScope
public class RoleProvider extends AbstractNameFilterablePageableDataProvider<Role> {

    private RoleService roleService;

    public RoleProvider(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    protected Page<Role> fetchFromBackEnd(Query<Role, String> query, Pageable pageable) {
        Page<Role> roles = roleService.findRoles(getFilter(query), pageable);
        return roles;
    }

    @Override
    protected int sizeInBackEnd(Query<Role, String> query) {
        Long count = roleService.countRoles(getFilter(query));
        return count.intValue();
    }
}
