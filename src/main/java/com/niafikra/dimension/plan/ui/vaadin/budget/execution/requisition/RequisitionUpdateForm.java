package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.vaadin.annotations.PropertyId;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class RequisitionUpdateForm extends RequisitionEditForm implements Notifier {

    private ConfirmButton updateSubmitButton;
    private boolean submitted;

    @PropertyId("role")
    private LabelField<Role> roleLabel = new LabelField<Role>("Role").withEnabled(false);
    @PropertyId("resource")
    private LabelField<Resource> resourceLabel = new LabelField<Resource>("Resource").withEnabled(false);

    @PostConstruct
    private void build() {

        addSavedHandler(requisition -> update());
        getSaveButton().setWidth("200px");
        getSaveButton().setCaption("Update requisition");

        updateSubmitButton = new ConfirmButton(
                "Update and submit",
                "Are you sure?",
                event -> updateSubmit()
        );

        updateSubmitButton.setWidth("200px");
        updateSubmitButton.addStyleNames(Theme.BUTTON_PRIMARY);
        updateSubmitButton.setEnabled(false);
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Update requisition");
        return window;
    }

    private void updateSubmit() {
        Requisition requisition = getRequisition();
        try {
            requisitionService.updateSubmit(requisition);
            closePopup();
            showSuccess("Successfully submitted requisition", requisition.toString());
        } catch (Exception e) {
            showError("Failed to submit requisition", e);
        }
    }

    private void update() {
        Requisition requisition = getRequisition();
        try {
            requisitionService.update(requisition);
            closePopup();
            showSuccess("Successfully updated requisition", requisition.toString());
        } catch (Exception e) {
            showError("Failed to update requisition", e);
        }
    }

    @Override
    public void setEntity(Requisition requisition) {
        submitted = requisitionService.isSubmitted(requisition);
        super.setEntity(requisition);
        updateSubmitButton.setVisible(!submitted);

        role.setVisible(!submitted);
        resource.setVisible(!submitted);

        roleLabel.setVisible(submitted);
        resourceLabel.setVisible(submitted);
    }

    @Override
    protected Component createContent() {
        MVerticalLayout content = (MVerticalLayout) super.createContent();
        content.addComponent(new MVerticalLayout(roleLabel,resourceLabel),1);
        return content;
    }

    @Override
    public MHorizontalLayout getToolbar() {
        return super.getToolbar().withComponentAsFirst(updateSubmitButton);
    }

    public ConfirmButton getUpdateSubmitButton() {
        return updateSubmitButton;
    }

    @Override
    protected void adjustSaveButtonState() {
        super.adjustSaveButtonState();

        //when updating it can only be submitted if it is still a draft not submmited for approval before
        updateSubmitButton.setEnabled(getSaveButton().isEnabled() && !submitted);
    }
}
