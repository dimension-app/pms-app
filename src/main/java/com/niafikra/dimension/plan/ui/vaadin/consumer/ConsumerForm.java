package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.category.ui.vaadin.NameDescriptionCategoryForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.Consumer;
import com.niafikra.dimension.plan.service.ConsumerService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 9:52 AM
 */
@SpringComponent
@PrototypeScope
public class ConsumerForm extends NameDescriptionCategoryForm<Consumer> {

    protected TextField code;
    private ConsumerService consumerService;
    private CheckBox active;

    public ConsumerForm(ConsumerService consumerService, CategoryService categoryService) {
        super(Consumer.class, Consumer.CATEGORY_TYPE, categoryService);
        this.consumerService = consumerService;

        active = new CheckBox("Is active?");
        code = new MTextField("Code").withFullWidth();
        addSavedHandler(consumer -> doSave(consumer));
    }

    protected void doSave(Consumer consumer) {
        try {
            consumer = consumerService.save(consumer);
            setEntity(consumer);

            String msg = "Saved consumer successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save consumer";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    @Override
    protected MVerticalLayout createFormFields() {
        return super.createFormFields().with(code).with(active);
    }
}
