package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.ui.vaadin.util.PrintWindow;
import com.niafikra.dimension.plan.domain.Expense;
import com.niafikra.dimension.plan.domain.Payment;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.PaymentForm;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.PaymentPrintable;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class RequisitionPaymentForm extends PaymentForm {
    private Requisition requisition;

    @Inject
    private RequisitionService requisitionService;

    @PostConstruct
    private void build() {
        addSavedHandler(payment -> pay(payment));
    }

    private void pay(Payment payment) {
        try {
            Expense expense = requisitionService.pay(requisition, payment);
            PrintWindow.create(PaymentPrintable.create(expense.getPayment()), "Do you want to print payment voucher?");
            closePopup();
            showSuccess("Successfully paid requisition " + payment.getAmount(), "Amount pending is: " + requisition.getPendingAmount());
        } catch (Exception e) {
            showError("Failed to pay requisition", e);
        }
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;

        Payment payment = new Payment(
                requisition.getCreator().toString(),
                requisition.getPendingAmount(),
                true);
        setEntity(payment);
    }
}
