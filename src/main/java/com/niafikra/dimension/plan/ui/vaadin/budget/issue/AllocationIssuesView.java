package com.niafikra.dimension.plan.ui.vaadin.budget.issue;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.service.IssueService;
import com.niafikra.dimension.issue.ui.vaadin.IssueView;
import com.niafikra.dimension.issue.ui.vaadin.IssuesGrid;
import com.niafikra.dimension.issue.ui.vaadin.IssuesView;
import com.niafikra.dimension.issue.ui.vaadin.ResolutionForm;
import com.niafikra.dimension.plan.domain.Allocation;
import com.niafikra.dimension.plan.service.AllocationService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

import java.util.List;

@ViewScope
@SpringComponent
public class AllocationIssuesView extends IssuesView {


    private Allocation allocation;
    private AllocationService allocationService;

    public AllocationIssuesView(IssuesGrid issuesGrid,
                                IssueView issueView,
                                AllocationIssueForm issueForm,
                                ResolutionForm resolutionForm,
                                IssueService issueService,
                                AllocationService allocationService,
                                User user) {
        super(issuesGrid, issueView, issueForm, resolutionForm, issueService, user);
        this.allocationService = allocationService;
    }

    @Override
    protected void load() {
        allocation = allocationService.getAllocation(allocation.getId());
        super.load();
    }

    @Override
    protected List<Issue> getIssues() {
        return issueService.getIssues(AllocationIssueUtil.getReference(allocation));
    }

    public void setAllocation(Allocation allocation) {
        this.allocation = allocation;
        getIssueForm().setAllocation(allocation);
        load();
    }


    @Override
    public AllocationIssueForm getIssueForm() {
        return (AllocationIssueForm) super.getIssueForm();
    }
}
