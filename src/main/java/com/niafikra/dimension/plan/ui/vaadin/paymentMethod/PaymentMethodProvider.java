package com.niafikra.dimension.plan.ui.vaadin.paymentMethod;

import com.niafikra.dimension.core.ui.vaadin.util.provider.AbstractNameFilterablePageableDataProvider;
import com.niafikra.dimension.plan.domain.PaymentMethod;
import com.niafikra.dimension.plan.service.PaymentMethodService;
import com.vaadin.data.provider.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 9:08 PM
 */
@Component
@PrototypeScope
public class PaymentMethodProvider extends AbstractNameFilterablePageableDataProvider<PaymentMethod> {
    private PaymentMethodService paymentMethodService;

    public PaymentMethodProvider(PaymentMethodService paymentMethodService) {
        this.paymentMethodService = paymentMethodService;
    }

    @Override
    protected Page<PaymentMethod> fetchFromBackEnd(Query<PaymentMethod, String> query, Pageable pageable) {
        Page<PaymentMethod> paymentMethods = paymentMethodService.findPaymentMethods(getFilter(query), pageable);
        return paymentMethods;
    }

    @Override
    protected int sizeInBackEnd(Query<PaymentMethod, String> query) {
        Long count = paymentMethodService.countPaymentMethods(getFilter(query));
        return count.intValue();
    }

}
