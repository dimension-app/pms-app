package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.category.ui.vaadin.CategoryCreateEditPanel;
import com.niafikra.dimension.plan.domain.Resource;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 5:08 PM
 */
@SpringComponent
@PrototypeScope
public class ResourceCategoryCRUDPanel extends CategoryCreateEditPanel {
    public static final String VIEW_NAME = "resourceCategories";

    public ResourceCategoryCRUDPanel(ResourceCategoryForm categoryForm, CategoryService categoryService) {
        super(Resource.CATEGORY_TYPE, categoryForm, categoryService);
        setMargin(false);
    }
}
