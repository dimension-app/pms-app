package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Retirement;
import com.niafikra.dimension.plan.service.ExpenseService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.inject.Inject;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.RetirementCreateView.VIEW_NAME;

@Secured(PMSPermission.RETIRE_EXPENSE)
@ViewComponent(value = MainDisplay.class, caption = "Retire expense")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RetirementCreateView extends RetirementEditView implements View {
    public static final String VIEW_NAME = "create-retirement";

    @Inject
    private ExpenseService expenseService;
    @Inject
    private User currentUser;


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long expenseId = Long.parseLong(event.getParameters());
        setRetirement(new Retirement(expenseService.getExpense(expenseId), currentUser));
    }
}
