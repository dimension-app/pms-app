package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.plan.domain.Budget;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.PMSPermission.CREATE_BUDGET;
import static com.niafikra.dimension.PMSPermission.VIEW_USER_BUDGETS;
import static com.niafikra.dimension.plan.ui.vaadin.budget.UserBudgetsView.VIEW_NAME;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/12/17 8:01 PM
 */
@Secured(VIEW_USER_BUDGETS)
@ViewComponent(value = MainDisplay.class, caption = "My budgets")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.FINANCE, caption = "My Budgets")
@VaadinFontIcon(VaadinIcons.BOOK_DOLLAR)
public class UserBudgetsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "user-budgets";

    private BudgetsTable budgetsTable;
    private BudgetCreateForm budgetCreateForm;

    public UserBudgetsView(BudgetsTable budgetsTable,
                           BudgetCreateForm budgetCreateForm,
                           User currentUser) {
        this.budgetsTable = budgetsTable;
        this.budgetCreateForm = budgetCreateForm;
        budgetsTable.getBudgetsProvider().filterUser(currentUser);
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        budgetsTable.setSizeFull();
        addComponent(budgetsTable);
        setExpandRatio(budgetsTable, 1);
    }

    @Secured(CREATE_BUDGET)
    @ViewMenuOption(value = "New Budget", icon = VaadinIcons.PLUS_CIRCLE)
    public void showNewBudgetForm() {
        budgetCreateForm.setEntity(new Budget());
        Window popup = budgetCreateForm.openInModalPopup();
        popup.setCaption("Create budget");
        popup.addCloseListener(e -> budgetsTable.getBudgetsProvider().refreshAll());
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(budgetsTable);
    }

}