package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.category.ui.vaadin.NameDescriptionCategoryForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.service.ResourceService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 11:02 AM
 */
@SpringComponent
@PrototypeScope
public class ResourceForm extends NameDescriptionCategoryForm<Resource> {

    private ResourceService resourceService;

    public ResourceForm(ResourceService resourceService, CategoryService categoryService) {
        super(Resource.class, Resource.CATEGORY_TYPE, categoryService);
        this.resourceService = resourceService;

        addSavedHandler(resource -> doSave(resource));
    }

    protected void doSave(Resource resource) {
        try {
            resource = resourceService.save(resource);
            setEntity(resource);

            String msg = "Saved resource successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save resource";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }
}
