package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.google.common.collect.ImmutableMap;
import com.niafikra.dimension.approval.ApprovalPermission;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.plan.domain.Retirement;
import com.niafikra.dimension.plan.domain.RetirementEntry;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.HtmlRenderer;
import org.springframework.context.ApplicationContext;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Locale;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

@PrototypeScope
@SpringComponent
public class RetirementEntriesAcceptanceGrid extends Grid<RetirementEntry> implements Notifier {

    @Inject
    private ApplicationContext applicationContext;
    @Inject
    private ApprovalTrackerService trackerService;
    @Inject
    private TemplateEngine templateEngine;
    @Inject
    private User currentUser;

    private Retirement retirement;

    @PostConstruct
    private void build() {
        setFooterVisible(true);
        setHeaderVisible(false);
        setBodyRowHeight(100);
        setSelectionMode(SelectionMode.NONE);
        addColumn(entry ->
                        templateEngine.process(
                                Templates.RETIREMENT_ENTRY_ROW,
                                new Context(Locale.getDefault(), ImmutableMap.of("entry", entry, "applicationContext", applicationContext))),
                new HtmlRenderer()
        ).setId("entry").setWidthUndefined().setExpandRatio(1);

        addComponentColumn(entry -> {
            CheckBox selector = new CheckBox();
            selector.setValue(entry.isAccepted());
            selector.addValueChangeListener(event -> {
                entry.setAccepted(event.getValue());
            });

            return selector;
        });

//        setSelectionMode(SelectionMode.MULTI);
//        asMultiSelect().addSelectionListener(event -> {
//            if(!event.isUserOriginated()) return;
//
//            try {
//                retirementService.acceptRetirementEntries(retirement, event.getAllSelectedItems());
//                showSuccess("Successful accepted " + event.getAllSelectedItems().size() + " retirement entries", retirement.toString());
//            } catch (Exception e) {
//                showError("Failed to accept retirement entries", e);
//            }
//        });

        appendFooterRow();
    }

    public void setRetirement(Retirement retirement) {
        this.retirement = retirement;
        setItems(retirement.getEntries());
        getFooterRow(0).getCell("entry").setText(retirement.getTotal().toString());
        //init selection of the one already marked accepted
        //asMultiSelect().setValue(retirement.getEntries().stream().filter(entry -> entry.isAccepted()).collect(Collectors.toSet()));
        setEnabled(canApprove(retirement));
    }

    private boolean canApprove(Retirement retirement) {
        return trackerService.canApprove(retirement, currentUser)||
                (!trackerService.isCompleted(retirement) && hasAuthority(ApprovalPermission.OVERRIDE_APPROVE_REQUEST));
    }
}
