package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.AbstractBudgetView;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetDisplay;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.MenuBar;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.PMSPermission.ADJUST_ALLOCATION;
import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment.AllocationAdjustmentView.VIEW_NAME;


/**
 * @Author Juma mketto
 * @Date 1/2/19.
 */
@Secured(PMSPermission.VIEW_BUDGET_ALLOCATIONS)
@ViewComponent(value = BudgetDisplay.class, caption = "Budget allocations adjustments")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllocationAdjustmentView extends AbstractBudgetView implements TableExportable {
    public static final String VIEW_NAME = "budget-allocation-adjustments";
    private MainHeader mainHeader;
    private AllocationAdjustmentForm adjustmentForm;
    private AllocationAdjustmentGrid adjustmentsGrid;
    private Budget budget;

    private MenuBar.MenuItem requestMenuItem;

    public AllocationAdjustmentView(MainHeader mainHeader,
                                    AllocationAdjustmentForm adjustmentForm,
                                    AllocationAdjustmentGrid adjustmentsGrid,
                                    BudgetService budgetService) {
        super(budgetService);
        this.mainHeader = mainHeader;
        this.adjustmentForm = adjustmentForm;
        this.adjustmentsGrid = adjustmentsGrid;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        adjustmentsGrid.setSizeFull();
        addComponent(adjustmentsGrid);

    }

    @Override
    public void setBudget(Budget budget) {
        this.budget = budget;
        this.adjustmentsGrid.getDataProvider().filterBudget(budget);

    }

    @Override
    public void attach() {
        super.attach();

        if (hasAuthority(ADJUST_ALLOCATION)) {
            requestMenuItem = mainHeader.getMenuBar().addItemAsFirst(
                    "Request Adjustment",
                    VaadinIcons.PLUS_CIRCLE,
                    sel -> doCreateAdjustment()
            );
        }
    }

    public void doCreateAdjustment() {
        adjustmentForm.setEntity(new AllocationAdjustment(budget));
        this.adjustmentForm.setBudget(budget);
        adjustmentForm
                .openInModalPopup()
                .addCloseListener(e -> adjustmentsGrid.getDataProvider().refreshAll());

    }

    @Override
    public void detach() {
        super.detach();

        if (hasAuthority(ADJUST_ALLOCATION)) {
            mainHeader.getMenuBar().removeItem(requestMenuItem);
        }
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(adjustmentsGrid);
    }
}
