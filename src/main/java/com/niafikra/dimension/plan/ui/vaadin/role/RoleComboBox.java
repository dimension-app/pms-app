package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.plan.domain.Role;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class RoleComboBox extends MComboBox<Role> {

    public RoleComboBox(RoleProvider provider) {
        setDataProvider(provider);
    }
}
