package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.category.ui.vaadin.CategoryForm;
import com.niafikra.dimension.plan.domain.Consumer;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 9:45 PM
 * <p>
 * <p>
 * Munny was disturbing me while writing this class
 */
@SpringComponent
@PrototypeScope
public class ConsumerCategoryForm extends CategoryForm {

    public ConsumerCategoryForm(CategoryService categoryService) {
        super(Consumer.CATEGORY_TYPE, categoryService);
    }
}
