package com.niafikra.dimension.plan.ui.vaadin.consumer;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.ui.vaadin.HasCategoryFormPopupCreatePanel;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.Consumer;
import com.niafikra.dimension.plan.service.ConsumerService;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 11:40 PM
 */
@SpringComponent
@PrototypeScope
public class ConsumerCreateViewPanel extends HasCategoryFormPopupCreatePanel<Consumer> {

    private ConsumerService consumerService;

    public ConsumerCreateViewPanel(ConsumerBackendGrid consumersGrid,
                                   ConsumerForm consumerForm,
                                   ConsumerService consumerService) {
        super(consumersGrid, consumerForm);
        this.consumerService = consumerService;
    }

    @PostConstruct
    protected void build() {
        setMargin(new MarginInfo(false, true, false, true));
    }

    @Override
    protected Consumer createCategorisedEntity(Category category) {
        return new Consumer(category);
    }

    @Override
    protected void onDelete(Consumer consumer) {
        try {
            consumerService.delete(consumer);
            String msg = "Deleted consumer successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to delete consumer";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    @Override
    public void filterCategory(Category category) {
        getEntriesGrid().getDataProvider().setCategoryFilter(category);
    }

    @Override
    public ConsumerBackendGrid getEntriesGrid() {
        return ConsumerBackendGrid.class.cast(super.getEntriesGrid());
    }
}
//
//    @Override
//    public ConsumerBackendGrid getEntriesGrid() {
//        return (ConsumerBackendGrid) super.getEntriesGrid();
//    }
//
//    @PostConstruct
//    @Override
//    protected void build() {
//        super.build();
//    }
//
//    @Override
//    protected Consumer createNewEntity() {
//        return new Consumer(category);
//    }
//
//    @Override
//    protected void onDelete(Consumer consumer) {
//        consumerService.delete(consumer);
//    }
//
//    public ConsumerProvider getDataProvider() {
//        return  getEntriesGrid().getDataProvider();
//    }
//
//    @Override
//    protected Component getCreateComponent() {
//        getForm().loadCategories();
//        return super.getCreateComponent();
//    }
//
//    @Override
//    protected Component getViewComponent(Consumer entry) {
//        getForm().loadCategories();
//        return super.getViewComponent(entry);
//    }
//
//    @Override
//    public ConsumerForm getForm() {
//        return (ConsumerForm) super.getForm();
//    }
//
//    @Override
//    public void filterCategory(Category category) {
//        this.category =category;
//        getDataProvider().setCategoryFilter(category);
//    }