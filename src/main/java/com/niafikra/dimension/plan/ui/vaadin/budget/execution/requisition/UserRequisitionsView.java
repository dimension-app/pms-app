package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.UserRequisitionBudgetSelector;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import de.steinwedel.messagebox.MessageBox;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import static com.niafikra.dimension.PMSPermission.CREATE_REQUISITION;
import static com.niafikra.dimension.PMSPermission.VIEW_USER_REQUISITIONS;
import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition.UserRequisitionsView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/26/17 11:31 AM
 */

@Secured(VIEW_USER_REQUISITIONS)
@ViewComponent(value = MainDisplay.class, caption = "My requisitions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.FINANCE, caption = "My Requisitions")
@VaadinFontIcon(VaadinIcons.WARNING)
public class UserRequisitionsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "user-requisitions";
    private MenuBar.MenuItem newRequisitionMenuItem;

    private MainHeader mainHeader;
    private RequisitionsGrid requisitionsGrid;
    private RequisitionCreateForm createForm;
    private UserRequisitionBudgetSelector budgetSelector;
    private MessageBox budgetSelectorWindow;

    public UserRequisitionsView(RequisitionsGrid requisitionsGrid,
                                RequisitionCreateForm createForm,
                                UserRequisitionBudgetSelector budgetSelector,
                                MainHeader mainHeader,
                                User currentUser) {
        this.requisitionsGrid = requisitionsGrid;
        this.createForm = createForm;
        this.mainHeader = mainHeader;
        this.budgetSelector = budgetSelector;

        setSizeFull();
        setMargin(false);
        requisitionsGrid.setSizeFull();
        requisitionsGrid.filterCreator(currentUser);
        addComponent(requisitionsGrid);

        budgetSelector.setSelectionMode(Grid.SelectionMode.NONE);
        budgetSelector.addItemClickListener(event -> {
            Budget budget = event.getItem();
            budgetSelectorWindow.close();
            showRequisitionCreateWindow(budget);
        });

        budgetSelector.setSizeFull();
    }

    private void selectBudget() {

        if (budgetSelector.getBudgets().size() == 1) {
            //if only one budget was loaded then just prepare requisition for that
            showRequisitionCreateWindow(budgetSelector.getBudgets().stream().findFirst().get());
        } else {
            budgetSelectorWindow = MessageBox.create()
                    .withCaption("Select budget")
                    .withMessage(budgetSelector)
                    .withWidth("80%")
                    .withHeight("70%")
                    .asModal(true);
            budgetSelectorWindow.open();
        }
    }

    private void showRequisitionCreateWindow(Budget budget) {
        Window window = createForm.show(new Requisition(budget));
        window.addCloseListener(e -> {
            requisitionsGrid.getDataProvider().refreshAll();
        });
    }

    @Override
    public void attach() {
        super.attach();

        if (hasAuthority(CREATE_REQUISITION)) {
            newRequisitionMenuItem = mainHeader.getMenuBar().addItemAsFirst(
                    "New Requisition",
                    FontAwesome.PLUS,
                    selectedItem -> selectBudget()
            );
        }

    }

    @Override
    public void detach() {
        super.detach();

        if (hasAuthority(CREATE_REQUISITION))
            mainHeader.getMenuBar().removeItem(newRequisitionMenuItem);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(requisitionsGrid);
    }
}
