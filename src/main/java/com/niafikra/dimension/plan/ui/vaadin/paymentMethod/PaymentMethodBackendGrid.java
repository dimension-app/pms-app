package com.niafikra.dimension.plan.ui.vaadin.paymentMethod;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.niafikra.dimension.plan.domain.PaymentMethod;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 10:47 PM
 */
@SpringComponent
@PrototypeScope
public class PaymentMethodBackendGrid extends NameFilterableBackendGrid<PaymentMethod> {

    public PaymentMethodBackendGrid(PaymentMethodProvider dataProvider) {
        super(dataProvider);
    }
}
