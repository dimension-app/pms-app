package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.plan.domain.Retirement;
import com.niafikra.dimension.plan.service.RetirementService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.SettlementPaymentForm;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.UI;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.RETIRE_EXPENSE;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.RetirementView.VIEW_NAME;

@Secured(PMSPermission.VIEW_RETIREMENT)
@ViewComponent(value = MainDisplay.class, caption = "Retirement")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RetirementView extends MVerticalLayout implements Notifier, View {

    public static final String VIEW_NAME = "retirement";

    @Inject
    private RetirementPanel retirementPanel;
    @Inject
    private SettlementPaymentForm settlementPaymentForm;
    @Inject
    private RetirementService retirementService;
    @Inject
    private ApprovalTrackerService trackerService;
    @Inject
    private Navigator navigator;
    @Inject
    private MainHeader mainHeader;
    @Inject
    private User currentUser;

    private Retirement retirement;


    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        retirementPanel.setSizeFull();
        withComponents(retirementPanel)
                .withExpand(retirementPanel, 1);
    }

    @Secured(PMSPermission.RETIRE_EXPENSE)
    @ViewMenuOption(value = "Edit", menu = "Retirement", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.MONEY_EXCHANGE)
    public void editRetirement() {
        navigator.navigateTo(RetirementUpdateView.class, retirement.getId());
    }

    @Secured(RETIRE_EXPENSE)
    @ViewMenuOption(value = "Submit", menu = "Retirement", icon = VaadinIcons.KEY, menuIcon = VaadinIcons.WARNING)
    public void confirmSubmit() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you wish to submit this retirement?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            retirementService.submit(retirement, currentUser);
                            UI.getCurrent().access(() -> navigator.reload());
                            showSuccess("Submitted retirement for approval successful", retirement.toString());
                        } catch (Exception e) {
                            showError("Failed to submit retirement for approval", e);
                        }
                    }
                }
        );
    }

    @Secured(PMSPermission.SETTLE_RETIREMENT)
    @ViewMenuOption(value = "Settle", menu = "Retirement", icon = VaadinIcons.MONEY, menuIcon = VaadinIcons.MONEY)
    public void showPaymentWindow() {
        settlementPaymentForm.setExpense(retirement.getExpense());
        settlementPaymentForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }

    @Secured(PMSPermission.DELETE_RETIREMENT)
    @ViewMenuOption(value = "Delete", menu = "Retirement", icon = VaadinIcons.TRASH, menuIcon = VaadinIcons.MONEY_EXCHANGE)
    public void confirmDelete() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you wish to delete this retirement?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        try {
                            retirementService.delete(retirement);
                            navigator.navigateBack();
                            showSuccess("Deleted retirement successful", retirement.toString());
                        } catch (Exception e) {
                            showError("Failed to delete retirement", e);
                        }
                    }
                }
        );
    }

    public void setRetirement(Retirement retirement) {
        this.retirement = retirement;
        retirementPanel.setRetirement(retirement);
        mainHeader.getMenuBar().setMenuVisible("Delete", !retirement.getExpense().hasSettlements());
        mainHeader.getMenuBar().setMenuVisible("Edit", !trackerService.isApprovalStarted(retirement));
        mainHeader.getMenuBar().setMenuVisible("Settle", retirement.getExpense().canSettle());
        mainHeader.getMenuBar().setMenuVisible("Submit", !trackerService.isRegistered(retirement));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long requestId = Long.parseLong(event.getParameters());
        setRetirement(retirementService.getRetirement(requestId));
    }

}
