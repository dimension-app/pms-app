package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.google.common.collect.ImmutableMap;
import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.plan.domain.RetirementEntry;
import com.vaadin.data.HasValue;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.renderers.HtmlRenderer;
import org.springframework.context.ApplicationContext;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.impl.form.factory.VerticalCrudFormFactory;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.*;

@PrototypeScope
@SpringComponent
public class RetirementEntryCrudUI extends GridCrud<RetirementEntry> {

    @Inject
    private TemplateEngine templateEngine;

    @Inject
    private ApplicationContext applicationContext;

    private List<RetirementEntry> entries = new LinkedList<>();

    public RetirementEntryCrudUI() {
        super(RetirementEntry.class, new HorizontalSplitCrudLayout());
        setCrudFormFactory(new RetirementEntryCrudFormFactory());
    }

    @PostConstruct
    private void build() {
        setClickRowToUpdate(true);
        getGrid().addColumn(retirementEntry ->
                        templateEngine.process(
                                Templates.RETIREMENT_ENTRY_ROW,
                                new Context(Locale.getDefault(), ImmutableMap.of("entry", retirementEntry, "applicationContext", applicationContext))),
                new HtmlRenderer()
        ).setId("entry").setWidthUndefined().setExpandRatio(1);
        getGrid().setColumns("entry");
        getGrid().setFooterVisible(true);
        getGrid().appendFooterRow();
        getGrid().setBodyRowHeight(100);
        getGrid().addStyleNames(Theme.GRID_LIGHT,Theme.GRID_NO_STRIPES, Theme.GRID_TEXT_SMALL, Theme.TABLE_BORDERLESS, Theme.TABLE_COMPACT);

        setCrudListener(new CrudListener<>() {
            @Override
            public Collection<RetirementEntry> findAll() {
                return entries;
            }

            @Override
            public RetirementEntry add(RetirementEntry entry) {
                entries.add(entry);
                updateFooter();
                return entry;
            }

            @Override
            public RetirementEntry update(RetirementEntry entry) {
                updateFooter();
                return entry;
            }

            @Override
            public void delete(RetirementEntry entry) {
                entries.remove(entry);
                updateFooter();
            }
        });
    }

    private void updateFooter() {
        getGrid().getFooterRow(0).getCell("entry").setText(calculateEntriesTotalAmount().toString());
    }

    private Money calculateEntriesTotalAmount() {
        return entries.stream().map(RetirementEntry::getAmount).reduce(Money.getZERO(), Money::plus);
    }

    public Collection<RetirementEntry> getEntries() {
        return entries;
    }

    public void setEntries(Collection<RetirementEntry> entries) {
        this.entries = new LinkedList<>(entries);
        refreshGrid();

        //initiate click of add button
        addButtonClicked();
    }

    private class RetirementEntryCrudFormFactory extends VerticalCrudFormFactory<RetirementEntry> {

        public RetirementEntryCrudFormFactory() {
            super(RetirementEntry.class);
            setUseBeanValidation(true);

            setFieldProvider("amount", () -> createAmountField());
            setFieldProvider("description", () -> createDescriptionField());
            setFieldProvider("attachments", () -> createAttachmentsField());

            setVisibleProperties("amount", "description", "attachments");
        }

        @Override
        protected List<HasValue> buildFields(CrudOperation operation, RetirementEntry entry, boolean readOnly) {
            if (operation.equals(CrudOperation.ADD)) {
                //initialise time created to differentiate with other entries in grid
                entry.setTimeCreated(LocalDateTime.now());
                entry.setAmount(Money.getZERO());
                entry.setAttachments(new LinkedHashSet<>());
            }

            return super.buildFields(operation, entry, readOnly);
        }

        private HasValue createAttachmentsField() {
            AttachmentsField<Set<Attachment>> attachmentsField = applicationContext.getBean(AttachmentsField.class);
            attachmentsField.setValue(new LinkedHashSet<>());
            return attachmentsField;
        }

        private HasValue createDescriptionField() {
            TextArea description = new TextArea();
            description.setRows(3);
            return description;
        }

        private HasValue createAmountField() {
            return new MoneyField("Amount");
        }


    }
}
