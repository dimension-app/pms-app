package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.category.ui.vaadin.CategorisedItemsManagementPanel;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.Resource;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/8/17 12:36 AM
 */

@Secured({PMSPermission.CONFIGURE_RESOURCES})
@ViewInfo(value = "Resources", section = "Planning", icon = VaadinIcons.COINS)
@ViewComponent(value = SettingDisplay.class, caption = "Resources")
@SpringView(name = ResourcesManagementPanel.VIEW_NAME, ui = MainUI.class)
public class ResourcesManagementPanel extends CategorisedItemsManagementPanel<Resource> implements SettingView, TableExportable {
    public static final String VIEW_NAME = "resources";

    private ResourceCategoryCRUDPanel categoryCRUDPanel;
    private ResourceCreateViewPanel resourceCreateViewPanel;

    public ResourcesManagementPanel(ResourceCategoryCRUDPanel categoryCRUDPanel, ResourceCreateViewPanel resourceCreateViewPanel) {
        super(categoryCRUDPanel, resourceCreateViewPanel);
        this.categoryCRUDPanel = categoryCRUDPanel;
        this.resourceCreateViewPanel = resourceCreateViewPanel;
    }

    @Override
    public TableExport getTableExport() {
        return resourceCreateViewPanel.getTableExport();
    }
}
