package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.plan.domain.Expense;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.ui.vaadin.Theme.TEXT_RIGHT;

@PrototypeScope
@SpringComponent
public class MinimalExpensesGrid extends Grid<Expense> {


    @PostConstruct
    private void build() {

        addColumn(expense -> expense.getPayment().getReference())
                .setId("reference")
                .setCaption("Reference")
                .setSortable(true);

        addColumn(expense -> DateUtils.formatDateTime(expense.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Time")
                .setHidable(true)
                .setSortable(true);

        addColumn(expense -> expense.getPayment().getAmount())
                .setId("amount")
                .setCaption("Amount")
                .setSortable(true)
                .setStyleGenerator(item -> TEXT_RIGHT);

        addColumn(expense -> expense.getPayment().getMethod())
                .setId("paymentMethod")
                .setCaption("Payment")
                .setHidable(true)
                .setSortable(true);
    }


}
