package com.niafikra.dimension.plan.ui.vaadin.budget.execution.allocationAdjustment;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.plan.domain.AllocationAdjustment;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.AllocationAdjustmentService;
import com.niafikra.dimension.plan.service.ResourceService;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.annotations.PropertyId;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;

/**
 * @Author Juma mketto
 * @Date 1/2/19.
 */
@PrototypeScope
@SpringComponent
public class AllocationAdjustmentForm extends AbstractForm<AllocationAdjustment> {
    @PropertyId("resource")
    private ComboBox<Resource> resource = new ComboBox<>("Resource");

    @PropertyId("role")
    private ComboBox<Role> role = new ComboBox<>("Role");

    @PropertyId("proposedAmount")
    private MoneyField proposedAmount = new MoneyField("Proposed Amount");

    @PropertyId("reason")
    private TextArea reason = new TextArea("Reason");

    @Inject
    private AttachmentsField<Set<Attachment>> attachments;

    @Inject
    private AllocationAdjustmentService service;

    @Inject
    private RoleService roleService;

    @Inject
    private ResourceService resourceService;

    @Inject
    private User currentUser;

    private Budget budget;

    public AllocationAdjustmentForm() {
        super(AllocationAdjustment.class);
    }

    private void loadResources() {
        if (budget == null) throw new IllegalStateException("Allocation Adjustment form must have a budget specified");
        Optional<Role> userRole = this.role.getOptionalValue();

        resource.clear();
        if (userRole.isPresent()) {
            Set<Resource> resources = resourceService.getResources(currentUser, budget.getCostCenter());
            resource.setItems(resources);
        }
    }

    @PostConstruct
    private void build() {
        getSaveButton().setWidth("200px");
        addSavedHandler(this::submit);

        setWidth("600px");
        resource.setWidth("100%");
        role.setWidth("100%");
        proposedAmount.setWidth("100%");
        reason.setWidth("100%");
        reason.setRows(4);

        attachments.setWidth("100%");
        attachments.setCaption("Attachments");

        role.addValueChangeListener(event -> loadResources());
    }

    private void submit(AllocationAdjustment adjustment) {

        try {
            service.createSubmit(adjustment,currentUser);
            closePopup();
            showSuccess("Successful submit allocation adjustment request");
        } catch (Exception e) {
            showError("Failed to request allocation adjustment", e);
        }
    }


    @Override
    protected Component createContent() {
        return new MVerticalLayout(role, resource, proposedAmount, reason, attachments, getToolbar()).withFullWidth();
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("600px");
        window.setCaption("Request Allocation adjustment");
        return window;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
        role.setItems(roleService.findAllowedRoles(budget.getCostCenter(), currentUser));
    }
}
