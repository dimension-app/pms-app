package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.service.ResourceService;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 9:10 PM
 */
@SpringComponent
@PrototypeScope
public class ResourcesBackendGrid extends Grid<Resource> {

    private ResourceService.ResourceFilter filter = ResourceService.ResourceFilter.builder().build();

    public ResourcesBackendGrid(ResourceProvider dataProvider) {
        super(dataProvider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {
        addColumn(resource -> resource.getName())
                .setId("name")
                .setCaption("Resource name")
                .setSortable(true);

        HeaderRow headerRow = appendHeaderRow();
        createFilteringTextField(
                headerRow,
                "name",
                "Filter by name",
                event -> {
                    filter.setName(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        getDataProvider().setFilter(filter);
    }

    @Override
    public ConfigurableFilterDataProvider<Resource, Void, ResourceService.ResourceFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Resource, Void, ResourceService.ResourceFilter>) super.getDataProvider();
    }

    public ResourceService.ResourceFilter getFilter() {
        return filter;
    }
}
