package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Retirement;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.RETIRE_EXPENSE;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.UserRetirementsView.VIEW_NAME;

@Secured(RETIRE_EXPENSE)
@ViewComponent(value = MainDisplay.class, caption = "My Retirements")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.FINANCE, caption = "My Retirements")
@VaadinFontIcon(VaadinIcons.MONEY)
public class UserRetirementsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "user-retirements";

    @Inject
    private RetirementsGrid retirementsGrid;
    @Inject
    private Navigator navigator;
    @Inject
    private User currentUser;

    @PostConstruct
    private void build() {

        setSizeFull();
        setMargin(false);
        retirementsGrid.setSizeFull();
        retirementsGrid.getDataProvider().setCreator(currentUser);

        addComponentsAndExpand(retirementsGrid);
        setExpandRatio(retirementsGrid, 1);

        if (SecurityUtils.hasAuthority(PMSPermission.VIEW_RETIREMENT)) {
            retirementsGrid.addContextClickListener(event -> {
                Grid.GridContextClickEvent<Retirement> contextEvent = (Grid.GridContextClickEvent<Retirement>) event;
                showRetirementWindow(contextEvent.getItem());
            });
        }
    }

    private void showRetirementWindow(Retirement retirement) {
        navigator.navigateTo(RetirementView.class, retirement.getId());
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(retirementsGrid);
    }
}