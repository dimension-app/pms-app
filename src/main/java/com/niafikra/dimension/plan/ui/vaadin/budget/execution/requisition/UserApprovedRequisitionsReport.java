package com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.TrackersTable;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.Requisition;
import com.niafikra.dimension.plan.service.RequisitionService;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.requisition.UserApprovedRequisitionsReport.VIEW_NAME;


@Secured(PMSPermission.VIEW_USER_APPROVED_REQUISITIONS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "My Approved Requisitions")
@ViewInfo(icon = VaadinIcons.USER_CHECK, section = "Approval", value = "My Approved Requisitions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class UserApprovedRequisitionsReport extends MVerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "user-approved-requisitions";

    @Inject
    private TrackersTable trackersTable;

    @Inject
    private RequisitionService requisitionService;

    @Inject
    private ApprovalTrackerService trackerService;

    @Inject
    private User user;

    @PostConstruct
    private void build() {

        setSizeFull();
        setMargin(false);
        trackersTable.setSizeFull();
        withComponent(trackersTable).withExpandRatio(trackersTable, 1);


        trackersTable.getFilter().setApprovedUser(user);
        trackersTable.getFilter().setTypeFilter(Requisition.APPROVAL_TYPE);

        trackersTable.addColumn(tracker -> {
            Requisition requisition = requisitionService.getRequisition(tracker);
            return requisition.getBudget();
        }).setId("budget").setCaption("Budget");

        trackersTable.removeColumn("type");

        trackersTable.addColumn(tracker -> isOverriden(tracker) ? "YES" : "NO")
                        .setId("overridden").setCaption("Is Override?");

        trackersTable.setColumnOrder("timeCreated","creator","description","budget","status","overridden","actions");
    }

    // TODO THIS IMPLEMENTATION IS SEPARATED BECAUSE FOR THE PURPOSE OF THIS REPORT IT IS SAFE TO ASSUME THAT
    // TODO WHEN FINDING ONE EVENT FOR USER WHICH WAS OVERRIDEN THEN WE CAN REPORT IT AS OVERRIDEN. NOTE THAT THIS MIGHT NOT BE THE CASE FOR
    // TODO THE LOGIC TO BE GENERAL BECAUSE THE APPROVAL IMPLEMENTATION ALLOWS PRESENCE OF MORE THAN ONE EVENTS FOR THE SAME USER ON A TRACKER
    // TODO OF WHICH ONE CAN BE OVERRIDEN WHILE THE OTHER IS NOT.
    private boolean isOverriden(Tracker tracker) {

        return trackerService.getEvents(tracker).stream().anyMatch(event -> event.getUser().equals(user) && event.isOverrode());
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(trackersTable);
    }
}
