package com.niafikra.dimension.plan.ui.vaadin.budget.planning;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.OpenerButton;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.util.io.ImportButton;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.plan.domain.AllocationProposal;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.AllocationService;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.AbstractBudgetView;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetDisplay;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.UI;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import java.io.InputStream;
import java.util.List;

import static com.niafikra.dimension.plan.ui.vaadin.budget.planning.AllocationsPlanView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/19/17 5:55 PM
 */

@ViewComponent(value = BudgetDisplay.class, caption = "Budget allocations planning")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllocationsPlanView extends AbstractBudgetView implements Notifier, View, TableExportable {
    public static final String VIEW_NAME = "allocations-plan";

    private AllocationPlanningTable planningTable;
    private MButton saveButton;
    private ImportButton importButton;
    private OpenerButton importFormatButton;
    private Navigator navigator;
    private TemplateView budgetSummary;
    private AllocationService allocationService;
    private Budget budget;

    public AllocationsPlanView(
            AllocationPlanningTable planningTable,
            TemplateView budgetSummary,
            BudgetService budgetService,
            AllocationService allocationService,
            Navigator navigator) {
        super(budgetService);
        this.budgetSummary = budgetSummary;
        this.budgetSummary.setTemplatePath(Templates.BUDGET_HEADER);
        this.navigator = navigator;
        this.allocationService = allocationService;
        this.planningTable = planningTable;

        setSizeFull();
        setMargin(new MarginInfo(false, false, true, false));

        budgetSummary.withFullWidth();
        addComponent(budgetSummary);

        planningTable.setSizeFull();
        addComponent(planningTable);
        setExpandRatio(planningTable, 1);

        importFormatButton = new OpenerButton(
                "Download Format",
                VaadinIcons.DOWNLOAD);
        importFormatButton.withWidth("200px");

        importButton = new ImportButton();
        importButton.withIcon(VaadinIcons.UPLOAD)
                .withCaption("Import")
                .withWidth("200px");
        importButton.setImportProcessor(importStream -> importAllocations(importStream));

        saveButton = new MButton(
                VaadinIcons.CHECK_SQUARE,
                "Save Allocations",
                event -> doSubmit()
        ).withWidth("200px");

        MHorizontalLayout footer = new MHorizontalLayout(importFormatButton, importButton, saveButton);
        addComponent(footer);
        setComponentAlignment(footer, Alignment.MIDDLE_CENTER);
    }

    private boolean importAllocations(InputStream importStream) {
        try {
            allocationService.importAllocations(budget, importStream);
            UI.getCurrent().access(() -> navigator.reload());
            showSuccess("Successful imported allocations");
            return true;
        } catch (Exception e) {
            showError("Failed to import allocations", e);
            return false;
        }
    }

    private void doSubmit() {
        try {
            List<AllocationProposal> proposals = planningTable.getAllocations();
            allocationService.propose(proposals);
            UI.getCurrent().access(() -> navigator.reload());
            showSuccess("Successfully submitted allocations", "Total amount is +" + getTotalAllocations());
        } catch (Exception e) {
            showError("Failed to submit proposed allocation", e);
        }

    }

    public Money getTotalAllocations() {
        return planningTable.getTotalAllocations();
    }


    public void setBudget(Budget budget) {
        this.budget = budget;

        if (budget.isApproved())
            throw new IllegalArgumentException("Budget planning view can only process pending approval budgets");
        budgetSummary.putBinding("budget", budget).render();
        planningTable.setBudget(budget);
        importFormatButton.getOpener().setUrl(Budget.generateAllocationsImportUrl(this.budget));
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(planningTable);
    }
}
