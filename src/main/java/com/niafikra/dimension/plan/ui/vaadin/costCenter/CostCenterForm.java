package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:25 PM
 */
@SpringComponent
@PrototypeScope
public class CostCenterForm extends NameDescriptionForm<CostCenter> {

    private CostCenterService costCenterService;

    public CostCenterForm(CostCenterService costCenterService) {
        super(CostCenter.class);
        this.costCenterService = costCenterService;

        addSavedHandler(costCenter -> doSave(costCenter));
    }

    protected void doSave(CostCenter costCenter) {
        try {
            costCenter = costCenterService.save(costCenter);
            setEntity(costCenter);

            String msg = "Saved cost center successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save cost center";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

}
