package com.niafikra.dimension.plan.ui.vaadin.approval;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.grid.GridAddRemoveField;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.group.domain.Group;
import com.niafikra.dimension.group.service.GroupService;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.domain.RequisitionApprovalFlow;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.CostCenterService;
import com.niafikra.dimension.plan.service.RequisitionApprovalFlowService;
import com.niafikra.dimension.plan.service.ResourceService;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.ComboBox;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.plan.ui.vaadin.approval.RequisitionApprovalEntryCRUDView.VIEW_NAME;


@Secured({PMSPermission.CONFIGURE_REQUISITION_APPROVAL_FLOWS})
@ViewComponent(value = SettingDisplay.class, caption = "Requisition Approval Flows")
@ViewInfo(value = "Requisition Approval Flows", section = "Approval", icon = VaadinIcons.STOCK)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RequisitionApprovalEntryCRUDView extends MGridCrudView<RequisitionApprovalFlow> implements SettingView, Notifier {
    public static final String VIEW_NAME = "requisition-approval-flows";

    public static final String[] TABLE_VISIBLE_FIELDS = {"costCenter", "role", "resource", "category"};
    public static final String[] FORM_VISIBLE_FIELDS = {"costCenter", "role", "resource", "category", "levels"};

    private RequisitionApprovalFlowService flowService;
    private GroupService groupService;
    private CostCenterService centerService;
    private RoleService roleService;
    private ResourceService resourceService;
    private CategoryService categoryService;

    public RequisitionApprovalEntryCRUDView(RequisitionApprovalFlowService flowService,
                                            GroupService groupService, CostCenterService centerService,
                                            RoleService roleService, ResourceService resourceService,
                                            CategoryService categoryService) {
        super(RequisitionApprovalFlow.class, new HorizontalSplitCrudLayout());

        this.flowService = flowService;
        this.groupService = groupService;
        this.centerService = centerService;
        this.roleService = roleService;
        this.resourceService = resourceService;
        this.categoryService = categoryService;
    }

    @PostConstruct
    private void build() {
        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);

        getGrid()
                .addColumn(flow -> flow.getLevels().size())
                .setId("levelsCount")
                .setCaption("Levels");

        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("levels", () -> createLevelsSelector());
        getCrudFormFactory().setFieldProvider("costCenter", () -> createCostCenterSelector());
        getCrudFormFactory().setFieldProvider("role", () -> createRoleSelector());
        getCrudFormFactory().setFieldProvider("resource", () -> createResourceSelector());
        getCrudFormFactory().setFieldProvider("category", () -> createCategorySelector());

        setFindAllOperation(() -> flowService.findAll());
        setAddOperation(flow -> doSave(flow));
        setUpdateOperation(flow -> doUpdate(flow));
        setDeleteOperation(flow -> doDelete(flow));
    }

    private HasValue createCategorySelector() {
        ComboBox<Category> categorySelector = new ComboBox<>("Resource", categoryService.getAll(Resource.CATEGORY_TYPE));
        return categorySelector;
    }

    private HasValue createResourceSelector() {
        ComboBox<Resource> resourceSelector = new ComboBox<>("Resource", resourceService.getAll());
        return resourceSelector;
    }

    private HasValue createRoleSelector() {
        ComboBox<Role> roleSelector = new ComboBox<>("Role", roleService.getAll());
        return roleSelector;
    }

    private HasValue createCostCenterSelector() {
        ComboBox<CostCenter> centerSelector = new ComboBox<>("Cost Center", centerService.getAll());
        return centerSelector;
    }

    private HasValue createLevelsSelector() {
        GridAddRemoveField<Group> levelSelector = new GridAddRemoveField<>();
        levelSelector.getAddRemovePanel()
                .getGrid()
                .addColumn(level -> level.toString()).setId("group").setCaption("Level");

        levelSelector.getAddRemovePanel()
                .getGrid()
                .addColumn(level -> levelSelector.getValue().indexOf(level) + 1)
                .setId("stage")
                .setCaption("Stage")
                .setSortable(false)
                .setWidth(100);

        levelSelector.getAddRemovePanel().getSelector().setItems(groupService.getAll());
        levelSelector.getAddRemovePanel().getGrid().setHeightByRows(5);
        return levelSelector;
    }

    private void doDelete(RequisitionApprovalFlow flow) {
        try {
            flowService.delete(flow);
            showSuccess("Deleted approval flow successful");
        } catch (Exception e) {
            showError("Failed to delete approval flow", e);
        }
    }

    private RequisitionApprovalFlow doUpdate(RequisitionApprovalFlow flow) {
        try {
            flow = flowService.update(flow);
            showSuccess("Updated approval flow successful");
            return flow;
        } catch (Exception e) {
            showError("Failed to update approval flow", e);
            return null;
        }
    }

    private RequisitionApprovalFlow doSave(RequisitionApprovalFlow flow) {
        try {
            flow = flowService.create(flow);
            showSuccess("Added new approval flow successful");
            return flow;
        } catch (Exception e) {
            showError("Failed to add new approval flow", e);
            return null;
        }
    }
}
