package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.util.HasLogger;
import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import com.niafikra.dimension.plan.ui.vaadin.resource.ResourceListGrid;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.springframework.util.CollectionUtils;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Set;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/7/17 12:39 AM
 */
@SpringComponent
@PrototypeScope
public class RoleViewPanel extends VerticalLayout implements HasLogger {

    private RoleForm roleForm;
    private RoleUsersManagementPanel usersPanel;
    private ResourceListGrid resourcesGrid;
    private RoleService roleService;
    private Role role;
    private TabSheet tabs;

    private RoleCreateViewPanel createViewPanel;

    public RoleViewPanel(RoleForm roleForm,
                         RoleUsersManagementPanel usersPanel,
                         RoleService roleService,
                         ResourceListGrid resourcesGrid) {
        this.roleForm = roleForm;
        this.usersPanel = usersPanel;
        this.resourcesGrid = resourcesGrid;
        this.roleService = roleService;

        tabs = new TabSheet();

        build();
    }

    private void build() {
        setSizeFull();
        addComponent(tabs);

        tabs.setSizeFull();
        tabs.addTab(roleForm, "Basics");
        roleForm.addSavedHandler(role -> {
            reloadRole();
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();
        });


        usersPanel.setSizeFull();
        tabs.addTab(usersPanel, "Person");
        usersPanel.getSaveButton().addClickListener(event -> reloadRole());

        resourcesGrid.setSizeFull();
        resourcesGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tabs.addTab(resourcesGrid, "Resources");
        resourcesGrid.addSelectionListener(event -> {
            if (!event.isUserOriginated()) return;
            Set<Resource> selectedResources = event.getAllSelectedItems();
            updateResources(selectedResources);
        });
    }

    private void updateResources(Set<Resource> resources) {
        try {
            role = roleService.updateRoleResources(role, resources);
            setRole(role);
            createViewPanel.getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Updated role resources successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to updated role resources";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    private void reloadRole() {
        role = roleService.getRole(role.getId());
        setRole(role);
    }


    public void setRole(Role role) {
        this.role = role;
        roleForm.setEntity(role);
        usersPanel.setRole(role);

        resourcesGrid.deselectAll();
        if (!CollectionUtils.isEmpty(roleService.getResources(role)))
            resourcesGrid.asMultiSelect().select(roleService.getResources(role).stream().toArray(Resource[]::new));
    }

    public void setCreateViewPanel(RoleCreateViewPanel createViewPanel) {
        this.createViewPanel = createViewPanel;
        usersPanel.setRoleCreateViewPanel(createViewPanel);
    }
}
