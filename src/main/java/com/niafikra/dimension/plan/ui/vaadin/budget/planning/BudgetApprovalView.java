package com.niafikra.dimension.plan.ui.vaadin.budget.planning;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetAllocationsView;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MVerticalLayout;

import static com.niafikra.dimension.plan.ui.vaadin.budget.planning.BudgetApprovalView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/24/17 7:02 PM
 */

@Secured(PMSPermission.VIEW_BUDGET_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@ViewComponent(value = ApprovalDisplay.class, caption = "Budget approval")
public class BudgetApprovalView extends MVerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-budget-approval";
    private BudgetAllocationsView budgetAllocationsView;
    private BudgetService budgetService;
    private ApprovalTrackerService trackerService;
    private MButton openBudgetViewButtton;
    private Navigator navigator;
    private Budget budget;

    public BudgetApprovalView(BudgetAllocationsView budgetAllocationsView,
                              BudgetService budgetService,
                              Navigator navigator,
                              ApprovalTrackerService trackerService) {
        this.budgetAllocationsView = budgetAllocationsView;
        this.budgetService = budgetService;
        this.navigator = navigator;
        this.trackerService = trackerService;

        setSizeFull();
        setMargin(new MarginInfo(false, false, true, false));
        budgetAllocationsView.setSizeFull();
        addComponent(budgetAllocationsView);
        setExpandRatio(budgetAllocationsView, 1);

        openBudgetViewButtton = new MButton("Open budget view")
                .withWidth("200px")
                .withListener(event -> openBudgetView());
        openBudgetViewButtton.setVisible(SecurityUtils.hasAuthority(PMSPermission.VIEW_BUDGET_ALLOCATIONS));
        addComponent(openBudgetViewButtton);
        setComponentAlignment(openBudgetViewButtton, Alignment.MIDDLE_CENTER);
    }

    private void openBudgetView() {
        navigator.navigateTo(BudgetAllocationsView.class, budget.getId());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);
        budget = budgetService.getBudget(tracker);
        budgetAllocationsView.setBudget(budget);
    }
}
