package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.plan.domain.Retirement;
import com.niafikra.dimension.plan.service.RetirementService;
import com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.ExpensePanel;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.LinkedHashSet;

public class RetirementEditView extends VerticalLayout implements Notifier {

    @Inject
    protected RetirementService retirementService;

    @Inject
    protected RetirementEntryCrudUI entryCrudUI;

    @Inject
    protected ExpensePanel expensePanel;

    @Inject
    protected User currentUser;

    @Inject
    protected Navigator navigator;

    protected MButton save = new MButton("Save").withWidth("250px");
    protected MButton submit = new MButton("Submit").withStyleName(Theme.BUTTON_PRIMARY).withWidth("250px");
    protected Retirement retirement;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        TabSheet tabs = new TabSheet();

        entryCrudUI.setSizeFull();

        MHorizontalLayout footer = new MHorizontalLayout(save, submit);
        tabs.addTab(
                new MVerticalLayout(entryCrudUI, footer)
                        .withAlign(footer, Alignment.MIDDLE_CENTER)
                        .withExpand(entryCrudUI, 1)
                        .withFullSize()
                        .withMargin(true),
                "Retirement",
                VaadinIcons.MONEY_EXCHANGE
        );

        expensePanel.setSizeFull();
        expensePanel.setMargin(true);
        tabs.addTab(
                new MVerticalLayout(expensePanel).withFullSize().withMargin(true),
                "Expense",
                VaadinIcons.MONEY
        );

        tabs.setSizeFull();
        addComponentsAndExpand(tabs);

        save.addClickListener(() -> doSave());
        submit.addClickListener(() -> doSubmit());
    }

    protected void doSubmit() {
        retirement.setEntries(new LinkedHashSet<>(entryCrudUI.getEntries()));
        try {
            retirement = retirementService.submit(retirement, currentUser);
            previewRetiremet();
            showSuccess("Successfully submitted retirement for approval");
        } catch (Exception e) {
            showError("Failed to submit retirement for approval", e);
        }
    }

    private void previewRetiremet() {
        navigator.navigateTo(RetirementView.class, retirement.getId());
    }

    protected void doSave() {
        retirement.setEntries(new LinkedHashSet<>(entryCrudUI.getEntries()));

        try {
            retirement = retirementService.save(retirement);
            showSuccess("Successfully saved retirement");
            previewRetiremet();
        } catch (Exception e) {
            showError("Failed to save retirement", e);
        }
    }

    public void setRetirement(Retirement retirement) {
        this.retirement = retirement;
        this.entryCrudUI.setEntries(retirement.getEntries());
        expensePanel.setExpense(retirement.getExpense());
    }
}