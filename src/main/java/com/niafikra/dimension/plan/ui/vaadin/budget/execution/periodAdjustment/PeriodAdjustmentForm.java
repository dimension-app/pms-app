package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.PeriodAdjustmentService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class PeriodAdjustmentForm extends AbstractForm<PeriodAdjustment> {

    private DateField proposedEndDate = new DateField("Proposed End Date");
    private TextArea reason = new TextArea("Reason");

    private PeriodAdjustmentService adjustmentService;

    public PeriodAdjustmentForm(PeriodAdjustmentService adjustmentService) {
        super(PeriodAdjustment.class);
        this.adjustmentService = adjustmentService;
    }

    @PostConstruct
    private void build() {
        getSaveButton().setWidth("200px");
        addSavedHandler(periodAdjustment -> doSubmit(periodAdjustment));

        setWidth("600px");
        proposedEndDate.setWidth("100%");
        reason.setWidth("100%");
        reason.setRows(4);
    }

    private void doSubmit(PeriodAdjustment adjustment) {

        try {
            adjustmentService.createSubmit(adjustment);
            closePopup();
            showSuccess("Successful submit period adjustment request");
        } catch (Exception e) {
            showError("Failed to request period adjustment", e);
        }
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(proposedEndDate, reason, getToolbar()).withFullWidth();
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("600px");
        window.setCaption("Request period adjustment");
        return window;
    }

}
