package com.niafikra.dimension.plan.ui.vaadin.costCenter;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableListGrid;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.CostCenterService;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 3:30 PM
 */
@Component
@PrototypeScope
public class CostCenterListGrid extends NameFilterableListGrid<CostCenter> {

    public CostCenterListGrid(CostCenterService centerService) {
        super();
        setItems(centerService.getAll());
    }
}
