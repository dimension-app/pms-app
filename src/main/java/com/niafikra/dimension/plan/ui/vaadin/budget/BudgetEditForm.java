package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.domain.CostCenter;
import com.niafikra.dimension.plan.service.BudgetService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/19/17 3:35 PM
 */
@SpringComponent
@PrototypeScope
public class BudgetEditForm extends AbstractForm<Budget> {

    public static final String FIELD_WIDTH = "400px";
    private MTextField title = new MTextField("Title");
    private MoneyField fund = new MoneyField("Allocated Funds");
    private MoneyField cost = new MoneyField("PO Value");
    private TextArea description = new TextArea("Description");
    private LabelField<LocalDate> startDate = new LabelField<>("Start date");
    private LabelField<LocalDate> endDate = new LabelField<>("End Date");
    private LabelField<CostCenter> costCenter = new LabelField<>("Cost center");
    private CheckBox allowExpensesWithLessBalance = new CheckBox("Can allow expenses even with less balance");
    private CheckBox allowRequestWithLessBalance = new CheckBox("Can allow requests even with less balance");
    private CheckBox allowRequestWithLessGrossBalance = new CheckBox("Can allow request even with less gross balance");
    private CheckBox allowRequestWithSimilarPending = new CheckBox("Can allow new request while similar is pending approval");
    private AttachmentsField<Set<Attachment>> attachments;
    private BudgetService budgetService;

    public BudgetEditForm(AttachmentsField<Set<Attachment>> attachments, BudgetService budgetService) {
        super(Budget.class);
        this.attachments = attachments;
        this.budgetService = budgetService;
    }

    @PostConstruct
    private void build() {
        title.setWidth(FIELD_WIDTH);
        fund.setWidth(FIELD_WIDTH);
        cost.setWidth(FIELD_WIDTH);
        startDate.setWidth("100%");
        endDate.setWidth("100%");
        attachments.setCaption("Attachments");
        attachments.setWidth(FIELD_WIDTH);

        costCenter.setWidth(FIELD_WIDTH);
        description.setWidth(FIELD_WIDTH);

        addSavedHandler(budget -> doSave(budget));
    }


    private void doSave(Budget budget) {
        budget = budgetService.update(budget);
        closePopup();
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Update Budget");
        window.setWidth("600px");
        return window;
    }

    @Override
    protected Component createContent() {
        HorizontalLayout toolbar = getToolbar();

        return new MFormLayout(
                costCenter,
                new MHorizontalLayout(startDate, endDate).withWidth(FIELD_WIDTH).withCaption("Period"),
                title,
                cost,
                fund,
                description,
                attachments,
                allowExpensesWithLessBalance,
                allowRequestWithLessBalance,
                allowRequestWithLessGrossBalance,
                allowRequestWithSimilarPending,
                toolbar).withMargin(true);
    }
}