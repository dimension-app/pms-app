package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableListGrid;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 7:38 PM
 */
@Component
@PrototypeScope
public class RoleListGrid extends NameFilterableListGrid<Role> {

    public RoleListGrid(RoleService roleService) {
        super();
        setItems(roleService.getAll());
    }
}
