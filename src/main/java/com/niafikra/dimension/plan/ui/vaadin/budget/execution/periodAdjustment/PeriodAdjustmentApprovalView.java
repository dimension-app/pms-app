package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.PeriodAdjustmentService;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAnyAuthority;
import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment.PeriodAdjustmentApprovalView.VIEW_NAME;


@Secured(PMSPermission.VIEW_PERIOD_ADJUSTMENT_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME)
@ViewComponent(value = ApprovalDisplay.class, caption = "Period adjustment approval")
public class PeriodAdjustmentApprovalView extends VerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-periodadjustment-approval";
    private PeriodEndDateAdjustmentForm periodEndDateAdjustmentForm;
    private PeriodAdjustmentPanel adjustmentPanel;
    private ApprovalTrackerService trackerService;
    private PeriodAdjustmentService periodAdjustmentService;
    private Navigator navigator;
    private User currentUser;

    public PeriodAdjustmentApprovalView(PeriodAdjustmentPanel adjustmentPanel,
                                        PeriodEndDateAdjustmentForm periodEndDateAdjustmentForm,
                                        Navigator navigator,
                                        User currentUser,
                                        ApprovalTrackerService trackerService,
                                        PeriodAdjustmentService periodAdjustmentService) {
        this.adjustmentPanel = adjustmentPanel;
        this.navigator = navigator;
        this.currentUser = currentUser;
        this.trackerService = trackerService;
        this.periodAdjustmentService = periodAdjustmentService;
        this.periodEndDateAdjustmentForm = periodEndDateAdjustmentForm;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        addComponent(adjustmentPanel);
        setSpacing(true);

        periodEndDateAdjustmentForm.setMargin(true);
        addComponent(periodEndDateAdjustmentForm);
        periodEndDateAdjustmentForm.getSubmitButton().addClickListener(() -> UI.getCurrent().access(() -> navigator.reload()));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);
        PeriodAdjustment periodAdjustment = periodAdjustmentService.findAdjustment(tracker);
        adjustmentPanel.setPeriodAdjustment(periodAdjustment);
        periodEndDateAdjustmentForm.setPeriodAdjustment(periodAdjustment);

        if (!periodAdjustment.isApproved()) {
            periodEndDateAdjustmentForm.setVisible(
                    trackerService.canApprove(tracker,currentUser) || hasAnyAuthority(PMSPermission.OVERRIDE_APPROVE_REQUEST, PMSPermission.OVERRIDE_DECLINE_REQUEST)
            );
        } else periodEndDateAdjustmentForm.setVisible(false);
    }
}
