package com.niafikra.dimension.plan.ui.vaadin.budget;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.plan.domain.Allocation;
import com.vaadin.ui.VerticalLayout;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Component
@PrototypeScope
public class AllocationPanel extends VerticalLayout {

    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        addComponentsAndExpand(templatePanel);
        templatePanel
                .getTemplateView()
                .setTemplatePath(Templates.ALLOCATION);
    }

    public AllocationPanel setAllocation(Allocation allocation) {
        templatePanel
                .getTemplateView()
                .putBinding("allocation", allocation).render();
        return this;
    }
}
