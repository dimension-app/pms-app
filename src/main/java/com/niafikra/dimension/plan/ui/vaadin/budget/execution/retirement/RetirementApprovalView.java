package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalDisplay;
import com.niafikra.dimension.approval.ui.vaadin.tracker.ApprovalView;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Retirement;
import com.niafikra.dimension.plan.service.RetirementService;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MPanel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.RetirementApprovalView.VIEW_NAME;


/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/24/17 7:02 PM
 */

@Secured(PMSPermission.VIEW_RETIREMENT_APPROVAL_REQUEST)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@ViewComponent(value = ApprovalDisplay.class, caption = "Retirement approval")
public class RetirementApprovalView extends MVerticalLayout implements ApprovalView {
    public static final String VIEW_NAME = "ap-retirement-approval";

    @Inject
    private RetirementEntriesAcceptanceGrid acceptanceGrid;
    @Inject
    private TemplateView templateView;
    @Inject
    private RetirementService retirementService;
    @Inject
    private ApprovalTrackerService trackerService;
    private Retirement retirement;

    @PostConstruct
    private void build() {
        setSizeFull();

        acceptanceGrid.addStyleNames(Theme.TABLE_BORDERLESS);
        acceptanceGrid.setWidth("100%");
        templateView.setTemplatePath(Templates.EXPENSE);
        MPanel basePanel = new MPanel(new MVerticalLayout(acceptanceGrid, templateView).withFullWidth().withExpand(templateView, 1));
        basePanel.setSizeFull();
        basePanel.addStyleNames(Theme.PANEL_BORDERLESS);
        add(basePanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);
        Retirement retirement = retirementService.getRetirement(tracker);
        setRetirement(retirement);
    }

    private void setRetirement(Retirement retirement) {
        this.retirement = retirement;

        acceptanceGrid.setRetirement(retirement);
        acceptanceGrid.setHeightByRows(retirement.getEntries().size());
        templateView.putBinding("expense", retirement.getExpense()).render();
    }

    @Override
    public void onApprove() {
        updateAcceptedEntries();
    }

    @Override
    public void onOverrideApprove() {
        updateAcceptedEntries();
    }

    private void updateAcceptedEntries() {
        retirementService.acceptRetirementEntries(retirement, retirement.getAcceptedEntries());
    }
}
