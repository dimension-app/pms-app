package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;


import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.PeriodAdjustmentService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.MButton;

import java.time.LocalDate;

@PrototypeScope
@SpringComponent
public class PeriodEndDateAdjustmentForm extends HorizontalLayout implements Notifier {

    private DateField proposedEndDate = new DateField();
    private MButton submitButton = new MButton("Save changes");

    private PeriodAdjustment periodAdjustment;
    private PeriodAdjustmentService adjustmentService;

    public PeriodEndDateAdjustmentForm(PeriodAdjustmentService adjustmentService) {
        this.adjustmentService = adjustmentService;

        setCaption("Propose a different end date");
        addComponents(proposedEndDate, submitButton);
        submitButton.addClickListener(() -> submitChanges());
    }

    private void submitChanges() {
        LocalDate date = proposedEndDate.getValue();
        if (date == null) {
            showWarning("Please specify a new end date to adjust");
            return;
        }

        try {
            adjustmentService.updateProposedDate(periodAdjustment, date);
            showSuccess("Successful changed End Date for period adjustment" + periodAdjustment.getBudget());
        } catch (Exception e) {
            showError("Failed to request Change on End Date for period adjustment", e.toString());
        }
    }

    public DateField getProposedEndDate() {
        return proposedEndDate;
    }

    public MButton getSubmitButton() {
        return submitButton;
    }

    public PeriodAdjustment getPeriodAdjustment() {
        return periodAdjustment;
    }

    public void setPeriodAdjustment(PeriodAdjustment periodAdjustment) {
        this.periodAdjustment = periodAdjustment;
        proposedEndDate.setValue(periodAdjustment.getProposedEndDate());
    }

}
