package com.niafikra.dimension.plan.ui.vaadin.budget.execution.periodAdjustment;

import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.plan.domain.PeriodAdjustment;
import com.niafikra.dimension.plan.service.PeriodAdjustmentService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.MSize;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class PeriodAdjustmentsGrid extends Grid<PeriodAdjustment> {

    private PeriodAdjustmentPanel adjustmentPanel;
    private PeriodAdjustmentService adjustmentService;
    private PeriodAdjustmentProvider adjustmentProvider;

    public PeriodAdjustmentsGrid(PeriodAdjustmentPanel adjustmentPanel,
                                 PeriodAdjustmentProvider adjustmentProvider,
                                 PeriodAdjustmentService adjustmentService) {
        super(adjustmentProvider);
        this.adjustmentPanel = adjustmentPanel;
        this.adjustmentService = adjustmentService;
        this.adjustmentProvider = adjustmentProvider;
    }

    @PostConstruct
    private void build() {

        setSelectionMode(SelectionMode.NONE);

        addColumn(periodAdjustment -> periodAdjustment.getTimeCreated())
                .setId("timeCreated")
                .setCaption("Created On")
                .setSortable(true);

        addColumn(periodAdjustment -> periodAdjustment.getCreator())
                .setId("creator")
                .setCaption("Created by")
                .setSortable(true);

        addColumn(periodAdjustment -> periodAdjustment.getReason())
                .setId("reason")
                .setCaption("Reason")
                .setWidth(400);

        addColumn(periodAdjustment -> generateStatus(periodAdjustment))
                .setId("status")
                .setCaption("Status")
                .setSortable(false);

        addItemClickListener(event -> showAdjustment(event.getItem()));



    }

    private void showAdjustment(PeriodAdjustment adjustment) {
        adjustmentPanel.setPeriodAdjustment(adjustment);
        new SubWindow(adjustmentPanel)
                .withCaption(adjustment.toString())
                .show()
                .withSize(MSize.size("60%", "70%"));
    }

    private String generateStatus(PeriodAdjustment periodAdjustment) {
        return adjustmentService.generateStatus(periodAdjustment);
    }

    public PeriodAdjustmentProvider getDataProvider() {
        return (PeriodAdjustmentProvider) super.getDataProvider();
    }
}
