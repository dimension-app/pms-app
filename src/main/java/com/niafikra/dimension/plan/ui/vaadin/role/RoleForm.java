package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 9:18 PM
 */
@Component
@PrototypeScope
public class RoleForm extends NameDescriptionForm<Role> {

    private RoleService roleService;

    public RoleForm(RoleService roleService) {
        super(Role.class);
        this.roleService = roleService;
        addSavedHandler(entity -> doSave(entity));
    }

    private void doSave(Role role) {
        try {
            role = roleService.save(role);
            setEntity(role);


            String msg = "Saved role successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save role";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }
}
