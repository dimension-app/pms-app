package com.niafikra.dimension.plan.ui.vaadin.resource;

import com.niafikra.dimension.plan.domain.Resource;
import com.niafikra.dimension.plan.service.ResourceService;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/5/17 9:48 PM
 */
@Component
@PrototypeScope
public class ResourceListGrid extends Grid<Resource> {

    private TextField nameFilter, categoryFilter;

    public ResourceListGrid(ResourceService resourceService) {
        super(Resource.class);
        setItems(resourceService.getAll());
        setColumns("name", "category");

        buildFilters();
    }

    private void buildFilters() {
        HeaderRow headerRow = appendHeaderRow();

        nameFilter = createFilteringTextField(
                headerRow,
                "name",
                "Type name to filter",
                event -> applyFilters()
        );

        categoryFilter = createFilteringTextField(
                headerRow,
                "category",
                "Type category to filter",
                event -> applyFilters()
        );

    }

    private void applyFilters() {
        getListDataProvider().clearFilters();
        if (!StringUtils.isEmpty(nameFilter.getValue())) {
            getListDataProvider().addFilter(resource -> resource.getName().toLowerCase().contains(nameFilter.getValue().toLowerCase()));
        }

        if (!StringUtils.isEmpty(categoryFilter.getValue())) {
            getListDataProvider().addFilter(resource -> resource.getCategory().getName().toLowerCase().contains(categoryFilter.getValue().toLowerCase()));
        }
    }

    public ListDataProvider<Resource> getListDataProvider() {
        return (ListDataProvider<Resource>) super.getDataProvider();
    }
}
