package com.niafikra.dimension.plan.ui.vaadin.role;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.create.SplitCreateViewPanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.plan.domain.Role;
import com.niafikra.dimension.plan.service.RoleService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/26/17 8:46 PM
 */

@Secured({PMSPermission.CONFIGURE_ROLES})
@ViewComponent(value = SettingDisplay.class, caption = "Roles")
@ViewInfo(value = "Roles", section = "Planning", icon = VaadinIcons.USER_STAR)
@SpringView(name = RoleCreateViewPanel.VIEW_NAME, ui = MainUI.class)
public class RoleCreateViewPanel extends SplitCreateViewPanel<Role> implements SettingView {
    public static final String VIEW_NAME = "roles";

    private RoleForm roleForm;
    private RoleViewPanel roleViewPanel;
    private RoleService roleService;

    public RoleCreateViewPanel(
            RolesBackendGrid entriesGrid,
            RoleForm createForm,
            RoleViewPanel viewPanel,
            RoleService roleService) {
        super(entriesGrid);
        this.roleForm = createForm;
        this.roleViewPanel = viewPanel;
        this.roleService = roleService;
    }

    @PostConstruct
    protected void build() {
        setSizeFull();
        setMargin(false);
        VaadinUtils.addRemoveButton(getEntriesGrid(), this::onDelete);
        roleForm.addSavedHandler(role -> resetView());
    }

    private void onDelete(Role role) {
        try {
            roleService.delete(role);
            getEntriesGrid().getDataProvider().refreshAll();

            String msg = "Deleted role successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to delete role";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }


    @Override
    protected Component getCreateComponent() {
        roleForm.setEntity(new Role());
        return roleForm;
    }

    @Override
    protected Component getViewComponent(Role role) {
        roleViewPanel.setRole(role);
        roleViewPanel.setCreateViewPanel(this);
        return roleViewPanel;
    }

    @Override
    public RolesBackendGrid getEntriesGrid() {
        return (RolesBackendGrid) super.getEntriesGrid();
    }
}
