package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.core.domain.Printable;
import com.niafikra.dimension.plan.domain.Payment;

public class PaymentPrintable {

    private PaymentPrintable(){}

    public static Printable create(Payment payment){
        return () -> String.format("/payment/print/%d", payment.getId());
    }
}
