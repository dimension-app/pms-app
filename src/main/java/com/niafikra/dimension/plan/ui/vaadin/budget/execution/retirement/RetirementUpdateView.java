package com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.service.RetirementService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.inject.Inject;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.retirement.RetirementUpdateView.VIEW_NAME;


@Secured(PMSPermission.RETIRE_EXPENSE)
@ViewComponent(value = MainDisplay.class, caption = "Update retirement")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RetirementUpdateView extends RetirementEditView implements View {
    public static final String VIEW_NAME = "update-retirement";

    @Inject
    private RetirementService retirementService;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long retirementId = Long.parseLong(event.getParameters());
        setRetirement(retirementService.getRetirement(retirementId));
    }
}
