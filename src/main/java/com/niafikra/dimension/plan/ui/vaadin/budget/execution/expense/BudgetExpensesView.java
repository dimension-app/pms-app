package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.plan.domain.Budget;
import com.niafikra.dimension.plan.service.BudgetService;
import com.niafikra.dimension.plan.ui.vaadin.budget.AbstractBudgetView;
import com.niafikra.dimension.plan.ui.vaadin.budget.BudgetDisplay;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.BudgetExpensesView.VIEW_NAME;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 10/1/17 7:01 PM
 */

@Secured(PMSPermission.VIEW_BUDGET_EXPENSES)
@ViewComponent(value = BudgetDisplay.class, caption = "Budget expenses")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class BudgetExpensesView extends AbstractBudgetView implements TableExportable {
    public static final String VIEW_NAME = "budgetexpenses";

    private TemplateView budgetSummary;
    private ExpensesGrid expensesGrid;

    private Budget budget;

    public BudgetExpensesView(
            TemplateView budgetSummary,
            ExpensesGrid expensesGrid,
            BudgetService budgetService) {
        super(budgetService);
        this.budgetSummary = budgetSummary;
        this.budgetSummary.setTemplatePath(Templates.BUDGET_HEADER);
        this.expensesGrid = expensesGrid;
    }


    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        setSpacing(false);

        budgetSummary.setWidth("100%");
        addComponent(budgetSummary);
        expensesGrid.setSizeFull();
        addComponent(expensesGrid);
        setExpandRatio(expensesGrid, 1);
    }

    @Override
    public void setBudget(Budget budget) {
        this.budget = budget;

        budgetSummary.putBinding("budget", budget).render();
        expensesGrid.getExpenseFilter().setBudget(budget);
        expensesGrid.getDataProvider().refreshAll();
    }


    @Override
    public TableExport getTableExport() {
        return new ExcelExport(expensesGrid);
    }
}
