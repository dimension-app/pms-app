package com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.plan.ui.vaadin.budget.execution.expense.AllExpensesReport.VIEW_NAME;

@Secured(PMSPermission.VIEW_ALL_EXPENSES_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "All expenses")
@ViewInfo(icon = VaadinIcons.MONEY, section = "Expenditure", value = "All Expenses")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class AllExpensesReport extends VerticalLayout implements ReportView, TableExportable {
    public static final String VIEW_NAME = "all-expenses";

    private ExpensesGrid expensesGrid;

    public AllExpensesReport(ExpensesGrid expensesGrid) {
        this.expensesGrid = expensesGrid;

        setSizeFull();
        setMargin(false);
        expensesGrid.setSizeFull();
        addComponent(expensesGrid);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(expensesGrid);
    }
}
