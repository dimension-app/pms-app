package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.HasCategory;
import com.niafikra.dimension.core.ui.vaadin.util.create.NameFilterableBackendGrid;
import com.niafikra.dimension.core.ui.vaadin.util.provider.NameCategoryFilterableDataProvider;
import com.niafikra.dimension.core.util.HasName;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/6/17 9:17 PM
 */
public class CategorisedBackendGrid<T extends HasName & HasCategory> extends NameFilterableBackendGrid<T> {

    public CategorisedBackendGrid(NameCategoryFilterableDataProvider<T> dataProvider) {
        super(dataProvider);
        addColumn(t -> t.getCategory())
                .setId("category")
                .setCaption("Category")
                .setSortable(true);
    }

    @Override
    public NameCategoryFilterableDataProvider<T> getDataProvider() {
        return (NameCategoryFilterableDataProvider<T>) super.getDataProvider();
    }

}
