package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;
import com.vaadin.ui.Component;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 9/8/17 8:01 PM
 */
public interface HasCategoryList<T extends HasCategory> extends Component {

    void filterCategory(Category category);
}
