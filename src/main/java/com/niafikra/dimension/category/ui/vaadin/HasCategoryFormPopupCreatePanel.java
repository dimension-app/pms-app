package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;
import com.niafikra.dimension.core.ui.vaadin.util.create.FormPopupCreateViewPanel;
import com.niafikra.dimension.core.util.HasName;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/29/17 11:40 PM
 */

public abstract class HasCategoryFormPopupCreatePanel<T extends HasCategory & HasName> extends FormPopupCreateViewPanel<T> implements HasCategoryList<T> {

    private Category category;

    public HasCategoryFormPopupCreatePanel(Grid<T> categorisedsGrid,
                                           NameDescriptionCategoryForm<T> categorisedForm) {
        super(categorisedsGrid, categorisedForm);
    }

    @Override
    protected T createNewEntity() {
        return createCategorisedEntity(category);
    }

    protected abstract T createCategorisedEntity(Category category);


    @Override
    protected Component getCreateComponent() {
        getForm().loadCategories();
        return super.getCreateComponent();
    }

    @Override
    protected Component getViewComponent(T entry) {
        getForm().loadCategories();
        return super.getViewComponent(entry);
    }

    @Override
    public NameDescriptionCategoryForm<T> getForm() {
        return (NameDescriptionCategoryForm<T>) super.getForm();
    }

}
