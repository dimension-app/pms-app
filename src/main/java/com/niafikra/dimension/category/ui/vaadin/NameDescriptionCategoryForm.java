package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.util.HasName;
import com.vaadin.ui.ComboBox;
import org.vaadin.viritin.layouts.MVerticalLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/31/17 10:31 AM
 */
public class NameDescriptionCategoryForm<T extends HasName & HasCategory> extends NameDescriptionForm<T> {

    protected ComboBox<Category> category = new ComboBox<>("Category");

    private CategoryService categoryService;
    private String categoryType;

    public NameDescriptionCategoryForm(Class<T> type, String categoryType, CategoryService categoryService) {
        super(type);
        category.setWidth("100%");
        this.categoryType = categoryType;
        this.categoryService = categoryService;
    }

    public void loadCategories() {
        category.setItems(categoryService.getLeaves(categoryType));
    }

    @Override
    protected MVerticalLayout createFormFields() {
        loadCategories();
        return new MVerticalLayout(name, description, category);
    }
}
