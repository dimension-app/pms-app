package com.niafikra.dimension.category.ui.vaadin;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.NameDescriptionForm;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.vaadin.annotations.PropertyId;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ComponentContainer;
import org.vaadin.viritin.layouts.MFormLayout;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/30/17 4:27 PM
 */

public class CategoryForm extends NameDescriptionForm<Category> {

    @PropertyId("parent")
    private ComboBox<Category> parentCategorySelector = new ComboBox<>("Parent");
    private CategoryService categoryService;
    private String categoryType;

    public CategoryForm(String type, CategoryService categoryService) {
        super(Category.class);
        this.categoryService = categoryService;
        this.categoryType = type;
        parentCategorySelector.setWidth("100%");

        addSavedHandler(entity -> doSave(entity));
        reloadParentCategoriesItems();
    }

    private void doSave(Category category) {
        try {
            categoryService.save(category);
            String msg = "Saved category successful";
            getLogger().info(msg);
            NotificationUtils.showSuccess(msg);
        } catch (Exception e) {
            String msg = "Failed to save category";
            getLogger().error(msg, e);
            NotificationUtils.showError(msg);
        }
    }

    public void reloadParentCategoriesItems() {
        parentCategorySelector.setItems(categoryService.getAll(categoryType));
    }

    @Override
    protected ComponentContainer createFormFields() {
        return new MFormLayout(name, description, parentCategorySelector).withMargin(false);
    }

}
