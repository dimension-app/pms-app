package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import javax.annotation.PostConstruct;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.PendingDispenseRequestEntriesView.VIEW_NAME;


@RequiredArgsConstructor
@Secured(PMSPermission.VIEW_PENDING_DISPENSE_INVENTORY_REQUESTS)
@ViewComponent(value = MainDisplay.class, caption = "Pending Dispense")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "Pending Dispense",order = 3)
@VaadinFontIcon(VaadinIcons.SPINNER)
public class PendingDispenseRequestEntriesView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "pending-dispense-inventory-requisition-entries";

    private final Navigator navigator;
    private final RequestEntriesGrid entriesGrid;

    @PostConstruct
    private void build(){
        setSizeFull();
        setMargin(false);

        entriesGrid.getDataFilter().setApproved(true);
        entriesGrid.getDataFilter().setFulFilled(false);
        entriesGrid.getDataProvider().refreshAll();
        entriesGrid.setSizeFull();

        if (SecurityUtils.hasAuthority(PMSPermission.DISPENSE_INVENTORY_REQUEST)) {
            entriesGrid.addItemClickListener(event -> {
                entriesGrid.showDispenseWindow(event.getItem());
            });
        }

        addComponent(entriesGrid);
    }

}
