package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import javax.annotation.PostConstruct;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.UserRequestEntriesView.VIEW_NAME;


@RequiredArgsConstructor
@Secured(PMSPermission.VIEW_USER_INVENTORY_REQUISITION_ENTRIES)
@ViewComponent(value = MainDisplay.class, caption = "My Requests")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "My Requests",order = 2)
@VaadinFontIcon(VaadinIcons.LIST_OL)
public class UserRequestEntriesView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "user-inventory-requisition-entries";

    private final Navigator navigator;
    private final RequestEntriesGrid entriesGrid;
    private final User currentUser;

    @PostConstruct
    private void build(){
        setSizeFull();
        setMargin(false);

        entriesGrid.removeColumn("requisition.creator");
        entriesGrid.getDataFilter().setCreator(currentUser);
        entriesGrid.getDataProvider().refreshAll();
        entriesGrid.setSizeFull();
        entriesGrid.addItemClickListener(event -> {
            navigator.navigateTo(RequestView.class, event.getItem().getRequisition().getId());
        });

        addComponent(entriesGrid);
    }

    @Secured(PMSPermission.CREATE_INVENTORY_REQUISITION)
    @ViewMenuOption(value = "New Request", icon = VaadinIcons.PLUS_CIRCLE)
    public void newInventoryRequisition() {
        navigator.navigateTo(NewRequestView.class);
    }

}
