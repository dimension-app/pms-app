package com.niafikra.dimension.inventory.ui.vaadin.store;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.StoreService;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

@Component
@PrototypeScope
public class StoreComboBox extends MComboBox<Store> {

    public StoreComboBox(StoresProvider provider) {
        setDataProvider(provider.withConvertedFilter(s -> StoreService.StoreFilter.builder().name(s).build()));
    }
}
