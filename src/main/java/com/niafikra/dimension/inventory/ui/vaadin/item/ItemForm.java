package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.unit.MeasurementUnit;
import com.niafikra.dimension.inventory.service.MeasurementUnitService;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;

public abstract class ItemForm extends AbstractForm<Item> {

    public static final String FIELD_WIDTH = "400px";

    protected MTextField code = new MTextField("Code");
    protected MTextField name = new MTextField("Name");
    protected TextArea description = new TextArea("Description");
    protected ComboBox<MeasurementUnit> baseMeasurementUnit = new ComboBox<>("Base Unit");
    protected MTextField baseUnitUPC = new MTextField("UPC");
    protected ComboBox<Category> category = new ComboBox<>("Category");

    protected CategoryService categoryService;
    private MeasurementUnitService unitService;

    public ItemForm(CategoryService categoryService, MeasurementUnitService unitService) {
        super(Item.class);
        this.categoryService = categoryService;
        this.unitService = unitService;

        code.setWidth(FIELD_WIDTH);
        name.setWidth(FIELD_WIDTH);
        description.setWidth(FIELD_WIDTH);
        description.setRows(5);
        category.setItems(categoryService.getAll(Item.CATEGORY_TYPE));
        category.setWidth(FIELD_WIDTH);
        baseUnitUPC.setWidth(FIELD_WIDTH);
        baseMeasurementUnit.setItems(unitService.findAll());
        baseMeasurementUnit.setWidth(FIELD_WIDTH);
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("600px");
        return window;
    }

    @Override
    protected void bind() {
        super.bind();
    }

    @Override
    protected Component createContent() {
        return new MFormLayout(code, name, category, baseMeasurementUnit, baseUnitUPC, description, getToolbar()).withMargin(true);
    }
}
