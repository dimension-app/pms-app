package com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense;

import com.niafikra.dimension.inventory.service.BatchService;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseBatchData;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.BatchDataField;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class DispenseBatchDataField extends BatchDataField<DispenseBatchData> {

    public DispenseBatchDataField(BatchService batchService) {
        super(DispenseBatchData.class, batchService);
    }
}
