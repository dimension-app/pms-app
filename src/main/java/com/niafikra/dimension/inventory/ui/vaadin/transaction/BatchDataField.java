package com.niafikra.dimension.inventory.ui.vaadin.transaction;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.GridCrudField;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.inventory.domain.Batch;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.BatchService;
import com.niafikra.dimension.inventory.transaction.BatchData;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.vaadin.data.HasValue;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.FooterRow;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.form.impl.form.factory.VerticalCrudFormFactory;
import org.vaadin.crudui.layout.impl.WindowBasedCrudLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;


public abstract class BatchDataField<T extends BatchData> extends GridCrudField<T, Set<T>> {

    protected BatchService batchService;
    protected Item item;
    protected Store store;

    @Inject
    protected TemplateView batchSummaryLabel;

    public BatchDataField(Class<T> type, BatchService batchService) {
        super(type);

        this.batchService = batchService;
    }

    @PostConstruct
    private void build() {
        batchSummaryLabel.setTemplatePath(Templates.BATCH).withFullWidth();
        getGridCrud().setCrudFormFactory(new BatchDataCrudFormFactory());

        getGridCrud().getGrid().setColumns("batchCode", "quantity", "notes");
        getCrudLayout().setWindowCaption(CrudOperation.ADD, "Add new batch entries");
        getCrudLayout().setWindowCaption(CrudOperation.UPDATE, "Update batch entries");

        getGridCrud().getGrid().appendFooterRow();
        addValueChangeListener(event -> {
            updateFooterSummary();
        });
    }

    public Item getItem() {
        return item;
    }

    public BatchDataField setItem(Item item) {
        this.item = item;
        return this;
    }

    public Store getStore() {
        return store;
    }

    public BatchDataField setStore(Store store) {
        this.store = store;
        return this;
    }

    protected void updateFooterSummary() {
        if (item == null) return;

        FooterRow footerRow = getGridFooterRow();

        Quantity total = getValue().stream()
                .map(batchData -> batchData.getQuantity())
                .reduce(item.getBaseUnit().toZeroQuantity(), Quantity::add);
        footerRow.getCell("quantity").setText(total.toString());
    }

    protected FooterRow getGridFooterRow() {
        return getGridCrud().getGrid().getFooterRow(0);
    }

    public WindowBasedCrudLayout getCrudLayout() {
        return (WindowBasedCrudLayout) getGridCrud().getCrudLayout();
    }

    protected class BatchDataCrudFormFactory extends VerticalCrudFormFactory<T> {

        public BatchDataCrudFormFactory() {
            super(BatchDataField.this.getType());

            setUseBeanValidation(true);

            setFieldProvider("batchCode", () -> createBatchSelector());
            setFieldProvider("quantity", () -> createQuantitySelector());
            setFieldProvider("notes", () -> createNotesBox());

            setVisibleProperties("batchCode", "quantity", "notes");

        }

        @Override
        protected List<HasValue> buildFields(CrudOperation operation, T batchData, boolean readOnly) {
            if (operation.equals(CrudOperation.ADD)) {
                batchData.setQuantity(item.getBaseUnit().toZeroQuantity());
                batchService.findBatchCodes(item, store).stream().findFirst().ifPresent(code -> batchData.setBatchCode(code));
            }

            return super.buildFields(operation, batchData, readOnly);
        }


        protected TextArea createNotesBox() {
            TextArea notesBox = new TextArea("Notes");
            return notesBox;
        }

        protected QuantityField createQuantitySelector() {
            return new QuantityField()
                    .withCaption("Quantity")
                    .withFullWidth();
        }

        protected MComboBox<String> createBatchSelector() {
            return new MComboBox<String>()
                    .withCaption("Batch No.")
                    .withFullWidth()
                    .withItems(batchService.findBatchCodes(item, store))
                    .withValueChangeListener(event -> updateBatchSummary(event.getValue()));
        }

        @Override
        public Component buildNewForm(CrudOperation operation,
                                      T batchData,
                                      boolean readOnly,
                                      Button.ClickListener cancelButtonClickListener,
                                      Button.ClickListener operationButtonClickListener) {
            VerticalLayout base = (VerticalLayout) super.buildNewForm(operation, batchData, readOnly, cancelButtonClickListener, operationButtonClickListener);

            base.addComponent(batchSummaryLabel);
            if (!StringUtils.isEmpty(batchData.getBatchCode()))
                updateBatchSummary(batchData.getBatchCode());
            return base;
        }

        protected void updateBatchSummary(String batchCode) {
            batchSummaryLabel.clear();

            if (batchCode != null && item != null && store != null) {
                Optional<Batch> batch = batchService.findBatch(batchCode, item, store);
                if (!batch.isPresent()) return;

                batchSummaryLabel.putBinding("batch", batch.get());
                batchSummaryLabel.render();
            }
        }
    }
}
