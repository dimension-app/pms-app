package com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.user.UserComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.domain.transaction.Dispense;
import com.niafikra.dimension.inventory.service.DispenseService;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseData;
import com.niafikra.dimension.inventory.ui.vaadin.store.UserStoreComboBox;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.themes.ValoTheme;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils.*;
import static com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense.DispenseItemsView.VIEW_NAME;
import static com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense.ItemBatchDispenseData.toDispenseData;
import static java.util.stream.IntStream.range;

@RequiredArgsConstructor
@Secured(PMSPermission.RECEIVE_ITEM)
@ViewComponent(value = MainDisplay.class, caption = "Dispense Items")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "Dispense Items")
@VaadinFontIcon(VaadinIcons.MINUS_SQUARE_LEFT_O)
public class DispenseItemsView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "dispense-inventory-items";

    private final UserStoreComboBox storeSelector;
    private final UserComboBox associatedUserComboBox;
    private final DispenseItemsEditor dispenseItemsEditor;
    private final TemplateView dispensesTemplateView;
    private final DispenseService dispenseService;
    private final User currentUser;

    private DateTimeField timeSelector = new DateTimeField("Time");
    private MButton receiveButton = new MButton("Dispense Items")
            .withStyleName(ValoTheme.BUTTON_PRIMARY)
            .withListener(event -> doSubmit())
            .withWidth("50%");

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(new MarginInfo(false, false, true, false));

        dispensesTemplateView.setTemplatePath(Templates.DISPENSE_DATA_ENTRIES);
        timeSelector.setWidth("300px");
        timeSelector.setValue(LocalDateTime.now());

        storeSelector.setWidth("300px");
        storeSelector.setCaption("Store");
        storeSelector.addValueChangeListener(event -> {
            dispenseItemsEditor.getEntriesGrid().setStore(event.getValue());
        });

        associatedUserComboBox.setWidth("300px");
        associatedUserComboBox.setCaption("Associated User");
        add(new MHorizontalLayout(timeSelector, storeSelector, associatedUserComboBox));

        dispenseItemsEditor.setSizeFull();
        dispenseItemsEditor.setMargin(false);
        add(dispenseItemsEditor);
        setExpandRatio(dispenseItemsEditor, 1);

        add(receiveButton, Alignment.MIDDLE_CENTER);

        initialise();
    }

    private void doSubmit() {
        Store store = storeSelector.getValue();
        LocalDateTime time = timeSelector.getValue();
        User associatedUser = associatedUserComboBox.getValue();

        List<ItemBatchDispenseData> entries = dispenseItemsEditor.getValidEntries();

        if (store == null || time == null || associatedUser == null) {
            showWarning("Please select store, time and associated user first");
            return;
        }

        if (entries.isEmpty()) {
            showWarning("Please specify items to dispense correctly");
            return;
        }

        Map<Item, List<ItemBatchDispenseData>> batchGroups = entries.stream().collect(Collectors.groupingBy(ItemBatchDispenseData::getItem));

        List<DispenseData> dispensesData = batchGroups.keySet()
                .stream()
                .map(item -> toDispenseData(time, store, associatedUser, currentUser, item, batchGroups.get(item)))
                .collect(Collectors.toList());

        dispensesTemplateView.putBinding("dispenses", dispensesData)
                .putBinding("time", time)
                .putBinding("creator", currentUser)
                .putBinding("associatedUser", associatedUser)
                .putBinding("store", store)
                .render();

        MessageBox.create().asModal(true)
                .withCaption("Please confirm the items to be dispensed")
                .withMessage(dispensesTemplateView)
                .withWidth("80%")
                .withOkButton(
                        () -> {
                            try {
                                List<Dispense> dispenses = dispenseService.dispense(dispensesData);
                                dispenseItemsEditor.getEntriesGrid().clear();

                                initialise();
                                showSuccess("Successfully dispensed items from store " + store);
                            } catch (Exception e) {
                                e.printStackTrace();
                                showError("Failed to dispense items from store " + store, e.getMessage());
                            }
                        }, ButtonOption.style(ValoTheme.BUTTON_PRIMARY))
                .withCancelButton()
                .open();
    }

    private void initialise() {
        //initialised with 3 items
        range(0, 3).forEach(
                v -> dispenseItemsEditor
                        .getEntriesGrid()
                        .addEntry(dispenseItemsEditor.getNewItemSupplier().get())
        );
    }
}
