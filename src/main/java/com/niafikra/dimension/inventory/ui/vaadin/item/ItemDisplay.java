package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.menu.MMenuBar;
import com.niafikra.dimension.core.ui.vaadin.view.AbstractMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.transaction.deposit.DepositData;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseData;
import com.niafikra.dimension.inventory.ui.vaadin.item.alternateUnit.ItemAlternateUnitView;
import com.niafikra.dimension.inventory.ui.vaadin.item.batch.ItemBatchesView;
import com.niafikra.dimension.inventory.ui.vaadin.item.stockConfig.ItemStockConfigView;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit.DepositForm;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense.StockDispenseForm;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@DisplayComponent
public class ItemDisplay extends AbstractMainContentDisplay {

    @Inject
    private ItemInfoTemplatePanel itemInfoPanel;

    private MVerticalLayout itemContentHolder;
    @Inject
    private ItemService itemService;
    @Inject
    private ItemUpdateForm itemUpdateForm;
    @Inject
    private Navigator navigator;
    @Inject
    private DepositForm depositForm;
    @Inject
    private StockDispenseForm dispenseForm;

    private MenuBar.MenuItem itemMenu;
    private Item item;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        HorizontalSplitPanel baseSplitPanel = new HorizontalSplitPanel();
        baseSplitPanel.setSizeFull();
        baseSplitPanel.setSplitPosition(35);
        addComponent(baseSplitPanel);
        setExpandRatio(baseSplitPanel, 1);

        baseSplitPanel.setFirstComponent(itemInfoPanel);
        baseSplitPanel.setSecondComponent(createItemViewsPanel());

    }

    @Secured(PMSPermission.EDIT_ITEM)
    @ViewMenuOption(value = "Edit", menu = "Item", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.STOCK)
    public void doEdit() {
        itemUpdateForm.setEntity(getItem());
        itemUpdateForm.openInModalPopup()
                .addCloseListener(e -> {
                    //reload it in the background
                    UI.getCurrent().access(() -> setItem(itemService.getItem(item.getId())));
                });
    }

    @Secured(PMSPermission.DELETE_ITEM)
    @ViewMenuOption(value = "Delete", menu = "Item", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.STOCK, separator = true)
    public void doDelete() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure?",
                dialog -> {
                    if (dialog.isConfirmed()) {
                        itemService.delete(item);
                        navigator.navigateTo(ItemsView.VIEW_NAME);
                    }
                }
        );
    }

    @Secured(PMSPermission.RECEIVE_ITEM)
    @ViewMenuOption(value = "Receive", menu = "Item", icon = VaadinIcons.ARROW_CIRCLE_DOWN_O, menuIcon = VaadinIcons.STOCK)
    public void doReceive() {
        depositForm.setEntity(new DepositData(item));
        depositForm.openInModalPopup();
    }


    @Secured(PMSPermission.DISPENSE_ITEM)
    @ViewMenuOption(value = "Dispense", menu = "Item", icon = VaadinIcons.ARROW_CIRCLE_UP, menuIcon = VaadinIcons.STOCK)
    public void doDispense() {
        dispenseForm.setEntity(new DispenseData(item));
        dispenseForm.openInModalPopup();
    }

    private Component createItemViewsPanel() {
        MMenuBar menuBar = new MMenuBar();
        menuBar.addStyleNames(Theme.MENUBAR_BORDERLESS, Theme.MENUBAR_SMALL);
        menuBar.setWidth("100%");
        menuBar.addItem("Batches", sel -> navigator.navigateTo(ItemBatchesView.class, item.getId()));
        menuBar.addItem("Transactions", sel -> navigator.navigateTo(ItemTransactionsView.class, item.getId()));
        menuBar.addItem("Alt. Units", sel -> navigator.navigateTo(ItemAlternateUnitView.class, item.getId()));
        menuBar.addItem("Stock Config", sel -> navigator.navigateTo(ItemStockConfigView.class, item.getId()));

        itemContentHolder = new MVerticalLayout().withMargin(false).withFullSize();
        return new MVerticalLayout(menuBar, itemContentHolder)
                .withExpand(itemContentHolder, 1)
                .withMargin(new MarginInfo(false, true, true, true))
                .withFullSize();
    }

    @Override
    public void show(View view) {
        itemContentHolder.removeAllComponents();
        Component viewComponent = view.getViewComponent();
        itemContentHolder.with(viewComponent).withExpand(viewComponent, 1);

        String parameters = navigator.getRecentViewChangeEvent().getParameters();
        Long itemId = Long.parseLong(parameters);
        setItem(itemService.getItem(itemId));
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
        itemInfoPanel.setItemInfo(item);
    }


}
