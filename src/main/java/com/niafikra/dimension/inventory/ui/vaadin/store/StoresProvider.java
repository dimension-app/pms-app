package com.niafikra.dimension.inventory.ui.vaadin.store;

import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.StoreService;
import com.niafikra.dimension.inventory.service.StoreService.StoreFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.ArrayList;
import java.util.List;

@SpringComponent
@PrototypeScope
@RequiredArgsConstructor
public class StoresProvider extends PageableDataProvider<Store, StoreFilter> {
    private final StoreService storeService;
    
    @Override
    protected Page<Store> fetchFromBackEnd(Query<Store, StoreFilter> query, Pageable pageable) {
        return storeService.findStores(query.getFilter().orElseGet(() -> StoreFilter.builder().build()),pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("name", SortDirection.ASCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Store, StoreFilter> query) {
        return storeService.countStores(query.getFilter().orElseGet(() -> StoreFilter.builder().build())).intValue();
    }
}
