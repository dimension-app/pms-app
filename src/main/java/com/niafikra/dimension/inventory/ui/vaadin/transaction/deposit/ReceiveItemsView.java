package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.util.io.ExcelImporter;
import com.niafikra.dimension.core.ui.vaadin.util.io.ImportButton;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.util.IOUtils;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.DepositService;
import com.niafikra.dimension.inventory.transaction.deposit.DepositData;
import com.niafikra.dimension.inventory.ui.vaadin.store.UserStoreComboBox;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.themes.ValoTheme;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.button.DownloadButton;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;
import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit.ReceiveItemsView.VIEW_NAME;
import static java.util.stream.IntStream.range;

@RequiredArgsConstructor
@Secured({PMSPermission.RECEIVE_ITEM})
@ViewComponent(value = MainDisplay.class, caption = "Receive Items")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "Receive Items")
@VaadinFontIcon(VaadinIcons.TRUCK)
public class ReceiveItemsView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "receive-inventory-items";

    private final UserStoreComboBox storeSelector;

    private final ReceiveItemsEditor receiveItemsEditor;
    private final TemplateView depositsTemplateView;

    private final DepositService depositService;
    private final User currentUser;
    private final DepositImportFacade importFacade;

    private DateTimeField timeSelector = new DateTimeField("Time");
    MButton receiveButton = new MButton("Receive Items")
            .withStyleName(ValoTheme.BUTTON_FRIENDLY)
            .withListener(event -> doSubmit())
            .withFullWidth();

    DownloadButton downloadImportFormatButton = new DownloadButton()
            .withCaption("Download Template")
            .withIcon(VaadinIcons.DOWNLOAD_ALT)
            .withFullWidth();

    ImportButton importButton = new ImportButton();


    @PostConstruct
    private void build() throws FileNotFoundException {
        setSizeFull();
        setMargin(new MarginInfo(false, false, true, false));

        depositsTemplateView.setTemplatePath(Templates.DEPOSIT_DATA_ENTRIES);

        timeSelector.setWidth("300px");
        timeSelector.setValue(LocalDateTime.now());

        storeSelector.setWidth("300px");
        storeSelector.setCaption("Store");
        add(new MHorizontalLayout(timeSelector, storeSelector));

        receiveItemsEditor.setSizeFull();
        receiveItemsEditor.setMargin(false);
        add(receiveItemsEditor);
        setExpandRatio(receiveItemsEditor, 1);

        downloadImportFormatButton.setFileName("Received Items Template.xls");
        downloadImportFormatButton.setWriter(this::doDownload);

        importButton.withCaption("Import Received Items").withStyleName(ValoTheme.BUTTON_PRIMARY).withWidth("100%");
        importButton.setImportProcessor((ExcelImporter) workbook -> {

            try {
                importFacade.importDepositExcelSheet(workbook.getSheet("DEPOSIT"), timeSelector.getValue(), storeSelector.getValue(), currentUser);
                return true;
            } catch (Exception e) {
                NotificationUtils.showError("Failed to import received items", e.getMessage());
                return false;
            }
        });
        add(new MHorizontalLayout(receiveButton, importButton, downloadImportFormatButton).withWidth("90%"), Alignment.MIDDLE_CENTER);

        initialise();
    }

    private void doDownload(OutputStream stream) {
        String tempaltePath = "templates/inventory/deposit_template.xls";
        try {
            FileInputStream templateStream = IOUtils.readResourceFileInputStream(tempaltePath);
            byte[] templateBytes = org.apache.commons.io.IOUtils.toByteArray(templateStream);
            stream.write(templateBytes);
        } catch (IOException e) {
            NotificationUtils.showError("Failed to download template :" + tempaltePath, e.getMessage());
        }
    }

    private void doSubmit() {
        Store store = storeSelector.getValue();
        LocalDateTime time = timeSelector.getValue();
        List<ItemBatchDepositData> entries = receiveItemsEditor.getValidEntries();

        if (store == null || time == null) {
            NotificationUtils.showWarning("Please select store and time first");
            return;
        }

        if(entries.isEmpty()){
            NotificationUtils.showWarning("Please specify the items received correctly");
            return;
        }

        Map<Item,List<ItemBatchDepositData>> batchGroups = entries.stream()
                .collect(Collectors.groupingBy(ItemBatchDepositData::getItem));

        List<DepositData> depositsData =  batchGroups.keySet().stream()
                .map(item -> ItemBatchDepositData.toDepositData(time,store,currentUser,item,batchGroups.get(item)))
                .collect(Collectors.toList());

        depositsTemplateView.putBinding("deposits",depositsData)
                .putBinding("time",time)
                .putBinding("creator",currentUser)
                .putBinding("store",store)
                .render();

        MessageBox.create()
                .asModal(true)
                .withCaption("Please confirm the items to be deposited")
                .withMessage(depositsTemplateView)
                .withWidth("80%")
                .withOkButton(() -> {
                    try {
                        depositService.deposit(depositsData);
                        receiveItemsEditor.getEntriesGrid().clear();

                        initialise();
                        NotificationUtils.showSuccess("Successfully received items to store " + store);
                    } catch (Exception e) {
                        e.printStackTrace();
                        NotificationUtils.showError("Failed to receive items to store " + store, e.getMessage());
                    }
                }, ButtonOption.style(ValoTheme.BUTTON_PRIMARY))
                .withCancelButton()
                .open();
    }

    private void initialise() {
        //initialised with 3 items
        range(0, 3).forEach(
                v -> receiveItemsEditor
                        .getEntriesGrid()
                        .addEntry(receiveItemsEditor.getNewItemSupplier().get())
        );
    }
}
