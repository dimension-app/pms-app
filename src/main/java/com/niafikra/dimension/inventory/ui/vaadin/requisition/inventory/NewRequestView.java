package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisitionEntry;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.DateField;
import com.vaadin.ui.themes.ValoTheme;
import de.steinwedel.messagebox.ButtonOption;
import de.steinwedel.messagebox.MessageBox;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;
import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.List;
import static com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils.*;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.NewRequestView.VIEW_NAME;
import static java.lang.String.format;
import static java.util.stream.IntStream.range;

@Secured(PMSPermission.CREATE_INVENTORY_REQUISITION)
@ViewComponent(value = MainDisplay.class, caption = "New Request")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "New Request", order = 1)
@VaadinFontIcon(VaadinIcons.PLUS_CIRCLE_O)
@RequiredArgsConstructor
public class NewRequestView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "new-inventory-request";

    private final RequestEntryEditor entriesEditor;
    private final TemplateView requisitionTemplateView;
    private final InventoryRequisitionService requisitionService;
    private final User currentUser;

    private DateField dueDateSelector = new DateField("Due Date");

    private MButton submitButton = new MButton("Submit Request")
            .withStyleName(ValoTheme.BUTTON_PRIMARY)
            .withListener(event -> doSubmit())
            .withWidth("50%");

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(new MarginInfo(false, false, true, false));

        requisitionTemplateView.setTemplatePath(Templates.INVENTORY_REQUISITION_PREVIEW);
        dueDateSelector.setWidth("300px");
        dueDateSelector.setValue(LocalDate.now());

        add(new MHorizontalLayout(dueDateSelector));

        entriesEditor.setSizeFull();
        entriesEditor.setMargin(false);
        add(entriesEditor);
        setExpandRatio(entriesEditor, 1);

        add(submitButton, Alignment.MIDDLE_CENTER);

        initialise();
    }

    private void doSubmit() {
        LocalDate dueDate = dueDateSelector.getValue();

        List<InventoryRequisitionEntry> entries = entriesEditor.getValidEntries();

        if (entries.isEmpty()) {
            showWarning("Please specify items to dispense correctly");
            return;
        }


        requisitionTemplateView.putBinding("entries", entries)
                .putBinding("dueDate", dueDate)
                .putBinding("creator", currentUser)
                .render();

        MessageBox.create().asModal(true)
                .withCaption("Please confirm the items you are about to request")
                .withMessage(requisitionTemplateView)
                .withWidth("80%")
                .withOkButton(
                        () -> {
                            try {
                                InventoryRequisition requisition = requisitionService.request(
                                        new LinkedHashSet<>(entries),
                                        dueDate,
                                        currentUser
                                );

                                entriesEditor.getEntriesGrid().clear();

                                initialise();
                                showSuccess(format("Successfully requested %d items", entries.size()));
                            } catch (Exception e) {
                                e.printStackTrace();
                                showError(format("Failed to request %d items", entries.size()),e.getMessage());
                            }
                        }, ButtonOption.style(ValoTheme.BUTTON_PRIMARY))
                .withCancelButton()
                .open();
    }

    private void initialise() {
        //initialised with 3 items
        range(0, 3).forEach(
                v -> entriesEditor
                        .getEntriesGrid()
                        .addEntry(entriesEditor.getNewItemSupplier().get())
        );
    }
}
