package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.core.ui.vaadin.util.grid.ItemEditGrid;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisitionEntry;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.AbstractValidator;
import com.vaadin.spring.annotation.SpringComponent;
import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class RequestEntriesQuantityEditGrid extends ItemEditGrid<InventoryRequisitionEntry> {

    public RequestEntriesQuantityEditGrid() {
        super(InventoryRequisitionEntry.class);
    }

    @PostConstruct
    private void build() {
        addColumn(key -> getItem(key).getItem())
                .setId("item")
                .setCaption("Item")
                .setExpandRatio(1);

        addColumn(key -> getItem(key).getRequestedQuantity())
                .setId("requestedQuantity")
                .setCaption("Requested Qty")
                .setMaximumWidth(200);

        addColumn(key -> getItem(key).getApprovedQuantity())
                .setId("approvedQuantity")
                .setCaption("Approved Qty")
                .setMaximumWidth(200);

        addComponentColumn(key -> {
            QuantityField quantityField = new QuantityField()
                    .withFullWidth();
            quantityField.getQuantity().setDescription("Enter quantity");

            Item bindedDataItem = getBinder(key).getBean().getItem();
            quantityField.setEnabled(bindedDataItem != null);

            getBinder(key).removeBinding("approvedQuantity");
            getBinder(key).forField(quantityField)
                    .asRequired("Quantity must be specified")
                    .withValidator(new NotLessThanZeroQuantityValidator())
                    .bind("approvedQuantity");
            quantityField.setRequiredIndicatorVisible(false);

            return quantityField;

        }).setCaption("New Quantity")
                .setId("newApprovedQuantity")
                .setSortable(false)
                .setExpandRatio(1);
    }
}
