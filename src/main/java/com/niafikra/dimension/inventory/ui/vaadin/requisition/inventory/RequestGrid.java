package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import lombok.RequiredArgsConstructor;
import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
@RequiredArgsConstructor
public class RequestGrid extends Grid<InventoryRequisition> {

    private final Navigator navigator;

    @PostConstruct
    private void build() {
        addColumn(requisition -> DateUtils.formatDateTime(requisition.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Time Created")
                .setSortable(true)
                .setWidth(100);

        addColumn(requisition -> DateUtils.formatDate(requisition.getDueDate()))
                .setId("dueDate")
                .setCaption("Due Date")
                .setSortable(true);

        addColumn(requisition -> requisition.getEntries().size())
                .setId("itemsCounts")
                .setCaption("Items Count")
                .setSortable(true);


//        HeaderRow filterRow = appendHeaderRow();

        setSelectionMode(SelectionMode.NONE);
        addItemClickListener(event -> {
            InventoryRequisition requisition = event.getItem();
            navigator.navigateTo(RequestView.class, requisition.getId());
        });
    }
}
