package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.inventory.domain.Item;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;

import javax.annotation.PostConstruct;
import java.util.Collection;

public abstract class ItemSelector extends Grid<Item> {

    public ItemSelector() {
        addColumn(item -> item.getName())
                .setId("name")
                .setCaption("Item name")
                .setSortable(true);
        addColumn(item -> item.getDescription())
                .setId("description")
                .setCaption("Description")
                .setSortable(true);
        addColumn(item -> item.getBaseUnit())
                .setId("baseUnit")
                .setCaption("Base Unit")
                .setSortable(true);


    }

    @PostConstruct
    public void load() {
        Collection<Item> items = findItems();
        setItems(items);
    }

    protected abstract Collection<Item> findItems();

    public Collection<Item> getItems() {
        ListDataProvider<Item> provider = (ListDataProvider<Item>) getDataProvider();
        return provider.getItems();
    }
}
