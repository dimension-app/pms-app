package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.vaadin.navigator.ViewChangeListener;
import org.vaadin.crudui.layout.CrudLayout;

public class AbstractItemGridCrud<T> extends MGridCrudView<T> {

    protected ItemService itemService;
    protected Item currentItem;

    public AbstractItemGridCrud(ItemService itemService, Class<T> domainType, CrudLayout crudLayout) {
        super(domainType, crudLayout);
        this.itemService = itemService;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long itemId = Long.parseLong(event.getParameters());
        currentItem = itemService.getItem(itemId);
    }

}
