package com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.grid.ItemEditGrid;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.BatchService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemComboBox;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import org.springframework.context.ApplicationContext;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;

@SpringComponent
@PrototypeScope
public class DispenseItemEditGrid extends ItemEditGrid<ItemBatchDispenseData> {
    @Inject
    private ApplicationContext context;
    @Inject
    private BatchService batchService;
    @Inject
    private ItemService itemService;

    private Store store;

    public DispenseItemEditGrid() {
        super(ItemBatchDispenseData.class);
        setEnabled(false);
    }

    public void setStore(Store store) {
        this.store = store;
        setEnabled(store != null);
    }

    @PostConstruct
    private void build() {
        setBodyRowHeight(70);
        addStyleName("centered-content-grid-70");

        addComponentColumn(key -> {
            ItemComboBox itemSelector = context.getBean(ItemComboBox.class);
            itemSelector.setPlaceholder("Select Item");
            itemSelector.setWidth("100%");

            getBinder(key).removeBinding("item");
            getBinder(key).forField(itemSelector).asRequired("Item must be selected").bind("item");
            itemSelector.setRequiredIndicatorVisible(false);

            Label itemStockBalanceLabel = new Label();
            itemSelector.addValueChangeListener(event -> {
                event.getSource().getOptionalValue().ifPresentOrElse(
                        item -> {
                            Quantity quantity = itemService.calculateStockBalance(item, store);
                            String balanceText = String.format("Balance : %s", quantity);
                            itemStockBalanceLabel.setValue(balanceText);
                        },
                        () -> itemStockBalanceLabel.setValue(null)
                );
            });

            return new MVerticalLayout(itemSelector, itemStockBalanceLabel)
                    .withAlign(itemSelector, Alignment.MIDDLE_CENTER)
                    .withMargin(false)
                    .withFullSize();


        }).setCaption("Item").setId("item").setSortable(false);

        addComponentColumn(key -> {
            MComboBox<String> batchSelector = new MComboBox<String>()
                    .withFullWidth();
            batchSelector.setPlaceholder("Enter the batch code");

            Item bindedDataItem = getBinder(key).getBean().getItem();
            batchSelector.setEnabled(bindedDataItem != null);

            getBinder(key).removeBinding("batchCode");
            getBinder(key).forField(batchSelector).asRequired("Batch must be selected").bind("batchCode");
            batchSelector.setRequiredIndicatorVisible(false);

            getBinder(key).addValueChangeListener(e -> {
                if (ItemComboBox.class.isInstance(e.getSource())) {
                    batchSelector.clear();
                    ItemComboBox itemSelector = ItemComboBox.class.cast(e.getSource());
                    itemSelector.getOptionalValue().ifPresentOrElse(
                            selectedItem -> {
                                batchSelector.setEnabled(true);
                                batchSelector.setItems(batchService.findBatchCodes(selectedItem));
                            },
                            () -> {
                                batchSelector.setEnabled(false);
                                batchSelector.setItems(Collections.EMPTY_LIST);
                            }
                    );
                }
            });


            Label batchStockBalanceLabel = new Label();
            batchSelector.addValueChangeListener(event -> {
                event.getSource().getOptionalValue().ifPresentOrElse(
                        batch -> {
                            Item item = getBinder(key).getBean().getItem();
                            Quantity balance = batchService.findBatch(batch, item, store).map(b -> b.getBalance()).get();
                            String balanceText = String.format("Balance : %s", balance);
                            batchStockBalanceLabel.setValue(balanceText);
                        }, () -> batchStockBalanceLabel.setValue(null)
                );
            });

            return new MVerticalLayout(batchSelector, batchStockBalanceLabel)
                    .withMargin(false)
                    .withFullSize();
        }).setCaption("Batch").setId("batchCode").setSortable(false);

        addComponentColumn(key -> {
            QuantityField quantityField = new QuantityField()
                    .withFullWidth();
            quantityField.getQuantity().setDescription("Enter quantity");

            Item bindedDataItem = getBinder(key).getBean().getItem();
            quantityField.setEnabled(bindedDataItem != null);

            getBinder(key).removeBinding("quantity");
            getBinder(key).forField(quantityField).asRequired("Quantity must be specified").bind("quantity");
            quantityField.setRequiredIndicatorVisible(false);

            getBinder(key).addValueChangeListener(e -> {
                if (ItemComboBox.class.isInstance(e.getSource())) {
                    quantityField.clear();
                    e.getSource().getOptionalValue().ifPresentOrElse(
                            selectedItem -> {
                                quantityField.setValue(((Item) selectedItem).getBaseUnit().toZeroQuantity());
                                quantityField.setEnabled(true);
                            },
                            () -> {
                                quantityField.clear();
                                quantityField.setEnabled(false);
                            }
                    );
                }
            });

//            return new MVerticalLayout(quantityField)
//                    .withAlign(quantityField, Alignment.MIDDLE_CENTER)
//                    .withMargin(new MarginInfo(true,false))
//                    .withFullSize();

            return quantityField;

        }).setCaption("Quantity").setId("quantity").setSortable(false);

        addComponentColumn(key -> {
            TextArea notesField = new TextArea();
            notesField.setWidthFull();
            notesField.setRows(3);
            notesField.setPlaceholder("Enter notes for this deposit transaction");

            getBinder(key).removeBinding("notes");
            getBinder(key).forField(notesField).bind("notes");

            return notesField;
        }).setCaption("Notes").setId("notes").setSortable(false);
    }
}
