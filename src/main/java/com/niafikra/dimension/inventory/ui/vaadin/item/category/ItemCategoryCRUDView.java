package com.niafikra.dimension.inventory.ui.vaadin.item.category;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.inventory.domain.Item;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.inventory.ui.vaadin.item.category.CategoryCRUDView.VIEW_NAME;

@Secured({PMSPermission.CONFIGURE_INVENTORY_CATEGORIES})
@ViewComponent(value = SettingDisplay.class, caption = "Item categories")
@ViewInfo(value = "Categories", section = "Inventory", icon = VaadinIcons.ARCHIVES)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ItemCategoryCRUDView extends CategoryCRUDView implements SettingView, Notifier {

    public ItemCategoryCRUDView(CategoryService categoryService) {
        super(categoryService, Item.CATEGORY_TYPE);
    }

}
