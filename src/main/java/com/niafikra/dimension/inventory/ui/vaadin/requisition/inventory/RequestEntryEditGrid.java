package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.util.grid.ItemEditGrid;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisitionEntry;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemComboBox;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TextArea;
import org.springframework.context.ApplicationContext;
import javax.annotation.PostConstruct;
import java.util.Set;

@SpringComponent
@PrototypeScope
public class RequestEntryEditGrid extends ItemEditGrid<InventoryRequisitionEntry> {

    private ApplicationContext context;

    public RequestEntryEditGrid(ApplicationContext context) {
        super(InventoryRequisitionEntry.class);
        this.context = context;
    }

    @PostConstruct
    private void build() {

        setBodyRowHeight(100);
        addStyleName("centered-content-grid-100");

        addComponentColumn(key -> {
            ItemComboBox itemSelector = context.getBean(ItemComboBox.class);
            itemSelector.setPlaceholder("Select Item");
            itemSelector.setWidth("100%");

            getBinder(key).removeBinding("item");
            getBinder(key).forField(itemSelector).asRequired("Item must be selected").bind("item");
            itemSelector.setRequiredIndicatorVisible(false);

            return itemSelector;
        }).setCaption("Item").setId("item").setSortable(false);


        addComponentColumn(key -> {
            QuantityField quantityField = new QuantityField()
                    .withFullWidth();
            quantityField.getQuantity().setDescription("Enter quantity");

            Item bindedDataItem = getBinder(key).getBean().getItem();
            quantityField.setEnabled(bindedDataItem != null);

            getBinder(key).removeBinding("requestedQuantity");
            getBinder(key).forField(quantityField).asRequired("Quantity must be specified").bind("requestedQuantity");
            quantityField.setRequiredIndicatorVisible(false);

            getBinder(key).addValueChangeListener(e -> {
                if (ItemComboBox.class.isInstance(e.getSource())) {
                    quantityField.clear();
                    e.getSource().getOptionalValue().ifPresentOrElse(
                            selectedItem -> {
                                quantityField.setValue(((Item) selectedItem).getBaseUnit().toZeroQuantity());
                                quantityField.setEnabled(true);
                            },
                            () -> {
                                quantityField.clear();
                                quantityField.setEnabled(false);
                            }
                    );
                }
            });

            return quantityField;

        }).setCaption("Quantity").setId("requestedQuantity").setSortable(false);

        addComponentColumn(key -> {
            TextArea notesField = new TextArea();
            notesField.setWidthFull();
            notesField.setRows(4);
            notesField.setPlaceholder("Enter notes for this deposit transaction");

            getBinder(key).removeBinding("notes");
            getBinder(key).forField(notesField).bind("notes");

            return notesField;
        }).setCaption("Notes").setId("notes").setSortable(false);

        addComponentColumn(key -> {
            AttachmentsField<Set<Attachment>> attachmentsField = context.getBean(AttachmentsField.class);
            attachmentsField.getAttachmentPanel().getAttachmentTable().setHeightByRows(2);
            attachmentsField.getAttachmentPanel().getAttachmentTable().setHeight("70px");
            attachmentsField.getAttachmentPanel().setHeight("100px");
            attachmentsField.setHeight("100px");

            getBinder(key).removeBinding("attachments");
            getBinder(key).forField(attachmentsField).bind("attachments");

            return attachmentsField;
        }).setCaption("Attachments").setId("attachments").setSortable(false);
    }
}
