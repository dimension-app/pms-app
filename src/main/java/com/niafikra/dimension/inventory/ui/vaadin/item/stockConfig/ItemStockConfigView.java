package com.niafikra.dimension.inventory.ui.vaadin.item.stockConfig;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.domain.StockConfig;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.StockConfigService;
import com.niafikra.dimension.inventory.ui.vaadin.QuantityField;
import com.niafikra.dimension.inventory.ui.vaadin.item.AbstractItemGridCrud;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemDisplay;
import com.vaadin.data.HasValue;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.form.impl.form.factory.VerticalCrudFormFactory;
import org.vaadin.crudui.layout.impl.WindowBasedCrudLayout;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

import static com.niafikra.dimension.inventory.ui.vaadin.item.stockConfig.ItemStockConfigView.VIEW_NAME;


@Secured(PMSPermission.CONFIGURE_STOCK)
@ViewComponent(value = ItemDisplay.class, caption = "Item Batches")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ItemStockConfigView extends AbstractItemGridCrud<StockConfig> implements Notifier {
    public static final String VIEW_NAME = "item-stock-config";
    public static final String[] TABLE_VISIBLE_FIELDS = {"store", "minimum", "maximum", "preExpiryNotificationDays"};
    public static final String[] FORM_VISIBLE_FIELDS = {"store", "minimum", "maximum", "preExpiryNotificationDays"};
    private StockConfigService stockConfigService;

    public ItemStockConfigView(StockConfigService stockConfigService, ItemService itemService) {
        super(itemService, StockConfig.class, new WindowBasedCrudLayout());
        this.stockConfigService = stockConfigService;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setCrudFormFactory(new ItemStockCrudFieldFactory());
        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        setFindAllOperation(() -> fetchAll());
        setAddOperation(stockConfig -> doCreate(stockConfig));
        setUpdateOperation(stockConfig -> doUpdate(stockConfig));
        setDeleteOperation(stockConfig -> doDelete(stockConfig));
    }

    private HasValue createStoreSelector() {
        MComboBox<Store> store = new MComboBox<>();
        store.setItems(stockConfigService.findStoresPendingConfiguration(currentItem));
        return store;
    }

    private List<StockConfig> fetchAll() {
        if (currentItem == null) return Collections.emptyList();
        return stockConfigService.findAll(currentItem);
    }

    private void doDelete(StockConfig stockConfig) {
        try {
            stockConfigService.delete(stockConfig);
            showSuccess("Deleted stock configuration successful");
        } catch (Exception e) {
            showError("Failed to delete stock configuration");
        }
    }

    private StockConfig doUpdate(StockConfig stockConfig) {
        try {
            stockConfig = stockConfigService.update(stockConfig);
            showSuccess("Updated Stock configuration successful");
            return stockConfig;

        } catch (Exception e) {
            showError("Failed to update Stock configuration", e);
            return null;
        }
    }

    private StockConfig doCreate(StockConfig stockConfig) {
        try {
            stockConfig = stockConfigService.create(stockConfig);
            showSuccess("Successful created stock configuration for item ", stockConfig.getItem().toString());
            return stockConfig;
        } catch (Exception e) {
            showError("Failed to create stock configuration", e);
            return null;
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);
        refreshGrid();
    }

    private class ItemStockCrudFieldFactory extends VerticalCrudFormFactory<StockConfig> {

        public ItemStockCrudFieldFactory() {
            super(StockConfig.class);

            setVisibleProperties(FORM_VISIBLE_FIELDS);
            setUseBeanValidation(true);
            setFieldProvider("store", () -> createStoreSelector());
            setFieldProvider("minimum", () -> new QuantityField("Minimum"));
            setFieldProvider("maximum", () -> new QuantityField("Maximum"));
        }

        @Override
        protected List<HasValue> buildFields(CrudOperation operation, StockConfig stockConfig, boolean readOnly) {
            if (operation.equals(CrudOperation.ADD))
                stockConfig.init(currentItem);
            return super.buildFields(operation, stockConfig, readOnly);
        }
    }
}
