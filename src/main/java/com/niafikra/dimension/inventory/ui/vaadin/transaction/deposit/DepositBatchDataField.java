package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.inventory.service.BatchService;
import com.niafikra.dimension.inventory.transaction.deposit.DepositBatchData;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.BatchDataField;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.DateField;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.stream.Stream;

@PrototypeScope
@SpringComponent
public class DepositBatchDataField extends BatchDataField<DepositBatchData> {

    public DepositBatchDataField(BatchService batchService) {
        super(DepositBatchData.class, batchService);
    }

    @PostConstruct
    private void build() {
        getGridCrud().setCrudFormFactory(new DepositBatchDataCrudFormFactory());
        getGridCrud().getGrid().setColumns("batchCode","expiryDate", "quantity", "unitBuyingPrice", "notes");
    }


    private class DepositBatchDataCrudFormFactory extends BatchDataCrudFormFactory {
        public DepositBatchDataCrudFormFactory() {
            super();
            setVisibleProperties("batchCode","expiryDate", "quantity", "unitBuyingPrice", "notes");
            setFieldProvider("unitBuyingPrice", this::createUnitBuyingPriceField);
            setFieldProvider("expiryDate",this::createExpiryDateField);
        }

        private DateField createExpiryDateField() {
            return new DateField("Expiry Date");
        }

        private MoneyField createUnitBuyingPriceField() {
            return new MoneyField("Unit Price");
        }

        @Override
        protected MComboBox<String> createBatchSelector() {
            MComboBox<String> batchCode = super.createBatchSelector();
            batchCode.setItems(batchService.findBatchCodes(item));
            batchCode.setNewItemProvider(val -> {
                batchCode.setItems(Stream.concat(Stream.of(val), batchService.findBatchCodes(item).stream()).sorted());
                return Optional.of(val);
            });
            return batchCode;
        }

    }
}
