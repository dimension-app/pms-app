package com.niafikra.dimension.inventory.ui.vaadin.transaction;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.inventory.service.StoreService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.transaction.TansactionsReportView.VIEW_NAME;


@Secured(PMSPermission.VIEW_ALL_TRANSACTIONS_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "All transactions")
@ViewInfo(icon = VaadinIcons.WARNING, section = "Inventory", value = "All Transactions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class TansactionsReportView extends VerticalLayout implements ReportView {

    public static final String VIEW_NAME = "all-transactions-report";

    @Inject
    private TransactionsGrid transactionsGrid;
    @Inject
    private StoreService storeService;
    @Inject
    private User currentUser;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        transactionsGrid.setSizeFull();
        transactionsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        addComponentsAndExpand(transactionsGrid);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        transactionsGrid.getDataProvider().setStores(storeService.findOperatedStore(currentUser));
    }
}
