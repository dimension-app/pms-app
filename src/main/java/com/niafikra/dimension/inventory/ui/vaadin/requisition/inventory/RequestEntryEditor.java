package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.core.ui.vaadin.util.grid.ItemsEditor;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisitionEntry;
import com.vaadin.spring.annotation.SpringComponent;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/13/16 12:37 AM
 */

@SpringComponent
@PrototypeScope
public class RequestEntryEditor extends ItemsEditor<InventoryRequisitionEntry> {

    public RequestEntryEditor(RequestEntryEditGrid editGrid) {
        super(editGrid, InventoryRequisitionEntry::new);
    }

    @PostConstruct
    private void build() {
        getAddItemButton().setCaption("Add Entry");
        getEntriesGrid().addRemoveColumn();
    }

    @Override
    public RequestEntryEditGrid getEntriesGrid() {
        return RequestEntryEditGrid.class.cast(super.getEntriesGrid());
    }
}
