package com.niafikra.dimension.inventory.ui.vaadin.item.category;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.vaadin.data.HasValue;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.layout.CrudLayout;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;


public class CategoryCRUDView extends TreeGridCrud<Category> {

    public static final String VIEW_NAME = "inventory-categories";

    public static final String[] TABLE_VISIBLE_FIELDS = {"name"};
    public static final String[] FORM_VISIBLE_FIELDS = {"name", "parent", "description"};

    private CategoryService categoryService;
    private String categoryType;

    public CategoryCRUDView(CategoryService categoryService, String categoryType) {
        this(categoryService, categoryType, new HorizontalSplitCrudLayout());

    }

    public CategoryCRUDView(CategoryService categoryService, String categoryType, CrudLayout crudLayout) {
        super(Category.class, crudLayout);

        this.categoryService = categoryService;
        this.categoryType = categoryType;

        setRootsProvider(() -> categoryService.getRoots(categoryType));
        setChildrenProvider(category -> categoryService.getChildren(category));
        setAddOperation(category -> doSave(category));
        setUpdateOperation(category -> doUpdate(category));
        setDeleteOperation(category -> doDelete(category));

        getCrudFormFactory().setFieldProvider("name", () -> createNameField());
        getCrudFormFactory().setFieldProvider("description", () -> createDescriptionField());
        getCrudFormFactory().setFieldProvider("parent", () -> createParentSelector());


        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);
        getCrudFormFactory().setUseBeanValidation(true);
    }

    @Override
    protected void showForm(CrudOperation operation, Category category, boolean readOnly, String successMessage, Button.ClickListener buttonClickListener) {
        if (operation == CrudOperation.ADD) category.setType(categoryType);
        super.showForm(operation, category, readOnly, successMessage, buttonClickListener);
    }

    private void doDelete(Category category) {
        categoryService.delete(category);
    }

    private Category doUpdate(Category category) {
        return categoryService.save(category);
    }

    private Category doSave(Category category) {
        return categoryService.save(category);
    }

    private HasValue createParentSelector() {
        ComboBox<Category> parentSelector = new ComboBox<>("Parent");
        parentSelector.setItems(categoryService.getAll(categoryType));
        return parentSelector;
    }

    private HasValue createDescriptionField() {
        TextArea description = new TextArea("Description");
        return description;
    }

    private HasValue createNameField() {
        TextField description = new TextField("Description");
        return description;
    }
}
