package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.inventory.domain.Quantity;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.AbstractValidator;

public class MoreThanZeroQuantityValidator extends AbstractValidator<Quantity> {

    protected MoreThanZeroQuantityValidator() {
        super("Quantity should be more than zero");
    }

    @Override
    public ValidationResult apply(Quantity quantity, ValueContext context) {
        if(quantity.isLessThanZero() || quantity.isZero())
            return ValidationResult.error(getMessage(quantity));
        return ValidationResult.ok();
    }
}
