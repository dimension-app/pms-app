package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.io.POIUtils;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.domain.unit.ItemUnit;
import com.niafikra.dimension.inventory.domain.unit.MeasurementUnit;
import com.niafikra.dimension.inventory.service.DepositService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.MeasurementUnitService;
import com.niafikra.dimension.inventory.service.StoreService;
import com.niafikra.dimension.inventory.transaction.deposit.DepositBatchData;
import com.niafikra.dimension.inventory.transaction.deposit.DepositData;
import com.niafikra.dimension.money.Money;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.IterableUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DepositImportFacade {

    private static final String COLUMN_NAME_TIME = "TIME";
    private static final String COLUMN_NAME_STORE = "STORE";
    private static final String COLUMN_NAME_ITEM_CODE = "ITEM CODE";
    private static final String COLUMN_NAME_BATCH_CODE = "BATCH CODE";
    private static final String COLUMN_NAME_QUANTITY = "QUANTITY";
    private static final String COLUMN_NAME_UNIT = "UNIT";
    private static final String COLUMN_NAME_UNIT_PRICE = "UNIT PRICE";
    private static final String COLUMN_NAME_CURRENCY = "CURRENCY";
    private static final String COLUMN_NAME_EXPIRY_DATE = "EXPIRY DATE";
    private static final String COLUMN_NAME_NOTES = "NOTES";

    private final StoreService storeService;
    private final ItemService itemService;
    private final MeasurementUnitService unitService;
    private final DepositService depositService;

    @Transactional
    public void importDepositExcelSheet(Sheet sheet, LocalDateTime defaultTime, Store defaultStore, User user) {

        Iterator<Row> rowIterator = sheet.rowIterator();

        Row headerRow = rowIterator.next();
        List<String> headers = toStringList(headerRow);

        boolean containsTime = headers.contains("TIME");
        if (defaultTime == null && !containsTime)
            throw new IllegalArgumentException("The time for the deposit transaction is not specified");

        boolean containsStore = headers.contains("STORE");
        if (defaultStore == null && !containsStore)
            throw new IllegalArgumentException("The store for the deposit transaction is not specified");


        List<DepositData> deposits = new LinkedList<>();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            Store store = defaultStore;
            LocalDateTime time = defaultTime;

            if (headers.contains(COLUMN_NAME_TIME)) {
                int timeCellIndex = headers.indexOf(COLUMN_NAME_TIME);
                time = POIUtils.readLocalDateTimeValue(row, timeCellIndex, defaultTime != null, defaultTime, null, null);
            }

            if(headers.contains(COLUMN_NAME_STORE)) {
                int storeCellIndex = headers.indexOf(COLUMN_NAME_STORE);
                String storeName = POIUtils.readStringValue(row, storeCellIndex, defaultStore != null, null, null, null);
                if (!StringUtils.isEmpty(storeName)) store = storeService.findStore(storeName).orElseThrow(
                        () -> throwIllegalArgumentException(row.getRowNum(), storeCellIndex, "There is no store with name " + storeName)
                );
            }

            int itemCodeIndex = headers.indexOf(COLUMN_NAME_ITEM_CODE);
            String itemCode = POIUtils.readStringValue(row, itemCodeIndex, false, null, null, null);
            Item item = itemService.findItem(itemCode).orElseThrow(() -> throwIllegalArgumentException(row.getRowNum(), itemCodeIndex, "There is no item with code " + itemCode));

            int batchCodeIndex = headers.indexOf(COLUMN_NAME_BATCH_CODE);
            String batchCode = POIUtils.readStringValue(row, batchCodeIndex, false, null, null, null);

            int quantityIndex = headers.indexOf(COLUMN_NAME_QUANTITY);
            Double quantityValue = POIUtils.readNumericValue(row, quantityIndex, false, null, null, null);

            int unitIndex = headers.indexOf(COLUMN_NAME_UNIT);
            String unitName = POIUtils.readStringValue(row, unitIndex, false, null, null, null);
            MeasurementUnit unit = unitService.findUnit(unitName).orElseThrow(() -> throwIllegalArgumentException(row.getRowNum(), unitIndex, "There is no unit with name " + unitName));
            ItemUnit itemUnit = item.findUnit(unit).orElseThrow(() -> throwIllegalArgumentException(row.getRowNum(), unitIndex, unit + " is not configured as a unit for " + item));

            Quantity quantity = itemUnit.toQuantity(BigInteger.valueOf(quantityValue.longValue()));

            int unitPriceIndex = headers.indexOf(COLUMN_NAME_UNIT_PRICE);
            Double unitPriceValue = POIUtils.readNumericValue(row, unitPriceIndex, false, null, null, null);

            int currencyIndex = headers.indexOf(COLUMN_NAME_CURRENCY);
            String currency = POIUtils.readStringValue(row, currencyIndex, false, null, null, null);

            int expiryDateCellIndex = headers.indexOf(COLUMN_NAME_EXPIRY_DATE);
            LocalDate expiryDate = POIUtils.readLocalDateValue(row, expiryDateCellIndex, true, null, null, null);

            int notesIndex = headers.indexOf(COLUMN_NAME_NOTES);
            String notes = POIUtils.readStringValue(row, notesIndex, true, null, null, null);

            DepositData deposit = new DepositData(item);
            deposit.setTime(time);
            deposit.setStore(store);
            deposit.setUser(user);

            DepositBatchData depositBatch = new DepositBatchData();
            depositBatch.setQuantity(quantity);
            depositBatch.setBatchCode(batchCode);
            depositBatch.setUnitBuyingPrice(new Money(BigDecimal.valueOf(unitPriceValue), currency));
            depositBatch.setExpiryDate(expiryDate);
            depositBatch.setNotes(notes);

            deposit.setBatches(Set.of(depositBatch));

            deposits.add(deposit);
        }

        depositService.deposit(deposits);
    }

    private IllegalArgumentException throwIllegalArgumentException(int row, int cell, String message) {
        return new IllegalArgumentException("At row:" + row + " col:" + cell + " " + message);
    }

    private List<String> toStringList(Row row) {
        return IterableUtils.toList(row)
                .stream()
                .map(Cell::getStringCellValue)
                .collect(Collectors.toList());
    }
}
