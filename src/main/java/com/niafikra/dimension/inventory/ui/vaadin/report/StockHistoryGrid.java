package com.niafikra.dimension.inventory.ui.vaadin.report;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.service.DepositService;
import com.niafikra.dimension.inventory.service.DispenseService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.TransactionFilter;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemsProvider;
import com.niafikra.dimension.money.Money;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDateTime;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;


@ViewScope
@SpringComponent
public class StockHistoryGrid extends Grid<Item> implements Notifier {

    @Inject
    private ItemService itemService;
    @Inject
    private CategoryService categoryService;
    @Inject
    private DepositService depositService;
    @Inject
    private DispenseService dispenseService;

    private LocalDateTime startTime = DateUtils.getStartOfThisMonth(), endTime = DateUtils.getEndOfThisMonth();

    public StockHistoryGrid(ItemsProvider itemsProvider) {
        super(itemsProvider);
    }

    @PostConstruct
    private void build() {

        addColumn(item -> item.getCode())
                .setId("code")
                .setCaption("Code")
                .setSortable(true)
                .setWidth(100);

        addColumn(item -> item.getName())
                .setId("name")
                .setCaption("Item")
                .setSortable(true);

        addColumn(item -> item.getCategory())
                .setId("category")
                .setCaption("Category")
                .setSortable(true);

        addColumn(item -> item.getBaseUnit())
                .setId("baseUnit")
                .setCaption("Base Unit")
                .setWidth(100);

        addColumn(item -> calculateOpeningStock(item))
                .setId("openingStock")
                .setCaption("Opening Stock")
                .setSortable(false);


        addColumn(item -> calculateTotalDeposit(item))
                .setId("received")
                .setCaption("Received")
                .setSortable(false);

        addColumn(item -> calculateTotalOperatingStock(item))
                .setId("totalOperatingStock")
                .setCaption("Total")
                .setSortable(false);

        addColumn(item -> calculateAverageDepositsBaseUnitPrice(item))
                .setId("averageDepositsBaseUnitPrice")
                .setCaption("Base Unit Price")
                .setSortable(false);

        addColumn(item -> calculateTotalDispensed(item))
                .setId("dispensed")
                .setCaption("Issued")
                .setSortable(false);

        addColumn(item -> itemService.calculateStockBalance(item, endTime))
                .setId("closingStock")
                .setCaption("Closing Stock")
                .setSortable(false);

        addColumn(item -> calculateTotalDispensedValue(item))
                .setId("totalDispensedValue")
                .setCaption("Cost")
                .setSortable(false);

        HeaderRow filterRow = appendHeaderRow();
        createFilteringTextField(
                filterRow,
                "code",
                "Filter by item code",
                event -> getDataProvider().setCodeFilter(event.getValue())
        );

        createFilteringTextField(
                filterRow,
                "name",
                "Filter by item name",
                event -> getDataProvider().setNameFilter(event.getValue())
        );

        createFilteringComboBox(
                filterRow,
                "category",
                "Filter by category",
                categoryService.getAll(Item.CATEGORY_TYPE),
                event -> getDataProvider().setCategory(event.getValue())
        );
    }

    private Money calculateTotalDispensedValue(Item item) {
        return dispenseService.calculateTotalDispensedValue(
                TransactionFilter.builder()
                        .item(item)
                        .startTime(startTime)
                        .endTime(endTime)
                        .build()
        );
    }

    private Quantity calculateTotalDispensed(Item item) {
        return dispenseService.calculateTotalDispensed(
                TransactionFilter.builder()
                        .item(item)
                        .startTime(startTime)
                        .endTime(endTime)
                        .build()
        ).orElse(item.getBaseUnit().toZeroQuantity());
    }

    private Money calculateAverageDepositsBaseUnitPrice(Item item) {
        return depositService.calculateAverageBaseUnitPrice(
                TransactionFilter.builder()
                        .item(item)
                        .startTime(startTime)
                        .endTime(endTime)
                        .build()
        );
    }

    private Quantity calculateTotalOperatingStock(Item item) {
        return depositService.calculateOperatingStock(item, startTime, endTime);
    }

    private Quantity calculateTotalDeposit(Item item) {
        return depositService.calculateTotalDeposit(item, startTime, endTime);
    }

    private Quantity calculateOpeningStock(Item item) {
        return itemService.calculateStockBalance(item, startTime);
    }


    public void setPerid(LocalDateTime start, LocalDateTime end) {
        if (start == null || end == null || start.isAfter(end)) {
            showWarning("Enter the period for fetching history correctly");
            return;
        }

        this.startTime = start;
        this.endTime = end;

        getDataProvider().refreshAll();
    }

    @Override
    public ItemsProvider getDataProvider() {
        return (ItemsProvider) super.getDataProvider();
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }
}
