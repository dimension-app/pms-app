package com.niafikra.dimension.inventory.ui.vaadin.unit;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.inventory.domain.unit.MeasurementUnit;
import com.niafikra.dimension.inventory.service.MeasurementUnitService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.inventory.ui.vaadin.unit.UnitCRUDView.VIEW_NAME;


@Secured({PMSPermission.CONFIGURE_UNITS})
@ViewComponent(value = SettingDisplay.class, caption = "Units")
@ViewInfo(value = "Measurement Unit", section = "Inventory", icon = VaadinIcons.ABACUS)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class UnitCRUDView extends MGridCrudView<MeasurementUnit> implements SettingView, Notifier {
    public static final String VIEW_NAME = "inventory-units";

    public static final String[] TABLE_VISIBLE_FIELDS = {"name"};
    public static final String[] FORM_VISIBLE_FIELDS = {"name"};

    private MeasurementUnitService unitService;

    public UnitCRUDView(MeasurementUnitService unitService) {
        super(MeasurementUnit.class, new HorizontalSplitCrudLayout());
        this.unitService = unitService;
    }

    @PostConstruct
    private void build() {
        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);

        getCrudFormFactory().setUseBeanValidation(true);

        setFindAllOperation(() -> unitService.findAll());
        setAddOperation(unit -> doSave(unit));
        setUpdateOperation(unit -> doUpdate(unit));
        setDeleteOperation(unit -> doDelete(unit));
    }

    private void doDelete(MeasurementUnit unit) {
        try {
            unitService.delete(unit);
            showSuccess("Deleted unit successful");
        } catch (Exception e) {
            showError("Failed to delete unit", e);
        }
    }

    private MeasurementUnit doUpdate(MeasurementUnit unit) {
        try {
            unit = unitService.update(unit);
            showSuccess("Updated unit successful");
            return unit;
        } catch (Exception e) {
            showError("Failed to update unit", e);
            return null;
        }
    }

    private MeasurementUnit doSave(MeasurementUnit unit) {
        try {
            unit = unitService.create(unit);
            showSuccess("Added new unit successful");
            return unit;
        } catch (Exception e) {
            showError("Failed to add new unit", e);
            return null;
        }
    }
}
