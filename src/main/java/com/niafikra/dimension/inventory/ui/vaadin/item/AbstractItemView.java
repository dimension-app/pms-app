package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.StoreService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractItemView extends MVerticalLayout implements View {

    @Inject
    protected ItemService itemService;
    @Inject
    protected StoreService storeService;
    protected Item currentItem;
    protected Set<Store> userStores;
    @Inject
    protected User currentUser;

    @PostConstruct
    private void build() {
        this.userStores = storeService.findOperatedStore(currentUser).stream().collect(Collectors.toSet());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long itemId = Long.parseLong(event.getParameters());
        currentItem = itemService.getItem(itemId);
    }

}
