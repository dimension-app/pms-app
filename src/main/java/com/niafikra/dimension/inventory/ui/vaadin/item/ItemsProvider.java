package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.core.ui.vaadin.util.ProviderUtils;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.ArrayList;
import java.util.List;

@SpringComponent
@PrototypeScope
public class ItemsProvider extends PageableDataProvider<Item, String> {
    private String nameFilter, codeFilter;
    private Category category;

    private ItemService itemService;

    public ItemsProvider(ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    protected Page<Item> fetchFromBackEnd(Query<Item, String> query, Pageable pageable) {
        return itemService.findAll(ProviderUtils.getFilter(query, nameFilter), codeFilter, category, pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        List<QuerySortOrder> sortOrders = new ArrayList<>();
        sortOrders.add(new QuerySortOrder("name", SortDirection.ASCENDING));
        return sortOrders;
    }

    @Override
    protected int sizeInBackEnd(Query<Item, String> query) {
        return itemService.countAll(ProviderUtils.getFilter(query, nameFilter), codeFilter, category).intValue();
    }

    public void setNameFilter(String nameFilter) {
        this.nameFilter = nameFilter;
        refreshAll();
    }

    public void setCodeFilter(String codeFilter) {
        this.codeFilter = codeFilter;
        refreshAll();
    }

    public void setCategory(Category category) {
        this.category = category;
        refreshAll();
    }
}
