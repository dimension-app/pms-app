package com.niafikra.dimension.inventory.ui.vaadin.store;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.user.UsersProvider;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.StoreService;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.TwinColSelect;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.inventory.ui.vaadin.store.StoreCRUDView.VIEW_NAME;


@Secured({PMSPermission.CONFIGURE_STORES})
@ViewComponent(value = SettingDisplay.class, caption = "Stores")
@ViewInfo(value = "Store", section = "Inventory", icon = VaadinIcons.SHOP)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class StoreCRUDView extends MGridCrudView<Store> implements SettingView, Notifier {
    public static final String VIEW_NAME = "inventory-stores";

    public static final String[] TABLE_VISIBLE_FIELDS = {"name"};
    public static final String[] FORM_VISIBLE_FIELDS = {"name", "operators"};

    private StoreService storeService;
    private UsersProvider usersProvider;

    public StoreCRUDView(UsersProvider usersProvider, StoreService storeService) {
        super(Store.class, new HorizontalSplitCrudLayout());
        this.storeService = storeService;
        this.usersProvider = usersProvider;
    }

    @PostConstruct
    private void build() {
        getGrid().setColumns(TABLE_VISIBLE_FIELDS);
        getCrudFormFactory().setVisibleProperties(FORM_VISIBLE_FIELDS);

        getGrid()
                .addColumn(store -> store.getOperators().size())
                .setId("operatorsCount")
                .setCaption("Operators");

        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("operators", () -> createOperatorSelector());

        setFindAllOperation(() -> storeService.findAll());
        setAddOperation(store -> doSave(store));
        setUpdateOperation(store -> doUpdate(store));
        setDeleteOperation(store -> doDelete(store));
    }

    private HasValue createOperatorSelector() {
        TwinColSelect<User> operatorSelector = new TwinColSelect<User>("Operators", usersProvider);
        return operatorSelector;
//        GridMultiSelect<User> operatorSelector = new GridMultiSelect<>();
//        operatorSelector.getGrid().setDataProvider(usersProvider);
//        operatorSelector.getGrid().addColumn(user -> user.getName());
//        return operatorSelector;
    }

    private void doDelete(Store store) {
        try {
            storeService.delete(store);
            showSuccess("Deleted store successful");
        } catch (Exception e) {
            showError("Failed to delete store", e);
        }
    }

    private Store doUpdate(Store store) {
        try {
            store = storeService.update(store);
            showSuccess("Updated store successful");
            return store;
        } catch (Exception e) {
            showError("Failed to update store", e);
            return null;
        }
    }

    private Store doSave(Store store) {
        try {
            store = storeService.create(store);
            showSuccess("Added new store successful");
            return store;
        } catch (Exception e) {
            showError("Failed to add new store", e);
            return null;
        }
    }
}
