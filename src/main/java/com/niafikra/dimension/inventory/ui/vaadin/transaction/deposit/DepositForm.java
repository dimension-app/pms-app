package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.DepositService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.transaction.deposit.DepositData;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.BatchDataField;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.TransactionForm;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class DepositForm extends TransactionForm<DepositData> {

    public static final String FIELD_WIDTH = "100%";

    @Inject
    protected DepositBatchDataField batches;

    @Inject
    private Navigator navigator;

    private DepositService depositService;
    private User currentUser;

    public DepositForm(DepositService depositService, ItemService itemService, User currentUser) {
        super(DepositData.class, itemService);
        this.depositService = depositService;
        this.currentUser = currentUser;
    }

    @PostConstruct
    private void build() {
        item.setWidth(FIELD_WIDTH);
        time.setWidth(FIELD_WIDTH);
        store.setWidth(FIELD_WIDTH);
        batches.setWidth(FIELD_WIDTH);
        addSavedHandler(deposit -> doSubmit(deposit));
        setResetHandler(deposit -> doReset(deposit.getItem()));
    }

    @Override
    public BatchDataField getBatchDataField() {
        return batches;
    }

    private void doReset(Item item) {
        setEntity(new DepositData(item));
    }

    private void doSubmit(DepositData deposit) {
        deposit.setUser(currentUser);

        try {
            depositService.deposit(deposit);
            showSuccess("Successful deposited : " + deposit.getTotalQuantity(), item.toString());
            UI.getCurrent().access(() -> navigator.reload());
            doReset(deposit.getItem());
        } catch (Exception e) {
            showError("Failed to deposit", e);
        }
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                item,
                time,
                store,
                batches,
                getToolbar(),
                itemSummaryLabel
        ).withFullWidth();
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Deposit item batches");
        return window;
    }
}