package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.inventory.domain.Item;
import com.vaadin.ui.VerticalLayout;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

@Component
@PrototypeScope
public class ItemInfoTemplatePanel extends VerticalLayout {

    private TemplatePanel templatePanel;

    public ItemInfoTemplatePanel(TemplatePanel templatePanel) {
        this.templatePanel = templatePanel;
        this.templatePanel.getTemplateView().setTemplatePath(Templates.ITEM);
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        addComponent(templatePanel);
    }

    public void setItemInfo(Item item) {
        templatePanel.getTemplateView().putBinding("item", item);
        templatePanel.getTemplateView().render();
    }
}
