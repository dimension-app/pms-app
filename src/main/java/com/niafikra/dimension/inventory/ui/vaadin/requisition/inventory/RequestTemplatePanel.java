package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.vaadin.spring.annotation.SpringComponent;
import lombok.Getter;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class RequestTemplatePanel extends MVerticalLayout {

    @Getter
    @Inject
    private TemplatePanel templatePanel;

    @PostConstruct
    private void build() {
        this.templatePanel.getTemplateView().setTemplatePath(Templates.INVENTORY_REQUISITION);
        templatePanel.setSizeFull();
        withComponents(templatePanel).withExpand(templatePanel, 1);
    }

    public void setRequisition(InventoryRequisition requisition) {
        templatePanel.getTemplateView().putBinding("inventoryRequisition", requisition).render();
    }
}
