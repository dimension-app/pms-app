package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.inventory.domain.Item;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;

@Component
@PrototypeScope
public class ItemComboBox extends MComboBox<Item> {

    public ItemComboBox(ItemsProvider itemsProvider) {
        setDataProvider(itemsProvider);
    }
}
