package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.MeasurementUnitService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class ItemUpdateForm extends ItemForm {
    private ItemService itemService;

    public ItemUpdateForm(CategoryService categoryService, MeasurementUnitService unitService,
                          ItemService itemService) {

        super(categoryService, unitService);
        this.itemService = itemService;
        code.setReadOnly(true);
        addSavedHandler(item -> {
            doUpdate(item);
        });
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Change inventory items");
        return window;
    }

    private void doUpdate(Item item) {
        try {
            itemService.create(item);
            showSuccess("Change inventory items successful", item.toString());
            closePopup();
        } catch (Exception e) {
            showError("Failed to change items", e);
        }
    }


}
