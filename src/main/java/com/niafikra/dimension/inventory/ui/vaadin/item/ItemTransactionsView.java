package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.service.StoreService;
import com.niafikra.dimension.inventory.ui.vaadin.transaction.TransactionsGrid;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.item.ItemTransactionsView.VIEW_NAME;


@Secured(PMSPermission.VIEW_ITEM_TRANSACTIONS)
@ViewComponent(value = ItemDisplay.class, caption = "Item Transactions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ItemTransactionsView extends AbstractItemView {
    public static final String VIEW_NAME = "item-transaction";

    @Inject
    private TransactionsGrid transactionsGrid;
    @Inject
    private StoreService storeService;
    @Inject
    private User currentUser;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        transactionsGrid.setSizeFull();
        transactionsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        addComponentsAndExpand(transactionsGrid);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);

        transactionsGrid.getDataProvider().setItem(currentItem);
        transactionsGrid.getDataProvider().setStores(storeService.findOperatedStore(currentUser));
    }
}
