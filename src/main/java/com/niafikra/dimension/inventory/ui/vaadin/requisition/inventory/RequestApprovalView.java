package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.approval.domain.Tracker;
import com.niafikra.dimension.approval.service.ApprovalTrackerService;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.layouts.MVerticalLayout;
import javax.annotation.PostConstruct;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.RequestApprovalView.VIEW_NAME;


@RequiredArgsConstructor
@Secured(PMSPermission.VIEW_INVENTORY_REQUISITION_APPROVAL_REQUEST)
@ViewComponent(value = MainDisplay.class, caption = "Inventory Requisition")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RequestApprovalView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "ap-inventory-requisition-approval";

    private final RequestTemplatePanel templatePanel;
    private final ApprovalTrackerService trackerService;
    private final InventoryRequisitionService requisitionService;
    private final Navigator navigator;
    private final MainHeader mainHeader;
    private final User currentUser;

    private InventoryRequisition requisition;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        templatePanel.setSizeFull();
        templatePanel.setMargin(false);
        withComponents(templatePanel).withExpand(templatePanel, 1);
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long trackerId = Long.parseLong(event.getParameters());
        Tracker tracker = trackerService.getTracker(trackerId);
        InventoryRequisition requisition = requisitionService.getInventoryRequisition(tracker);

        setRequisition(requisition);

        //show the edit action only if you are the current approver
        mainHeader.getMenuBar().setMenuVisible("Edit", trackerService.canApprove(requisition, currentUser));
    }

    private void setRequisition(InventoryRequisition requisition) {
        this.requisition = requisition;
        templatePanel.setRequisition(requisition);
    }

    @Secured(PMSPermission.EDIT_INVENTORY_REQUISITION)
    @ViewMenuOption(value = "Edit", icon = VaadinIcons.EDIT)
    public void editInventoryRequisition() {
        navigator.navigateTo(RequestEntriesQuantityEditView.class,this.requisition.getId());
    }
}
