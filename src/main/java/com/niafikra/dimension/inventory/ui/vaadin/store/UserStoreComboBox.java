package com.niafikra.dimension.inventory.ui.vaadin.store;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.StoreService;
import com.vaadin.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import java.util.List;

@PrototypeScope
@SpringComponent
@RequiredArgsConstructor
public class UserStoreComboBox extends MComboBox<Store> {
    private final StoreService storeService;
    private final User currentUser;

    @PostConstruct
    private void build(){
        List<Store> userStores = storeService.findOperatedStore(currentUser);
        setItems(userStores);
        if(userStores.size() == 1)
            setValue(userStores.iterator().next());
    }
}
