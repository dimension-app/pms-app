package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

import java.util.Map;
import java.util.stream.Collectors;

import static com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils.showError;
import static com.niafikra.dimension.core.ui.vaadin.util.NotificationUtils.showSuccess;
import static com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory.RequestEntriesQuantityEditView.VIEW_NAME;

@RequiredArgsConstructor
@Secured(PMSPermission.EDIT_INVENTORY_REQUISITION)
@ViewComponent(value = MainDisplay.class, caption = "Edit Inventory Requisition Quantity")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RequestEntriesQuantityEditView extends MVerticalLayout implements View {
    public static final String VIEW_NAME = "inventory-requisition-quantity-edit";

    @Getter
    private final TemplateView requisitionSummaryTemplate;
    private final RequestEntriesQuantityEditGrid quantityEditGrid;
    private final InventoryRequisitionService requisitionService;
    private final User currentUser;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(true);
        this.requisitionSummaryTemplate.setTemplatePath(Templates.INVENTORY_REQUISITION_SUMMARY);
        withComponents(requisitionSummaryTemplate, quantityEditGrid).withExpand(quantityEditGrid, 1);


        MButton submitButton = new MButton("Update Request")
                .withStyleName(ValoTheme.BUTTON_PRIMARY)
                .withListener(event -> doSubmit())
                .withWidth("50%");
        addComponent(submitButton);
        setComponentAlignment(submitButton, Alignment.MIDDLE_CENTER);
    }

    private void doSubmit() {
        ConfirmDialog.show(
                getUI(),
                "Are you sure you want to update this request quantities?",
                confirmDialog -> {
                    if (confirmDialog.isConfirmed()) {
                        try {
                           Map<Long, Quantity> quantities = quantityEditGrid.getValidItems().stream()
                                    .collect(Collectors.toMap(entry -> entry.getId(),entry -> entry.getApprovedQuantity()));

                            requisitionService.changeQuantity(quantities,currentUser);
                            showSuccess("Successfully updated inventory request");
                        } catch (Exception e) {
                            e.printStackTrace();
                            showError("Failed to update inventory request", e.getMessage());
                        }
                    }
                }
        );

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long requisitionId = Long.parseLong(event.getParameters());
        setRequisition(requisitionService.getInventoryRequisition(requisitionId));
    }

    private void setRequisition(InventoryRequisition requisition) {
        requisitionSummaryTemplate.putBinding("inventoryRequisition", requisition).render();
        quantityEditGrid.setEntries(requisition.getEntries());
    }
}
