package com.niafikra.dimension.inventory.ui.vaadin.item;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.unit.BaseUnit;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.inventory.ui.vaadin.item.ItemsView.VIEW_NAME;

@UIScope
@Secured({PMSPermission.VIEW_INVENTORY_ITEMS})
@ViewComponent(value = MainDisplay.class, caption = "Items")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.INVENTORY, caption = "Items")
@VaadinFontIcon(VaadinIcons.STOCK)
public class ItemsView extends VerticalLayout implements View, TableExportable {

    public static final String VIEW_NAME = "inventory-items";
    private MenuBar.MenuItem newMenuItem;

    private ItemCreateForm createForm;
    private MainHeader mainHeader;
    private ItemsGrid itemsGrid;

    public ItemsView(ItemCreateForm createForm, MainHeader mainHeader, ItemsGrid itemsGrid) {
        this.createForm = createForm;
        this.mainHeader = mainHeader;
        this.itemsGrid = itemsGrid;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        itemsGrid.setSizeFull();
        addComponent(itemsGrid);
        setExpandRatio(itemsGrid, 1);
    }

    @Override
    public void attach() {
        super.attach();

        if (hasAuthority(PMSPermission.CREATE_INVENTORY_ITEM)) {
            newMenuItem = mainHeader.getMenuBar().addItemAsFirst(
                    "New Item",
                    FontAwesome.PLUS,
                    selectedItem -> showCreateForm()
            );
        }

    }

    private void showCreateForm() {
        Item item = new Item(new BaseUnit());
        createForm.setEntity(item);
        createForm.openInModalPopup().addCloseListener(e -> itemsGrid.getDataProvider().refreshAll());
    }

    @Override
    public void detach() {
        super.detach();

        if (hasAuthority(PMSPermission.CREATE_INVENTORY_ITEM))
            mainHeader.getMenuBar().removeItem(newMenuItem);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(itemsGrid);
    }
}
