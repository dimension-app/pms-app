package com.niafikra.dimension.inventory.ui.vaadin.report;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.report.ReportDisplay;
import com.niafikra.dimension.core.ui.vaadin.report.ReportView;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.report.StockHistoryReportView.VIEW_NAME;


@Secured(PMSPermission.VIEW_STOCK_HISTORY_REPORT)
@ViewComponent(value = ReportDisplay.class, caption = "Stock History")
@ViewInfo(icon = VaadinIcons.TRENDIND_DOWN, section = "Inventory", value = "Stock History")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class StockHistoryReportView extends VerticalLayout implements ReportView {
    public static final String VIEW_NAME = "stock-history-report";

    @Inject
    private StockHistoryGrid stockHistoryGrid;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        LocalDateTimeRangeSelector periodSelector = new LocalDateTimeRangeSelector();
        periodSelector.setWidth("300px");
        periodSelector.setPeriod(stockHistoryGrid.getStartTime(), stockHistoryGrid.getEndTime());

        periodSelector.addValueChangeListener(event -> {
            stockHistoryGrid.setPerid(periodSelector.getStart(), periodSelector.getEnd());
        });

        addComponent(periodSelector);

        stockHistoryGrid.setSizeFull();
        stockHistoryGrid.setSelectionMode(Grid.SelectionMode.NONE);
        addComponentsAndExpand(stockHistoryGrid);
    }

}
