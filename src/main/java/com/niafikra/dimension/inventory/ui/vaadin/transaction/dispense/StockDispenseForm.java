package com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.user.UserComboBox;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.service.DispenseService;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseData;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class StockDispenseForm extends DispenseForm {

    private DispenseService dispenseService;
    private User currentUser;

    @Inject
    private UserComboBox associatedUser;

    private Navigator navigator;

    public StockDispenseForm(DispenseService dispenseService,
                             ItemService itemService,
                             User currentUser,
                             Navigator navigator) {
        super(itemService);

        this.dispenseService = dispenseService;
        this.currentUser = currentUser;
        this.navigator = navigator;
    }

    @PostConstruct
    private void build() {
        associatedUser.withCaption("Associated user");
        associatedUser.setWidth(FIELD_WIDTH);

        addSavedHandler(dispenseData -> doDispense(dispenseData));
        setResetHandler(dispenseData -> doReset(dispenseData.getItem()));


    }

    private void doReset(Item item) {
        setEntity(new DispenseData(item));
    }

    private void doDispense(DispenseData dispenseData) {
        dispenseData.setUser(currentUser);
        try {
            dispenseService.dispense(dispenseData);
            showSuccess("Successful dispensed : " + dispenseData.getTotalQuantity(), item.toString());
            UI.getCurrent().access(() -> navigator.reload());
            doReset(dispenseData.getItem());
        } catch (Exception e) {
            showError("Failed to dispense items", e);
        }
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                item,
                new MHorizontalLayout(store, time).withFullWidth(),
                associatedUser.withFullWidth(),
                batches,
                getToolbar(),
                itemSummaryLabel
        ).withFullWidth();
    }

}
