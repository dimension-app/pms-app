package com.niafikra.dimension.inventory.ui.vaadin.transaction.dispense;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseBatchData;
import com.niafikra.dimension.inventory.transaction.dispense.DispenseData;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class ItemBatchDispenseData extends DispenseBatchData {
    @NotNull
    private Item item;

    public ItemBatchDispenseData() {
        super();
    }

    public static DispenseData toDispenseData(LocalDateTime time,
                                              Store store,
                                              User associatedUser,
                                              User creator,
                                              Item item,
                                              List<ItemBatchDispenseData> itemBatchDispenses) {
        DispenseData dispenseData = new DispenseData(item);
        dispenseData.setUser(creator);
        dispenseData.setAssociatedUser(associatedUser);
        dispenseData.setStore(store);
        dispenseData.setTime(time);
        dispenseData.setBatches(
                itemBatchDispenses
                        .stream()
                        .map(data -> data.toDispenseBatchData())
                        .collect(Collectors.toSet())
        );

        return dispenseData;
    }

    private DispenseBatchData toDispenseBatchData() {
        DispenseBatchData batchData = new DispenseBatchData(
                getBatchCode(),
                getQuantity(),
                getNotes()
        );

        return batchData;
    }
}
