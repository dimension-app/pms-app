package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisition;
import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisitionEntry;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService.InventoryRequisitionEntryFilter;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemComboBox;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.components.grid.HeaderRow;
import lombok.Getter;

import javax.annotation.PostConstruct;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;
import static com.niafikra.dimension.core.util.DateUtils.formatDateTime;
import static com.vaadin.ui.themes.ValoTheme.MENUBAR_BORDERLESS;
import static com.vaadin.ui.themes.ValoTheme.MENUBAR_SMALL;

@PrototypeScope
@SpringComponent
public class RequestEntriesGrid extends Grid<InventoryRequisitionEntry> {

    private final InventoryRequisitionService requisitionService;
    private final Navigator navigator;
    private final ItemComboBox itemFilter;
    private final InventoryRequestEntryDispenseForm dispenseForm;

    @Getter
    private InventoryRequisitionEntryFilter dataFilter;

    public RequestEntriesGrid(RequestEntriesProvider provider,
                              InventoryRequisitionService requisitionService,
                              InventoryRequestEntryDispenseForm dispenseForm,
                              ItemComboBox itemFilter,
                              Navigator navigator) {
        super(provider.withConfigurableFilter());
        this.requisitionService = requisitionService;
        this.itemFilter = itemFilter;
        this.navigator = navigator;
        this.dispenseForm = dispenseForm;
    }

    @Override
    public ConfigurableFilterDataProvider<InventoryRequisitionEntry, Void, InventoryRequisitionEntryFilter> getDataProvider() {
        return ConfigurableFilterDataProvider.class.cast(super.getDataProvider());
    }

    @PostConstruct
    private void build() {
        dataFilter = InventoryRequisitionEntryFilter.builder().build();
        getDataProvider().setFilter(dataFilter);
        getDataProvider().refreshAll();

        addColumn(entry -> formatDateTime(entry.getRequisition().getTimeCreated()))
                .setId("requisition.timeCreated")
                .setCaption("Time Created")
                .setSortable(true)
                .setMaximumWidth(200)
                .setMinimumWidthFromContent(true);

        addColumn(entry -> entry.getRequisition().getCreator())
                .setId("requisition.creator")
                .setCaption("Created By")
                .setSortable(true)
                .setMaximumWidth(200)
                .setMinimumWidthFromContent(true);

        addColumn(entry -> entry.getItem())
                .setId("item")
                .setCaption("Item")
                .setSortable(true)
                .setExpandRatio(1);

        addColumn(entry -> entry.getRequestedQuantity())
                .setId("requestedQuantity")
                .setCaption("Requested Qty")
                .setSortable(true)
                .setMaximumWidth(150)
                .setMinimumWidthFromContent(true);

        addColumn(entry -> entry.getApprovedQuantity())
                .setId("approvedQuantity")
                .setCaption("Approved Qty")
                .setSortable(true)
                .setMaximumWidth(150)
                .setMinimumWidthFromContent(true);

        addColumn(entry -> requisitionService.getPendingQuantity(entry))
                .setId("pendingQuantity")
                .setCaption("Pending Qty")
                .setSortable(false)
                .setMaximumWidth(150)
                .setMinimumWidthFromContent(true);

        addColumn(entry -> requisitionService.getStatus(entry))
                .setId("status")
                .setCaption("Status")
                .setSortable(false)
                .setMaximumWidth(200)
                .setMinimumWidthFromContent(true);

        addComponentColumn(entry -> {

            MenuBar menu = new MenuBar();
            menu.addStyleNames(MENUBAR_BORDERLESS, MENUBAR_SMALL);
            MenuBar.MenuItem optionsMenu = menu.addItem("", null);

            if (hasAuthority(PMSPermission.VIEW_INVENTORY_REQUISITION)) {
                optionsMenu.addItem(
                        "View Request",
                        VaadinIcons.INFO_CIRCLE_O,
                        option -> navigateInventoryRequisitionView(entry.getRequisition())
                );
            }


            if (hasAuthority(PMSPermission.DISPENSE_INVENTORY_REQUEST)) {
                optionsMenu.addItem(
                        "Dispense",
                        VaadinIcons.INFO_CIRCLE_O,
                        option -> showDispenseWindow(entry)
                );
            }

            return menu;
        }).setId("actions").setWidth(70);


        HeaderRow filterRow = appendHeaderRow();

        LocalDateTimeRangeSelector timeCreatedRangePanel = new LocalDateTimeRangeSelector();
        timeCreatedRangePanel.setWidth("200px");
        filterRow.getCell("requisition.timeCreated").setComponent(timeCreatedRangePanel);
        timeCreatedRangePanel.addValueChangeListener(event -> {
            dataFilter.setTimeCreatedRange(timeCreatedRangePanel.getStart(), timeCreatedRangePanel.getEnd());
            getDataProvider().refreshAll();
        });

        itemFilter.setWidthFull();
        filterRow.getCell("item").setComponent(itemFilter);
        itemFilter.addValueChangeListener(event -> {
            dataFilter.setItem(event.getValue());
            getDataProvider().refreshAll();
        });

        setSelectionMode(SelectionMode.NONE);

    }

    private void navigateInventoryRequisitionView(InventoryRequisition requisition) {
        navigator.navigateTo(RequestView.class, requisition.getId());
    }

    public void showDispenseWindow(InventoryRequisitionEntry entry) {
        dispenseForm.setInventoryRequisitionEntry(entry);
        dispenseForm.openInModalPopup().addCloseListener(e -> UI.getCurrent().access(() -> navigator.reload()));
    }
}
