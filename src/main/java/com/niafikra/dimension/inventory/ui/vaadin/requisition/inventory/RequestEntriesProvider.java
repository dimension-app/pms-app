package com.niafikra.dimension.inventory.ui.vaadin.requisition.inventory;

import com.niafikra.dimension.inventory.domain.requisition.InventoryRequisitionEntry;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService;
import com.niafikra.dimension.inventory.service.InventoryRequisitionService.InventoryRequisitionEntryFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.List;

import static com.niafikra.dimension.inventory.service.InventoryRequisitionService.InventoryRequisitionEntryFilter.builder;

@PrototypeScope
@SpringComponent
@RequiredArgsConstructor
public class RequestEntriesProvider
        extends PageableDataProvider<InventoryRequisitionEntry, InventoryRequisitionEntryFilter> {

    private final InventoryRequisitionService requisitionService;

    @Override
    protected Page<InventoryRequisitionEntry> fetchFromBackEnd(
            Query<InventoryRequisitionEntry, InventoryRequisitionEntryFilter> query,
            Pageable pageable) {
        return requisitionService.findAll(
                query.getFilter().orElseGet(builder()::build),
                pageable
        );
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return QuerySortOrder.desc("requisition.timeCreated").build();
    }

    @Override
    protected int sizeInBackEnd(Query<InventoryRequisitionEntry, InventoryRequisitionEntryFilter> query) {
        return requisitionService.countAll(
                query.getFilter().orElseGet(builder()::build)
        ).intValue();
    }
}
