package com.niafikra.dimension.inventory.ui.vaadin.item.batch;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.inventory.service.BatchService;
import com.niafikra.dimension.inventory.ui.vaadin.item.AbstractItemView;
import com.niafikra.dimension.inventory.ui.vaadin.item.ItemDisplay;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.inventory.ui.vaadin.item.batch.ItemBatchesView.VIEW_NAME;


@Secured(PMSPermission.VIEW_ITEM_BATCHES)
@ViewComponent(value = ItemDisplay.class, caption = "Item Batches")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class ItemBatchesView extends AbstractItemView {
    public static final String VIEW_NAME = "item-batches";

    @Inject
    private ItemBatchesGrid batchesGrid;
    @Inject
    private BatchService batchService;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        batchesGrid.setSizeFull();
        addComponent(batchesGrid);
        setExpandRatio(batchesGrid, 1);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);
        batchesGrid.setItems(batchService.findAll(currentItem, userStores));
    }
}
