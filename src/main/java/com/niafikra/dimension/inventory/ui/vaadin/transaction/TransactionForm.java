package com.niafikra.dimension.inventory.ui.vaadin.transaction;

import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.TemplateView;
import com.niafikra.dimension.inventory.domain.Item;
import com.niafikra.dimension.inventory.domain.Quantity;
import com.niafikra.dimension.inventory.domain.Store;
import com.niafikra.dimension.inventory.service.ItemService;
import com.niafikra.dimension.inventory.service.StoreService;
import com.niafikra.dimension.inventory.transaction.TransactionData;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.Window;
import org.vaadin.viritin.fields.LabelField;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

public abstract class TransactionForm<T extends TransactionData> extends AbstractForm<T> {
    protected LabelField<Item> item = new LabelField<>("Item");
    protected DateTimeField time = new DateTimeField("Time");
    protected MComboBox<Store> store = new MComboBox<Store>().withCaption("Store");

    @Inject
    protected TemplateView itemSummaryLabel;

    @Inject
    private StoreService storeService;

    private ItemService itemService;

    @Inject
    private User currentUser;

    public TransactionForm(Class<T> type, ItemService itemService) {
        super(type);
        this.itemService = itemService;
    }

    @PostConstruct
    private void build() {
        List<Store> stores = storeService.findOperatedStore(currentUser);
        store.withItems(stores);
        store.addValueChangeListener(event -> updateItemStoreBalanceSummary());
        itemSummaryLabel.setTemplatePath(Templates.STORE_ITEM_BALANCE).withFullWidth();

        store.addValueChangeListener(event -> {
            getBatchDataField().setReadOnly(event.getValue() == null);
            getBatchDataField().setStore(event.getValue());
        });

        getBatchDataField().getGridCrud().getGrid().setHeightByRows(4);
        getBatchDataField().setReadOnly(true);
    }

    public abstract BatchDataField getBatchDataField();

    private void updateItemStoreBalanceSummary() {
        Store itemStore = store.getValue();
        if (itemStore == null) return;

        Item item = getEntity().getItem();
        Quantity balance = itemService.calculateStockBalance(item, itemStore);
        itemSummaryLabel.putBinding("store", itemStore)
                .putBinding("item", item)
                .putBinding("balance", balance)
                .render();

    }


    @Override
    public void setEntity(T transaction) {
        super.setEntity(transaction);

        if (store.getValue() == null) {
            ListDataProvider<Store> dataProvider = (ListDataProvider<Store>) store.getDataProvider();
            if (dataProvider.getItems().size() == 1)
                store.setValue(dataProvider.getItems().stream().findFirst().get());
        }

        getBatchDataField().setItem(transaction.getItem());
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("650px");
        return window;
    }
}
