package com.niafikra.dimension.inventory.ui.vaadin.transaction.deposit;

import com.niafikra.dimension.core.ui.vaadin.util.grid.ItemsEditor;
import com.niafikra.dimension.core.util.PrototypeScope;
import com.vaadin.spring.annotation.SpringComponent;

import javax.annotation.PostConstruct;

/**
 * @author Boniface Chacha
 * @email boniface.chacha@niafikra.com
 * @email bonifacechacha@gmail.com
 * @date 8/13/16 12:37 AM
 */

@SpringComponent
@PrototypeScope
public class ReceiveItemsEditor extends ItemsEditor<ItemBatchDepositData> {

    public ReceiveItemsEditor(DepositItemEditGrid editGrid) {
        super(editGrid, ItemBatchDepositData::new);
    }

    @PostConstruct
    private void build() {
        getAddItemButton().setCaption("Add Entry");
        getEntriesGrid().addRemoveColumn();
    }

}
