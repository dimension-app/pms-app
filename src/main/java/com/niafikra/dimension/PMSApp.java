package com.niafikra.dimension;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.vaadin.spring.events.annotation.EnableEventBus;


@EnableAsync
@EnableCaching
@EnableEventBus
@SpringBootApplication
public class PMSApp extends SpringBootServletInitializer {

    public static String baseUrl;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PMSApp.class).properties("spring.config.name: dimension");
    }

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "dimension");
        SpringApplication.run(PMSApp.class, args);
    }
}