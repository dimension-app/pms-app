package com.niafikra.dimension.issue.repository;

import com.niafikra.dimension.core.repository.DimensionRepository;
import com.niafikra.dimension.issue.domain.Comment;

public interface CommentRepository extends DimensionRepository<Comment, Long> {
}