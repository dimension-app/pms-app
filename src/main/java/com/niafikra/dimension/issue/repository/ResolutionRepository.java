package com.niafikra.dimension.issue.repository;

import com.niafikra.dimension.core.repository.DimensionRepository;
import com.niafikra.dimension.issue.domain.Resolution;

public interface ResolutionRepository extends DimensionRepository<Resolution, Long> {
}
