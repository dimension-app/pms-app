package com.niafikra.dimension.issue.repository;

import com.niafikra.dimension.core.repository.DimensionRepository;
import com.niafikra.dimension.issue.domain.Issue;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IssueRepository extends DimensionRepository<Issue, Long> {
    List<Issue> findAllByReference(String reference);
}
