package com.niafikra.dimension.issue.service;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.core.security.SecurityUtils;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.security.service.UserService;
import com.niafikra.dimension.issue.domain.Comment;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.domain.Note;
import com.niafikra.dimension.issue.domain.Resolution;
import com.niafikra.dimension.issue.repository.CommentRepository;
import com.niafikra.dimension.issue.repository.IssueRepository;
import com.niafikra.dimension.issue.repository.ResolutionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class IssueService {
    private IssueRepository issueRepository;
    private CommentRepository commentRepository;
    private ResolutionRepository resolutionRepository;
    private UserService userService;

    public IssueService(IssueRepository issueRepository,
                        CommentRepository commentRepository,
                        ResolutionRepository resolutionRepository,
                        UserService userService) {
        this.issueRepository = issueRepository;
        this.commentRepository = commentRepository;
        this.resolutionRepository = resolutionRepository;
        this.userService = userService;
    }

    @Transactional
    public Issue createIssue(String reference, String content, Set<Attachment> attachments, User creator) {
        Issue issue = new Issue();
        issue.setReference(reference);
        issue.setContent(content);
        issue.setAttachments(attachments);
        issue.setCreator(creator);

        return createIssue(issue);
    }

    @Transactional
    public Issue createIssue(Issue issue) {
        User user = SecurityUtils.getCurrentUser(userService);
        issue.setCreator(user);
        return issueRepository.save(issue);
    }

    @Transactional
    public Issue createComment(Issue issue, Comment comment) {
        if (issue.isResolved())
            throw new IllegalStateException("Issue is already resolved, you can not add a comment.");

        User user = SecurityUtils.getCurrentUser(userService);
        comment.setCreator(user);
        issue.addComment(comment);
        return issueRepository.save(issue);
    }

    @Transactional
    public Issue resolve(Issue issue, Resolution resolution) {
        User user = SecurityUtils.getCurrentUser(userService);

        if (!issue.getCreator().equals(user))
            throw new UnsupportedOperationException("Only the creator of issue is allowed to mark it as resolved");

        if (issue.isResolved())
            throw new IllegalStateException("Issue is already resolved");

        resolution.setCreator(user);
        resolutionRepository.save(resolution);
        issue.setResolution(resolution);
        return issueRepository.save(issue);
    }

    public Issue getIssue(Long issueId) {
        return issueRepository.findById(issueId)
                .orElseThrow(() -> new IllegalArgumentException("There is no issue with id " + issueId));
    }

    @Transactional
    public List<Issue> getIssues(String reference) {
        return issueRepository.findAllByReference(reference)
                .stream()
                .sorted(Comparator.comparing(Note::getTimeCreated).reversed())
                .distinct()
                .collect(Collectors.toList());
    }

    @Transactional
    public int countIssues(String reference) {
        return getIssues(reference).size();
    }


    @Transactional
    public boolean isResolved(String reference) {
        return getIssues(reference).stream().allMatch(Issue::isResolved);
    }

    public boolean hasIssues(String reference) {
        return !getIssues(reference).isEmpty();
    }
}
