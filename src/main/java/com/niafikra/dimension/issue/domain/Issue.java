package com.niafikra.dimension.issue.domain;

import com.niafikra.dimension.core.security.domain.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Entity
@Table(name = "IS_Issue")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"comments", "resolution"})
@ToString(callSuper = true, exclude = {"comments", "resolution"})
public class Issue extends Note {

    @NotNull
    @NotBlank
    private String reference;

    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private Set<Comment> comments;

    @OneToOne(cascade = CascadeType.ALL)
    private Resolution resolution;

    public void addComment(Comment comment) {
        if (comments == null)
            comments = new LinkedHashSet<>();
        comments.add(comment);
    }

    public boolean isResolved() {
        return resolution != null;
    }

    public boolean canResolve(User user) {
        // you can resolve only if you created the issue and it is not yet resolved
        return !isResolved() && getCreator().equals(user);
    }

    public List<Comment> getCommentsSorted() {
        return getComments()
                .stream()
                .sorted(Comparator.comparing(Comment::getTimeCreated))
                .collect(Collectors.toList());
    }
}
