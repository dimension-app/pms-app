package com.niafikra.dimension.issue.domain;


import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.core.security.domain.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "IS_Note")
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id", "content"})
public class Note implements Comparable<Note> {
    @Id
    @GeneratedValue
    private Long id;

    @Version
    private Integer version;

    @Lob
    @NotNull
    @NotBlank
    private String content;

    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private Set<Attachment> attachments;

    @CreationTimestamp
    private LocalDateTime timeCreated;

    @UpdateTimestamp
    private LocalDateTime lastUpdated;

    @ManyToOne
    @NotNull
    private User creator;

    @Override
    public int compareTo(Note other) {
        return this.timeCreated.compareTo(other.timeCreated);
    }

    public String toString() {
        return String.format("%s : %s", creator, content);
    }
}
