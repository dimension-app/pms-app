package com.niafikra.dimension.issue.ui.vaadin;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.domain.Note;
import com.niafikra.dimension.issue.domain.Resolution;
import com.niafikra.dimension.issue.service.IssueService;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

@ViewScope
@SpringComponent
public abstract class IssuesView extends VerticalLayout {
    private IssuesGrid issuesGrid;
    private IssueView issueView;
    private MButton createIssue;
    private ConfirmButton resolveButton;

    private MHorizontalLayout header;
    private IssueForm issueForm;
    private ResolutionForm resolutionForm;

    private Issue currentIssue;

    protected User user;
    protected IssueService issueService;

    public IssuesView(IssuesGrid issuesGrid,
                      IssueView issueView,
                      IssueForm issueForm,
                      ResolutionForm resolutionForm,
                      IssueService issueService,
                      User user) {

        this.issuesGrid = issuesGrid;
        this.issueView = issueView;
        this.issueForm = issueForm;
        this.resolutionForm = resolutionForm;

        this.user = user;
        this.issueService = issueService;
    }


    @PostConstruct
    private void build() {
        setSizeFull();
        createIssue = new MButton("New Issue")
                .withWidth("150px")
                .withStyleName(Theme.BUTTON_PRIMARY)
                .addClickListener(() -> doCreateIssue())
                .withVisible(hasAuthority(PMSPermission.POST_ISSUE));

        resolveButton = new ConfirmButton()
                .withCaption("Resolve")
                .withWidth("150px")
                .withStyleName(Theme.BUTTON_FRIENDLY)
                .addClickListener(() -> doResolve());
        header = new MHorizontalLayout(resolveButton, createIssue);

        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
        splitPanel.setSizeFull();
        addComponent(splitPanel);

        issuesGrid.setSizeFull();
        splitPanel.setFirstComponent(issuesGrid);

        issueView.setSizeFull();
        issueView.setMargin(false);
        splitPanel.setSecondComponent(
                new MVerticalLayout(header, issueView)
                        .withFullSize()
                        .withExpand(issueView, 1)
                        .withMargin(new MarginInfo(false, true, false, true))
        );

        issuesGrid.setSelectionMode(Grid.SelectionMode.NONE);
        issuesGrid.addItemClickListener(event -> {
            showIssue(event.getItem());
        });

        //reload the view after creating a comment
        issueView.getCommentForm().addSavedHandler(comment -> load());
        issueForm.addSavedHandler(issue -> {
            //if the issue was successful created then reload
            load();
            showIssue(issue);
        });
        resolutionForm.addSavedHandler(resolution -> load());
    }

    protected void doResolve() {
        resolutionForm.setIssue(currentIssue);
        resolutionForm.setEntity(new Resolution());
        resolutionForm.openInModalPopup();
    }

    protected void showIssue(Issue issue) {
        this.currentIssue = issue;
        issueView.setIssue(issue);
        resolveButton.setVisible(issue.canResolve(user) && hasAuthority(PMSPermission.RESOLVE_ISSUE));
    }

    protected void doCreateIssue() {
        issueForm.prepareCreateIssue();
        issueForm.openInModalPopup();
    }

    protected void load() {
        List<Issue> issues = getIssues();
        issues.sort(Comparator.comparing(Note::getTimeCreated).reversed());
        issuesGrid.setItems(issues);

        if (currentIssue != null) {
            currentIssue = issueService.getIssue(currentIssue.getId());
            showIssue(currentIssue);
        } else {
            Optional<Issue> issue = issues.stream().findFirst();
            if (issue.isPresent()) showIssue(issue.get());
        }
    }

    protected abstract List<Issue> getIssues();

    public IssuesGrid getIssuesGrid() {
        return issuesGrid;
    }

    public IssueView getIssueView() {
        return issueView;
    }

    public IssueForm getIssueForm() {
        return issueForm;
    }

    public ResolutionForm getResolutionForm() {
        return resolutionForm;
    }

    public Issue getCurrentIssue() {
        return currentIssue;
    }
}
