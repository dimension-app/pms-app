package com.niafikra.dimension.issue.ui.vaadin;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsButton;
import com.niafikra.dimension.issue.domain.Comment;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Set;

@PrototypeScope
@SpringComponent
public class CommentForm extends NoteForm<Comment> {

    public CommentForm(AttachmentsButton<Set<Attachment>> attachmentsButton) {
        super(Comment.class, attachmentsButton);
    }
}
