package com.niafikra.dimension.issue.ui.vaadin;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsButton;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.issue.domain.Note;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Window;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.Set;

public class NoteForm<T extends Note> extends AbstractForm<T> implements Notifier {
    private TextArea content = new TextArea();
    private AttachmentsField<Set<Attachment>> attachments;
    private AttachmentsButton<Set<Attachment>> attachmentsButton;

    public NoteForm(Class<T> type, AttachmentsButton<Set<Attachment>> attachmentsButton) {
        super(type);
        this.attachmentsButton = attachmentsButton;
        this.attachments = attachmentsButton.getAttachmentsField();

        content.setWidth("100%");
        content.setRows(3);
        content.setRequiredIndicatorVisible(false);

        getSaveButton().setCaption("Submit");
        getSaveButton().setWidth("150px");

        attachmentsButton
                .withCaption("Attach")
                .withIcon(VaadinIcons.UPLOAD_ALT)
                .withWidth("150px");
    }

    @Override
    protected MHorizontalLayout createContent() {
        MVerticalLayout toolbar = new MVerticalLayout(getSaveButton(), attachmentsButton).withMargin(false);
        toolbar.setWidth("160px");
        return new MHorizontalLayout(content, toolbar)
                .withAlign(toolbar, Alignment.MIDDLE_CENTER)
                .withExpand(content, 1)
                .withFullWidth();
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setWidth("600px");
        return window;
    }
}
