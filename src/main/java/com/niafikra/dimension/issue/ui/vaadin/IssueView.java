package com.niafikra.dimension.issue.ui.vaadin;

import com.google.common.collect.ImmutableMap;
import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.issue.domain.Comment;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.service.IssueService;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MPanel;

import javax.annotation.PostConstruct;
import java.util.Locale;

import static com.niafikra.dimension.core.security.SecurityUtils.hasAuthority;

@PrototypeScope
@SpringComponent
public class IssueView extends VerticalLayout implements Notifier {

    private CommentForm commentForm;
    private MLabel issueLabel = new MLabel();
    private MPanel issuePanel = new MPanel();

    private IssueService issueService;
    private SpringTemplateEngine templateEngine;

    private Issue issue;

    public IssueView(CommentForm commentForm,
                     IssueService issueService,
                     SpringTemplateEngine templateEngine) {
        this.commentForm = commentForm;
        this.issueService = issueService;
        this.templateEngine = templateEngine;
    }

    @PostConstruct
    private void build() {
        issueLabel.setWidth("100%");
        issueLabel.withContentMode(ContentMode.HTML);

        issuePanel.withFullSize();
        issuePanel.withStyleName(Theme.PANEL_BORDERLESS);
        addComponent(issuePanel.withContent(issueLabel));
        setExpandRatio(issuePanel, 1);

        commentForm.addSavedHandler(comment -> doSave(comment));
        commentForm.setWidth("100%");
        addComponent(commentForm);
    }

    private void doSave(Comment comment) {

        try {
            issue = issueService.createComment(issue, comment);
            //reload issue view
            setIssue(issue);
            showSuccess("Comment added successful");
        } catch (Exception e) {
            showError("Failed to add comment", e);
        }
    }

    public void setIssue(Issue issue) {
        this.issue = issue;

        issueLabel.setValue(
                templateEngine.process(Templates.ISSUE,
                        new Context(Locale.getDefault(), ImmutableMap.of("issue", issue))));

        //init empty comment
        commentForm.setEntity(new Comment());
        commentForm.setVisible(!issue.isResolved() && hasAuthority(PMSPermission.COMMENT_ISSUE));
    }

    public CommentForm getCommentForm() {
        return commentForm;
    }
}
