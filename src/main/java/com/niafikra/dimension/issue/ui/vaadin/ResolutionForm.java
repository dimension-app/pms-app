package com.niafikra.dimension.issue.ui.vaadin;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsButton;
import com.niafikra.dimension.issue.domain.Issue;
import com.niafikra.dimension.issue.domain.Resolution;
import com.niafikra.dimension.issue.service.IssueService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import java.util.Set;

@PrototypeScope
@SpringComponent
public class ResolutionForm extends NoteForm<Resolution> {

    private Issue issue;
    private IssueService issueService;

    public ResolutionForm(AttachmentsButton<Set<Attachment>> attachmentsButton,
                          IssueService issueService) {
        super(Resolution.class, attachmentsButton);
        this.issueService = issueService;
        addSavedHandler(resolution -> doResolve(resolution));
    }

    private void doResolve(Resolution resolution) {
        try {
            issue = issueService.resolve(issue, resolution);
            closePopup();
            showSuccess("Resolved issue successful");
        } catch (Exception e) {
            showError("Failed to resolve issue", e);
        }
    }

    @Override
    protected MHorizontalLayout createContent() {
        return super.createContent().withMargin(true);
    }

    @Override
    public Window openInModalPopup() {
        Window window = super.openInModalPopup();
        window.setCaption("Resolve issue");
        return window;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }
}
