package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.Formula;
import org.vaadin.viritin.label.MLabel;

public class ReadOnlyFormulaEditor<T extends Formula> extends MLabel implements FormulaEditor<T> {

    @Override
    public void setFormula(T formula) {
        setValue(formula.toString());
    }

    @Override
    public void setUpdateCallback(Callback callback) {

    }
}
