package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.Formula;
import com.vaadin.ui.Component;

public interface FormulaEditor<T extends Formula> extends Component {

    void setFormula(T formula);

    void setUpdateCallback(Callback callback);

    interface Callback {
        void call();
    }
}
