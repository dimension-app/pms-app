package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.service.ContractService;
import com.niafikra.dimension.payroll.service.ContractService.ContractFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.List;

@PrototypeScope
@SpringComponent
public class ContractsProvider extends PageableDataProvider<Contract, ContractFilter> {

    @Inject
    private ContractService contractService;

    @Override
    protected Page<Contract> fetchFromBackEnd(Query<Contract, ContractFilter> query, Pageable pageable) {
        return contractService.findAll(query.getFilter(), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(
                new QuerySortOrder("startDate", SortDirection.DESCENDING),
                new QuerySortOrder("code", SortDirection.DESCENDING)
        );
    }

    @Override
    protected int sizeInBackEnd(Query<Contract, ContractFilter> query) {
        return contractService.count(query.getFilter()).intValue();
    }
}
