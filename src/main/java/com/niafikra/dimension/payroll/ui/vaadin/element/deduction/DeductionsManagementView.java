package com.niafikra.dimension.payroll.ui.vaadin.element.deduction;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.niafikra.dimension.payroll.ui.vaadin.element.HasCategoryManagementView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.payroll.ui.vaadin.element.deduction.DeductionsManagementView.VIEW_NAME;


@Secured({PMSPermission.CONFIGURE_DEDUCTIONS})
@ViewComponent(value = SettingDisplay.class, caption = "Deductions")
@ViewInfo(value = "Deductions", section = "Payroll", icon = VaadinIcons.MINUS_SQUARE_LEFT_O)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class DeductionsManagementView extends HasCategoryManagementView<Deduction> implements SettingView {
    public static final String VIEW_NAME = "deductions";

    public DeductionsManagementView(DeductionCategoryCrud categoryCrud, DeductionCrud deductionCrud) {
        super(categoryCrud, deductionCrud);
        categoryCrud.setCaption("Deduction Categories");
    }
}