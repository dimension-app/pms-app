package com.niafikra.dimension.payroll.ui.vaadin.period;

import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.niafikra.dimension.payroll.service.PayPeriodService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


@ViewScope
@SpringComponent
public class PayPeriodGrid extends Grid<PayPeriodType> implements Notifier {

    @Inject
    private PayPeriodService periodService;

    @PostConstruct
    private void build() {
        addColumn(PayPeriodType::getSpecifier)
                .setId("specifier")
                .setCaption("Type")
                .setSortable(true);

        addColumn(PayPeriodType::getSteps)
                .setId("steps")
                .setCaption("Steps")
                .setSortable(true);

        refresh();
    }

    public void refresh() {
        setItems(periodService.findAllPayPeriodTypes());
    }
}
