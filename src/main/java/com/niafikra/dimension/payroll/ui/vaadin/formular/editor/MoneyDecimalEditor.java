package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.domain.formula.AbstractTwoInputFormula;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaSelector;
import com.niafikra.dimension.payroll.ui.vaadin.formular.NumberFormulaSelector;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;

@PrototypeScope
@SpringComponent
public class MoneyDecimalEditor extends VerticalLayout implements FormulaEditor<AbstractTwoInputFormula<Money, BigDecimal>> {

    @Inject
    private FormulaService formulaService;

    @Inject
    private MoneyFormulaSelector moneySelector;

    private NumberFormulaSelector decimalSelector = new NumberFormulaSelector();
    private Callback updateCallback;

    private Binder<AbstractTwoInputFormula> binder = new BeanValidationBinder<>(AbstractTwoInputFormula.class);

    private AbstractTwoInputFormula<Money, BigDecimal> formula;

    @PostConstruct
    private void build() {
        setWidth("100%");

        moneySelector.forAllFormulas();
        moneySelector.setWidth("100%");
        decimalSelector.setWidth("100%");

        addComponents(moneySelector, decimalSelector);

        binder.forField(moneySelector)
                .withValidator(new BeanValidator(AbstractTwoInputFormula.class, "first"))
                .withConverter(val -> formulaService.initMoneyFormula(val), formular -> formular == null ? "" : formular.getName())
                .bind("first");

        binder.forField(decimalSelector)
                .withValidator(new BeanValidator(AbstractTwoInputFormula.class, "second"))
                .withConverter(val -> FormulaService.initNumber(val), formular -> formular == null ? "" : formular.getName())
                .bind("second");


        moneySelector.addValueChangeListener(event -> {
            if (updateCallback != null) updateCallback.call();
        });

        decimalSelector.addValueChangeListener(event -> {
            if (updateCallback != null) updateCallback.call();
        });
    }

    @Override
    public void setFormula(AbstractTwoInputFormula<Money, BigDecimal> formula) {
        this.formula = formula;
        binder.setBean(formula);
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }

}
