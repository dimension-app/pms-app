package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.formula.MoneyScriptFormula;
import com.niafikra.dimension.script.domain.ScriptInfo;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class ScriptFormulaEditor extends MComboBox<ScriptInfo> implements FormulaEditor<MoneyScriptFormula> {

    private Callback updateCallback;
    private MoneyScriptFormula formula;

    public ScriptFormulaEditor() {
        setWidth("100%");

        addValueChangeListener(event -> {
            formula.setScript(event.getValue());

            if (updateCallback != null)
                updateCallback.call();
        });
    }

    @Override
    public void setFormula(MoneyScriptFormula formula) {
        this.formula = formula;
        setValue(formula.getScript());
    }

    @Override
    public void setUpdateCallback(Callback callback) {
        this.updateCallback = callback;
    }
}
