package com.niafikra.dimension.payroll.ui.vaadin.formular.wrapper;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.formula.wrapper.FormulaWrapper;
import com.niafikra.dimension.payroll.domain.formula.wrapper.MoneyFormulaWrapper;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import java.util.Comparator;

@PrototypeScope
@SpringComponent
public class MoneyFormulaWrapperComboBox extends MComboBox<MoneyFormulaWrapper> {

    public MoneyFormulaWrapperComboBox(FormulaService formulaService) {
        setItems(formulaService.findAllFormulaWrappers().stream().sorted(Comparator.comparing(FormulaWrapper::getName)));
    }
}
