package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted;

import com.niafikra.dimension.attachment.domain.Attachment;
import com.niafikra.dimension.attachment.ui.vaadin.util.AttachmentsField;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeComboBox;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedPay;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income.SubmitedIncomeForm;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Set;

public abstract class AbstractPaySubmitForm<T extends SubmittedPay> extends AbstractForm<T> {

    public static final String FIELD_WIDTH = "300px";

    @Inject
    protected EmployeeComboBox employee;

    protected DateField date = new DateField("Date");
    protected DateField dueDate = new DateField("Due Date");

    protected TextArea description = new TextArea("Description");

    @Inject
    protected AttachmentsField<Set<Attachment>> attachments;

    public AbstractPaySubmitForm(Class<T> type) {
        super(type);
    }

    @PostConstruct
    private void build() {
        employee.setCaption("Employee");
        employee.setWidth(FIELD_WIDTH);
        date.setWidth(FIELD_WIDTH);
        dueDate.setWidth(FIELD_WIDTH);
        description.setWidth("600px");
        attachments.setWidth(FIELD_WIDTH);
        attachments.setCaption("Attachments");
        getSaveButton().setWidth(SubmitedIncomeForm.FIELD_WIDTH);
    }

}
