package com.niafikra.dimension.payroll.ui.vaadin.payroll;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateRangeSelector;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.payroll.domain.contract.pay.Payroll;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.niafikra.dimension.payroll.ui.vaadin.period.PayPeriodTypeComboBox;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class PayrollsGrid extends Grid<Payroll> {

    @Inject
    private PayrollService payrollService;

    @Inject
    private PayPeriodTypeComboBox periodTypeFilter;

    public PayrollsGrid(PayrollsProvider provider) {
        super(provider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {
        addColumn(Payroll::getId)
                .setId("id")
                .setCaption("Code")
                .setSortable(true)
                .setHidable(true);


        addColumn(payroll -> DateUtils.formatDateTime(payroll.getTimeCreated()))
                .setId("timeCreated")
                .setCaption("Created")
                .setSortable(true)
                .setHidable(true);

        addColumn(payroll -> payroll.getPeriod())
                .setId("period")
                .setCaption("Period")
                .setSortable(false)
                .setHidable(true);

        addColumn(payroll -> payrollService.calculateTotal(payroll))
                .setId("totalPayroll")
                .setCaption("Total")
                .setSortable(true)
                .setHidable(true);

        addColumn(payroll -> payroll.getPeriod().getType())
                .setId("period.type")
                .setCaption("Type")
                .setSortable(true)
                .setHidable(true);

        addColumn(payroll -> payroll.getStatus())
                .setId("status")
                .setCaption("Status")
                .setSortable(false)
                .setHidable(true);

        addColumn(payroll -> payroll.getPeriod().getStart())
                .setId("period.start")
                .setCaption("From")
                .setSortable(true)
                .setHidable(true);

        addColumn(payroll -> payroll.getPeriod().getEnd())
                .setId("period.end")
                .setCaption("To")
                .setSortable(true)
                .setHidable(true);

        PayrollService.PayrollFilter filter = PayrollService.PayrollFilter.builder().build();
        getDataProvider().setFilter(filter);

        HeaderRow filtersRow = appendHeaderRow();
        periodTypeFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        periodTypeFilter.addValueChangeListener(event -> {
            filter.setPeriodType(event.getValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("period.type").setComponent(periodTypeFilter);

        LocalDateTimeRangeSelector timeCreatedFilter = new LocalDateTimeRangeSelector();
        timeCreatedFilter.setWidth("200px");
        filtersRow.getCell("timeCreated").setComponent(timeCreatedFilter);
        timeCreatedFilter.addValueChangeListener(event -> {
            filter.setTimeCreatedRange(timeCreatedFilter.getStart(), timeCreatedFilter.getEnd());
            getDataProvider().refreshAll();
        });

        LocalDateRangeSelector periodStartFilter = new LocalDateRangeSelector();
        periodStartFilter.setWidth("200px");
        filtersRow.getCell("period.start").setComponent(periodStartFilter);
        periodStartFilter.addValueChangeListener(event -> {
            filter.setPeriodStartRange(periodStartFilter.getStart(), periodStartFilter.getEnd());
            getDataProvider().refreshAll();
        });

        LocalDateRangeSelector periodEndFilter = new LocalDateRangeSelector();
        periodEndFilter.setWidth("200px");
        filtersRow.getCell("period.end").setComponent(periodEndFilter);
        periodEndFilter.addValueChangeListener(event -> {
            filter.setPeriodEndRange(periodEndFilter.getStart(), periodEndFilter.getEnd());
            getDataProvider().refreshAll();
        });

    }

    @Override
    public ConfigurableFilterDataProvider<Payroll, Void, PayrollService.PayrollFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Payroll, Void, PayrollService.PayrollFilter>) super.getDataProvider();
    }
}
