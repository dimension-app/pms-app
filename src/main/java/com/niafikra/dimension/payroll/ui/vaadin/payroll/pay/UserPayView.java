package com.niafikra.dimension.payroll.ui.vaadin.payroll.pay;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.security.domain.User;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.hr.service.EmployeeService;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;
import org.vaadin.viritin.label.MLabel;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.pay.UserPayView.VIEW_NAME;


@Secured(PMSPermission.VIEW_USER_PAYS)
@ViewComponent(value = MainDisplay.class, caption = "My Salaries/Wages")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.HR, caption = "My Salaries/Wages")
@VaadinFontIcon(VaadinIcons.USER_CARD)
public class UserPayView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "user-pays";

    @Inject
    private PayGrid payGrid;

    @Inject
    private Navigator navigator;

    @Inject
    private User user;

    @Inject
    private EmployeeService employeeService;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        Optional<Employee> employee = employeeService.findEmployee(user);
        if(employee.isPresent()){
            payGrid.setSizeFull();
            payGrid.setSelectionMode(Grid.SelectionMode.NONE);
            payGrid.addItemClickListener(event -> {
                navigator.navigateTo(PayView.class, event.getItem().getId());
            });

            addComponentsAndExpand(payGrid);

            payGrid.getFilter().setEmployee(employee.get());
            payGrid.getDataProvider().refreshAll();
            payGrid.getEmployeeFilter().setVisible(false);
        }else{
            addComponent(new MLabel("Ooops! There is no employee records associated with your account.\n Kindly contact system administrator")
                    .withStyleName(Theme.LABEL_COLORED, Theme.LABEL_H1).withFullWidth());
        }

    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(payGrid);
    }
}
