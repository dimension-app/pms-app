package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedDeduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.VIEW_SUBMITTED_DEDUCTIONS;
import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction.SubmittedDeductionView.VIEW_NAME;

@Secured(VIEW_SUBMITTED_DEDUCTIONS)
@ViewComponent(value = MainDisplay.class, caption = "View submitted deduction details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class SubmittedDeductionView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "submitted-deduction";

    @Inject
    private DeductionService deductionService;
    @Inject
    private TemplatePanel templatePanel;
    @Inject
    private Navigator navigator;
    @Inject
    private MainHeader mainHeader;

    private SubmittedDeduction deduction;

    @PostConstruct
    private void build() {
        setSizeFull();
        templatePanel.setSizeFull();
        templatePanel.getTemplateView().setTemplatePath(Templates.SUBMITTED_DEDUCTION);
        addComponentsAndExpand(templatePanel);
    }

    public void setSubmittedDeduction(SubmittedDeduction deduction) {
        this.deduction = deduction;
        templatePanel.getTemplateView().putBinding("submittedDeduction", deduction).render();
        mainHeader.getMenuBar().setMenuVisible("Edit", !deduction.isProcessed());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long deductionId = Long.parseLong(event.getParameters());
        setSubmittedDeduction(deductionService.findSubmittedDeduction(deductionId));
    }

    @Secured(PMSPermission.UPDATE_SUBMITTED_DEDUCTION)
    @ViewMenuOption(value = "Edit", menu = "Deduction", order = 1, icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.MINUS_SQUARE_LEFT_O)
    public void showDeductionUpdateForm() {
        navigator.navigateTo(SubmittedDeductionUpdateView.class, deduction.getId());
    }

}
