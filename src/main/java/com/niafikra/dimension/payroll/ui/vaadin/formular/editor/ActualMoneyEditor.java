package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.payroll.domain.formula.ActualMoney;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class ActualMoneyEditor extends VerticalLayout implements FormulaEditor<ActualMoney> {
    private Callback updateCallback;
    private ActualMoney formula;
    private MoneyField moneyField = new MoneyField();


    @PostConstruct
    private void build() {
        addComponentsAndExpand(moneyField);
        moneyField.addValueChangeListener(event -> {
            formula.setMoney(event.getValue());
            updateCallback.call();
        });
    }

    @Override
    public void setFormula(ActualMoney formula) {
        this.formula = formula;
        moneyField.setValue(formula.getMoney());
    }

    @Override
    public void setUpdateCallback(Callback callback) {
        this.updateCallback = callback;
    }
}
