package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedDeduction;
import com.niafikra.dimension.payroll.service.DeductionService.SubmittedDeductionFilter;
import com.niafikra.dimension.payroll.ui.vaadin.element.deduction.DeductionComboBox;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.AbstractSubmittedPayGrid;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class SubmittedDeductionGrid extends AbstractSubmittedPayGrid<SubmittedDeduction, SubmittedDeductionFilter> {

    @Inject
    private DeductionComboBox deductionFilter;

    public SubmittedDeductionGrid(SubmittedDeductionsProvider provider) {
        super(provider,new SubmittedDeductionFilter());
    }

    @PostConstruct
    private void build() {

        addColumn(SubmittedDeduction::getDeduction)
                .setId("deduction")
                .setCaption("Deduction")
                .setSortable(true);


        addColumn(SubmittedDeduction::getCalculatedEmployeeContrAmount)
                .setId("calculatedEmployeeContr")
                .setCaption("Employee Contr.")
                .setSortable(true);

        addColumn(SubmittedDeduction::getCalculatedEmployerContrAmount)
                .setId("calculatedEmployerContr")
                .setCaption("Employer Contr.")
                .setSortable(true);

        addColumn(SubmittedDeduction::getEmployeeContr)
                .setId("employeeContr")
                .setCaption("Employee Contr. Formula")
                .setSortable(true);

        addColumn(SubmittedDeduction::getEmployerContr)
                .setId("employerContr")
                .setCaption("Employer Contr. Formula")
                .setSortable(true);

        HeaderRow filtersRow = getFiltersRow();

        deductionFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        deductionFilter.addValueChangeListener(event -> {
            filter.setDeduction(event.getValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("deduction").setComponent(deductionFilter);
    }
}
