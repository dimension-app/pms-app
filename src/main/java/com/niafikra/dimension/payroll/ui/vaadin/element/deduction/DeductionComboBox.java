package com.niafikra.dimension.payroll.ui.vaadin.element.deduction;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;


@PrototypeScope
@SpringComponent
public class DeductionComboBox extends MComboBox<Deduction> {

    public DeductionComboBox(DeductionService deductionService) {
        setItems(deductionService.findAll());
    }
}
