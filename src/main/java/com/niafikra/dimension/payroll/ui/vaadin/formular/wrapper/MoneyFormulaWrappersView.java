package com.niafikra.dimension.payroll.ui.vaadin.formular.wrapper;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrud;
import com.niafikra.dimension.core.ui.vaadin.util.MHorizontalSplitCrudLayout;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.payroll.domain.formula.wrapper.MoneyFormulaWrapper;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.fields.MTextField;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.formular.wrapper.MoneyFormulaWrappersView.VIEW_NAME;


@Secured(PMSPermission.CONFIGURE_MONEY_FORMULA_WRAPPERS)
@ViewComponent(value = SettingDisplay.class, caption = "Money Formula")
@ViewInfo(value = "Money Formula", section = "Payroll", icon = VaadinIcons.CALC)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class MoneyFormulaWrappersView extends MGridCrud<MoneyFormulaWrapper> implements SettingView, Notifier {

    public static final String VIEW_NAME = "money-formula";

    @Inject
    private FormulaService formulaService;

    @Inject
    private MoneyFormulaField formulaField;

    public MoneyFormulaWrappersView() {
        super(MoneyFormulaWrapper.class, MHorizontalSplitCrudLayout.create(35));
    }

    @PostConstruct
    private void build() {
        setSizeFull();

        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("name", () -> new MTextField());
        getCrudFormFactory().setFieldProvider("wrappedFormula", () -> {
            formulaField.clear();
            return formulaField;
        });

        getCrudFormFactory().setVisibleProperties("name", "wrappedFormula");

        getGrid().setColumns("name");

        setFindAllOperation(() -> formulaService.findAllFormulaWrappers());
        setAddOperation(formula -> doSave(formula));
        setUpdateOperation(formula -> doSave(formula));
        setDeleteOperation(formula -> doDelete(formula));
    }

    private void doDelete(MoneyFormulaWrapper formula) {
        try {
            formulaService.delete(formula);
            showSuccess("Successfully deleted formula", formula.toString());
        } catch (Exception e) {
            showError("Failed to delete formula", e);
        }
    }

    private MoneyFormulaWrapper doSave(MoneyFormulaWrapper formula) {
        try {
            formula = formulaService.save(formula);
            showSuccess("Successfully saved formula", formula.toString());
            return formula;
        } catch (Exception e) {
            showError("Failed to save formula", e);
            return null;
        }
    }
    
}
