package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.DeductionBasedFormula;
import com.niafikra.dimension.payroll.ui.vaadin.element.deduction.DeductionComboBox;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class DeductionBasedFormulaEditor extends MVerticalLayout implements FormulaEditor<DeductionBasedFormula> {
    private Callback updateCallback;
    private DeductionBasedFormula formula;

    @Inject
    private DeductionComboBox deductionSelector;

    private CheckBox employerContrCheck;

    @PostConstruct
    private void build() {
        setWidth("100%");
        setMargin(false);

        deductionSelector.setWidth("100%");

        deductionSelector.addValueChangeListener(event -> {
            formula.setDeduction(event.getValue());

            if (updateCallback != null)
                updateCallback.call();
        });

        employerContrCheck = new CheckBox("Apply employer contribution");
        employerContrCheck.addValueChangeListener(event -> {
            formula.setOnEmployerContr(event.getValue());

            if (updateCallback != null)
                updateCallback.call();
        });

        add(deductionSelector, employerContrCheck);
    }

    @Override
    public void setFormula(DeductionBasedFormula formula) {
        this.formula = formula;

        deductionSelector.setValue(formula.getDeduction());
        employerContrCheck.setValue(formula.isOnEmployerContr());
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }
}

