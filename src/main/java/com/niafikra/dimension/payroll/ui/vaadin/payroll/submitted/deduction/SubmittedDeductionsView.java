package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.VIEW_SUBMITTED_DEDUCTIONS;
import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction.SubmittedDeductionsView.VIEW_NAME;

@Secured(VIEW_SUBMITTED_DEDUCTIONS)
@ViewComponent(value = MainDisplay.class, caption = "Deductions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.HR, caption = "Deductions")
@VaadinFontIcon(VaadinIcons.MINUS_SQUARE_LEFT_O)
public class SubmittedDeductionsView extends VerticalLayout implements View, TableExportable {

    public static final String VIEW_NAME = "submitted-deductions";

    @Inject
    private SubmittedDeductionGrid deductionsGrid;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        deductionsGrid.setSizeFull();
        deductionsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        deductionsGrid.addItemClickListener(event -> {
            navigator.navigateTo(SubmittedDeductionView.class, event.getItem().getId());
        });

        addComponentsAndExpand(deductionsGrid);
    }

    @Secured(PMSPermission.SUBMIT_EMPLOYEE_DEDUCTION)
    @ViewMenuOption(value = "Record Deduction", icon = VaadinIcons.PLUS_CIRCLE)
    public void showDeductionSubmitForm() {
        navigator.navigateTo(DeductionSubmitView.class);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(deductionsGrid);
    }
}
