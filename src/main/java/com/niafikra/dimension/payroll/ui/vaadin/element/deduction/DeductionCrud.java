package com.niafikra.dimension.payroll.ui.vaadin.element.deduction;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.niafikra.dimension.payroll.ui.vaadin.element.PayElementCrud;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.vaadin.data.HasValue;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collection;

@PrototypeScope
@SpringComponent
public class DeductionCrud extends PayElementCrud<Deduction> implements Notifier {

    @Inject
    private DeductionService deductionService;
    @Inject
    private CategoryService categoryService;

    @Inject
    private MoneyFormulaField employeeContrField;
    @Inject
    private MoneyFormulaField employerContrField;

    public DeductionCrud() {
        super(Deduction.class);
    }

    @PostConstruct
    private void build() {
        Grid<Deduction> grid = getGrid();
        grid.setColumns("name", "category");

        employeeContrField.setCaption("Employee Contribution");
        employerContrField.setCaption("Employer Contribution");

        getCrudFormFactory().setVisibleProperties("name", "reference", "active","category", "employeeContr", "employerContr");
        getCrudFormFactory().setFieldProvider("category", () -> createCategorySelector());
        getCrudFormFactory().setFieldProvider("employeeContr", () -> {
            employeeContrField.clear();
            return employeeContrField;
        });
        getCrudFormFactory().setFieldProvider("employerContr", () -> {
            employerContrField.clear();
            return employerContrField;
        });

        setFindAllOperation(() -> deductionService.findAll());
        setAddOperation(deduction -> doCreate(deduction));
        setUpdateOperation(deduction -> doUpdate(deduction));
        setDeleteOperation(deduction -> doDelete(deduction));
    }

    private HasValue createCategorySelector() {
        return new MComboBox<Category>("Category").withItems(categoryService.getAll(Deduction.CATEGORY_TYPE));
    }

    private Deduction doCreate(Deduction deduction) {
        try {
            deduction = deductionService.create(deduction);
            showSuccess("Successful created deduction", deduction.toString());
            return deduction;
        } catch (Exception e) {
            showError("Failed to create deduction", e);
            throw e;
        }
    }

    private Deduction doUpdate(Deduction deduction) {
        try {
            deduction = deductionService.update(deduction);
            showSuccess("Successfully updated deduction", deduction.toString());
            return deduction;
        } catch (Exception e) {
            showError("Failed to update deduction", e);
            throw e;
        }
    }

    private void doDelete(Deduction deduction) {
        try {
            deductionService.delete(deduction);
            showSuccess("Successfully deleted deduction", deduction.toString());
        } catch (Exception e) {
            showError("Failed to update deduction", e);
            throw e;
        }
    }

    @Override
    protected Collection<Category> getFilterCategories() {
        return categoryService.getAll(Deduction.CATEGORY_TYPE);
    }
}
