package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.util.DateUtils;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedDeduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.deduction.DeductionSubmitView.VIEW_NAME;

@Secured(PMSPermission.SUBMIT_EMPLOYEE_DEDUCTION)
@ViewComponent(value = MainDisplay.class, caption = "Submit employee deduction")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class DeductionSubmitView extends SubmittedDeductionForm implements View, Notifier {

    public static final String VIEW_NAME = "submit-deduction";

    @Inject
    private DeductionService deductionService;


    @PostConstruct
    private void build() {
        setSizeFull();

        addSavedHandler(deduction -> doSubmit(deduction));
    }

    private void doSubmit(SubmittedDeduction deduction) {
        try {
            deduction = deductionService.submit(deduction);
            showSuccess(String.format("Successful submitted deduction for %s", deduction.getEmployee()));
            loadNewDeduction();
        } catch (Exception e) {
            showError(String.format("Failed to submit deduction for %s", deduction.getEmployee()), e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        loadNewDeduction();
    }

    private void loadNewDeduction() {
        SubmittedDeduction deduction = new SubmittedDeduction();
        deduction.setDate(LocalDate.now());
        deduction.setDueDate(DateUtils.getStartOfNextMonth().toLocalDate());

        setEntity(deduction);
    }
}
