package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedIncome;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.niafikra.dimension.payroll.service.IncomeService.SubmittedIncomeFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.List;

@PrototypeScope
@SpringComponent
public class SubmittedIncomesProvider extends PageableDataProvider<SubmittedIncome, SubmittedIncomeFilter> {

    @Inject
    private IncomeService incomeService;

    @Override
    protected Page<SubmittedIncome> fetchFromBackEnd(Query<SubmittedIncome, SubmittedIncomeFilter> query, Pageable pageable) {
        return incomeService.findSubmittedIncomes(query.getFilter(), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(
                new QuerySortOrder("date", SortDirection.DESCENDING)
        );
    }

    @Override
    protected int sizeInBackEnd(Query<SubmittedIncome, SubmittedIncomeFilter> query) {
        return incomeService.countSubmittedIncomes(query.getFilter()).intValue();
    }
}
