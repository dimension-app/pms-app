package com.niafikra.dimension.payroll.ui.vaadin.formular.table;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.MGridCrud;
import com.niafikra.dimension.core.ui.vaadin.util.MGridLayoutCrudFormFactory;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTable;
import com.niafikra.dimension.payroll.service.TableService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractSplitPanel;
import com.vaadin.ui.Button;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MGridLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.formular.table.MoneyTablesView.VIEW_NAME;


@Secured(PMSPermission.CONFIGURE_MONEY_TABLES)
@ViewComponent(value = SettingDisplay.class, caption = "Money Tables")
@ViewInfo(value = "Money Tables", section = "Payroll", icon = VaadinIcons.TABLE)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class MoneyTablesView extends MGridCrud<MoneyTable> implements SettingView, Notifier {

    public static final String VIEW_NAME = "money-table";

    @Inject
    private TableService tableService;

    @Inject
    private MoneyTableEntriesField tableEntriesField;

    public MoneyTablesView() {
        super(MoneyTable.class, new MoneyTablesViewLayout());
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        setCrudFormFactory(new MoneyTableCrudFormFactory());

        tableEntriesField.setSizeFull();

        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("name", () -> new MTextField());
        getCrudFormFactory().setFieldProvider("entries", () -> tableEntriesField);
        getCrudFormFactory().setVisibleProperties("name", "entries");

        getGrid().setColumns("name");

        setFindAllOperation(() -> tableService.findMoneyTables());
        setAddOperation(table -> doSave(table));
        setUpdateOperation(table -> doSave(table));
        setDeleteOperation(table -> doDelete(table));
    }

    private void doDelete(MoneyTable table) {
        try {
            tableService.delete(table);
            showSuccess("Successful deleted money table", table.toString());
        } catch (Exception e) {
            showError("Failed to delete money table", e);
        }
    }

    private MoneyTable doSave(MoneyTable table) {
        try {
            table = tableService.save(table);
            showSuccess("Successful saved money table", table.toString());
            return table;
        } catch (Exception e) {
            showError("Failed to save money table", e);
            return null;
        }
    }

    private static class MoneyTablesViewLayout extends HorizontalSplitCrudLayout {
        public MoneyTablesViewLayout() {
            super();
            secondComponent.setSizeFull();
        }

        @Override
        protected AbstractSplitPanel getMainLayout() {
            AbstractSplitPanel splitPanel = super.getMainLayout();
            splitPanel.setSplitPosition(20);
            return splitPanel;
        }
    }

    private static class MoneyTableCrudFormFactory extends MGridLayoutCrudFormFactory<MoneyTable> {

        public MoneyTableCrudFormFactory() {
            super(MoneyTable.class, 1, 2);
        }

        @Override
        public MVerticalLayout buildNewForm(CrudOperation operation, MoneyTable domainObject, boolean readOnly, Button.ClickListener cancelButtonClickListener, Button.ClickListener operationButtonClickListener) {
            return super.buildNewForm(operation, domainObject, readOnly, cancelButtonClickListener, operationButtonClickListener).withFullSize();
        }

        @Override
        protected MGridLayout initGridLayout() {
           MGridLayout layout = super.initGridLayout();
           layout.setRowExpandRatio(1,1);
           return layout;
        }
    }

}
