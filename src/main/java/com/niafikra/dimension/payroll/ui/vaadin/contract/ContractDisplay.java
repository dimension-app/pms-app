package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.view.DisplayComponent;
import com.niafikra.dimension.core.ui.vaadin.view.EntireMainContentDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.service.ContractService;
import com.niafikra.dimension.payroll.ui.vaadin.contract.pay.RecurringDeductionsView;
import com.niafikra.dimension.payroll.ui.vaadin.contract.pay.RecurringIncomesView;
import com.vaadin.icons.VaadinIcons;
import org.springframework.security.access.annotation.Secured;

import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.UPDATE_CONTRACT;
import static com.niafikra.dimension.PMSPermission.VIEW_CONTRACTS;

@DisplayComponent
public class ContractDisplay extends EntireMainContentDisplay {

    @Inject
    private ContractService contractService;

    private Contract contract;

    public void setContract(Contract contract) {
        this.contract = contract;
        this.mainHeader.setHeading(contract.toString());
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        Long contractId = Long.parseLong(event.getParameters());
        setContract(contractService.findContract(contractId));
    }

    @Secured(VIEW_CONTRACTS)
    @ViewMenuOption(value = "View", menu = "Contract", order = 0, icon = VaadinIcons.FILE_PRESENTATION, menuIcon = VaadinIcons.HOURGLASS)
    public void showContractDetails() {
        navigator.navigateTo(ContractView.class, contract.getId());
    }

    @Secured(UPDATE_CONTRACT)
    @ViewMenuOption(value = "Edit", menu = "Contract", order = 1, icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.HOURGLASS, separator = true)
    public void showContractUpdateForm() {
        navigator.navigateTo(ContractUpdateView.class, contract.getId());
    }

    @Secured({PMSPermission.MANAGE_CONTRACT_RECURRING_INCOMES})
    @ViewMenuOption(value = "Incomes", menu = "Contract", icon = VaadinIcons.PLUS_CIRCLE_O, order = 2, menuIcon = VaadinIcons.HOURGLASS)
    public void manageEmployeeRecurringIncomes() {
        navigator.navigateTo(RecurringIncomesView.class, contract.getId());
    }

    @Secured(PMSPermission.MANAGE_CONTRACT_RECURRING_DEDUCTIONS)
    @ViewMenuOption(value = "Deductions", menu = "Contract", icon = VaadinIcons.MINUS_CIRCLE_O, order = 3, menuIcon = VaadinIcons.HOURGLASS)
    public void manageEmployeeRecurringDeductions() {
        navigator.navigateTo(RecurringDeductionsView.class, contract.getId());
    }

}
