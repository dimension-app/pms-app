package com.niafikra.dimension.payroll.ui.vaadin.bank;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.hr.domain.Contact;
import com.niafikra.dimension.payroll.domain.Bank;
import com.niafikra.dimension.payroll.service.BankService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextArea;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;
import org.vaadin.viritin.fields.EmailField;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.bank.BanksView.VIEW_NAME;


@Secured(PMSPermission.CONFIGURE_BANKS)
@ViewComponent(value = SettingDisplay.class, caption = "Banks")
@ViewInfo(value = "Banks", section = "Payroll", icon = VaadinIcons.TABLE)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class BanksView extends GridCrud<Bank> implements SettingView, Notifier {

    public static final String VIEW_NAME = "banks";

    @Inject
    private BankService bankService;

    public BanksView() {
        super(Bank.class, new HorizontalSplitCrudLayout());
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        getCrudFormFactory().setUseBeanValidation(true);
        getCrudFormFactory().setFieldProvider("contact.email", () -> new EmailField("Email"));
        getCrudFormFactory().setFieldProvider("contact.address", () -> new TextArea("Address"));
        getCrudFormFactory().setVisibleProperties("code", "name", "contact.email", "contact.phone", "contact.address");
        getGrid().setColumns("code", "name", "contact.email", "contact.phone");

        setFindAllOperation(() -> bankService.findAll());
        setAddOperation(bank -> doSave(bank));
        setUpdateOperation(bank -> doSave(bank));
        setDeleteOperation(bank -> doDelete(bank));
    }

    private void doDelete(Bank bank) {
        try {
            bankService.delete(bank);
            showSuccess("Successful deleted  bank", bank.toString());
        } catch (Exception e) {
            showError("Failed to delete  bank", e);
        }
    }

    @Override
    protected void showForm(CrudOperation operation, Bank bank, boolean readOnly, String successMessage, Button.ClickListener buttonClickListener) {
        if (operation == CrudOperation.ADD) bank.setContact(new Contact());
        super.showForm(operation, bank, readOnly, successMessage, buttonClickListener);
    }

    private Bank doSave(Bank bank) {
        try {
            bank = bankService.save(bank);
            showSuccess("Successful saved  bank details", bank.toString());
            return bank;
        } catch (Exception e) {
            showError("Failed to save bank details", e);
            return null;
        }
    }
}
