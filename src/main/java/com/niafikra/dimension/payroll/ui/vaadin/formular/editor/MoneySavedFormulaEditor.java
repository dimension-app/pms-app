package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.wrapper.MoneyFormulaWrapper;
import com.niafikra.dimension.payroll.domain.formula.wrapper.MoneySavedFormula;
import com.niafikra.dimension.payroll.ui.vaadin.formular.wrapper.MoneyFormulaWrapperComboBox;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


@PrototypeScope
@SpringComponent
public class MoneySavedFormulaEditor extends MVerticalLayout implements FormulaEditor<MoneySavedFormula> {
    private Callback updateCallback;
    private MoneySavedFormula formula;

    @Inject
    private MoneyFormulaWrapperComboBox wrapperSelector;

    @PostConstruct
    private void build() {
        setWidth("100%");
        setMargin(false);

        wrapperSelector.setWidth("100%");
        add(wrapperSelector);

        wrapperSelector.addValueChangeListener(event -> {
            formula.setWrapper(event.getValue());

            if (updateCallback != null)
                updateCallback.call();
        });
    }

    @Override
    public void setFormula(MoneySavedFormula formula) {
        this.formula = formula;

        //if the money wrapper is of class MoneyFormulaWrapper then set as the value
        //since the wrapper can be any other extension of AbstractWrapper<Money>
        if (formula.getWrapper() != null && MoneyFormulaWrapper.class.isAssignableFrom(formula.getWrapper().getClass()))
            wrapperSelector.setValue((MoneyFormulaWrapper) formula.getWrapper());
        else wrapperSelector.clear();
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }
}
