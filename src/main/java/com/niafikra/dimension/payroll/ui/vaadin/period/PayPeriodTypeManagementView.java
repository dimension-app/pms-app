package com.niafikra.dimension.payroll.ui.vaadin.period;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.util.create.SplitCreateViewPanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.niafikra.dimension.payroll.service.PayPeriodService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import org.springframework.security.access.annotation.Secured;

import javax.inject.Inject;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.addActionButton;
import static com.niafikra.dimension.payroll.ui.vaadin.period.PayPeriodTypeManagementView.VIEW_NAME;


@Secured(PMSPermission.CONFIGURE_PAY_PERIODS)
@ViewComponent(value = SettingDisplay.class, caption = "Pay Periods")
@ViewInfo(value = "Pay Periods", section = "Payroll", icon = VaadinIcons.CALENDAR_USER)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PayPeriodTypeManagementView extends SplitCreateViewPanel<PayPeriodType> implements SettingView, Notifier {
    public static final String VIEW_NAME = "pay-periods";

    @Inject
    private PayPeriodPanel periodPanel;
    @Inject
    private PayPeriodForm periodForm;
    @Inject
    private PayPeriodService periodService;

    public PayPeriodTypeManagementView(PayPeriodGrid periodGrid) {
        super(periodGrid);

        addActionButton(
                periodGrid,
                "Should this period be removed completely",
                type -> doDelete(type),
                button -> button.setIcon(VaadinIcons.TRASH)
        );

        addActionButton(
                periodGrid,
                "Should this period be deactivated",
                type -> doDeactivate(type),
                button -> button.setIcon(VaadinIcons.LOCK)
        );
    }

    private void doDeactivate(PayPeriodType type) {
        showViewContent(type);
    }

    private void doDelete(PayPeriodType type) {
        try {
            periodService.delete(type);
            getEntriesGrid().refresh();
            showCreateContent();
            showSuccess("Successful deleted period type", type.toString());
        } catch (Exception e) {
            showError("Failed to delete period type", e);
        }
    }

    @Override
    protected Component getCreateComponent() {
        periodForm.setEntity(new PayPeriodType());
        return periodForm;
    }

    @Override
    protected Component getViewComponent(PayPeriodType entry) {
        return periodPanel.setPayPeriodType(entry);
    }

    @Override
    public PayPeriodGrid getEntriesGrid() {
        return (PayPeriodGrid) super.getEntriesGrid();
    }
}
