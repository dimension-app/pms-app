package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.IncomeBasedFormula;
import com.niafikra.dimension.payroll.ui.vaadin.element.income.IncomeComboBox;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class IncomeBasedFormulaEditor extends MVerticalLayout implements FormulaEditor<IncomeBasedFormula> {
    private Callback updateCallback;
    private IncomeBasedFormula formula;

    @Inject
    private IncomeComboBox incomeSelector;

    @PostConstruct
    private void build() {
        setWidth("100%");
        setMargin(false);

        incomeSelector.setWidth("100%");
        add(incomeSelector);

        incomeSelector.addValueChangeListener(event -> {
            formula.setIncome(event.getValue());

            if (updateCallback != null)
                updateCallback.call();
        });
    }

    @Override
    public void setFormula(IncomeBasedFormula formula) {
        this.formula = formula;

        incomeSelector.setValue(formula.getIncome());
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }
}

