package com.niafikra.dimension.payroll.ui.vaadin.payroll;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.Payroll;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.PayrollView.VIEW_NAME;


@Secured(PMSPermission.VIEW_PAYROLL)
@ViewComponent(value = PayrollDisplay.class, caption = "Payroll Details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PayrollView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "payroll";


    @Inject
    private PayrollService payrollService;
    @Inject
    private TemplatePanel templatePanel;

    private Payroll payroll;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        templatePanel.setSizeFull();
        templatePanel
                .getTemplateView()
                .setTemplatePath(Templates.PAYROLL);
        addComponentsAndExpand(templatePanel);
    }

    public void setPayroll(Payroll payroll) {
        this.payroll = payroll;
        templatePanel.getTemplateView()
                .putBinding("payroll", payroll)
                .render();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long payrollId = Long.parseLong(event.getParameters());
        setPayroll(payrollService.findPayroll(payrollId));
    }
}
