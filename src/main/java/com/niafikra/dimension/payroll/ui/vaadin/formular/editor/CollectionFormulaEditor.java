package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.core.ui.vaadin.util.SubWindow;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.domain.formula.Formula;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaSelector;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class CollectionFormulaEditor extends VerticalLayout implements FormulaEditor<Formula<Money>> {

    private Formula formula;
    private Grid<Formula> inputsGrid = new Grid<>();
    private MButton addInputButton = new MButton("Add").withIcon(VaadinIcons.PLUS_CIRCLE);
    @Inject
    private MoneyFormulaSelector formulaSelector;
    @Inject
    private FormulaService formulaService;

    private Callback updateCallback;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        inputsGrid.setSizeFull();
        addComponents(addInputButton, inputsGrid);
        setExpandRatio(inputsGrid, 1);

        formulaSelector.forAllFormulas();
        formulaSelector.setWidth("400px");
        formulaSelector.addValueChangeListener(event -> {
            if(!formulaSelector.isEmpty()){
                String formulaName = event.getValue();
                formula.getChildren().add(formulaService.initMoneyFormula(formulaName));
                refreshIputsGrid();
                updateCallback.call();
                formulaSelector.clear();
            }
        });

        addInputButton.addClickListener(() -> {
            new SubWindow(new MVerticalLayout(formulaSelector))
                    .withCaption("Select ")
                    .show();
        });

        inputsGrid.addColumn(entry -> entry.toString());
        VaadinUtils.addRemoveButton(inputsGrid, entry -> {
            formula.getChildren().remove(entry);
            refreshIputsGrid();
            updateCallback.call();
        });
    }

    @Override
    public void setFormula(Formula<Money> formula) {
        this.formula = formula;
        refreshIputsGrid();
    }

    private void refreshIputsGrid() {
        inputsGrid.setItems(formula.getChildren());
    }

    @Override
    public void setUpdateCallback(Callback callback) {
        this.updateCallback = callback;
    }

}
