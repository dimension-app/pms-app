package com.niafikra.dimension.payroll.ui.vaadin.contract.pay;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.RecurringDeductionConfig;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.niafikra.dimension.payroll.ui.vaadin.contract.ContractDisplay;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;

import static com.niafikra.dimension.PMSPermission.MANAGE_CONTRACT_RECURRING_DEDUCTIONS;
import static com.niafikra.dimension.payroll.ui.vaadin.contract.pay.RecurringDeductionsView.VIEW_NAME;

@Secured(MANAGE_CONTRACT_RECURRING_DEDUCTIONS)
@ViewComponent(value = ContractDisplay.class, caption = "Recurring Deductions")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class RecurringDeductionsView extends RecurringPayPanel<RecurringDeductionConfig> implements Notifier {
    public static final String VIEW_NAME = "recurring-deductions";

    @Inject
    private DeductionService deductionService;

    @Inject
    private MoneyFormulaField employeeContrFormulaField;

    @Inject
    private MoneyFormulaField employerContrFormulaField;

    public RecurringDeductionsView() {
        super(RecurringDeductionConfig.class);
        setCrudFormFactory(new RecurringDeductionCrudFormFactory());
    }

    @PostConstruct
    private void build() {
        employeeContrFormulaField.setCaption("Employee Contribution");
        employerContrFormulaField.setCaption("Employer Contribution");

        getGrid().setColumns("deduction", "actualStartDate", "actualEndDate");
        setFindAllOperation(() -> {
            if (getContract() == null) return Collections.emptyList();
            return getContract().getRecurringDeductions();
        });

        setAddOperation(deduction -> doCreate(deduction));
        setUpdateOperation(deduction -> doUpdate(deduction));
        setDeleteOperation(deduction -> doDelete(deduction));
    }

    private void doDelete(RecurringDeductionConfig deduction) {
        try {
            contract = contractService.cancelRecurringDeduction(deduction);
            refreshGrid();
            showSuccess("Successfully cancelled recurring deduction", deduction.getDeduction().toString());
        } catch (Exception e) {
            showError("Failed to cancel recurring deduction", e);
        }
    }

    private RecurringDeductionConfig doCreate(RecurringDeductionConfig deduction) {
        try {
            contract = contractService.registerRecurringDeduction(contract, deduction);
            refreshGrid();
            showSuccess("Successfully added contractual recurring deduction", deduction.getDeduction().toString());
            return deduction;
        } catch (Exception e) {
            showError("Failed to add recurring deduction", e);
            return null;
        }
    }

    private RecurringDeductionConfig doUpdate(RecurringDeductionConfig deduction) {
        try {
            contractService.update(deduction);
            reload();
            showSuccess("Successfully updated contractual recurring deduction", deduction.getDeduction().toString());
            return deduction;
        } catch (Exception e) {
            showError("Failed to update recurring deduction", e);
            throw e;
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long contractId = Long.parseLong(event.getParameters());
        setContract(contractService.findContract(contractId));
    }


    protected class RecurringDeductionCrudFormFactory extends RecurringPayCrudFormFactory<RecurringDeductionConfig> {

        public RecurringDeductionCrudFormFactory() {
            super(RecurringDeductionConfig.class);

            setFieldProvider(
                    "employeeContr",
                    () -> {
                        employeeContrFormulaField.clear();
                        return employeeContrFormulaField;
                    }
            );

            setFieldProvider(
                    "employerContr",
                    () -> {
                        employerContrFormulaField.clear();
                        return employerContrFormulaField;
                    }
            );

            setFieldProvider("deduction", () -> new MComboBox("Deduction").withItems(deductionService.findAll()));
            setVisibleProperties("deduction", "employeeContr", "employerContr", "startDate", "endDate", "startPeriodIndex", "skipPeriods", "excludePeriodIndices");
        }
    }

}
