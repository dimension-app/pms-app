package com.niafikra.dimension.payroll.ui.vaadin.element.income;

import com.niafikra.dimension.category.ui.vaadin.CategoryCrud;
import com.niafikra.dimension.payroll.domain.element.Income;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;


@PrototypeScope
@SpringComponent
public class IncomeCategoryCrud extends CategoryCrud {

    public IncomeCategoryCrud() {
        super(Income.CATEGORY_TYPE);
    }
}
