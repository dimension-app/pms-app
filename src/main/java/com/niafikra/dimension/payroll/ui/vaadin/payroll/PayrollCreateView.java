package com.niafikra.dimension.payroll.ui.vaadin.payroll;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.Payroll;
import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.niafikra.dimension.payroll.domain.element.Income;
import com.niafikra.dimension.payroll.domain.period.PayPeriod;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.niafikra.dimension.payroll.service.PayPeriodService;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.niafikra.dimension.payroll.ui.vaadin.period.PayPeriodTypeComboBox;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TwinColSelect;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Year;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.PayrollCreateView.VIEW_NAME;


@Secured(PMSPermission.CREATE_PAYROLL)
@ViewComponent(value = MainDisplay.class, caption = "New Payroll")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class PayrollCreateView extends AbstractForm<Payroll> implements View, Notifier {
    public static final String VIEW_NAME = "create-payroll";
    public static final String FIELD_WIDTH = "400px";

    @Inject
    private IncomeService incomeService;
    @Inject
    private DeductionService deductionService;
    @Inject
    private PayPeriodService periodService;

    @Inject
    private PayrollService payrollService;

    @Inject
    private Navigator navigator;

    private TwinColSelect<Income> incomes = new TwinColSelect<>("Incomes");

    private TwinColSelect<Deduction> deductions = new TwinColSelect<>("Deductions");

    @Inject
    private PayPeriodTypeComboBox periodTypeSelector;

    private MComboBox<PayPeriod> period = new MComboBox<PayPeriod>();
    private MComboBox<Integer> yearSelector = new MComboBox<Integer>("Year");

    private ConfirmButton runButton = new ConfirmButton(
            "Run",
            "Are you sure you want to run this payroll?",
            event -> doCreateRun()
    );


    public PayrollCreateView() {
        super(Payroll.class);
    }

    @PostConstruct
    private void build() {
        setSizeFull();

//        periodTypeSelector.setCaption("Type");
        periodTypeSelector.setWidth(FIELD_WIDTH);
        periodTypeSelector.addValueChangeListener(event -> loadPayPeriodSelector());
        period.setWidth(FIELD_WIDTH);
        incomes.setWidth(FIELD_WIDTH);

        List<Income> is = incomeService.findActiveIncomes();
        incomes.setItems(is);
        incomes.setValue(is.stream().collect(Collectors.toSet()));
        deductions.setWidth(FIELD_WIDTH);

        List<Deduction> ds = deductionService.findActiveDeductions();
        deductions.setItems(ds);
        deductions.setValue(ds.stream().collect(Collectors.toSet()));

        yearSelector.withWidth(FIELD_WIDTH)
                .withItems(IntStream.range(2000, Year.now().plusYears(1).getValue()).boxed().collect(Collectors.toList()));
        yearSelector.addValueChangeListener(event -> loadPayPeriodSelector());

        runButton.setEnabled(false);
        runButton.addStyleNames(Theme.BUTTON_DANGER);
        runButton.setWidth("200px");
        getSaveButton().setWidth("200px");
        addSavedHandler(payroll -> doCreate(payroll));

        yearSelector.setValue(Year.now().getValue());
    }

    private void loadPayPeriodSelector() {
        period.setItems(generatePayPeriods());
    }

    private void doCreate(Payroll payroll) {
        try {
            payroll = payrollService.create(payroll);
            showSuccess("Payroll created successfully", payroll.toString());
            navigator.navigateTo(PayrollView.class,payroll.getId());
        } catch (Exception e) {
            showError("Failed to create payroll", e);
        }
    }


    private void doCreateRun() {
        try {
            Payroll payroll = payrollService.createRun(getEntity());
            showSuccess("Payroll was ran successfully", payroll.toString());
            navigator.navigateTo(PayrollView.class,payroll.getId());
        } catch (Exception e) {
            showError("Failed to run payroll", e);
        }
    }


    private List<PayPeriod> generatePayPeriods() {
        Integer year = yearSelector.getValue();
        PayPeriodType periodType = periodTypeSelector.getValue();

        if (year != null && periodType != null)
            return periodType.generatePeriods(periodService.getStartFinancialYear(year));
        return Collections.emptyList();
    }

    @Override
    protected Component createContent() {
        return new MPanel(
                new MFormLayout(
                        yearSelector,
                        new MHorizontalLayout(periodTypeSelector, period).withCaption("Period"),
                        new MHorizontalLayout(incomes, deductions),
                        getToolbar().add(runButton)
                )
        ).withStyleName(Theme.PANEL_BORDERLESS).withFullSize();
    }

    @Override
    protected void adjustSaveButtonState() {
        super.adjustSaveButtonState();
        runButton.setEnabled(getSaveButton().isEnabled());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        setEntity(new Payroll());
    }
}
