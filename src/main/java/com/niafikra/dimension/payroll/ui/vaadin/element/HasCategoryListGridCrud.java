package com.niafikra.dimension.payroll.ui.vaadin.element;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.domain.HasCategory;
import com.vaadin.data.provider.ListDataProvider;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.CrudFormFactory;
import org.vaadin.crudui.layout.CrudLayout;

public abstract class HasCategoryListGridCrud<T extends HasCategory> extends GridCrud<T> {

    public HasCategoryListGridCrud(Class<T> domainType) {
        super(domainType);
    }

    public HasCategoryListGridCrud(Class<T> domainType, CrudLayout crudLayout, CrudFormFactory<T> crudFormFactory) {
        super(domainType, crudLayout, crudFormFactory);
    }

    public HasCategoryListGridCrud(Class<T> domainType, CrudLayout crudLayout) {
        super(domainType, crudLayout);
    }

    public abstract void filterByCategory(Category category);

    public ListDataProvider<T> getDataProvider() {
        return (ListDataProvider<T>) getGrid().getDataProvider();
    }
}
