package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.element.Income;
import com.niafikra.dimension.payroll.domain.formula.system.AbstractIncomeSystemFormula;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TwinColSelect;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class IncomeSystemFormulaEditor<T extends AbstractIncomeSystemFormula> extends TwinColSelect<Income> implements FormulaEditor<T> {

    @Inject
    private IncomeService incomeService;

    private Callback updateCallback;
    private T formula;

    @PostConstruct
    private void build() {
        setCaption("Select incomes to exclude");
        setWidth("100%");
        setItems(incomeService.findAll());

        addValueChangeListener(event -> {
            formula.setExcludedIncomes(event.getValue());

            if (updateCallback != null)
                updateCallback.call();
        });
    }

    public void setFormula(T formula) {
        this.formula = formula;
        setValue(formula.getExcludedIncomes());
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }

}
