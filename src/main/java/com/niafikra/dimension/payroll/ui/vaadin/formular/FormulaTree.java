package com.niafikra.dimension.payroll.ui.vaadin.formular;

import com.niafikra.dimension.payroll.domain.formula.Formula;
import com.vaadin.data.TreeData;
import com.vaadin.ui.Tree;


public class FormulaTree<R> extends Tree<Formula<R>> {

    private Formula formula;

    private void addFormulaEntry(Formula<R> parent, Formula<R> formula) {
        getTreeData().addItem(parent, formula);
        formula.getChildren().forEach(entry -> addFormulaEntry(formula, Formula.class.cast(entry)));
    }

    public void expandAll() {
        expandRecursively(getTreeData().getRootItems(), 10);
    }

    public Formula<R> getFormula() {
        return formula;
    }

    public void setFormula(Formula<R> formula) {
        this.formula = formula;

        TreeData<Formula<R>> data = new TreeData<>();
        setTreeData(data);

        if (formula != null)
            addFormulaEntry(null, formula);
    }
}
