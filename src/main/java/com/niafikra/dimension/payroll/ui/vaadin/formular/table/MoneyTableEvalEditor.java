package com.niafikra.dimension.payroll.ui.vaadin.formular.table;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTable;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTableEval;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.niafikra.dimension.payroll.service.TableService;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaSelector;
import com.niafikra.dimension.payroll.ui.vaadin.formular.editor.FormulaEditor;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.VerticalLayout;
import lombok.Getter;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class MoneyTableEvalEditor extends VerticalLayout implements FormulaEditor<MoneyTableEval> {

    @Inject
    private TableService tableService;
    @Inject
    private FormulaService formulaService;

    @Getter
    private MComboBox<MoneyTable> tableSelector = new MComboBox<>();

    @Getter
    @Inject
    private MoneyFormulaSelector moneySelector;

    private FormulaEditor.Callback updateCallback;

    private Binder<MoneyTableEval> binder = new BeanValidationBinder<>(MoneyTableEval.class);

    private MoneyTableEval formula;

    @PostConstruct
    private void build() {
        setWidth("100%");

        moneySelector.forAllFormulas();
        moneySelector.setWidth("100%");
        tableSelector.setItems(tableService.findMoneyTables());
        tableSelector.setWidth("100%");

        addComponents(moneySelector, tableSelector);

        binder.forField(moneySelector)
                .withValidator(new BeanValidator(MoneyTableEval.class, "input"))
                .withConverter(val -> formulaService.initMoneyFormula(val), formular -> formular == null ? "" : formular.getName())
                .bind("input");

        binder.forField(tableSelector)
                .withValidator(new BeanValidator(MoneyTableEval.class, "table"))
                .bind("table");


        moneySelector.addValueChangeListener(event -> {
            if (updateCallback != null) updateCallback.call();
        });

        tableSelector.addValueChangeListener(event -> {
            if (updateCallback != null) updateCallback.call();
        });
    }

    @Override
    public void setFormula(MoneyTableEval formula) {
        this.formula = formula;
        binder.setBean(formula);
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }

}
