package com.niafikra.dimension.payroll.ui.vaadin.element.income;

import com.niafikra.dimension.payroll.domain.element.Deduction;
import com.niafikra.dimension.payroll.service.DeductionService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class DeductionListGrid extends PayElementListGrid<Deduction> {
    
    public DeductionListGrid(DeductionService incomeService) {
        super(incomeService.findAll());
    }
}
