package com.niafikra.dimension.payroll.ui.vaadin.element.income;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.category.service.CategoryService;
import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.payroll.domain.element.Income;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.niafikra.dimension.payroll.ui.vaadin.element.PayElementCrud;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.vaadin.data.HasValue;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collection;

@PrototypeScope
@SpringComponent
public class IncomeCrud extends PayElementCrud<Income> implements Notifier {

    @Inject
    private IncomeService incomeService;
    @Inject
    private CategoryService categoryService;

    @Inject
    private MoneyFormulaField formulaField;

    public IncomeCrud() {
        super(Income.class);
    }

    @PostConstruct
    private void build() {
        Grid<Income> grid = getGrid();
        grid.setColumns("name", "category");

        formulaField.setCaption("Formula");

        getCrudFormFactory().setVisibleProperties("name","active", "reference", "category", "formula");
        getCrudFormFactory().setFieldProvider("category", () -> createCategorySelector());
        getCrudFormFactory().setFieldProvider("formula", () -> {
            formulaField.clear();
            return formulaField;
        });

        setFindAllOperation(() -> incomeService.findAll());
        setAddOperation(income -> doCreate(income));
        setUpdateOperation(income -> doUpdate(income));
        setDeleteOperation(income -> doDelete(income));
    }

    private HasValue createCategorySelector() {
        return new MComboBox<Category>("Category").withItems(categoryService.getAll(Income.CATEGORY_TYPE));
    }

    private Income doCreate(Income income) {
        try {
            income = incomeService.create(income);
            showSuccess("Successful created income", income.toString());
            return income;
        } catch (Exception e) {
            showError("Failed to create income", e);
            throw e;
        }
    }

    private Income doUpdate(Income income) {
        try {
            income = incomeService.update(income);
            showSuccess("Successfully updated income", income.toString());
            return income;
        } catch (Exception e) {
            showError("Failed to update income", e);
            throw e;
        }
    }

    private void doDelete(Income income) {
        try {
            incomeService.delete(income);
            showSuccess("Successfully deleted income", income.toString());
        } catch (Exception e) {
            showError("Failed to update income", e);
            throw e;
        }
    }

    @Override
    protected Collection<Category> getFilterCategories() {
        return categoryService.getAll(Income.CATEGORY_TYPE);
    }
}
