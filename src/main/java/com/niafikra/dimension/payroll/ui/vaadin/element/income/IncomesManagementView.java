package com.niafikra.dimension.payroll.ui.vaadin.element.income;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingDisplay;
import com.niafikra.dimension.core.ui.vaadin.settings.SettingView;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewInfo;
import com.niafikra.dimension.payroll.domain.element.Income;
import com.niafikra.dimension.payroll.ui.vaadin.element.HasCategoryManagementView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import static com.niafikra.dimension.payroll.ui.vaadin.element.income.IncomesManagementView.VIEW_NAME;

@Secured(PMSPermission.CONFIGURE_INCOMES)
@ViewComponent(value = SettingDisplay.class, caption = "Incomes")
@ViewInfo(value = "Incomes", section = "Payroll", icon = VaadinIcons.PLUS_SQUARE_LEFT_O)
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class IncomesManagementView extends HasCategoryManagementView<Income> implements SettingView {
    public static final String VIEW_NAME = "incomes";

    public IncomesManagementView(IncomeCategoryCrud categoryCrud, IncomeCrud incomeCrud) {
        super(categoryCrud, incomeCrud);
        categoryCrud.setCaption("Income Categories");
    }
}