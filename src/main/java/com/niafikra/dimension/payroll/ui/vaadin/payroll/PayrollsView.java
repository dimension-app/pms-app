package com.niafikra.dimension.payroll.ui.vaadin.payroll;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.PayrollsView.VIEW_NAME;

@Secured(PMSPermission.VIEW_PAYROLL)
@ViewComponent(value = MainDisplay.class, caption = "Payrolls")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.HR, caption = "Payrolls")
@VaadinFontIcon(VaadinIcons.MONEY_DEPOSIT)
public class PayrollsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "payrolls";

    @Inject
    private PayrollsGrid payrollsGrid;

    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        payrollsGrid.setSizeFull();
        payrollsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        payrollsGrid.addItemClickListener(event -> {
            navigator.navigateTo(PayrollView.class, event.getItem().getId());
        });

        addComponentsAndExpand(payrollsGrid);
    }

    @Secured(PMSPermission.CREATE_PAYROLL)
    @ViewMenuOption(value = "New Payroll", icon = VaadinIcons.PLUS_CIRCLE)
    public void showPayrollCreateForm() {
        navigator.navigateTo(PayrollCreateView.class);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(payrollsGrid);
    }
}
