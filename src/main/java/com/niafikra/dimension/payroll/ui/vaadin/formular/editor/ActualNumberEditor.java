package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.ActualNumber;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.spring.annotation.PrototypeScope;

import java.math.BigDecimal;

@PrototypeScope
@SpringComponent
public class ActualNumberEditor extends TextField implements FormulaEditor<ActualNumber> {
    private Callback updateCallback;
    private ActualNumber formula;

    public ActualNumberEditor() {
        setWidth("100%");

        addValueChangeListener(event -> {
            if (!StringUtils.isEmpty(event.getValue()))
                formula.setNumber(new BigDecimal(event.getValue()));
            else formula.setNumber(null);

            if (updateCallback != null)
                updateCallback.call();
        });
    }

    @Override
    public void setFormula(ActualNumber formula) {
        this.formula = formula;
        setValue(String.valueOf(formula.getNumber()));
    }

    @Override
    public void setUpdateCallback(Callback callback) {
        this.updateCallback = callback;
    }
}
