package com.niafikra.dimension.payroll.ui.vaadin.element;

import com.niafikra.dimension.category.domain.Category;
import com.niafikra.dimension.payroll.domain.element.PayElement;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.annotation.PostConstruct;
import java.util.Collection;

import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringComboBox;
import static com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils.createFilteringTextField;

public abstract class PayElementCrud<T extends PayElement> extends HasCategoryListGridCrud<T> {

    private Category currentCategory;
    private TextField nameFilter;
    private ComboBox<Category> categoryFilter;

    public PayElementCrud(Class<T> domainType) {
        super(domainType, new HorizontalSplitCrudLayout());
    }

    @PostConstruct
    private void build() {
        getCrudFormFactory().setUseBeanValidation(true);

        HeaderRow filterRow = getGrid().appendHeaderRow();
        nameFilter = createFilteringTextField(filterRow, "name", "Filter name", event -> doFilter());
        categoryFilter = createFilteringComboBox(filterRow, "category", "Filter category", getFilterCategories(), event -> doFilter());
    }

    protected abstract Collection<Category> getFilterCategories();

    @Override
    public void filterByCategory(Category category) {
        this.currentCategory = category;
        categoryFilter.setValue(category);
    }

    private void doFilter() {
        Category category = categoryFilter.getValue();
        String name = nameFilter.getValue();

        getDataProvider().setFilter(
                t -> (category == null || t.getCategory().equals(category))
                        && (StringUtils.isEmpty(name) || t.getName().toLowerCase().contains(name.toLowerCase()))
        );
    }

    @Override
    protected void showForm(CrudOperation operation, T domainObject, boolean readOnly, String successMessage, Button.ClickListener buttonClickListener) {
        if (operation.equals(CrudOperation.ADD)) domainObject.setCategory(currentCategory);
        super.showForm(operation, domainObject, readOnly, successMessage, buttonClickListener);
    }
}
