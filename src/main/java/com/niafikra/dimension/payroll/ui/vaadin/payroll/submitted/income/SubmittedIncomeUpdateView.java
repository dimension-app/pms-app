package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedIncome;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income.SubmittedIncomeUpdateView.VIEW_NAME;


@Secured(PMSPermission.SUBMIT_EMPLOYEE_INCOME)
@ViewComponent(value = MainDisplay.class, caption = "Update employee income")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class SubmittedIncomeUpdateView extends SubmitedIncomeForm implements View, Notifier {

    public static final String VIEW_NAME = "update-income";

    @Inject
    private IncomeService incomeService;

    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();

        addSavedHandler(income -> doUpdate(income));
    }

    private void doUpdate(SubmittedIncome income) {
        try {
            income = incomeService.update(income);
            showSuccess(String.format("Successful updated income for %s", income.getEmployee()));
            navigator.navigateTo(SubmittedIncomeView.class, income.getId());
        } catch (Exception e) {
            showError(String.format("Failed to update income for %s", income.getEmployee()), e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long incomeId = Long.parseLong(event.getParameters());
        setEntity(incomeService.findSubmittedIncome(incomeId));
    }

}
