package com.niafikra.dimension.payroll.ui.vaadin.period;

import com.niafikra.dimension.core.ui.vaadin.util.AbstractForm;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType.Specifier;
import com.niafikra.dimension.payroll.service.PayPeriodService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.EnumSelect;
import org.vaadin.viritin.fields.IntegerField;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;


@PrototypeScope
@SpringComponent
public class PayPeriodForm extends AbstractForm<PayPeriodType> {

    private IntegerField steps = new IntegerField("Steps");
    private EnumSelect<Specifier> specifier = new EnumSelect<>("Specifier", Specifier.class);

    @Inject
    private PayPeriodService periodService;

    @Inject
    private PayPeriodGrid periodGrid;

    public PayPeriodForm() {
        super(PayPeriodType.class);

        addSavedHandler(type -> doCreate(type));
    }

    private void doCreate(PayPeriodType type) {
        try {
            periodService.create(type);
            periodGrid.refresh();
            showSuccess("Successful created period type", type.toString());
        } catch (Exception e) {
            showError("Failed to create period type", e);
        }
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(specifier,steps, getToolbar());
    }
}
