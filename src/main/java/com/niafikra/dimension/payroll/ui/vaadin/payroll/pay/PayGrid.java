package com.niafikra.dimension.payroll.ui.vaadin.payroll.pay;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateRangeSelector;
import com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeComboBox;
import com.niafikra.dimension.payroll.domain.contract.pay.Pay;
import com.niafikra.dimension.payroll.service.PayrollService.PayFilter;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@PrototypeScope
@SpringComponent
public class PayGrid extends Grid<Pay> {

    @Inject
    private EmployeeComboBox employeeFilter;

    private PayFilter filter;

    public PayGrid(PayProvider provider) {
        super(provider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {
        addColumn(Pay::getId)
                .setId("id")
                .setCaption("Reference")
                .setSortable(true)
                .setHidable(true);


        addColumn(pay -> pay.getPayroll().getPeriod().getEnd())
                .setId("payroll.period.end")
                .setCaption("Date")
                .setSortable(true)
                .setHidable(true);

        addColumn(pay -> pay.getEmployee())
                .setId("contract.employee")
                .setCaption("Employee")
                .setSortable(true)
                .setHidable(true);

        addColumn(pay -> pay.getBasicAmount())
                .setId("basicAmount")
                .setCaption("Basic Income")
                .setSortable(true)
                .setHidable(true);

        addColumn(pay -> pay.calculateTotalOtherIncome())
                .setId("otherIncome")
                .setCaption("Other Income")
                .setSortable(false)
                .setHidable(true);


        addColumn(pay -> pay.calculateGrossIncome())
                .setId("grossIncome")
                .setCaption("Gross Income")
                .setSortable(false)
                .setHidable(true);


        addColumn(pay -> pay.calculateTotalEmployeeContribution())
                .setId("totalEmployeeDeductions")
                .setCaption("Total Deductions")
                .setSortable(false)
                .setHidable(true);


        addColumn(pay -> pay.calculateNetIncome())
                .setId("netIncome")
                .setCaption("Net Income")
                .setSortable(false)
                .setHidable(true);

        filter = PayFilter.builder().build();
        getDataProvider().setFilter(filter);

        HeaderRow filtersRow = appendHeaderRow();
        VaadinUtils.createFilteringIntegerField(
                filtersRow,
                "id",
                "Filter reference",
                event -> {
                    Integer value = event.getValue();
                    filter.setId(value == null ? null : value.longValue());
                    getDataProvider().refreshAll();
                }
        );

        employeeFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        employeeFilter.addValueChangeListener(event -> {
            filter.setEmployee(event.getValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("contract.employee").setComponent(employeeFilter);

        LocalDateRangeSelector endRangeFilter = new LocalDateRangeSelector();
        endRangeFilter.setWidth("200px");
        filtersRow.getCell("payroll.period.end").setComponent(endRangeFilter);
        endRangeFilter.addValueChangeListener(event -> {
            filter.setPeriodEndDateRange(endRangeFilter.getStart(), endRangeFilter.getEnd());
        });
    }

    @Override
    public ConfigurableFilterDataProvider<Pay, Void, PayFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Pay, Void, PayFilter>) super.getDataProvider();
    }

    public PayFilter getFilter() {
        return filter;
    }

    public EmployeeComboBox getEmployeeFilter() {
        return employeeFilter;
    }
}
