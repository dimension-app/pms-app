package com.niafikra.dimension.payroll.ui.vaadin.contract;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.CREATE_CONTRACT;
import static com.niafikra.dimension.PMSPermission.VIEW_CONTRACTS;
import static com.niafikra.dimension.payroll.ui.vaadin.contract.ContractsView.VIEW_NAME;

@Secured(VIEW_CONTRACTS)
@ViewComponent(value = MainDisplay.class, caption = "Contracts")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.HR, caption = "Contracts")
@VaadinFontIcon(VaadinIcons.HOURGLASS)
public class ContractsView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "contracts";

    @Inject
    private ContractsGrid contractsGrid;

    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        contractsGrid.setSizeFull();
        contractsGrid.setSelectionMode(Grid.SelectionMode.NONE);
        contractsGrid.addItemClickListener(event -> {
            navigator.navigateTo(ContractView.class, event.getItem().getId());
        });

        addComponentsAndExpand(contractsGrid);
    }

    @Secured(CREATE_CONTRACT)
    @ViewMenuOption(value = "New Contract", icon = VaadinIcons.PLUS_CIRCLE)
    public void showSalaryContractCreateForm() {
        navigator.navigateTo(ContractCreateView.class);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(contractsGrid);
    }
}
