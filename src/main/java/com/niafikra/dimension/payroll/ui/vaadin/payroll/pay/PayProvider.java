package com.niafikra.dimension.payroll.ui.vaadin.payroll.pay;

import com.niafikra.dimension.payroll.domain.contract.pay.Pay;
import com.niafikra.dimension.payroll.service.PayrollService;
import com.niafikra.dimension.payroll.service.PayrollService.PayFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.List;


@PrototypeScope
@SpringComponent
public class PayProvider extends PageableDataProvider<Pay, PayFilter> {

    @Inject
    private PayrollService payrollService;

    @Override
    protected Page<Pay> fetchFromBackEnd(Query<Pay, PayFilter> query, Pageable pageable) {
        return payrollService.findPays(query.getFilter(),pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(
                new QuerySortOrder("contract.employee.firstName", SortDirection.ASCENDING),
                new QuerySortOrder("contract.employee.lastName", SortDirection.ASCENDING)
        );
    }

    @Override
    protected int sizeInBackEnd(Query<Pay, PayFilter> query) {
        return payrollService.countPays(query.getFilter()).intValue();
    }

}
