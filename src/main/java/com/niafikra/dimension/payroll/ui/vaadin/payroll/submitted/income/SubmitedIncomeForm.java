package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedIncome;
import com.niafikra.dimension.payroll.ui.vaadin.element.income.IncomeComboBox;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.AbstractPaySubmitForm;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;

import javax.inject.Inject;


@PrototypeScope
@SpringComponent
public class SubmitedIncomeForm extends AbstractPaySubmitForm<SubmittedIncome> {

    @Inject
    protected IncomeComboBox income;

    @Inject
    private MoneyFormulaField formulaField;

    public SubmitedIncomeForm() {
        super(SubmittedIncome.class);
    }

    @Override
    protected void bind() {
        super.bind();

        getBinder().forField(formulaField)
                .withValidator(new BeanValidator(SubmittedIncome.class, "formula"))
                .bind("formula");
    }

    @Override
    protected Component createContent() {
        income.setWidth(FIELD_WIDTH);
        income.setCaption("Income");
        formulaField.setWidth(FIELD_WIDTH);
        formulaField.setCaption("Formula");
        description.setWidth("600px");

        return new MPanel(
                new MFormLayout(
                        employee,
                        income,
                        formulaField,
                        new MHorizontalLayout(date, dueDate),
                        description,
                        attachments,
                        getToolbar()
                )

        ).withStyleName(Theme.PANEL_BORDERLESS).withFullSize();
    }
}
