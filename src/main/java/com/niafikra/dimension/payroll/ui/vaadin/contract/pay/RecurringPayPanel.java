package com.niafikra.dimension.payroll.ui.vaadin.contract.pay;

import com.niafikra.dimension.core.ui.vaadin.util.MGridCrudView;
import com.niafikra.dimension.core.ui.vaadin.util.Notifier;
import com.niafikra.dimension.payroll.domain.contract.Contract;
import com.niafikra.dimension.payroll.domain.contract.pay.RecurringPayConfig;
import com.niafikra.dimension.payroll.service.ContractService;
import com.vaadin.ui.AbstractSplitPanel;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalSplitPanel;
import lombok.Getter;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

import javax.inject.Inject;
import java.util.LinkedHashSet;


public class RecurringPayPanel<T extends RecurringPayConfig> extends MGridCrudView<T> implements Notifier {

    @Inject
    protected ContractService contractService;

    @Getter
    protected Contract contract;

    public RecurringPayPanel(Class<T> type) {
        super(type, new RecurringPayCrudLayout());
    }


    @Override
    protected void showForm(CrudOperation operation, T config, boolean readOnly, String successMessage, Button.ClickListener listener) {
        if (operation == CrudOperation.ADD) {
            config.setStartDate(contract.getStartDate());
            config.setEndDate(contract.getEndDate());
            config.setExcludePeriodIndices(new LinkedHashSet<>());
        }
        super.showForm(operation, config, readOnly, successMessage, listener);
    }

    public void setContract(Contract contract) {
        this.contract = contract;
        refreshGrid();
    }


    public void reload() {
        setContract(contractService.findContract(contract.getId()));
    }

    private static class RecurringPayCrudLayout extends HorizontalSplitCrudLayout {
        @Override
        protected AbstractSplitPanel getMainLayout() {
            HorizontalSplitPanel mainLayout = (HorizontalSplitPanel) super.getMainLayout();
            mainLayout.setSplitPosition(40);
            return mainLayout;
        }
    }

}
