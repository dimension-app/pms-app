package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.core.ui.vaadin.MainHeader;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedIncome;
import com.niafikra.dimension.payroll.service.IncomeService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.UPDATE_SUBMITTED_INCOME;
import static com.niafikra.dimension.PMSPermission.VIEW_SUBMITTED_INCOMES;
import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income.SubmittedIncomeView.VIEW_NAME;

@Secured(VIEW_SUBMITTED_INCOMES)
@ViewComponent(value = MainDisplay.class, caption = "View submitted income details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class SubmittedIncomeView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "submitted-income";

    @Inject
    private IncomeService incomeService;
    @Inject
    private TemplatePanel templatePanel;
    @Inject
    private Navigator navigator;
    @Inject
    private MainHeader mainHeader;

    private SubmittedIncome income;

    @PostConstruct
    private void build() {
        setSizeFull();
        templatePanel.setSizeFull();
        templatePanel.getTemplateView().setTemplatePath(Templates.SUBMITTED_INCOME);
        addComponentsAndExpand(templatePanel);
    }

    public void setSubmittedIncome(SubmittedIncome income) {
        this.income = income;
        templatePanel.getTemplateView().putBinding("submittedIncome", income).render();
        mainHeader.getMenuBar().setMenuVisible("Edit", !income.isProcessed());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long incomeId = Long.parseLong(event.getParameters());
        setSubmittedIncome(incomeService.findSubmittedIncome(incomeId));
    }

    @Secured(UPDATE_SUBMITTED_INCOME)
    @ViewMenuOption(value = "Edit", menu = "Income", order = 1, icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.PLUS_SQUARE_LEFT_O)
    public void showIncomeUpdateForm() {
        navigator.navigateTo(SubmittedIncomeUpdateView.class, income.getId());
    }

}
