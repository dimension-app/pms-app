package com.niafikra.dimension.payroll.ui.vaadin.element;

import com.niafikra.dimension.category.domain.HasCategory;
import com.niafikra.dimension.category.ui.vaadin.CategoryCrud;
import com.vaadin.ui.HorizontalSplitPanel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

public class HasCategoryManagementView<T extends HasCategory> extends MVerticalLayout {

    private CategoryCrud categoryCrud;

    private HasCategoryListGridCrud<T> hasCategoryCrud;

    public HasCategoryManagementView(CategoryCrud categoryCrud, HasCategoryListGridCrud<T> hasCategoryCrud) {
        this.categoryCrud = categoryCrud;
        this.hasCategoryCrud = hasCategoryCrud;
    }

    @PostConstruct
    private void build() {
        setSizeFull();
        categoryCrud.setSizeFull();
        hasCategoryCrud.setSizeFull();
        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel(categoryCrud, hasCategoryCrud);
        splitPanel.setSplitPosition(20);
        addComponentsAndExpand(splitPanel);

        categoryCrud.getTree().asSingleSelect().addValueChangeListener(event -> hasCategoryCrud.filterByCategory(event.getValue()));
    }
}