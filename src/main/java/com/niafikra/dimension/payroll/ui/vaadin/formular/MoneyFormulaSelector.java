package com.niafikra.dimension.payroll.ui.vaadin.formular;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.money.Money;
import com.niafikra.dimension.payroll.domain.formula.AbstractFormula;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.stream.Stream;

@PrototypeScope
@SpringComponent
public class MoneyFormulaSelector extends MComboBox<String> {

    @Inject
    private FormulaService formulaService;

    public AbstractFormula<Money> initSelectedFormula() {
        return formulaService.initMoneyFormula(getValue());
    }

    public MoneyFormulaSelector forAllFormulas() {
        setItems(formulaService.listAllMoneyFormulaNames());
        return this;
    }

    public MoneyFormulaSelector forSystemFormulas() {
        setItems(formulaService.listSystemMoneyFormulaNames());
        return this;
    }

    public MoneyFormulaSelector forDefinedFormulas() {
        setItems(formulaService.listDefinedMoneyFormulaNames());
        return this;
    }

    public MoneyFormulaSelector forSavedFormulas() {
        setItems(formulaService.listMoneySavedFormulaNames());
        return this;
    }

    public MoneyFormulaSelector forSystemAndSavedFormulas() {
        setItems(Stream.concat(
                formulaService.listSystemMoneyFormulaNames().stream(),
                formulaService.listMoneySavedFormulaNames().stream())
        );
        return this;
    }
}
