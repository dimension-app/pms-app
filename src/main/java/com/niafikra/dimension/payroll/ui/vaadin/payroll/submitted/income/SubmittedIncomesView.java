package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income;

import com.niafikra.dimension.PMSPermission;
import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted.income.SubmittedIncomesView.VIEW_NAME;

@Secured(PMSPermission.VIEW_SUBMITTED_INCOMES)
@ViewComponent(value = MainDisplay.class, caption = "Incomes")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.HR, caption = "Incomes")
@VaadinFontIcon(VaadinIcons.PLUS_SQUARE_LEFT_O)
public class SubmittedIncomesView extends VerticalLayout implements View, TableExportable {

    public static final String VIEW_NAME = "submitted-incomes";

    @Inject
    private SubmittedIncomeGrid incomesGrid;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);

        incomesGrid.setSizeFull();
        incomesGrid.setSelectionMode(Grid.SelectionMode.NONE);
        incomesGrid.addItemClickListener(event -> {
            navigator.navigateTo(SubmittedIncomeView.class, event.getItem().getId());
        });

        addComponentsAndExpand(incomesGrid);
    }

    @Secured(PMSPermission.SUBMIT_EMPLOYEE_INCOME)
    @ViewMenuOption(value = "Record Income", icon = VaadinIcons.PLUS_CIRCLE)
    public void showIncomeSubmitForm() {
        navigator.navigateTo(IncomeSubmitView.class);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(incomesGrid);
    }
}
