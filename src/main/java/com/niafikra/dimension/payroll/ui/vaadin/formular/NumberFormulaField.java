package com.niafikra.dimension.payroll.ui.vaadin.formular;

import com.niafikra.dimension.payroll.domain.formula.ActualNumber;
import com.niafikra.dimension.payroll.domain.formula.Formula;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Grid;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.math.BigDecimal;

@PrototypeScope
@SpringComponent
public class NumberFormulaField extends CustomField<Formula<BigDecimal>> {

    private FormulaTree<BigDecimal> formulaTree = new FormulaTree<>();
    private MTextField actualNumberField = new MTextField();

    public NumberFormulaField() {
    }

    public NumberFormulaField(String caption) {
        this();
        setCaption(caption);
    }

    @Override
    protected Component initContent() {
        actualNumberField.withFullWidth();
        actualNumberField.addValueChangeListener(event -> {
            String value = event.getValue();
            if (StringUtils.isEmpty(value)) return;

            setValue(new ActualNumber(new BigDecimal(value)));
        });

        formulaTree.setSelectionMode(Grid.SelectionMode.NONE);

        return new MVerticalLayout(formulaTree, actualNumberField).withFullWidth().withMargin(false);
    }

    @Override
    protected void doSetValue(Formula<BigDecimal> value) {
        formulaTree.setFormula(value);
    }

    @Override
    public Formula<BigDecimal> getValue() {
        return formulaTree.getFormula();
    }
}
