package com.niafikra.dimension.payroll.ui.vaadin.formular.editor;

import com.niafikra.dimension.payroll.domain.formula.MoneyPercentageFormula;
import com.niafikra.dimension.payroll.service.FormulaService;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaSelector;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToBigDecimalConverter;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import lombok.Getter;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;

@PrototypeScope
@SpringComponent
public class MoneyPercentageEditor extends VerticalLayout implements FormulaEditor<MoneyPercentageFormula> {

    @Getter
    @Inject
    private MoneyFormulaSelector inputSelector;

    @Getter
    private TextField percentField = new TextField();

    private Callback updateCallback;

    @Inject
    private FormulaService formulaService;

    private Binder<MoneyPercentageFormula> binder = new BeanValidationBinder<>(MoneyPercentageFormula.class);

    private MoneyPercentageFormula formula;

    @PostConstruct
    private void build() {
        setWidth("100%");

        inputSelector.forAllFormulas();
        inputSelector.setWidth("100%");
        percentField.setWidth("100%");
        addComponents(inputSelector, percentField);

        binder.forField(inputSelector)
                .withValidator(new BeanValidator(MoneyPercentageFormula.class, "input"))
                .withConverter(val -> formulaService.initMoneyFormula(val), formular -> formular == null ? "" : formular.getName())
                .bind("input");

        binder.forField(percentField)
                .withConverter(new StringToBigDecimalConverter(BigDecimal.ZERO, "Failed to convert value to decimal"))
                .withValidator(new BeanValidator(MoneyPercentageFormula.class, "percent"))
                .bind("percent");

        percentField.addValueChangeListener(event -> {
            if (updateCallback != null) updateCallback.call();
        });

        inputSelector.addValueChangeListener(event -> {
            if (updateCallback != null) updateCallback.call();
        });
    }


    @Override
    public void setFormula(MoneyPercentageFormula formula) {
        this.formula = formula;
        binder.setBean(formula);
    }

    @Override
    public void setUpdateCallback(Callback updateCallback) {
        this.updateCallback = updateCallback;
    }
}
