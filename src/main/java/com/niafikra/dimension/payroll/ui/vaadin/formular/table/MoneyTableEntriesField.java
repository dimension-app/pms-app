package com.niafikra.dimension.payroll.ui.vaadin.formular.table;

import com.niafikra.dimension.core.ui.vaadin.util.GridCrudField;
import com.niafikra.dimension.core.ui.vaadin.util.MoneyField;
import com.niafikra.dimension.payroll.domain.formula.table.MoneyTableEntry;
import com.niafikra.dimension.payroll.ui.vaadin.formular.MoneyFormulaField;
import com.niafikra.dimension.payroll.ui.vaadin.formular.NumberFormulaField;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@PrototypeScope
@SpringComponent
public class MoneyTableEntriesField extends GridCrudField<MoneyTableEntry, Set<MoneyTableEntry>> {

    @Inject
    private MoneyFormulaField valueField;
    @Inject
    private NumberFormulaField excessFactorField;

    public MoneyTableEntriesField() {
        super(MoneyTableEntry.class);
    }

    @PostConstruct
    private void build() {
        valueField.setCaption("Value");
        excessFactorField.setCaption("Excess Factor");

        getGridCrud().getGrid().setColumns("from", "to", "value", "excessFactor");

        getGridCrud().getCrudFormFactory().setFieldProvider("from", () -> new MoneyField("From"));
        getGridCrud().getCrudFormFactory().setFieldProvider("to", () -> new MoneyField("To"));
        getGridCrud().getCrudFormFactory().setFieldProvider("value", () -> valueField);
        getGridCrud().getCrudFormFactory().setFieldProvider("excessFactor", () -> excessFactorField);
        getGridCrud().getCrudFormFactory().setVisibleProperties("from", "to", "value", "excessFactor");
    }

    @Override
    public Collection<MoneyTableEntry> findAll() {
        return super.findAll().stream().sorted().collect(Collectors.toList());
    }
}
