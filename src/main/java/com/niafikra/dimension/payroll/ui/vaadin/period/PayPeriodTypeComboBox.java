package com.niafikra.dimension.payroll.ui.vaadin.period;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.payroll.domain.period.PayPeriodType;
import com.niafikra.dimension.payroll.service.PayPeriodService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class PayPeriodTypeComboBox extends MComboBox<PayPeriodType> {

    public PayPeriodTypeComboBox(PayPeriodService periodService) {
        setItems(periodService.findAllPayPeriodTypes());
    }
}
