package com.niafikra.dimension.payroll.ui.vaadin.payroll.submitted;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateRangeSelector;
import com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeComboBox;
import com.niafikra.dimension.payroll.domain.contract.pay.submitted.SubmittedPay;
import com.niafikra.dimension.payroll.service.SubmittedPayFilter;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


public abstract class AbstractSubmittedPayGrid<T extends SubmittedPay, F extends SubmittedPayFilter> extends Grid<T> {

    protected F filter;
    @Inject
    protected EmployeeComboBox employeeFilter;

    public AbstractSubmittedPayGrid(DataProvider<T, F> provider, F filter) {
        super(provider.withConfigurableFilter());
        this.filter = filter;
    }

    @PostConstruct
    private void build() {

        addColumn(T::getDate)
                .setId("date")
                .setCaption("Date")
                .setSortable(true);

        addColumn(T::getStatus)
                .setId("status")
                .setCaption("Status")
                .setSortable(false)
                .setHidable(false);

        addColumn(T::getEmployee)
                .setId("employee")
                .setCaption("Employee")
                .setSortable(true);

        addColumn(T::getDueDate)
                .setId("dueDate")
                .setCaption("Due Date")
                .setSortable(true);

        HeaderRow filtersRow = appendHeaderRow();
        getDataProvider().setFilter(filter);

        LocalDateRangeSelector dateRangeFilter = new LocalDateRangeSelector();
        dateRangeFilter.setWidth("200px");
        filtersRow.getCell("date").setComponent(dateRangeFilter);
        dateRangeFilter.addValueChangeListener(event -> {
            filter.setDatetRange(dateRangeFilter.getStart(), dateRangeFilter.getEnd());
        });

        LocalDateRangeSelector dueDateRangeFilter = new LocalDateRangeSelector();
        dueDateRangeFilter.setWidth("200px");
        filtersRow.getCell("dueDate").setComponent(dueDateRangeFilter);
        dueDateRangeFilter.addValueChangeListener(event -> {
            filter.setDueDateRange(dueDateRangeFilter.getStart(), dueDateRangeFilter.getEnd());
        });

        employeeFilter.withFullWidth().addStyleNames(Theme.COMBOBOX_TINY);
        employeeFilter.addValueChangeListener(event -> {
            filter.setEmployee(event.getValue());
            getDataProvider().refreshAll();
        });
        filtersRow.getCell("employee").setComponent(employeeFilter);
    }

    public HeaderRow getFiltersRow() {
        return getHeaderRow(1);
    }

    @Override
    public ConfigurableFilterDataProvider<T, Void, F> getDataProvider() {
        return (ConfigurableFilterDataProvider<T, Void, F>) super.getDataProvider();
    }
}
