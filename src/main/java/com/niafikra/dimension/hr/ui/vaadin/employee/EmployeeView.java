package com.niafikra.dimension.hr.ui.vaadin.employee;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Templates;
import com.niafikra.dimension.core.ui.vaadin.util.TemplatePanel;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.hr.service.EmployeeService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.EDIT_EMPLOYEE;
import static com.niafikra.dimension.PMSPermission.VIEW_EMPLOYEES;
import static com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeView.VIEW_NAME;

@Secured(VIEW_EMPLOYEES)
@ViewComponent(value = MainDisplay.class, caption = "View Employee Details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class EmployeeView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "employee";

    @Inject
    private EmployeeService employeeService;
    @Inject
    private TemplatePanel templatePanel;
    @Inject
    private Navigator navigator;

    private Employee employee;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        templatePanel.setSizeFull();
        templatePanel.getTemplateView().setTemplatePath(Templates.EMPLOYEE);
        addComponentsAndExpand(templatePanel);
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        templatePanel.getTemplateView().putBinding("employee", employee).render();
    }

    @Secured(EDIT_EMPLOYEE)
    @ViewMenuOption(value = "Edit", menu = "Employee", icon = VaadinIcons.EDIT, menuIcon = VaadinIcons.USER)
    public void showEmployeeUpdateForm() {
        navigator.navigateTo(EmployeeUpdateForm.class, employee.getId());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long employeeId = Long.parseLong(event.getParameters());
        setEmployee(employeeService.findEmployee(employeeId));
    }
}
