package com.niafikra.dimension.hr.ui.vaadin.employee;


import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.Sections;
import com.niafikra.dimension.core.ui.vaadin.actions.TableExportable;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.core.ui.vaadin.view.ViewMenuOption;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.TableExport;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.springframework.security.access.annotation.Secured;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.spring.sidebar.annotation.VaadinFontIcon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.EDIT_EMPLOYEE;
import static com.niafikra.dimension.PMSPermission.VIEW_EMPLOYEES;
import static com.niafikra.dimension.hr.ui.vaadin.employee.EmployeesView.VIEW_NAME;


@Secured(VIEW_EMPLOYEES)
@ViewComponent(value = MainDisplay.class, caption = "Employees")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
@SideBarItem(sectionId = Sections.HR, caption = "Employees")
@VaadinFontIcon(VaadinIcons.USERS)
public class EmployeesView extends VerticalLayout implements View, TableExportable {
    public static final String VIEW_NAME = "employees";

    @Inject
    private EmployeesGrid employeesGrid;
    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();
        setMargin(false);
        employeesGrid.setSizeFull();
        employeesGrid.setSelectionMode(Grid.SelectionMode.NONE);
        employeesGrid.addItemClickListener(event -> {
            navigator.navigateTo(EmployeeView.class, event.getItem().getId());
        });

        addComponentsAndExpand(employeesGrid);
    }

    @Secured(EDIT_EMPLOYEE)
    @ViewMenuOption(value = "New Employee", icon = VaadinIcons.PLUS_CIRCLE)
    public void showEmployeeCreateForm() {
        navigator.navigateTo(EmployeeRegistrationForm.class);
    }

    @Override
    public TableExport getTableExport() {
        return new ExcelExport(employeesGrid);
    }
}
