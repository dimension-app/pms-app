package com.niafikra.dimension.hr.ui.vaadin.employee;

import com.niafikra.dimension.core.ui.vaadin.Theme;
import com.niafikra.dimension.core.ui.vaadin.util.VaadinUtils;
import com.niafikra.dimension.core.ui.vaadin.util.date.LocalDateTimeRangeSelector;
import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.hr.service.EmployeeService.EmployeeFilter;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.HeaderRow;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.viritin.fields.EnumSelect;

import javax.annotation.PostConstruct;

@PrototypeScope
@SpringComponent
public class EmployeesGrid extends Grid<Employee> {

    public EmployeesGrid(EmployeeProvider provider) {
        super(provider.withConfigurableFilter());
    }

    @PostConstruct
    private void build() {
        addColumn(Employee::getCode)
                .setCaption("Code")
                .setId("code")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getFirstName)
                .setCaption("First Name")
                .setId("firstName")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getMiddleName)
                .setCaption("Middle Name")
                .setId("middleName")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getLastName)
                .setCaption("Last Name")
                .setId("lastName")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getBirthDate)
                .setCaption("Date of Birth")
                .setId("birthDate")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getAge)
                .setCaption("Age")
                .setId("age")
                .setSortable(true)
                .setSortProperty("birthDate")
                .setHidable(true);

        addColumn(Employee::getGender)
                .setCaption("Gender")
                .setId("gender")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getMaritalStatus)
                .setCaption("Marital Status")
                .setId("maritalStatus")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getEmail)
                .setCaption("Email")
                .setId("contact.email")
                .setSortable(true)
                .setHidable(true);

        addColumn(Employee::getPhone)
                .setCaption("Phone")
                .setId("contact.phone")
                .setSortable(true)
                .setHidable(true);

        EmployeeFilter filter = EmployeeFilter.builder().build();
        getDataProvider().setFilter(filter);

        HeaderRow filterRow = appendHeaderRow();

        VaadinUtils.createFilteringTextField(filterRow,
                "firstName",
                "Filter first name",
                event -> {
                    filter.setFirstName(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        VaadinUtils.createFilteringTextField(filterRow,
                "middleName",
                "Filter middle name",
                event -> {
                    filter.setMiddleName(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        VaadinUtils.createFilteringTextField(filterRow,
                "lastName",
                "Filter last name",
                event -> {
                    filter.setLastName(event.getValue());
                    getDataProvider().refreshAll();
                }
        );


        VaadinUtils.createFilteringTextField(filterRow,
                "contact.email",
                "Filter email",
                event -> {
                    filter.setEmail(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        VaadinUtils.createFilteringTextField(filterRow,
                "contact.phone",
                "Filter phone",
                event -> {
                    filter.setPhone(event.getValue());
                    getDataProvider().refreshAll();
                }
        );

        filterRow.getCell("gender")
                .setComponent(new EnumSelect<Employee.Gender>(Employee.Gender.class)
                        .withStyleName(Theme.COMBOBOX_TINY, Theme.TEXT_SMALL)
                        .withValueChangeListener(event -> {
                            filter.setGender(event.getValue());
                            getDataProvider().refreshAll();
                        }));

        filterRow.getCell("maritalStatus")
                .setComponent(new EnumSelect<Employee.MaritalStatus>(Employee.MaritalStatus.class)
                        .withStyleName(Theme.COMBOBOX_TINY, Theme.TEXT_SMALL)
                        .withValueChangeListener(event -> {
                            filter.setMaritalStatus(event.getValue());
                            getDataProvider().refreshAll();
                        }));

        LocalDateTimeRangeSelector timeCreatedRangFilter = new LocalDateTimeRangeSelector();
        timeCreatedRangFilter.setWidth("200px");
        filterRow.getCell("birthDate").setComponent(timeCreatedRangFilter);
        timeCreatedRangFilter.addValueChangeListener(event -> {
            filter.setBirthDatePeriodRange(timeCreatedRangFilter.getStart().toLocalDate(), timeCreatedRangFilter.getEnd().toLocalDate());
        });

    }

    @Override
    public ConfigurableFilterDataProvider<Employee, Void, EmployeeFilter> getDataProvider() {
        return (ConfigurableFilterDataProvider<Employee, Void, EmployeeFilter>) super.getDataProvider();
    }
}
