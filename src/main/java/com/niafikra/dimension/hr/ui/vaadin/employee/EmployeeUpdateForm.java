package com.niafikra.dimension.hr.ui.vaadin.employee;

import com.niafikra.dimension.core.ui.vaadin.MainUI;
import com.niafikra.dimension.core.ui.vaadin.view.MainDisplay;
import com.niafikra.dimension.core.ui.vaadin.view.Navigator;
import com.niafikra.dimension.core.ui.vaadin.view.ViewComponent;
import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.hr.service.EmployeeService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.niafikra.dimension.PMSPermission.EDIT_EMPLOYEE;
import static com.niafikra.dimension.hr.ui.vaadin.employee.EmployeeUpdateForm.VIEW_NAME;

@Secured(EDIT_EMPLOYEE)
@ViewComponent(value = MainDisplay.class, caption = "Update Employee Details")
@SpringView(name = VIEW_NAME, ui = MainUI.class)
public class EmployeeUpdateForm extends EmployeeEditForm implements View {

    public static final String VIEW_NAME = "update-employee";

    @Inject
    private EmployeeService employeeService;

    @Inject
    private Navigator navigator;

    @PostConstruct
    private void build() {
        setSizeFull();
        addSavedHandler(employee -> doUpdate(employee));
    }

    private void doUpdate(Employee employee) {
        try {
            employeeService.update(employee);
            showSuccess("Successful updated employee", employee.toString());
            navigator.navigateBack();
        } catch (Exception e) {
            showError("Failed to update employee", e);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        Long employeeId = Long.parseLong(event.getParameters());
        setEntity(employeeService.findEmployee(employeeId));
    }
}
