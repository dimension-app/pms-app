package com.niafikra.dimension.hr.ui.vaadin.employee;

import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.hr.service.EmployeeService;
import com.niafikra.dimension.hr.service.EmployeeService.EmployeeFilter;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.vaadin.artur.spring.dataprovider.PageableDataProvider;
import org.vaadin.spring.annotation.PrototypeScope;

import javax.inject.Inject;
import java.util.List;

@PrototypeScope
@SpringComponent
public class EmployeeProvider extends PageableDataProvider<Employee, EmployeeFilter> {

    @Inject
    private EmployeeService employeeService;

    @Override
    protected Page<Employee> fetchFromBackEnd(Query<Employee, EmployeeFilter> query, Pageable pageable) {
        return employeeService.findAll(query.getFilter(), pageable);
    }

    @Override
    protected List<QuerySortOrder> getDefaultSortOrders() {
        return List.of(
                new QuerySortOrder("firstName", SortDirection.ASCENDING),
                new QuerySortOrder("middleName", SortDirection.ASCENDING),
                new QuerySortOrder("lastName", SortDirection.ASCENDING),
                new QuerySortOrder("code", SortDirection.ASCENDING)
        );
    }

    @Override
    protected int sizeInBackEnd(Query<Employee, EmployeeFilter> query) {
        return employeeService.count(query.getFilter()).intValue();
    }
}
