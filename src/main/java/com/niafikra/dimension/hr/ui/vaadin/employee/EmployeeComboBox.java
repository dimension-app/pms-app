package com.niafikra.dimension.hr.ui.vaadin.employee;

import com.niafikra.dimension.core.ui.vaadin.util.MComboBox;
import com.niafikra.dimension.hr.domain.Employee;
import com.niafikra.dimension.hr.service.EmployeeService;
import com.vaadin.spring.annotation.SpringComponent;
import org.vaadin.spring.annotation.PrototypeScope;

@PrototypeScope
@SpringComponent
public class EmployeeComboBox extends MComboBox<Employee> {

    public EmployeeComboBox(EmployeeProvider provider) {
        setDataProvider(provider.withConvertedFilter(nameFilter -> EmployeeService.EmployeeFilter.builder().firstName(nameFilter).build()));
    }
}
