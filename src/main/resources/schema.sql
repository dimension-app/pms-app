create table if not exists cr_authority
(
  id        bigint auto_increment
    primary key,
  name      varchar(255) not null,
  version   int          null,
  parent_id bigint       null,
  constraint UK_boqxmoqkbin64u6j7hh4pgqkg
  unique (name),
  constraint FKstcu1dp0wm8m6tecdentndq4
  foreign key (parent_id) references cr_authority (id)
);

create index FKstcu1dp0wm8m6tecdentndq4
  on cr_authority (parent_id);

create table if not exists authority_permissions
(
  authority_id bigint       not null,
  permissions  varchar(255) null,
  constraint FK4jv2tj2xdqprpjtkpjreip5s2
  foreign key (authority_id) references cr_authority (id)
);

create index FK4jv2tj2xdqprpjtkpjreip5s2
  on authority_permissions (authority_id);

create table if not exists ftp_attachment
(
  id      bigint auto_increment
    primary key,
  mime    varchar(255) null,
  name    varchar(255) not null,
  path    varchar(255) not null,
  version bigint       null
);

create table if not exists cr_user
(
  id                bigint auto_increment
    primary key,
  email             varchar(255) not null,
  external_identity varchar(255) not null,
  last_updated      datetime     null,
  name              varchar(255) not null,
  phone_number      varchar(255) null,
  time_created      datetime     null,
  username          varchar(255) not null,
  version           int          null,
  picture_id        bigint       null,
  constraint UK_pfp3qsi8yb842a270h8ukudj
  unique (email),
  constraint UK_462nwncjr7vfg4hu530d5ebut
  unique (external_identity),
  constraint UK_ft2vxgwrryfajqjxi3vn3drr6
  unique (username),
  constraint FKcqcuggw6cjq19ninjk5orylyr
  foreign key (picture_id) references ftp_attachment (id)
);

create index FKcqcuggw6cjq19ninjk5orylyr
  on cr_user (picture_id);


create table if not exists cr_user_cr_authority
(
  user_id        bigint not null,
  authorities_id bigint not null,
  primary key (user_id, authorities_id),
  constraint FKpjiw35ubd1wqntcdukh0e17yq
  foreign key (user_id) references cr_user (id),
  constraint FKsjyd4iaql0w72ydhsvadqkt7e
  foreign key (authorities_id) references cr_authority (id)
);

create index FKsjyd4iaql0w72ydhsvadqkt7e
  on cr_user_cr_authority (authorities_id);

INSERT INTO cr_user (id,
                     email,
                     external_identity,
                     last_updated,
                     name,
                     phone_number,
                     time_created,
                     username,
                     version,
                     picture_id)
VALUES (1,
        'admin@springframework.org',
        'admin',
        '2018-06-01 04:04:42',
        'System Administrator',
        null,
        '2018-06-01 04:04:42',
        'admin',
        0,
        null);
INSERT INTO `cr_authority` (`name`, `version`, `parent_id`)
VALUES ('Admin', 1, NULL);
INSERT INTO `authority_permissions` (`authority_id`, `permissions`)
VALUES (1, 'CONFIGURE_USERS');
INSERT INTO `authority_permissions` (`authority_id`, `permissions`)
VALUES (1, 'CONFIGURE_AUTHORITIES');
INSERT INTO `cr_user_cr_authority` (`user_id`, `authorities_id`)
VALUES (1, 1);

create table if not exists sc_script
(
  id      bigint auto_increment
    primary key,
  code    longtext     null,
  name    varchar(255) not null,
  version int          null,
  constraint UK_uj2ol9kvhiom7hf61hf5ljse
  unique (name)
);

INSERT INTO sc_script (id, code, name, version)
VALUES (1, 'return [] as Set', 'BudgetApprovalCriteria', 0);
INSERT INTO sc_script (id, code, name, version)
VALUES (2, 'return [] as Set', 'RequisitionApprovalCriteria', 0);
INSERT INTO sc_script (id, code, name, version)
VALUES (3, 'return [] as Set', 'RetirementApprovalCriteria', 0);
INSERT INTO sc_script (id, code, name, version)
VALUES (4, 'return [] as Set', 'ItemRequisitionApprovalCriteria', 0);
INSERT INTO sc_script (id, code, name, version)
VALUES (5, 'return [] as Set', 'DelegationApprovalCriteria', 0);
INSERT INTO sc_script (id, code, name, version)
VALUES (6, 'return [] as Set', 'PeriodAdjustmentApprovalCriteria', 0);