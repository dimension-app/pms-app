ALTER TABLE `cr_user`
DROP FOREIGN KEY `FKcqcuggw6cjq19ninjk5orylyr`;
ALTER TABLE `cr_user`
DROP COLUMN `picture_id`,
DROP COLUMN `external_identity`,
CHANGE COLUMN `username` `login_id` VARCHAR(255) NOT NULL ,
DROP INDEX `FKcqcuggw6cjq19ninjk5orylyr` ,
DROP INDEX `UK_462nwncjr7vfg4hu530d5ebut`;
;

-- ALTER TABLE `cr_authority`
-- DROP FOREIGN KEY `FKstcu1dp0wm8m6tecdentndq4`;
-- ALTER TABLE `cr_authority`
-- DROP COLUMN `parent_id`,
-- DROP INDEX `FKstcu1dp0wm8m6tecdentndq4`;
-- ;

ALTER TABLE `cr_authority`
RENAME TO  `cr_role`;

ALTER TABLE `cr_user`
CHANGE COLUMN `email` `email` VARCHAR(255) NULL ,
CHANGE COLUMN `time_created` `time_created` DATETIME NULL ,
DROP INDEX `UK_pfp3qsi8yb842a270h8ukudj`;
;

update hibernate_sequence set next_val=1000 where next_val < 1000;

ALTER TABLE `cr_user_cr_authority`
RENAME TO  `cr_user_roles`;

ALTER TABLE `cr_user_roles`
DROP FOREIGN KEY `FKjoasuy9f4ayl5lv7ofqwnlgsd`;
ALTER TABLE `cr_user_roles`
CHANGE COLUMN `authorities_id` `roles_id` BIGINT(20) NOT NULL ;
ALTER TABLE `cr_user_roles`
ADD CONSTRAINT `FKjoasuy9f4ayl5lv7ofqwnlgsd`
  FOREIGN KEY (`roles_id`)
  REFERENCES `cr_role` (`id`);

ALTER TABLE `authority_permissions`
RENAME TO  `cr_role_permissions`;

ALTER TABLE `cr_role_permissions`
DROP FOREIGN KEY `FK4jv2tj2xdqprpjtkpjreip5s2`;
ALTER TABLE `cr_role_permissions`
CHANGE COLUMN `authority_id` `cr_role_id` BIGINT(20) NOT NULL ;
ALTER TABLE `cr_role_permissions`
ADD CONSTRAINT `FK4jv2tj2xdqprpjtkpjreip5s2`
  FOREIGN KEY (`cr_role_id`)
  REFERENCES `cr_role` (`id`);

ALTER TABLE `cr_user_roles`
RENAME TO  `cr_user_cr_role`;

ALTER TABLE `cr_user_cr_role`
DROP FOREIGN KEY `FKa5lgwhq2616xlyfycclxoo9tq`;
ALTER TABLE `cr_user_cr_role`
CHANGE COLUMN `user_id` `cr_user_id` BIGINT(20) NOT NULL ;
ALTER TABLE `cr_user_cr_role`
ADD CONSTRAINT `FKa5lgwhq2616xlyfycclxoo9tq`
  FOREIGN KEY (`cr_user_id`)
  REFERENCES `cr_user` (`id`);

ALTER TABLE `is_issue`
ADD COLUMN `reference` VARCHAR(255) NOT NULL AFTER `resolution_id`;

UPDATE is_issue
INNER JOIN ap_tracker_is_issue ON is_issue.id = ap_tracker_is_issue.issues_id
SET is_issue.reference = concat('com.niafikra.dimension.approval.domain.Tracker:',ap_tracker_is_issue.tracker_id);

UPDATE is_issue
INNER JOIN pl_allocation_is_issue ON is_issue.id = pl_allocation_is_issue.issues_id
SET is_issue.reference = concat('com.niafikra.dimension.plan.domain.Allocation:',pl_allocation_is_issue.allocation_id);

DROP TABLE `ap_tracker_is_issue`;
DROP TABLE `pl_allocation_is_issue`;

ALTER TABLE `cr_delegation`
RENAME TO  `dl_delegation`;

ALTER TABLE `cr_group`
RENAME TO  `gr_group`;

ALTER TABLE `cr_group_cr_user`
RENAME TO  `gr_group_cr_user`;

ALTER TABLE `cr_group_pl_resource`
RENAME TO  `gr_group_pl_resource`;

ALTER TABLE `ap_strategy_cr_group`
RENAME TO  `ap_strategy_gr_group`;

ALTER TABLE `pl_cost_center_cr_group`
RENAME TO  `pl_cost_center_gr_group`;

ALTER TABLE `ftp_attachment`
RENAME TO  `at_attachment`;

ALTER TABLE `is_note_ftp_attachment`
RENAME TO  `is_note_at_attachment`;

ALTER TABLE `iv_item_requisition_ftp_attachment`
RENAME TO  `iv_item_requisition_at_attachment`;

ALTER TABLE `pl_allocation_adjustment_ftp_attachment`
RENAME TO  `pl_allocation_adjustment_at_attachment`;

ALTER TABLE `pl_budget_ftp_attachment`
RENAME TO  `pl_budget_at_attachment`;

ALTER TABLE `pl_payment_ftp_attachment`
RENAME TO  `pl_payment_at_attachment`;

ALTER TABLE `pl_requisition_ftp_attachment`
RENAME TO  `pl_requisition_at_attachment`;

ALTER TABLE `pl_retirement_entry_ftp_attachment`
RENAME TO  `pl_retirement_entry_at_attachment`;

ALTER TABLE `py_contract_ftp_attachment`
RENAME TO  `py_contract_at_attachment`;

ALTER TABLE `py_submitted_pay_ftp_attachment`
RENAME TO  `py_submitted_pay_at_attachment`;

ALTER TABLE `py_contact`
RENAME TO  `hr_contact`;

ALTER TABLE `pl_requisition_approval_flow_cr_group`
RENAME TO  `pl_requisition_approval_flow_gr_group`;