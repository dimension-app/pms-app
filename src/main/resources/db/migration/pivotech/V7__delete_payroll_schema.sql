drop table abstract_income_system_formula_py_income;
drop table money_system_formula;
drop table py_actual_money_formula;
drop table py_actual_number_formula;
drop table py_basic_income_formula;
drop table py_contract_at_attachment;
drop table py_contract_non_work_days;
drop table py_deduction_based_formula;
drop table py_gross_income_formula;
drop table py_income_based_formula;
drop table py_money_division_formula;
drop table py_money_list_formula_py_abstract_formula;
drop table py_money_multiply_formula;
drop table py_money_percentage_formula;
drop table py_money_saved_formula;
drop table py_formula_wrapper;
drop table py_money_script_formula;
drop table py_money_subtraction_formula;
drop table py_money_sum_formula;
drop table py_money_table_formula;
drop table py_money_table_py_money_table_entry;
drop table py_money_table;
drop table py_money_table_entry;
drop table py_other_income_formula;
drop table py_pay_deduction_entry;
drop table py_deduction_pay_entry;
drop table py_pay_income_entry;
drop table py_income_pay_entry;
drop table py_pay_py_pay_entry;
drop table py_pay_entry;
drop table py_payroll_deduction;
drop table py_payroll_income;
drop table py_recurring_deduction_config;
drop table py_recurring_income_config;
drop table py_recurring_pay_config_exclude_period_indices;
drop table py_submitted_deduction;
drop table py_deduction;
drop table py_submitted_income;
drop table py_income;
drop table py_submitted_pay_at_attachment;
drop table py_submitted_pay;
drop table py_pay;
drop table py_contract;
drop table py_bank;
drop table py_contract_termination;
drop table py_pay_element;
drop table py_payroll;
drop table py_pay_period_type;
drop table hr_employee;
drop table hr_contact;
