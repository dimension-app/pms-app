ALTER TABLE `schema_version`
  DROP COLUMN `version_rank`,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`installed_rank`),
  DROP INDEX `schema_version_vr_idx`;
alter table iv_base_unit
  drop foreign key FK69j3j4br9yejixqiwsakj2h7f;
drop table iv_item_alternate_units;
drop table iv_alternate_unit;
drop table iv_item_requisition_attachments;
drop table iv_item_requisition_dispenses;
drop table iv_item_requisition;
drop table iv_stock_config;
drop table iv_store_operators;
drop table iv_transaction;
drop table iv_adjustment_reason;
drop table iv_batch;
drop table iv_item_unit;
drop table iv_item;
drop table iv_base_unit;
drop table iv_store;
drop table iv_unit;
drop table ap_criteria_context;
ALTER TABLE `ap_strategy_levels`
RENAME TO `ap_strategy_cr_group`;
rename table
    ap_tracker_approvers to ap_tracker_cr_user;
rename table
    ap_tracker_events to ap_tracker_ap_event;
rename table
    ap_tracker_issues to ap_tracker_is_issue;
rename table
    cr_group_users to cr_group_cr_user;
rename table
    cr_group_resources to cr_group_pl_resource;
rename table
    cr_user_authorities to cr_user_cr_authority;
rename table
    is_issue_comments to is_issue_is_comment;
rename table
    is_note_attachments to is_note_ftp_attachment;
rename table
    pl_allocation_changes to pl_allocation_pl_allocation_change;
rename table
    pl_allocation_issues to pl_allocation_is_issue;
rename table
    pl_budget_attachments to pl_budget_ftp_attachment;
rename table
    pl_cost_center_roles to pl_cost_center_cr_group;
rename table
    pl_cost_center_resources to pl_cost_center_pl_resource;
rename table
    pl_cost_center_planners to pl_cost_center_cr_user;
rename table
    pl_payment_attachments to pl_payment_ftp_attachment;
rename table
    pl_requisition_approval_flow_levels to pl_requisition_approval_flow_cr_group;
rename table
    pl_requisition_attachments to pl_requisition_ftp_attachment;
rename table
    pl_requisition_expenses to pl_requisition_pl_expense;
drop table pl_retirement_attachments;
drop table pl_retirement_settlements;
drop table pl_retirement;