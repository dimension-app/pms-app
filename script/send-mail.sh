curl --header "Content-Type: application/json" \
-i \
--request POST \
--data '{
"subject":"test",
"body":"test",
"to":["bonifacechacha@gmail.com"],
"html":false,
"multipart":false
}' \
--user admin:adminstrongpassword \
https://dimension-mail-app.herokuapp.com/api/v1/mail
