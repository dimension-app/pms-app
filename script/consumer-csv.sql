LOAD DATA LOCAL INFILE
'/home/dimension/fleet.csv'
INTO TABLE pl_consumer
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(code,name,category_id,active,version);