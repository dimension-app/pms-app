create table cr_user
(
  id bigint auto_increment
    primary key,
  email varchar(255) not null,
  external_identity varchar(255) not null,
  last_updated datetime null,
  name varchar(255) not null,
  phone_number varchar(255) null,
  time_created datetime null,
  username varchar(255) not null,
  version int null,
  picture_id bigint null,
  constraint UK_pfp3qsi8yb842a270h8ukudj
  unique (email),
  constraint UK_462nwncjr7vfg4hu530d5ebut
  unique (external_identity),
  constraint UK_ft2vxgwrryfajqjxi3vn3drr6
  unique (username),
  constraint FKcqcuggw6cjq19ninjk5orylyr
  foreign key (picture_id) references ftp_attachment (id)
)
;

create index FKcqcuggw6cjq19ninjk5orylyr
  on cr_user (picture_id)
;

create table cr_authority
(
  id bigint auto_increment
    primary key,
  name varchar(255) not null,
  version int null,
  parent_id bigint null,
  constraint UK_boqxmoqkbin64u6j7hh4pgqkg
  unique (name),
  constraint FKstcu1dp0wm8m6tecdentndq4
  foreign key (parent_id) references cr_authority (id)
)
;

create index FKstcu1dp0wm8m6tecdentndq4
  on cr_authority (parent_id)
;

create table authority_permissions
(
  authority_id bigint not null,
  permissions varchar(255) null,
  constraint FK4jv2tj2xdqprpjtkpjreip5s2
  foreign key (authority_id) references cr_authority (id)
)
;

create index FK4jv2tj2xdqprpjtkpjreip5s2
  on authority_permissions (authority_id)
;



create table cr_user_cr_authority
(
  user_id bigint not null,
  authorities_id bigint not null,
  primary key (user_id, authorities_id),
  constraint FKpjiw35ubd1wqntcdukh0e17yq
  foreign key (user_id) references cr_user (id),
  constraint FKsjyd4iaql0w72ydhsvadqkt7e
  foreign key (authorities_id) references cr_authority (id)
)
;

create index FKsjyd4iaql0w72ydhsvadqkt7e
  on cr_user_cr_authority (authorities_id)
;


INSERT INTO `cr_authority` (`name`, `version`, `parent_id`) VALUES ('Admin', 1, NULL);
INSERT INTO `authority_permissions` (`authority_id`, `permissions`) VALUES (1, 'CONFIGURE_USERS');
INSERT INTO `authority_permissions` (`authority_id`, `permissions`) VALUES (1, 'CONFIGURE_AUTHORITIES');
INSERT INTO `cr_user_cr_authority` (`user_id`, `authorities_id`) VALUES (1, 1);